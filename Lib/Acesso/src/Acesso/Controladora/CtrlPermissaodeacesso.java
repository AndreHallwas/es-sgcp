/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Controladora;

import Acesso.Entidade.Permissaodeacesso;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlPermissaodeacesso extends CtrlBase{
    

    private static CtrlPermissaodeacesso ctrlpermissaodeacesso;

    public static CtrlPermissaodeacesso create() {
        if (ctrlpermissaodeacesso == null) {
            ctrlpermissaodeacesso = new CtrlPermissaodeacesso();
        }
        return ctrlpermissaodeacesso;
    }

    public CtrlPermissaodeacesso() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> permissaodeacesso = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            
            List<Permissaodeacesso> ResultPermissaodeacesso = em.createNamedQuery("Permissaodeacesso.findByPermacessoId", Permissaodeacesso.class)
            .setParameter("permacessoId", Integer.parseInt(Filtro)).getResultList();
            
            for (Permissaodeacesso permissaodeacessos : ResultPermissaodeacesso) {
                permissaodeacesso.add(permissaodeacessos);
            }
            
        } finally {
            if (em != null) {
                em.close();
            }
        }
        
        return permissaodeacesso;
    }

    public Object Salvar(String Detalhes) {
        Permissaodeacesso permissaodeacesso = new Permissaodeacesso(Detalhes);
        return super.Salvar(permissaodeacesso);
    }

    public Object Alterar(Object aPermisssaodeAcesso, Integer Id, String Detalhes) {
        Permissaodeacesso permissaodeacesso = null;
        if(aPermisssaodeAcesso != null && aPermisssaodeAcesso instanceof Permissaodeacesso){
            permissaodeacesso = (Permissaodeacesso) aPermisssaodeAcesso;
            permissaodeacesso.setPermacessoDetalhes(Detalhes);
            permissaodeacesso = (Permissaodeacesso) super.Alterar(permissaodeacesso);
        }
        return permissaodeacesso;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Permissaodeacesso.class);
    }
}
