/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Entidade;

import Animal.Entidade.Animal;
import Endereco.Entidade.Complementoendereco;
import JPA.Dao.JPADao;
import Venda.Entidade.Venda;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
    , @NamedQuery(name = "Cliente.findByCliId", query = "SELECT c FROM Cliente c WHERE c.cliId = :cliId")
    , @NamedQuery(name = "Cliente.findByCliCpf", query = "SELECT c FROM Cliente c WHERE c.cliCpf = :cliCpf")
    , @NamedQuery(name = "Cliente.findByCliRg", query = "SELECT c FROM Cliente c WHERE c.cliRg = :cliRg")
    , @NamedQuery(name = "Cliente.findByCliNome", query = "SELECT c FROM Cliente c WHERE c.cliNome = :cliNome")
    , @NamedQuery(name = "Cliente.findByCliTelefone", query = "SELECT c FROM Cliente c WHERE c.cliTelefone = :cliTelefone")
    , @NamedQuery(name = "Cliente.findByCliEmail", query = "SELECT c FROM Cliente c WHERE c.cliEmail = :cliEmail")
    , @NamedQuery(name = "Cliente.findByCliObs", query = "SELECT c FROM Cliente c WHERE c.cliObs = :cliObs")
    , @NamedQuery(name = "Cliente.findByCliDatacadastro", query = "SELECT c FROM Cliente c WHERE c.cliDatacadastro = :cliDatacadastro")
    , @NamedQuery(name = "Cliente.findByCliGeral", query = "SELECT c FROM Cliente c WHERE upper(c.cliNome) like upper(concat(:filtro,'%'))"
            + " or upper(c.cliCpf) like upper(concat(:filtro,'%'))")})  
public class Cliente extends JPADao implements Serializable, Pessoa {/* like upper(concat(:filtro,'%'))*/

    @Lob
    @Column(name = "cli_foto")
    private byte[] cliFoto;
    @JoinColumn(name = "end_id", referencedColumnName = "end_id")
    @ManyToOne(optional = false)
    private Complementoendereco endId;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cli_id")
    private Integer cliId;
    @Basic(optional = false)
    @Column(name = "cli_cpf")
    private String cliCpf;
    @Basic(optional = false)
    @Column(name = "cli_rg")
    private String cliRg;
    @Basic(optional = false)
    @Column(name = "cli_nome")
    private String cliNome;
    @Column(name = "cli_telefone")
    private String cliTelefone;
    @Column(name = "cli_email")
    private String cliEmail;
    @Column(name = "cli_obs")
    private String cliObs;
    @Column(name = "cli_datacadastro")
    @Temporal(TemporalType.DATE)
    private Date cliDatacadastro;
    
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "cliId")
    private Collection<Animal> animalCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "cliId")
    private Collection<Venda> vendaCollection = new ArrayList();

    public Cliente() {
    }

    public Cliente(Integer cliId) {
        this.cliId = cliId;
    }

    public Cliente(String cliCpf, String cliRg, byte[] cliFoto, String cliNome, String cliTelefone, String cliEmail,
            String cliObs) {
        this.cliCpf = cliCpf;
        this.cliRg = cliRg;
        this.cliFoto = cliFoto;
        this.cliNome = cliNome;
        this.cliTelefone = cliTelefone;
        this.cliEmail = cliEmail;
        this.cliObs = cliObs;
    }

    public Cliente(Integer cliId, String cliCpf, String cliRg, byte[] cliFoto, String cliNome, String cliTelefone,
            String cliEmail, String cliObs) {
        this.cliId = cliId;
        this.cliCpf = cliCpf;
        this.cliRg = cliRg;
        this.cliFoto = cliFoto;
        this.cliNome = cliNome;
        this.cliTelefone = cliTelefone;
        this.cliEmail = cliEmail;
        this.cliObs = cliObs;
    }

    public Cliente(Integer cliId, String cliCpf, String cliRg, String cliNome) {
        this.cliId = cliId;
        this.cliCpf = cliCpf;
        this.cliRg = cliRg;
        this.cliNome = cliNome;
    }

    public Integer getCliId() {
        return cliId;
    }

    public void setCliId(Integer cliId) {
        this.cliId = cliId;
    }

    public String getCliCpf() {
        return cliCpf;
    }

    public void setCliCpf(String cliCpf) {
        this.cliCpf = cliCpf;
    }

    public String getCliRg() {
        return cliRg;
    }

    public void setCliRg(String cliRg) {
        this.cliRg = cliRg;
    }
    
    public String getCliNome() {
        return cliNome;
    }

    public void setCliNome(String cliNome) {
        this.cliNome = cliNome;
    }

    public String getCliTelefone() {
        return cliTelefone;
    }

    public void setCliTelefone(String cliTelefone) {
        this.cliTelefone = cliTelefone;
    }

    public String getCliEmail() {
        return cliEmail;
    }

    public void setCliEmail(String cliEmail) {
        this.cliEmail = cliEmail;
    }

    public String getCliObs() {
        return cliObs;
    }

    public void setCliObs(String cliObs) {
        this.cliObs = cliObs;
    }

    public Date getCliDatacadastro() {
        return cliDatacadastro;
    }

    public void setCliDatacadastro(Date cliDatacadastro) {
        this.cliDatacadastro = cliDatacadastro;
    }

    @XmlTransient
    public Collection<Animal> getAnimalCollection() {
        return animalCollection;
    }

    public void setAnimalCollection(Collection<Animal> animalCollection) {
        this.animalCollection = animalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cliId != null ? cliId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.cliId == null && other.cliId != null) || (this.cliId != null && !this.cliId.equals(other.cliId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return cliNome;
    }

    public byte[] getCliFoto() {
        return cliFoto;
    }

    public void setCliFoto(byte[] cliFoto) {
        this.cliFoto = cliFoto;
    }

    public Complementoendereco getEndId() {
        return endId;
    }

    public void setEndId(Complementoendereco endId) {
        this.endId = endId;
    }

    @XmlTransient
    public Collection<Venda> getVendaCollection() {
        return vendaCollection;
    }

    public void setVendaCollection(Collection<Venda> vendaCollection) {
        this.vendaCollection = vendaCollection;
    }
    
}
