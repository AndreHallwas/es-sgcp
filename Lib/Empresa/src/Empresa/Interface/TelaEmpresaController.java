/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa.Interface;

import Empresa.Controladora.CtrlEmpresa;
import Endereco.Interface.TelaEnderecoController;
import Utils.Controladora.CtrlUtils;
import Utils.Imagem;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Imagem.TelaImagemFXMLController;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Stack;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaEmpresaController implements Initializable {

    @FXML
    private TextField txbNome;
    @FXML
    private TextField txbCNPJ;
    @FXML
    private TextField txbRazao2;
    @FXML
    private TextField txbFone;
    @FXML
    private Button btnCancelar;
    @FXML
    private VBox gpImagem;
    @FXML
    private VBox gpEndereco;
    
    private static boolean statusEmp;
    private static Node root;
    private char flag;
    private TelaImagemFXMLController aImagem;
    private TelaEnderecoController Endereco;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        aImagem = TelaImagemFXMLController.Create(this.getClass(), gpImagem);
        Endereco = TelaEnderecoController.Create(this.getClass(), gpEndereco);
        
        CtrlEmpresa ce = CtrlEmpresa.create();
        Object p = ce.Pesquisar();
        
        MaskFieldUtil.foneField(txbFone);
        MaskFieldUtil.cpfCnpjField(txbCNPJ);
        
        if (p != null) {
            CtrlEmpresa.setCampos(p, txbNome, txbCNPJ, txbFone, txbRazao2);
            Endereco.setComplementoEndereco(CtrlEmpresa.getEndereco(p));
            aImagem.setImagem(CtrlEmpresa.getImagem(p));
            btnCancelar.setDisable(false);
            flag = 1;
            statusEmp = true;
        } else {
            /////ControledeAcesso.DefaultEmpresa();
            btnCancelar.setDisable(true);
            flag = 0;
            statusEmp = false;
        }
    }

    private boolean verificaVazio(TextField Campo) {
        if (Campo.getText().trim().equals("") || Campo.getText().trim().equals(" ")) {
            Mensagem.ExibirLog("Erro: Campo Vazio");
            return true;
        }
        return false;
    }

    @FXML
    private void evtConcluir(MouseEvent event) {
        boolean teste;
        CtrlEmpresa cp = CtrlEmpresa.create();
        Stack pilha = new Stack();
        pilha.push(teste = verificaVazio(txbNome));
        pilha.push(teste = verificaVazio(txbCNPJ));
        pilha.push(teste = verificaVazio(txbFone));
        pilha.push(teste = verificaVazio(txbRazao2));
        while (!pilha.isEmpty()) {
            if ((boolean) pilha.pop() == true) {
                teste = false;
            }
        }
        teste = teste && !Endereco.valida();
        try {
            if (!teste) {
                if (flag == 0) {
                    cp.Salvar(txbNome.getText(), MaskFieldUtil.onlyAlfaNumericValue(txbCNPJ),
                            Endereco.getComplementoEndereco(), txbFone.getText(), txbRazao2.getText(), aImagem.getImagem());
                } else {
                    cp.Alterar(cp.Pesquisar(),txbNome.getText(), MaskFieldUtil.onlyAlfaNumericValue(txbCNPJ),
                            Endereco.getComplementoEndereco(), txbFone.getText(), txbRazao2.getText(), aImagem.getImagem());
                }
                statusEmp = true;
                evtCancelar(event);
            } else {
                Mensagem.ExibirLog("Erro: Campo Vazio");
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro na Tela de Empresa");
        }

    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.ClosePane(root, true);
        /*ControledeAcesso.Default();
        ControledeAcesso.selecionaPermissao();*/
    }

    private void evtimgClick(MouseEvent event) {
        Image im = Imagem.capturaImagem();
        if (im != null) {
            aImagem.setImagem(Imagem.ImageToBufferedImage(im));
        } else {
            Mensagem.Exibir("Não Foi Possivel Abrir a Imagem", 2);
        }
    }

    public static boolean isStatusEmp() {
        return statusEmp;
    }

    public static void setRoot(Node root) {
        TelaEmpresaController.root = root;
    }

}
