/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 *
 * @author Raizen
 */
public class FormatString {

    public static String MonetarySimple(double Value) {
        DecimalFormat df = new DecimalFormat("###,###.##");
        df.setMinimumFractionDigits(2);
        return df.format(Value);
    }
    
    public static String DoubleSimple(double Value) {
        BigDecimal bd = new BigDecimal(Value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.toString();
    }
    
    public static String Monetary(double Value) throws Exception{
        DecimalFormat df = new DecimalFormat("R$###,###.##");
        df.setMinimumFractionDigits(2);
        return df.format(Value);
    }
    
    public static String MonetaryPersistent(BigDecimal Value) {
        return MonetaryPersistent(Value.doubleValue());
    }
    
    public static String MonetaryPersistent(Double Value) {
        
        DecimalFormat df = new DecimalFormat("R$###,###.##");
        df.setMinimumFractionDigits(2);
        String ret;
        try {
            ret = Value != null ? df.format(Value.doubleValue()) : df.format(new Double(0));
        } catch (Exception ex) {
            ret = df.format(new Double(0));
        }
        return ret;
    }


}
