/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Controladora;

import ContaaPagar.Entidade.Contaapagar;
import ContaaReceber.Entidade.Contaareceber;
import ContaaReceber.Entidade.Parcelaareceber;
import Controladora.Base.CtrlBase;
import FormadePagamento.Entidade.Formadepagamento;
import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Usuario;
import Produto.Entidade.Produto;
import Servico.Entidade.Servico;
import Transacao.Transacao;
import Transacao.Transaction;
import Utils.Carrinho.Carrinho;
import Utils.DateUtils;
import Utils.Mensagem;
import Vacinacao.Entidade.Vacinacao;
import Venda.Entidade.Produtodavenda;
import Venda.Entidade.Servicodavenda;
import Venda.Entidade.Venda;
import com.jfoenix.controls.JFXComboBox;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlVenda extends CtrlBase {

    private static CtrlVenda ctrlvenda;

    public static CtrlVenda create() {
        if (ctrlvenda == null) {
            ctrlvenda = new CtrlVenda();
        }
        return ctrlvenda;
    }

    public CtrlVenda() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> venda = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Venda> ResultVenda = null;
            if (Filtro == null || Filtro.isEmpty()) {
                ResultVenda = em.createNamedQuery("Venda.findAll", Venda.class)
                        .getResultList();
            } else if (Tipo != null && !Tipo.trim().isEmpty()) {
                try {
                    if (Tipo.equalsIgnoreCase("Id")) {
                        ResultVenda = em.createNamedQuery("Venda.findByVendaId", Venda.class)
                                .setParameter("vendaId", Integer.parseInt(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("Periodo")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultVenda = em.createNamedQuery("Venda.findByBetween", Venda.class)
                                    .setParameter("datainicial", new Date(auxFiltro[0]))
                                    .setParameter("datafinal", new Date(auxFiltro[1])).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("Geral")) {
                        try {
                            ResultVenda = em.createNamedQuery("Venda.findByVendaData", Venda.class)
                                    .setParameter("vendaData", new Date(Filtro)).getResultList();
                        } catch (Exception ex) {
                            System.out.println(ex.getMessage());
                            ResultVenda = em.createNamedQuery("Venda.findByAllGeral", Venda.class)
                                    .setParameter("vendaId", Integer.parseInt(Filtro))
                                    .setParameter("vendaValorTotal", Double.parseDouble(Filtro)).getResultList();
                        }
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    try {
                        ResultVenda = em.createNamedQuery("Venda.findByVendaSituacao", Venda.class)
                                .setParameter("vendaSituacao", Boolean.parseBoolean(Filtro)).getResultList();
                    } catch (Exception exc) {
                        ResultVenda = em.createNamedQuery("Venda.findByVendaObs", Venda.class)
                                .setParameter("vendaObs", Filtro).getResultList();
                        System.out.println(ex.getMessage());
                    }
                }
            }

            for (Venda vendas : ResultVenda) {
                venda.add(vendas);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return venda;
    }

    public Object RegistrarVendaNovo(BigDecimal valorTotal, Integer Parcelas, String Obs, Object cliente,
            Object usuario, Object formadepagamento, Integer Acrescimo, Integer Desconto, Integer Juros,
            String Descricao, ArrayList<Object> produtos, ArrayList<Integer> quantidade,
            ArrayList<Object> valores, ArrayList<Object> datas) {

        Venda venda = null;
        EntityManager em = null;

        try {
            /////Inicia a Transacao
            em = getEntityManager();
            em.getTransaction().begin();

            boolean SituacaoGeral = false;

            /////Gera a Venda
            venda = new Venda(valorTotal, new Date(), Obs, SituacaoGeral);
            if (cliente != null && cliente instanceof Cliente) {
                venda.setCliId((Cliente) cliente);
            }
            if (usuario != null && usuario instanceof Usuario) {
                venda.setUsrId((Usuario) usuario);
            }

            /////Persiste a Venda para gerar o Id
            em.persist(venda);
            em.flush();

            /////Gera os Produtos e Servicos da Venda
            Produto produto;
            Produtodavenda produtodavenda;
            ArrayList<Produtodavenda> Produtosdavenda = new ArrayList();
            Servico servico;
            Servicodavenda servicodavenda;
            ArrayList<Servicodavenda> Servicosdavenda = new ArrayList();
            Vacinacao vacina;
            ArrayList<Vacinacao> Vacinasdavenda = new ArrayList();
            for (int i = 0; i < produtos.size(); i++) {
                if (produtos.get(i) instanceof Produto) {
                    produto = (Produto) produtos.get(i);
                    /////Gerar Produto da Venda
                    produtodavenda = new Produtodavenda(produto.getProdutoId(), venda.getVendaId(), quantidade.get(i), produto.getProdutoPreco().multiply(new BigDecimal(quantidade.get(i).intValue())));/////Quantidade
                    produtodavenda.setVenda(venda);
                    produtodavenda.setProduto(produto);
                    /////Subtrair ao Estoque
                    produto.setProdutoQuantidade(produto.getProdutoQuantidade() - quantidade.get(i));/////transacao
                    produto = em.merge(produto);
                    /////Adiciona na Lista
                    Produtosdavenda.add(produtodavenda);
                } else if (produtos.get(i) instanceof Servico) {
                    servico = (Servico) produtos.get(i);
                    /////Gerar Produto da Venda
                    servicodavenda = new Servicodavenda(servico.getServicoId(), venda.getVendaId(), quantidade.get(i), servico.getServicoValor().multiply(new BigDecimal(quantidade.get(i).intValue())));/////Quantidade
                    servicodavenda.setVenda(venda);
                    servicodavenda.setServico(servico);
                    servicodavenda = em.merge(servicodavenda);
                    /////Adiciona na Lista
                    Servicosdavenda.add(servicodavenda);
                } else if (produtos.get(i) instanceof Vacinacao) {
                    vacina = (Vacinacao) produtos.get(i);
                    vacina.setVendaId(venda);
                    vacina = em.merge(vacina);
                    /////Subtrair ao Estoque
                    Produto oproduto = vacina.getProduto();
                    oproduto.setProdutoQuantidade(oproduto.getProdutoQuantidade() - quantidade.get(i));/////transacao
                    oproduto = em.merge(oproduto);
                    vacina.setProduto(oproduto);
                    /////Adiciona na Lista
                    Vacinasdavenda.add(vacina);
                }
            }
            /////Adicionar vacina, Produtosdavenda e Servicosdavenda na Venda;
            venda.setProdutodavendaCollection(Produtosdavenda);
            venda.setServicodavendaCollection(Servicosdavenda);
            venda.setVacinacaoCollection(Vacinasdavenda);

            /////Gera a Conta a Receber
            Contaareceber contaareceber = new Contaareceber(valorTotal, SituacaoGeral, Acrescimo,
                    Desconto, Juros, new Date(), Descricao, Parcelas);
            if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
                contaareceber.setFormadepagamentoId((Formadepagamento) formadepagamento);
            }
            contaareceber.setVendaId(venda);

            /////Adicionar a Conta a Receber a Venda
            ArrayList<Contaareceber> Contasareceber = new ArrayList();
            Contasareceber.add(contaareceber);
            venda.setContaareceberCollection(Contasareceber);

            /////Persiste a conta para gerar o Id
            em.persist(contaareceber);
            em.flush();

            /////Gerar Parcelas a Receber
            BigDecimal ValorParcela;
            Date DataVencimentoParcela;
            Parcelaareceber parcelaaReceber;
            ArrayList<Parcelaareceber> Parcelasareceber = new ArrayList();
            for (int i = 0, NumerodaParcela = 1; i < Parcelas; i++, NumerodaParcela++) {
                ValorParcela = new BigDecimal((Double) valores.get(i));
                DataVencimentoParcela = (Date) datas.get(i);

                parcelaaReceber = new Parcelaareceber(contaareceber.getContaareceberId(), NumerodaParcela,
                        DataVencimentoParcela, null, ValorParcela, SituacaoGeral, BigDecimal.ZERO, BigDecimal.ZERO, new Date());
                parcelaaReceber.setFormadepagamentoId(contaareceber.getFormadepagamentoId());/////Old contaareceber.getFormadepagamentoId()
                parcelaaReceber.setContaareceber(contaareceber);
                /////Adiciona na Lista de Parcelas a Receber
                Parcelasareceber.add(parcelaaReceber);
            }
            /////Adicionar as Parcelas a Receber a Conta a Receber;
            contaareceber.setParcelaareceberCollection(Parcelasareceber);

            /////Finaliza a Venda
            venda = em.merge(venda);

            /////Finaliza a Transação
            em.getTransaction().commit();

            Message = "Venda Realizada com Sucesso";
        } catch (Exception ex) {
            if (em != null) {
                Mensagem.ExibirException(ex, ":CtrlVenda:284");
                em.getTransaction().rollback();
            }
            Message = "Erro, a Venda não Realizada";
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return venda;
    }

    public Venda ModificarVendaNovo(Object aVenda, Date Data, boolean SituacaoVenda, BigDecimal valorTotal, Integer Parcelas, String Obs, Object cliente,
            Object usuario, Object formadepagamento, Integer Acrescimo, Integer Desconto, Integer Juros,
            String Descricao, ArrayList<Object> produtos, ArrayList<Integer> quantidade,
            ArrayList<Object> valores, ArrayList<Object> datas) {

        Venda venda = null;
        EntityManager em = null;

        try {
            if (aVenda != null && aVenda instanceof Venda) {
                venda = (Venda) aVenda;
                /////Inicia a Transacao
                em = getEntityManager();
                em.getTransaction().begin();

                boolean SituacaoGeral = false;
                /////Altera a Venda
                venda.setVendaValorTotal(valorTotal);
                venda.setVendaData(Data);
                venda.setVendaObs(Obs);
                venda.setVendaSituacao(SituacaoVenda);

                if (cliente != null && cliente instanceof Cliente) {
                    venda.setCliId((Cliente) cliente);
                }
                if (usuario != null && usuario instanceof Usuario) {
                    venda.setUsrId((Usuario) usuario);
                }

                /////Alterar os Produtos da Venda
                if (venda.getProdutodavendaCollection() != null) {
                    Produtodavenda produtodavenda;
                    Collection<Produtodavenda> Produtoc = venda.getProdutodavendaCollection();
                    ArrayList<Produtodavenda> oProdutodavenda = new ArrayList();
                    for (Produtodavenda produtodavendas : Produtoc) {
                        oProdutodavenda.add(produtodavendas);
                    }
                    ArrayList<Produtodavenda> Adicionados = new ArrayList();
                    ArrayList<Produtodavenda> NaoModificados = new ArrayList();
                    ArrayList<Produtodavenda> Removidos = new ArrayList();
                    Removidos.addAll(oProdutodavenda);
                    for (Object oProduto : produtos) {
                        if (oProduto instanceof Produto) {
                            int i = 0;
                            while (i < oProdutodavenda.size() && !ComparaProdutodavenda((Produto) oProduto,
                                    oProdutodavenda.get(i).getProdutodavendaPK().getProdutoId())) {
                                i++;
                            }
                            if (i < oProdutodavenda.size()) {
                                produtodavenda = oProdutodavenda.get(i);
                                /////Altera o Estoque
                                produtodavenda.getProduto().setProdutoQuantidade((produtodavenda.getProduto().getProdutoQuantidade() + produtodavenda.getProdvQuantidade())
                                        - quantidade.get(i));
                                produtodavenda.setProduto(em.merge(produtodavenda.getProduto()));
                                /////Altera o Produto da Venda
                                produtodavenda.setProdvQuantidade(quantidade.get(i));
                                produtodavenda.setProdvValor(((Produto) oProduto).getProdutoPreco().multiply(new BigDecimal(quantidade.get(i).intValue())));
                                /////Adiciona na Lista de Não Modificados
                                NaoModificados.add(oProdutodavenda.get(i));
                                Removidos.remove(oProdutodavenda.get(i));
                            } else {
                                /////Gerar Produto da Venda
                                produtodavenda = new Produtodavenda(((Produto) oProduto).getProdutoId(), venda.getVendaId(), quantidade.get(i),
                                        ((Produto) oProduto).getProdutoPreco().multiply(new BigDecimal(quantidade.get(i).intValue())));
                                produtodavenda.setVenda(venda);
                                produtodavenda.setProduto((Produto) oProduto);
                                /////Adicionar a Lista de Novos
                                Adicionados.add(produtodavenda);
                            }
                        }
                    }
                    for (Produtodavenda Removido : Removidos) {
                        /////Adicionar Do Estoque
                        Removido.getProduto().setProdutoQuantidade(Removido.getProduto().getProdutoQuantidade() + Removido.getProdvQuantidade());
                        Removido.setProduto(em.merge(Removido.getProduto()));
                    }
                    for (Produtodavenda Adicionado : Adicionados) {
                        /////Subtrair Ao Estoque
                        Adicionado.getProduto().setProdutoQuantidade(Adicionado.getProduto().getProdutoQuantidade() - Adicionado.getProdvQuantidade());
                        Adicionado.setProduto(em.merge(Adicionado.getProduto()));
                    }
                    /////Adicionar Alterações a Venda
                    venda.setProdutodavendaCollection(new ArrayList());
                    venda.getProdutodavendaCollection().addAll(NaoModificados);
                    venda.getProdutodavendaCollection().addAll(Adicionados);
                }

                /////Alterar os Servicos da Venda
                if (venda.getServicodavendaCollection() != null) {
                    Servicodavenda servicodavenda;
                    Collection<Servicodavenda> Servicoc = venda.getServicodavendaCollection();
                    ArrayList<Servicodavenda> oServicodavenda = new ArrayList();
                    for (Servicodavenda servicodavendas : Servicoc) {
                        oServicodavenda.add(servicodavendas);
                    }
                    ArrayList<Servicodavenda> Adicionados = new ArrayList();
                    ArrayList<Servicodavenda> NaoModificados = new ArrayList();
                    ArrayList<Servicodavenda> Removidos = new ArrayList();
                    Removidos.addAll(oServicodavenda);
                    for (Object oServico : produtos) {
                        if (oServico instanceof Servico) {
                            int i = 0;
                            while (i < oServicodavenda.size() && !ComparaServicodavenda((Servico) oServico,
                                    oServicodavenda.get(i).getServicodavendaPK().getServicoId())) {
                                i++;
                            }
                            if (i < oServicodavenda.size()) {
                                servicodavenda = oServicodavenda.get(i);
                                /////Altera o Servico da Venda
                                servicodavenda.setServvQuantidade(quantidade.get(i));
                                servicodavenda.setServvValor(((Servico) oServico).getServicoValor().multiply(new BigDecimal(quantidade.get(i).intValue())));
                                /////Adiciona na Lista de Não Modificados
                                NaoModificados.add(oServicodavenda.get(i));
                                Removidos.remove(oServicodavenda.get(i));
                            } else {
                                /////Gerar Servico da Venda
                                servicodavenda = new Servicodavenda(((Servico) oServico).getServicoId(), venda.getVendaId(), quantidade.get(i),
                                        ((Servico) oServico).getServicoValor().multiply(new BigDecimal(quantidade.get(i).intValue())));
                                servicodavenda.setVenda(venda);
                                servicodavenda.setServico((Servico) oServico);
                                /////Adicionar a Lista de Novos
                                Adicionados.add(servicodavenda);
                            }
                        }
                    }
                    /////Adicionar Alterações a Venda
                    venda.setServicodavendaCollection(new ArrayList());
                    venda.getServicodavendaCollection().addAll(NaoModificados);
                    venda.getServicodavendaCollection().addAll(Adicionados);
                }

                /////Alterar as Vacinas da Venda
                if (venda.getVacinacaoCollection() != null) {
                    Vacinacao vacinadavenda;
                    Collection<Vacinacao> Vacinac = venda.getVacinacaoCollection();
                    ArrayList<Vacinacao> aVacinadavenda = new ArrayList();
                    for (Vacinacao servicodavendas : Vacinac) {
                        aVacinadavenda.add(servicodavendas);
                    }
                    ArrayList<Vacinacao> Adicionados = new ArrayList();
                    ArrayList<Vacinacao> NaoModificados = new ArrayList();
                    ArrayList<Vacinacao> Removidos = new ArrayList();
                    ArrayList<Integer> AdicionadosQ = new ArrayList();
                    ArrayList<Integer> RemovidosQ = new ArrayList();
                    Removidos.addAll(aVacinadavenda);
                    for (Object aVacina : produtos) {
                        if (aVacina instanceof Vacinacao) {
                            int i = 0;
                            while (i < aVacinadavenda.size() && !ComparaVacinadavenda((Vacinacao) aVacina,
                                    aVacinadavenda.get(i).getVacinacaoPK())) {
                                i++;
                            }
                            if (i < aVacinadavenda.size()) {
                                vacinadavenda = aVacinadavenda.get(i);
                                /////Adiciona na Lista de Não Modificados
                                NaoModificados.add(aVacinadavenda.get(i));
                                Removidos.remove(aVacinadavenda.get(i));
                                RemovidosQ.add(quantidade.get(i));
                            } else {
                                /////Gerar vacina da Venda
                                vacinadavenda = (Vacinacao) aVacina;
                                vacinadavenda.setVendaId(venda);
                                /////Adicionar a Lista de Novos
                                Adicionados.add(vacinadavenda);
                                AdicionadosQ.add(quantidade.get(i));
                            }
                        }
                    }
                    for (int i = 0; i < Removidos.size(); i++) {
                        Vacinacao Removido = Removidos.get(i);
                        /////Adicionar Do Estoque
                        Removido.getProduto().setProdutoQuantidade(Removido.getProduto().getProdutoQuantidade() + RemovidosQ.get(i));
                        Removido.setProduto(em.merge(Removido.getProduto()));
                        ///Remover a Venda
                        Removido.setVendaId(null);
                        Removido = em.merge(Removido);
                    }
                    for (int i = 0; i < Adicionados.size(); i++) {
                        Vacinacao Adicionado = Adicionados.get(i);
                        /////Subtrair Ao Estoque
                        Adicionado.getProduto().setProdutoQuantidade(Adicionado.getProduto().getProdutoQuantidade() - AdicionadosQ.get(i));
                        Adicionado.setProduto(em.merge(Adicionado.getProduto()));
                        ///Adicionar a Venda
                        Adicionado.setVendaId(venda);
                        Adicionado = em.merge(Adicionado);
                    }
                    /////Adicionar Alterações a Venda
                    venda.setVacinacaoCollection(new ArrayList());
                    venda.getVacinacaoCollection().addAll(NaoModificados);
                    venda.getVacinacaoCollection().addAll(Adicionados);
                }

                /////Guarda as Contas a Remover
                Collection<Contaareceber> Contasaremover = venda.getContaareceberCollection();
                /////Limpa as Contas da Venda
                venda.setContaareceberCollection(new ArrayList());
                /////Altera a Venda para Remover as Contas
                venda = em.merge(venda);
                em.flush();

                ///Remover as Parcelas da Venda
                if (Contasaremover != null) {
                    for (Contaareceber contaareceber : Contasaremover) {
                        em.remove(em.find(Contaareceber.class, contaareceber.getContaareceberId()));
                    }
                }

                /////Gera a Nova Conta a Receber
                Contaareceber contaareceber = new Contaareceber(valorTotal, SituacaoGeral, Acrescimo,
                        Desconto, Juros, new Date(), Descricao, Parcelas);
                if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
                    contaareceber.setFormadepagamentoId((Formadepagamento) formadepagamento);
                }
                contaareceber.setVendaId(venda);

                /////Adicionar a Conta a Receber a Venda
                ArrayList<Contaareceber> Contasareceber = new ArrayList();
                Contasareceber.add(contaareceber);
                venda.setContaareceberCollection(Contasareceber);

                /////Persiste a conta para gerar o Id
                em.persist(contaareceber);
                em.flush();

                /////Gerar Parcelas a Receber
                BigDecimal ValorParcela;
                Date DataVencimentoParcela;
                Parcelaareceber parcelaaReceber;
                ArrayList<Parcelaareceber> Parcelasareceber = new ArrayList();
                for (int i = 0, NumerodaParcela = 1; i < Parcelas; i++, NumerodaParcela++) {
                    ValorParcela = new BigDecimal((Double) valores.get(i));
                    DataVencimentoParcela = (Date) datas.get(i);

                    parcelaaReceber = new Parcelaareceber(contaareceber.getContaareceberId(), NumerodaParcela,
                            DataVencimentoParcela, null, ValorParcela, SituacaoGeral, BigDecimal.ZERO, BigDecimal.ZERO, new Date());
                    parcelaaReceber.setFormadepagamentoId(contaareceber.getFormadepagamentoId());/////Old contaareceber.getFormadepagamentoId()
                    parcelaaReceber.setContaareceber(contaareceber);
                    /////Adiciona na Lista de Parcelas a Receber
                    Parcelasareceber.add(parcelaaReceber);
                }
                /////Adicionar as Parcelas a Receber a Conta a Receber;
                contaareceber.setParcelaareceberCollection(Parcelasareceber);

                /////Finaliza a Venda
                venda = em.merge(venda);

                /////Finaliza a Transação
                em.getTransaction().commit();

                Message = "Venda Realizada com Sucesso";
            } else {
                Message = "Venda Inválida!";
            }

        } catch (Exception ex) {
            if (em != null) {
                Mensagem.ExibirException(ex, ":CtrlVenda:434");
                if (em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
            }
            Message = "Erro, a Venda não Realizada";
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return venda;
    }

    private boolean ComparaServicodavenda(Servico oServico, Integer servicodavenda) {
        return oServico.getServicoId().equals(servicodavenda);
    }

    private boolean ComparaProdutodavenda(Produto oProduto, Integer produtodavenda) {
        return oProduto.getProdutoId().equals(produtodavenda);
    }

    private boolean ComparaVacinadavenda(Vacinacao aVacina, Object vacinadavenda) {
        return aVacina.getVacinacaoPK().equals(vacinadavenda);
    }

    public boolean RemoverVenda(Integer Id) {
        boolean flag = true;

        Venda venda = null;
        EntityManager em = null;

        try {
            if (Id != null) {
                /////Inicia a Transacao
                em = getEntityManager();
                em.getTransaction().begin();

                venda = em.find(Venda.class, Id);
                if (venda != null) {
                    /////Adiciona ao Estoque
                    for (Produtodavenda produtodavenda : venda.getProdutodavendaCollection()) {
                        produtodavenda.getProduto().setProdutoQuantidade(produtodavenda.getProduto().getProdutoQuantidade() + produtodavenda.getProdvQuantidade());
                        produtodavenda.setProduto(em.merge(produtodavenda.getProduto()));
                    }
                    for (Vacinacao vacinacao : venda.getVacinacaoCollection()) {
                        vacinacao.getProduto().setProdutoQuantidade(vacinacao.getProduto().getProdutoQuantidade() + 1);
                        vacinacao.setProduto(em.merge(vacinacao.getProduto()));
                        vacinacao = em.merge(vacinacao);
                    }
                    /////Remove a Venda
                    em.remove(venda);
                }
                /////Finaliza a Transação
                em.getTransaction().commit();
                
                Message = "Remoção Realizada com sucesso!";
            }
        } catch (Exception ex) {
            if (em != null) {
                Mensagem.ExibirException(ex, ":CtrlVenda:475");
                em.getTransaction().rollback();
            }
            flag = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return flag;
    }

    public static void setCampos(Object venda, TextField txbValorTotal, TextField txbParcelas) {
        if (venda != null && venda instanceof Venda) {
            Venda auxVenda = (Venda) venda;
            txbValorTotal.setText(auxVenda.getVendaValorTotal() + "");
            if (txbParcelas != null && auxVenda.getContaareceberCollection() != null
                    && !auxVenda.getContaareceberCollection().isEmpty()) {
                Contaareceber conta = (Contaareceber) new ArrayList(auxVenda.getContaareceberCollection()).get(0);
                txbParcelas.setText(conta.getContaareceberParcelas() + "");
            }
        }
    }

    public static void setCampos(Object venda, Label lblValor, Label lblData, TextField txbVenda, TextField txbTotal) {
        if (venda != null && venda instanceof Venda) {
            Venda auxVenda = (Venda) venda;
            lblValor.setText(auxVenda.getVendaValorTotal() + "");
            lblData.setText(DateUtils.asLocalDate(auxVenda.getVendaData()).toString());
            txbVenda.setText(auxVenda.getVendaId() + "");
            txbTotal.setText(auxVenda.getVendaValorTotal() + "");
        }
    }

    public static void setCampos(Object venda, Label lblValor, Label lblData) {
        if (venda != null && venda instanceof Venda) {
            Venda auxVenda = (Venda) venda;
            lblValor.setText(auxVenda.getVendaValorTotal() + "");
            lblData.setText(DateUtils.asLocalDate(auxVenda.getVendaData()).toString());
        }
    }

    public static void setCampos(Object venda, TextField txbCliente) {
        if (venda != null && venda instanceof Venda) {
            Venda auxVenda = (Venda) venda;
            if (auxVenda.getCliId() != null) {
                txbCliente.setText(auxVenda.getCliId().getCliNome());
            }
        }
    }

    public static void setCampos(Object venda, TextField txbValorTotal, JFXComboBox<Object> cbFormadepagamento,
            TextField txbParcelas, DatePicker txbVencimento, TextField txbDiasEntreParcelas) {
        if (venda != null && venda instanceof Venda) {
            try {
                Venda aVenda = (Venda) venda;
                txbValorTotal.setText(aVenda.getVendaValorTotal().toString());
                if (aVenda.getContaareceberCollection() != null && aVenda.getContaareceberCollection().size() > 0) {
                    ArrayList<Contaareceber> Contas = new ArrayList(aVenda.getContaareceberCollection());
                    if (Contas.size() > 0) {
                        txbParcelas.setText(Contas.get(0).getContaareceberParcelas() + "");
                        ArrayList<Parcelaareceber> Parcelas = new ArrayList(Contas.get(0).getParcelaareceberCollection());
                        txbVencimento.setValue(DateUtils.asLocalDate(Parcelas.get(0).getParrDataVencimento()));
                        cbFormadepagamento.getSelectionModel().select(Parcelas.get(0).getFormadepagamentoId());
                        if (Contas.size() > 1) {
                            txbDiasEntreParcelas.setText(DateUtils.DiferenceDays(Parcelas.get(0).getParrDataVencimento(),
                                    Parcelas.get(1).getParrDataVencimento()) + "");
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public static Carrinho setCarrinho(Object venda) {
        Carrinho c = new Carrinho();
        if (venda != null && venda instanceof Venda) {
            Venda auxVenda = (Venda) venda;
            if (auxVenda.getProdutodavendaCollection() != null) {
                for (Produtodavenda produtodavenda : auxVenda.getProdutodavendaCollection()) {
                    c.add(produtodavenda.getProduto(), produtodavenda.getProdvQuantidade());
                }
            }
            if (auxVenda.getServicodavendaCollection() != null) {
                for (Servicodavenda servicodavenda : auxVenda.getServicodavendaCollection()) {
                    c.add(servicodavenda.getServico(), servicodavenda.getServvQuantidade());
                }
            }
            if (auxVenda.getVacinacaoCollection() != null) {
                for (Vacinacao vacinacaodavenda : auxVenda.getVacinacaoCollection()) {
                    c.add(vacinacaodavenda, 1);
                }
            }
        }
        return c;
    }

    public static Integer getId(Object venda) {
        if (venda != null && venda instanceof Venda) {
            return ((Venda) venda).getVendaId();
        }
        return -1;
    }

    public static Double getValorTotal(Object venda) {
        if (venda != null && venda instanceof Venda) {
            return ((Venda) venda).getVendaValorTotal().doubleValue();
        }
        return -1.0;
    }

    public static BigDecimal getValorFaltante(Object venda) {
        BigDecimal valor = new BigDecimal(0);
        if (venda != null && venda instanceof Venda) {
            BigDecimal ValorPago = new BigDecimal(0);
            BigDecimal ValorTotal = new BigDecimal(0);
            Venda aVenda = (Venda) venda;
            for (Contaareceber contaareceber : aVenda.getContaareceberCollection()) {
                for (Parcelaareceber parcelaareceber : contaareceber.getParcelaareceberCollection()) {
                    if (parcelaareceber.getParrSituacao() != null && parcelaareceber.getParrSituacao()) {
                        ValorPago = ValorPago.add(parcelaareceber.getParrValorPago());
                    }
                    ValorTotal = ValorTotal.add(parcelaareceber.getParrValor());
                }
            }
            valor = ValorTotal.subtract(ValorPago);
        }
        return valor;
    }

    public static ArrayList<Object> getParcelas(Object venda) {
        ArrayList<Object> Retorno = new ArrayList();
        if (venda != null && venda instanceof Venda) {
            Venda auxVenda = (Venda) venda;
            if (auxVenda.getContaareceberCollection() != null) {
                for (Contaareceber contaareceber : auxVenda.getContaareceberCollection()) {
                    Retorno.add(contaareceber);
                }
            }
        }
        return Retorno;
    }

    public static ArrayList<Object> getValorParcelas(Object venda) {//
        ArrayList<Object> Parcelas = new ArrayList();
        if (venda != null && venda instanceof Venda) {
            Venda aVenda = (Venda) venda;
            if (aVenda.getContaareceberCollection() != null && !aVenda.getContaareceberCollection().isEmpty()) {
                for (Contaareceber contaareceber : aVenda.getContaareceberCollection()) {
                    for (Parcelaareceber parcelaareceber : contaareceber.getParcelaareceberCollection()) {
                        Parcelas.add(parcelaareceber.getParrValor().doubleValue());
                    }
                }
            }
        }
        return Parcelas;
    }

    public static ArrayList<Object> getDataParcelas(Object venda) {//
        ArrayList<Object> Parcelas = new ArrayList();
        if (venda != null && venda instanceof Venda) {
            Venda aVenda = (Venda) venda;
            if (aVenda.getContaareceberCollection() != null && !aVenda.getContaareceberCollection().isEmpty()) {
                for (Contaareceber contaareceber : aVenda.getContaareceberCollection()) {
                    for (Parcelaareceber parcelaareceber : contaareceber.getParcelaareceberCollection()) {
                        Parcelas.add(parcelaareceber.getParrDataVencimento());
                    }
                }
            }
        }
        return Parcelas;
    }

    public static Object getCliente(Object venda) {
        if (venda != null && venda instanceof Venda) {
            Venda aVenda = (Venda) venda;
            return aVenda.getCliId();
        }
        return null;
    }

    public String VerificaPagamento(Object venda) {

        String Resultado = "";
        Contaareceber aConta = null;
        Boolean Situacao = null;

        if (venda != null && venda instanceof Venda) {
            Venda aVenda = (Venda) venda;
            ArrayList<Contaareceber> Contas = new ArrayList(aVenda.getContaareceberCollection());
            if (Contas.size() > 0) {
                aConta = Contas.get(0);
                ArrayList<Parcelaareceber> Parcelas = new ArrayList(aConta.getParcelaareceberCollection());
                if (Parcelas.size() > 0) {
                    int Count = 0;
                    for (Parcelaareceber parcelaareceber : Parcelas) {
                        if (parcelaareceber.getParrSituacao() == true) {
                            Count++;
                        }
                    }
                    if (Count >= Parcelas.size()) {
                        Resultado = "Venda Paga";
                        if (aConta.getContaareceberSituacao() == false) {
                            Situacao = true;
                        }
                    } else {
                        if (Count == 0) {
                            Resultado = "Não Pago";
                        } else if (Count < Parcelas.size()) {
                            Resultado = "Parcela Paga";
                        }
                        if (aConta.getContaareceberSituacao() == true) {
                            Situacao = false;
                        }
                    }
                }
            } else {
                Resultado = "Não Gerado";
            }
        } else {
            Resultado = "Venda Inválida!";
        }

        if (aConta != null && Situacao != null) {
            final Object Objeto = aConta;
            Transacao T = new Transacao() {
                @Override
                public Object Transacao(Object... Params) {
                    return em.merge(Objeto);
                }
            };
            aConta = (Contaareceber) T.Realiza();
        }
        return Resultado;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Venda.class);
    }

}
/////Inserir/excluir parcelas e aumentar contadores e valores V

/////Alterar Situação da Venda ao receber todas as parcelas V
/////pagamento parcial, Remover pagamento parcial ao estornar V
/////verificar se já está pago ou não. V
/////só receber a proxima a vencer V
