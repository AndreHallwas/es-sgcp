/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Interface;

import Servico.Controladora.CtrlGrupodeservico;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaGrupodeServicoController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcId;
    @FXML
    private TableColumn<Object, String> tcDescricao;
    private static Object grupodeservico;
    private static boolean btnPainel = true;
    @FXML
    private HBox btnGp;

    @Override
    public void initialize(URL url, ResourceBundle rb){
        tcId.setCellValueFactory(new PropertyValueFactory("GrupodeservicoId"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("GrupodeservicoDescricao"));
        CarregaTabela("");
        grupodeservico = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlGrupodeservico ctm = CtrlGrupodeservico.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            grupodeservico = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (grupodeservico != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhum Grupo de Serviço Selecionado", 2);
        }
    }

    public static TelaConsultaGrupodeServicoController Create(Class classe, Pane pndados) {
        TelaConsultaGrupodeServicoController CGrupodeservico = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Servico/Interface/TelaConsultaGrupodeservico.fxml"));
            Parent root = Loader.load();
            CGrupodeservico = (TelaConsultaGrupodeServicoController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CGrupodeservico;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaGrupodeServicoController.btnPainel = btnPainel;
    }

    public static Object getGrupodeservico() {
        return grupodeservico;
    }
}
