/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Atendimento.Controladora;

import Agendamento.Entidade.Agendamento;
import Animal.Entidade.Animal;
import Atendimento.Entidade.Atendimento;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlAtendimento extends CtrlBase{

    private static CtrlAtendimento ctrlatendimento;

    public static CtrlAtendimento create() {
        if (ctrlatendimento == null) {
            ctrlatendimento = new CtrlAtendimento();
        }
        return ctrlatendimento;
    }

    public static void setCampos(Object atendimento, TextArea taDescricaoGeral, TextArea taDescricaoVeterinario, TextArea taObservacoes, TextArea taSintomas) {
        if (atendimento != null && atendimento instanceof Atendimento) {
            Atendimento f = (Atendimento) atendimento;
            taDescricaoGeral.setText(f.getAtendimentoDescricaoGeral());
            taDescricaoVeterinario.setText(f.getAtendimentoDescricaoVeterinario());
            taObservacoes.setText(f.getAtendimentoObs());
            taSintomas.setText(f.getAtendimentoSintomas());
        }
    }

    public CtrlAtendimento() {
        super(Transaction.getEntityManagerFactory());
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Atendimento) {
            return ((Atendimento) p).getAtendimentoId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Atendimento) {
            txBusca.setText(((Atendimento) p).getAtendimentoId() + "");
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> atendimento = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Atendimento> ResultAtendimento = null;
            if (Filtro == null || Filtro.isEmpty()) {
                ResultAtendimento = em.createNamedQuery("Atendimento.findAll", Atendimento.class)
                        .getResultList();
            } else if (Tipo != null && !Tipo.trim().isEmpty()) {
                try {
                    if (Tipo.equalsIgnoreCase("Id")) {
                        ResultAtendimento = em.createNamedQuery("Atendimento.findByAtendimentoId", Atendimento.class)
                                .setParameter("atendimentoId", Integer.parseInt(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("Animal_Data")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultAtendimento = em.createNamedQuery("Atendimento.findByAnimalIdAndData", Atendimento.class)
                                    .setParameter("animalId", Integer.parseInt(auxFiltro[0]))
                                    .setParameter("atendimentoData", new Date(Long.parseLong(auxFiltro[1]))).getResultList();
                        }
                    }else if (Tipo.equalsIgnoreCase("Animal")) {
                        ResultAtendimento = em.createNamedQuery("Atendimento.findByAnimalId", Atendimento.class)
                                .setParameter("animalId", Integer.parseInt(Filtro)).getResultList();
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    ResultAtendimento = em.createNamedQuery("Atendimento.findByAtendimentoDescricaoGeral", Atendimento.class)
                            .setParameter("atendimentoDescricaoGeral", Integer.parseInt(Filtro)).getResultList();
                }
            }

            for (Atendimento atendimentos : ResultAtendimento) {
                atendimento.add(atendimentos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return atendimento;
    }

    public Object Salvar(String DescricaoGeral, String DescricaoVeterinario, String Sintomas, String Obs, Date Data, Object animal, Object agendamento) {
        Atendimento atendimento = new Atendimento(DescricaoGeral, DescricaoVeterinario, Sintomas, Obs, Data);
        if (animal != null && animal instanceof Animal) {
            atendimento.setAnimalId((Animal) animal);
        }
        if (agendamento != null && agendamento instanceof Agendamento) {
            atendimento.setAgendamentoId((Agendamento) agendamento);
        }
        return super.Salvar(atendimento);
    }

    public Object Alterar(Object oAtendimento, Integer Id, String DescricaoGeral, String DescricaoVeterinario, String Sintomas,
            String Obs, Date Data, Object animal,
            Object agendamento) {
        Atendimento atendimento = null;
        if (oAtendimento != null && oAtendimento instanceof Atendimento) {
            atendimento = (Atendimento) oAtendimento;
            
            atendimento.setAtendimentoData(Data);
            atendimento.setAtendimentoDescricaoGeral(DescricaoGeral);
            atendimento.setAtendimentoDescricaoVeterinario(DescricaoVeterinario);
            atendimento.setAtendimentoObs(Sintomas);
            atendimento.setAtendimentoSintomas(Sintomas);
            
            if (animal != null && animal instanceof Animal) {
                atendimento.setAnimalId((Animal) animal);
            }
            if (agendamento != null && agendamento instanceof Agendamento) {
                atendimento.setAgendamentoId((Agendamento) agendamento);
            }
            
            atendimento = (Atendimento) super.Alterar(atendimento);
        }
        return atendimento;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Atendimento.class);
    }

}
