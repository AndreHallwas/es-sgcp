/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Controladora;

import Controladora.Base.CtrlBase;
import Endereco.Entidade.Complementoendereco;
import Endereco.Entidade.Endereco;
import Endereco.Entidade.Enderecosimples;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlComplementoendereco extends CtrlBase {

    private static CtrlComplementoendereco ctrlcomplementoendereco;

    public static CtrlComplementoendereco create() {
        if (ctrlcomplementoendereco == null) {
            ctrlcomplementoendereco = new CtrlComplementoendereco();
        }
        return ctrlcomplementoendereco;
    }

    public static void setCampos(Object oComplementoendereco, TextField txbCep, TextField cbRua, TextField cbBairro,
            TextField cbCidade, TextField cbEstado, TextField cbPais, TextField cbNumero, TextField cbDescricao) {
        if (oComplementoendereco != null && oComplementoendereco instanceof Complementoendereco) {
            Complementoendereco en = (Complementoendereco) oComplementoendereco;
            cbNumero.setText(en.getEndNumero());
            cbDescricao.setText(en.getEndComplemento());
            CtrlEndereco.setCampos(en.getEndCep(), txbCep, cbRua, cbBairro, cbCidade, cbEstado, cbPais);
        }
    }

    public static void setCampos(Object oComplementoendereco, TextField cbLocal, TextField cbBairro,
            TextField cbNumero, TextField cbDescricao) {
        if (oComplementoendereco != null && oComplementoendereco instanceof Complementoendereco) {
            Complementoendereco en = (Complementoendereco) oComplementoendereco;
            cbNumero.setText(en.getEndNumero());
            cbDescricao.setText(en.getEndComplemento());
            CtrlEnderecosimples.setCampos(en.getEndsId(), cbLocal, cbBairro);
        }
    }

    public static Object getEndereco(Object oComplementoendereco) {
        if (oComplementoendereco != null && oComplementoendereco instanceof Complementoendereco) {
            Complementoendereco en = (Complementoendereco) oComplementoendereco;
            return en.getEndCep();
        }
        return null;
    }

    public static Object getEnderecoSimples(Object oComplementoendereco) {
        if (oComplementoendereco != null && oComplementoendereco instanceof Complementoendereco) {
            Complementoendereco en = (Complementoendereco) oComplementoendereco;
            return en.getEndsId();
        }
        return null;
    }

    public static boolean Compara(Object oComplementoendereco, Object Endereco, String Complemento, String Numero) {
        boolean flag = true;
        if (oComplementoendereco != null && oComplementoendereco instanceof Complementoendereco) {
            Complementoendereco en = (Complementoendereco) oComplementoendereco;
            flag = flag && Endereco.equals(en.getEndCep());
            flag = flag && en.getEndComplemento().equalsIgnoreCase(Complemento);
            flag = flag && en.getEndNumero().equalsIgnoreCase(Numero);
        } else {
            flag = false;
        }
        return flag;
    }

    public static boolean ComparaEnderecoSimples(Object oComplementoendereco, Object Endereco, String Complemento, String Numero) {
        boolean flag = true;
        if (oComplementoendereco != null && oComplementoendereco instanceof Complementoendereco) {
            Complementoendereco en = (Complementoendereco) oComplementoendereco;
            flag = flag && Endereco.equals(en.getEndsId());
            flag = flag && en.getEndComplemento().equalsIgnoreCase(Complemento);
            flag = flag && en.getEndNumero().equalsIgnoreCase(Numero);
        } else {
            flag = false;
        }
        return flag;
    }

    public static void setTabEndereco(Object endereco, TabPane tab) {
        if (tab != null && endereco != null && endereco instanceof Complementoendereco) {
            Platform.runLater(() -> {
                Complementoendereco oEndereco = (Complementoendereco) endereco;
                if (oEndereco.getEndsId() != null) {
                    tab.getSelectionModel().select(0);
                } else if (oEndereco.getEndCep() != null) {
                    tab.getSelectionModel().select(1);
                }
            });
        }
    }

    public CtrlComplementoendereco() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Param) {
        ArrayList<Object> complementoendereco = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Complementoendereco> ResultComplementoendereco = new ArrayList();
            if (Filtro != null && Filtro.trim().isEmpty()) {
                ResultComplementoendereco = em.createNamedQuery("Complementoendereco.findAll", Complementoendereco.class)
                        .getResultList();
            } else {
                try {
                    if (Param != null && Param.equalsIgnoreCase("CepNumeroComplemento")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null) {
                            if (auxFiltro.length == 3) {
                                ResultComplementoendereco = em.createNamedQuery("Complementoendereco.findByEndCep_Numero_Complemento", Complementoendereco.class)
                                        .setParameter("endCep", auxFiltro[0])
                                        .setParameter("endNumero", auxFiltro[1])
                                        .setParameter("endComplemento", auxFiltro[2]).getResultList();
                            } else if (auxFiltro.length == 2) {
                                ResultComplementoendereco = em.createNamedQuery("Complementoendereco.findByEndCep_Numero_Complemento", Complementoendereco.class)
                                        .setParameter("endCep", auxFiltro[0])
                                        .setParameter("endNumero", auxFiltro[1])
                                        .setParameter("endComplemento", "").getResultList();
                            }
                        }
                    } else if (Param != null && Param.equalsIgnoreCase("LocalNumeroBairroComplemento")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null) {
                            if (auxFiltro.length == 3) {
                                String bairro = "";
                                String complemento = "";
                                String[] auxFiltro2 = auxFiltro[2].split("#<:>#");
                                if(auxFiltro2 != null){
                                    if(auxFiltro2.length == 2){
                                        bairro = auxFiltro2[0].replaceAll("#b#", "");
                                        complemento = auxFiltro2[1].replaceAll("#c#", "");
                                    }else if(auxFiltro2.length == 1){
                                        if(auxFiltro2[1].contains("#b#")){
                                            bairro = auxFiltro2[0].replaceAll("#b#", "");
                                        }else{
                                            complemento = auxFiltro2[0].replaceAll("#c#", "");
                                        }
                                    }
                                }
                                ResultComplementoendereco = em.createNamedQuery("Complementoendereco.findByEndsLocal_Numero_Bairro_Complemento", Complementoendereco.class)
                                        .setParameter("endLocal", auxFiltro[0])
                                        .setParameter("endNumero", auxFiltro[1])
                                        .setParameter("endBairro", bairro)
                                        .setParameter("endComplemento", complemento).getResultList();
                            } else if (auxFiltro.length == 2) {
                                ResultComplementoendereco = em.createNamedQuery("Complementoendereco.findByEndsLocal_Numero_Bairro_Complemento", Complementoendereco.class)
                                        .setParameter("endLocal", auxFiltro[0])
                                        .setParameter("endNumero", auxFiltro[1])
                                        .setParameter("endBairro", "")
                                        .setParameter("endComplemento", "").getResultList();
                            }
                        }
                    } else {
                        ResultComplementoendereco = em.createNamedQuery("Complementoendereco.findByEndId", Complementoendereco.class)
                                .setParameter("endId", Integer.parseInt(Filtro)).getResultList();
                    }
                } catch (Exception ex) {
                    ResultComplementoendereco = em.createNamedQuery("Complementoendereco.findByEndComplemento", Complementoendereco.class)
                            .setParameter("endComplemento", Filtro).getResultList();
                }
            }
            for (Complementoendereco complementoenderecos : ResultComplementoendereco) {
                complementoendereco.add(complementoenderecos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return complementoendereco;
    }

    public Object Salvar(String Complemento, String Numero, Object endereco) {
        Complementoendereco complementoendereco = new Complementoendereco(Numero, Complemento);
        if (endereco != null && endereco instanceof Enderecosimples) {
            complementoendereco.setEndsId((Enderecosimples) endereco);
        } else if (endereco != null && endereco instanceof Endereco) {
            complementoendereco.setEndCep((Endereco) endereco);
        }
        return super.Salvar(complementoendereco);
    }

    public Object Alterar(Object oComplementoendereco, String Complemento, String Numero, Object endereco) {
        Complementoendereco complementoendereco = null;
        if (oComplementoendereco != null && oComplementoendereco instanceof Complementoendereco) {
            complementoendereco = (Complementoendereco) oComplementoendereco;
            complementoendereco.setEndComplemento(Complemento);
            complementoendereco.setEndNumero(Numero);
            if (endereco != null && endereco instanceof Enderecosimples) {
                complementoendereco.setEndsId((Enderecosimples) endereco);
            } else if (endereco != null && endereco instanceof Endereco) {
                complementoendereco.setEndCep((Endereco) endereco);
            }
            complementoendereco = (Complementoendereco) super.Alterar(complementoendereco);
        }
        return complementoendereco;
    }

    public boolean Remover(String Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Complementoendereco.class);
    }

}
