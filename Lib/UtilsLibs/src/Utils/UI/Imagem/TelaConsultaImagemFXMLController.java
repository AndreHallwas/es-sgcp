/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.UI.Imagem;

import Utils.Imagem;
import Utils.Mensagem;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaImagemFXMLController implements Initializable {

    @FXML
    private ImageView ImgView;
    @FXML
    private Button btnOK;
    private static BufferedImage Image;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            if (Image != null) {
                WritableImage im = Imagem.BufferedImageToImage(Image);
                ImgView.setFitHeight(620);
                ImgView.setFitWidth(im.getWidth() <= 1360 ? im.getWidth() : 1360);
                ImgView.setPreserveRatio(true);
                ImgView.setImage(im);
                Image = null;
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao Consultar Imagem na Tela de Imagem");
        }
    }

    @FXML
    private void evtOKClicked(ActionEvent event) {
        /*MainFXMLController._pndados.getChildren().clear();
        MainFXMLController._pndados.getChildren().addAll(MainFXMLController._pndados_bak);*/
        // get a handle to the stage
        Stage stage = (Stage) btnOK.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

    /**
     * @param aImage the Image to set
     */
    public static void setImage(BufferedImage aImage) {
        Image = aImage;
    }

}
