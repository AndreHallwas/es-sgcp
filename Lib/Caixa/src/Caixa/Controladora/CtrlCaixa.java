/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caixa.Controladora;

import Caixa.Entidade.Caixa;
import Caixa.Entidade.Movimentacaoapagar;
import Caixa.Entidade.Movimentacaoareceber;
import ContaaPagar.Entidade.Parcelaapagar;
import ContaaReceber.Entidade.Parcelaareceber;
import Controladora.Base.CtrlBase;
import FormadePagamento.Entidade.Formadepagamento;
import Pessoa.Entidade.Usuario;
import Transacao.Transacao;
import Transacao.Transaction;
import Utils.DateUtils;
import Utils.FormatString;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;

/**
 *
 * @author Raizen
 */
public class CtrlCaixa extends CtrlBase {

    private static CtrlCaixa ctrlcaixa;

    public static CtrlCaixa create() {
        if (ctrlcaixa == null) {
            ctrlcaixa = new CtrlCaixa();
        }
        return ctrlcaixa;
    }
    protected static Object CaixaAberto;

    public static BigDecimal getValorFinalCaixa(Object p) {
        if (p != null && p instanceof Caixa) {
            Caixa caixa = (Caixa) p;
            return caixa.getCaixaValorFinal() != null ? caixa.getCaixaValorFinal() : BigDecimal.ZERO;
        }
        return BigDecimal.ZERO;
    }

    public static BigDecimal getValorInicialCaixa(Object p) {
        if (p != null && p instanceof Caixa) {
            Caixa caixa = (Caixa) p;
            return caixa.getCaixaValorInicial() != null ? caixa.getCaixaValorInicial() : BigDecimal.ZERO;
        }
        return BigDecimal.ZERO;
    }

    public static LocalDate getDate(Object p) {
        if (p != null && p instanceof Caixa) {
            Caixa caixa = (Caixa) p;
            return DateUtils.asLocalDate(caixa.getCaixaData());
        }
        return LocalDate.now();
    }

    public CtrlCaixa() {
        super(Transaction.getEntityManagerFactory());
    }

    public Object RealizarMovimentacao(Object aParcela, EntityManager em) throws Exception {
        Object result = null;
        if (getCaixaAberto() && aParcela != null) {
            if (aParcela instanceof Parcelaapagar) {

                Parcelaapagar parcela = (Parcelaapagar) aParcela;
                Caixa caixa = (Caixa) CaixaAberto;
                /////Gera a Movimentação
                Movimentacaoapagar m = new Movimentacaoapagar(new Date());
                m.setCaixaId(caixa);
                m.setParcelaapagar(parcela);
                em.persist(m);

                /////Adiciona a Movimentação a Parcela
                parcela.getMovimentacaoapagarCollection().add(m);
                parcela = em.merge(parcela);

                /////Atualiza o Caixa
                caixa.getMovimentacaoapagarCollection().add(m);
                result = caixa = em.merge(caixa);
                em.flush();
            } else if (aParcela instanceof Parcelaareceber) {

                Parcelaareceber parcela = (Parcelaareceber) aParcela;
                Caixa caixa = (Caixa) CaixaAberto;
                /////Gera a Movimentação
                Movimentacaoareceber m = new Movimentacaoareceber(new Date());
                m.setCaixaId(caixa);
                m.setParcelaareceber(parcela);
                em.persist(m);

                /////Adiciona a Movimentação a Parcela
                parcela.getMovimentacaoareceberCollection().add(m);
                parcela = em.merge(parcela);

                /////Atualiza o Caixa
                caixa.getMovimentacaoareceberCollection().add(m);
                result = caixa = em.merge(caixa);
                em.flush();
            }
        }

        return result;
    }

    @Deprecated
    public boolean RealizarMovimentacao(Object aParcela) {
        Object result = null;
        if (getCaixaAberto() && aParcela != null && aParcela instanceof Parcelaareceber) {
            result = new Transacao() {
                @Override
                public Object Transacao(Object... Params) {
                    Parcelaareceber parcela = (Parcelaareceber) aParcela;
                    Caixa caixa = (Caixa) CaixaAberto;
                    /////Gera a Movimentação
                    Movimentacaoareceber m = new Movimentacaoareceber(new Date());
                    m.setCaixaId(caixa);
                    m.setParcelaareceber(parcela);

                    /////Atualiza o Caixa
                    caixa.getMovimentacaoareceberCollection().add(m);
                    return caixa = em.merge(caixa);
                }
            }.Realiza();
        }

        return result != null;
    }

    public Object RemoveMovimentacao(Object aParcela, EntityManager em) throws Exception {
        Object result = null;
        if (getCaixaAberto() && aParcela != null) {
            if (aParcela instanceof Parcelaapagar) {
                Parcelaapagar parcela = (Parcelaapagar) aParcela;
                Caixa CaixaAtual = (Caixa) CaixaAberto;
                /*em.find(Caixa.class, CaixaAtual.getCaixaId())*/
                ///Pega a Movimentação da parcela
                List<Movimentacaoapagar> movimentacoes = (List<Movimentacaoapagar>) parcela.getMovimentacaoapagarCollection();
                if (movimentacoes != null && movimentacoes.size() > 0) {
                    Movimentacaoapagar MovimentacaoParcela = movimentacoes.get(0);

                    /////Verifica se o Caixa da Movimentação é o mesmo que está aberto
                    if (MovimentacaoParcela.getCaixaId().getCaixaId().equals(CaixaAtual.getCaixaId())) {
                        CaixaAtual.getMovimentacaoapagarCollection().remove(MovimentacaoParcela);
                        result = CaixaAtual = em.merge(CaixaAtual);
                        if (!em.contains(MovimentacaoParcela)) {
                            MovimentacaoParcela = em.merge(MovimentacaoParcela);
                        }
                        em.remove(MovimentacaoParcela);
                    }
                }
            } else if (aParcela instanceof Parcelaareceber) {
                Parcelaareceber parcela = (Parcelaareceber) aParcela;
                Caixa CaixaAtual = (Caixa) CaixaAberto;
                /*em.find(Caixa.class, CaixaAtual.getCaixaId())*/
                ///Pega a Movimentação da parcela
                List<Movimentacaoareceber> movimentacoes = (List<Movimentacaoareceber>) parcela.getMovimentacaoareceberCollection();
                if (movimentacoes != null && movimentacoes.size() > 0) {
                    Movimentacaoareceber MovimentacaoParcela = movimentacoes.get(0);

                    /////Verifica se o Caixa da Movimentação é o mesmo que está aberto
                    if (MovimentacaoParcela.getCaixaId().getCaixaId().equals(CaixaAtual.getCaixaId())) {
                        CaixaAtual.getMovimentacaoareceberCollection().remove(MovimentacaoParcela);
                        result = CaixaAtual = em.merge(CaixaAtual);
                        if (!em.contains(MovimentacaoParcela)) {
                            MovimentacaoParcela = em.merge(MovimentacaoParcela);
                        }
                        em.remove(MovimentacaoParcela);
                    }
                }
            }
        }

        return result;
    }

    @Deprecated
    public boolean RemoveMovimentacao(Object aParcela) {
        Object result = null;
        if (getCaixaAberto() && aParcela != null && aParcela instanceof Parcelaareceber) {
            Parcelaareceber parcela = (Parcelaareceber) aParcela;
            Caixa CaixaAtual = (Caixa) CaixaAberto;

            ///Pega a Movimentação da parcela
            List<Movimentacaoareceber> movimentacoes = (List<Movimentacaoareceber>) parcela.getMovimentacaoareceberCollection();
            if (movimentacoes != null && movimentacoes.size() > 0) {
                Movimentacaoareceber MovimentacaoParcela = movimentacoes.get(0);

                /////Verifica se o Caixa da Movimentação é o mesmo que está aberto
                if (MovimentacaoParcela.getCaixaId().getCaixaId().equals(CaixaAtual.getCaixaId())) {

                    result = new Transacao() {
                        @Override
                        public Object Transacao(Object... Params) {

                            /*List<Movimentacao> MovimentacoesCaixa = (List<Movimentacao>) CaixaAtual.getMovimentacaoCollection();
                            int i = 0;
                            while (i < MovimentacoesCaixa.size() && MovimentacoesCaixa.get(i).getParcelaapagar() != null
                                    && MovimentacoesCaixa.get(i).getParcelaareceber().getParcelaareceberPK().equals(parcela.getParcelaareceberPK())) {
                                i++;
                            }*/
                            /////Atualiza o Caixa
                            /*if (i < MovimentacoesCaixa.size()) {
                                CaixaAtual.getMovimentacaoCollection().remove(MovimentacoesCaixa.get(i));
                            }*/
                            /////Atualiza o Caixa Removendo a Movimentação.
                            CaixaAtual.getMovimentacaoareceberCollection().remove(MovimentacaoParcela);

                            return em.merge(CaixaAtual);
                        }
                    }.Realiza();
                }
            }

        }

        return result != null;
    }

    public boolean Abrir(BigDecimal ValorInicial, Object usuario, LocalDate Data) {
        boolean flag = true;
        ///Verificar Caixa Anterior aberto

        ///Calcular Valor Inicial
        ////BigDecimal ValorInicial = new BigDecimal(1000);
        ///abrir caixa
        /////Pesquisar Caixa e verificar se ja foi aberto na data.
        ArrayList<Object> oCaixa = Pesquisar(DateUtils.asDate(Data), "data");
        //////Caso não tenha sido aberto
        if (oCaixa.isEmpty()) {
            CaixaAberto = Salvar(DateUtils.asDate(Data), ValorInicial, null, null, usuario);
        } else {
            /////Caso tenha sido aberto no dia;
            Caixa caixa = (Caixa) oCaixa.get(0);
            /////Caso o Caixa não esteja Fechado;
            if (caixa.getCaixaDataFechamento() != null && caixa.getCaixaValorFinal() != null) {
                CaixaAberto = Alterar(caixa, caixa.getCaixaId(), caixa.getCaixaData(),
                        ValorInicial, null, null, usuario);
            } else {
                CaixaAberto = caixa;
                Message = "O Caixa está Aberto";
            }
        }
        flag = flag && CaixaAberto != null;
        Message = flag ? "Caixa Aberto" : "Erro ao Abrir Caixa";
        return flag;
    }

    public boolean Fechar(BigDecimal ValorFinal, Object usuario) {
        boolean flag = true;
        ///Verificar se o Caixa está Aberto

        Caixa caixa = (Caixa) CaixaAberto;
        ///Verificar se é o Usuario que Abriu o Caixa

        ///Calcular Valor Final Baseado nas Movimentações
        ValorFinal = getValorFinal(caixa);

        ///Fechar o Caixa
        caixa = (Caixa) Alterar(caixa, caixa.getCaixaId(), caixa.getCaixaData(),
                caixa.getCaixaValorInicial(), ValorFinal,
                DateUtils.asDate(LocalDate.now()), usuario);

        if (caixa != null) {
            flag = true;
            CaixaAberto = null;
        }
        Message = flag ? "Caixa Fechado" : "Erro ao Fechar Caixa";
        return flag;
    }

    public BigDecimal getValoraPagar(Object oCaixa) {
        BigDecimal Valorapagar = new BigDecimal(0);
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            for (Movimentacaoapagar movimentacaoapagar : caixa.getMovimentacaoapagarCollection()) {
                Valorapagar = Valorapagar.add(movimentacaoapagar.getParcelaapagar().getParpValorPago());
            }
        }
        return Valorapagar;
    }

    public BigDecimal getValoraReceber(Object oCaixa) {
        BigDecimal Valorareceber = new BigDecimal(0);
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            for (Movimentacaoareceber movimentacaoareceber : caixa.getMovimentacaoareceberCollection()) {
                Valorareceber = Valorareceber.add(movimentacaoareceber.getParcelaareceber().
                        getParrValorPago());
            }
        }
        return Valorareceber;
    }

    public BigDecimal getValorFinal(Object oCaixa) {
        BigDecimal ValorFinal = BigDecimal.ZERO;
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            BigDecimal Valorareceber = getValoraReceber(caixa);
            BigDecimal Valorapagar = getValoraPagar(caixa);
            ValorFinal = caixa.getCaixaValorInicial().add(Valorareceber).subtract(Valorapagar);
        }
        return ValorFinal;
    }

    public BigDecimal getValoraPagarFisico(Object oCaixa) {
        BigDecimal Valorapagar = new BigDecimal(0);
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            for (Movimentacaoapagar movimentacaoapagar : caixa.getMovimentacaoapagarCollection()) {
                if (movimentacaoapagar.getParcelaapagar().getFormadepagamentoId() != null
                        && !movimentacaoapagar.getParcelaapagar().getFormadepagamentoId().getFormadepagamentoRecebimentoaprazo()) {
                    Valorapagar = Valorapagar.add(movimentacaoapagar.getParcelaapagar().getParpValorPago());
                }
            }
        }
        return Valorapagar;
    }

    public BigDecimal getValoraReceberFisico(Object oCaixa) {
        BigDecimal Valorareceber = new BigDecimal(0);
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            for (Movimentacaoareceber movimentacaoareceber : caixa.getMovimentacaoareceberCollection()) {
                if (movimentacaoareceber.getParcelaareceber().getFormadepagamentoId() != null
                        && !movimentacaoareceber.getParcelaareceber().getFormadepagamentoId().getFormadepagamentoRecebimentoaprazo()) {
                    Valorareceber = Valorareceber.add(movimentacaoareceber.getParcelaareceber().
                            getParrValorPago());
                }
            }
        }
        return Valorareceber;
    }

    public BigDecimal getValoraReceberaPrazo(Object oCaixa) {
        BigDecimal Valorareceber = new BigDecimal(0);
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            for (Movimentacaoareceber movimentacaoareceber : caixa.getMovimentacaoareceberCollection()) {
                if (movimentacaoareceber.getParcelaareceber().getFormadepagamentoId() != null
                        && movimentacaoareceber.getParcelaareceber().getFormadepagamentoId().getFormadepagamentoRecebimentoaprazo()) {
                    Valorareceber = Valorareceber.add(movimentacaoareceber.getParcelaareceber().
                            getParrValorPago());
                }
            }
        }
        return Valorareceber;
    }

    public BigDecimal getValoraPagaraPrazo(Object oCaixa) {
        BigDecimal Valorapagar = new BigDecimal(0);
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            for (Movimentacaoapagar movimentacaoapagar : caixa.getMovimentacaoapagarCollection()) {
                if (movimentacaoapagar.getParcelaapagar().getFormadepagamentoId() != null
                        && movimentacaoapagar.getParcelaapagar().getFormadepagamentoId().getFormadepagamentoRecebimentoaprazo()) {
                    Valorapagar = Valorapagar.add(movimentacaoapagar.getParcelaapagar().getParpValorPago());
                }
            }
        }
        return Valorapagar;
    }

    public BigDecimal getValoraFinalaPrazo(Object oCaixa) {
        BigDecimal ValorFinal = BigDecimal.ZERO;
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            BigDecimal Valorareceber = getValoraReceberaPrazo(caixa);
            BigDecimal Valorapagar = getValoraPagaraPrazo(caixa);
            ValorFinal = caixa.getCaixaValorInicial().add(Valorareceber).subtract(Valorapagar);
        }
        return ValorFinal;
    }

    public BigDecimal getValoraFinalFisico(Object oCaixa) {
        BigDecimal ValorFinal = BigDecimal.ZERO;
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            BigDecimal Valorareceber = getValoraReceberFisico(caixa);
            BigDecimal Valorapagar = getValoraPagarFisico(caixa);
            ValorFinal = caixa.getCaixaValorInicial().add(Valorareceber).subtract(Valorapagar);
        }
        return ValorFinal;
    }

    public List<Object> getMovimentacoesaPagar(Object oCaixa) {
        List<Object> Objetos = new ArrayList();
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            Objetos.addAll(caixa.getMovimentacaoapagarCollection());
        }
        return Objetos;
    }

    public ArrayList<Object> getRecebimentosporFormadePagamento(Object oCaixa) {
        ArrayList<Object> result = new ArrayList();
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa CaixaAnterior = (Caixa) oCaixa;
            TabelaFormadePagamentoValor Tabela = new TabelaFormadePagamentoValor();
            for (Movimentacaoapagar movimentacao : CaixaAnterior.getMovimentacaoapagarCollection()) {
                Tabela.add(movimentacao.getParcelaapagar().getFormadepagamentoId(), movimentacao.getParcelaapagar().getParpValorPago());
            }
            for (Movimentacaoareceber movimentacao : CaixaAnterior.getMovimentacaoareceberCollection()) {
                Tabela.add(movimentacao.getParcelaareceber().getFormadepagamentoId(), movimentacao.getParcelaareceber().getParrValorPago());
            }
            result.addAll(Tabela.get());
        }
        return result;
    }

    public List<Object> getMovimentacoesaReceber(Object oCaixa) {
        List<Object> Objetos = new ArrayList();
        if (oCaixa != null && oCaixa instanceof Caixa) {
            Caixa caixa = (Caixa) oCaixa;
            Objetos.addAll(caixa.getMovimentacaoareceberCollection());
        }
        return Objetos;
    }

    public static void setCampoInicial(Object caixa, TextField txbValor, TextField txbSaldo) {
        if (caixa != null && caixa instanceof Caixa) {
            Caixa f = (Caixa) caixa;
            txbValor.setText(f.getCaixaValorInicial() + "");
            txbSaldo.setText(f.getCaixaValorInicial() + "");
            /*taObs.setText(f.getCaixaDescricao());
            txbNome.setText(f.getCaixaNome());*/
        }
    }

    @Deprecated
    public void setValorInicial(Object caixa, TextField txbValor) {
        if (caixa != null && caixa instanceof Caixa) {
            Caixa f = (Caixa) caixa;
            if (f.getCaixaValorInicial() == null) {
                ArrayList<Object> Valor = Pesquisar(f.getCaixaData(), "caixa_anterior_valor");
                if (Valor != null && !Valor.isEmpty()) {
                    Caixa CaixaAnterior = (Caixa) Valor.get(0);
                    txbValor.setText(CaixaAnterior.getCaixaValorFinal() + "");
                }
            } else {
                txbValor.setText(f.getCaixaValorFinal() + "");
            }
        }
    }

    public BigDecimal getValorFinalCaixaAnterior() {
        ArrayList<Object> Valor = Pesquisar(new Object(), "caixa_ultimo");
        if (Valor != null && !Valor.isEmpty()) {
            Caixa CaixaAnterior = (Caixa) Valor.get(0);
            return CaixaAnterior.getCaixaValorFinal() != null
                    ? CaixaAnterior.getCaixaValorFinal() : BigDecimal.ZERO;
        }
        return BigDecimal.ZERO;
    }

    public static void setCampoFinal(Object caixa, TextField txbValor, TextField txbSaldo) {
        if (caixa != null && caixa instanceof Caixa) {
            Caixa f = (Caixa) caixa;
            String Valor;
            if (f.getCaixaValorFinal() == null) {
                Valor = f.getCaixaValorInicial() + "";
            } else {
                Valor = f.getCaixaValorFinal() + "";
            }
            txbValor.setText(Valor + "");
            txbSaldo.setText(Valor + "");
            /*taObs.setText(f.getCaixaDescricao());
            txbNome.setText(f.getCaixaNome());*/
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Caixa) {
            return ((Caixa) p).getCaixaId();
        }
        return -1;
    }

    public boolean getCaixaFechado(Object p) {
        if (p != null && p instanceof Caixa) {
            Caixa caixa = (Caixa) p;
            return caixa.getCaixaDataFechamento() != null && caixa.getCaixaValorFinal() != null;
        }
        return false;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Caixa) {
            txBusca.setText(((Caixa) p).getCaixaData().toString());
        }
    }

    public ArrayList<Object> Pesquisar(Object oFiltro, String Param) {
        ArrayList<Object> caixa = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Caixa> ResultCaixa = new ArrayList();
            if (oFiltro == null || oFiltro instanceof String && ((String) oFiltro).trim().isEmpty()) {
                ResultCaixa = em.createNamedQuery("Caixa.findAll", Caixa.class)
                        .getResultList();
            } else {
                try {
                    if (Param.equalsIgnoreCase("ID")) {
                        String Filtro = oFiltro + "";
                        ResultCaixa = em.createNamedQuery("Caixa.findByCaixaId", Caixa.class)
                                .setParameter("caixaId", Integer.parseInt(Filtro)).getResultList();
                    } else if (Param.equalsIgnoreCase("DATA")) {
                        Date Filtro = (Date) oFiltro;
                        ResultCaixa = em.createNamedQuery("Caixa.findByCaixaData", Caixa.class)
                                .setParameter("caixaData", Filtro).getResultList();
                    } else if (Param.equalsIgnoreCase("caixa_anterior_valor")) {
                        ResultCaixa = em.createNativeQuery("select * from caixa where caixa.caixa_data <"
                                + " (select caixa_data from caixa where caixa.caixa_data = ?adata)"
                                + " order by caixa_data desc limit 1", Caixa.class)
                                .setParameter("adata", ((Date) oFiltro), TemporalType.DATE).getResultList();
                    } else if (Param.equalsIgnoreCase("caixa_ultimo")) {
                        ResultCaixa = em.createNativeQuery("select * from caixa where caixa.caixa_data ="
                                + " (select max(caixa_data) from caixa)"
                                + " order by caixa_data desc limit 1", Caixa.class).getResultList();
                    } else if (Param.equalsIgnoreCase("caixa_aberto")) {
                        ResultCaixa = em.createNativeQuery("select * from caixa where caixa.caixa_valor_final IS null and"
                                + " caixa.caixa_data_fechamento IS null order by"
                                + " caixa_data asc", Caixa.class).getResultList();
                    } else if (Param.equalsIgnoreCase("data_anterior")) {
                        ResultCaixa = em.createNativeQuery("select * from caixa where caixa.caixa_data <"
                                + " (select caixa_data from caixa where caixa.caixa_data = ?adata)"
                                + " order by caixa_data desc limit 1", Caixa.class)
                                .setParameter("adata", ((Date) oFiltro), TemporalType.DATE).getResultList();
                    } else if (Param.equalsIgnoreCase("caixa_anterior_ao_ultimo")) {
                        ResultCaixa = em.createNativeQuery("select * from caixa where caixa.caixa_data <"
                                + " (select max(caixa_data) from caixa)"
                                + " order by caixa_data desc limit 1", Caixa.class).getResultList();
                    } else {
                        ResultCaixa = em.createNamedQuery("Caixa.findAll", Caixa.class)
                                .getResultList();
                    }

                } catch (Exception ex) {
                    ResultCaixa = em.createNamedQuery("Caixa.findAll", Caixa.class)
                            .getResultList();
                }
            }

            for (Caixa caixas : ResultCaixa) {
                caixa.add(caixas);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return caixa;
    }

    public Object Salvar(Date DataAbertura, BigDecimal ValorInicial, BigDecimal ValorFinal,
            Date DataFechamento, Object usuario) {
        Caixa caixa = new Caixa();
        caixa.setCaixaValorFinal(ValorFinal);
        caixa.setCaixaDataFechamento(DataFechamento);
        caixa.setCaixaData(DataAbertura);
        caixa.setCaixaValorInicial(ValorInicial);
        if (usuario != null && usuario instanceof Usuario) {
            caixa.setUsrId((Usuario) usuario);
        }
        return super.Salvar(caixa);
    }

    public Object Alterar(Object oCaixa, Integer Id, Date DataAbertura, BigDecimal ValorInicial,
            BigDecimal ValorFinal, Date DataFechamento, Object usuario) {
        Caixa caixa = null;
        if (oCaixa != null && oCaixa instanceof Caixa) {
            caixa = (Caixa) oCaixa;
            caixa.setCaixaValorFinal(ValorFinal);
            caixa.setCaixaDataFechamento(DataFechamento);
            caixa.setCaixaData(DataAbertura);
            caixa.setCaixaValorInicial(ValorInicial);
            if (usuario != null && usuario instanceof Usuario) {
                caixa.setUsrId((Usuario) usuario);
            }
            caixa = (Caixa) super.Alterar(caixa);
        }
        return caixa;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    public class TabelaFormadePagamentoValor {

        ArrayList<NodeFormadePagamentoValor> Tabela = new ArrayList();

        protected void Init() {
            if (Tabela == null) {
                Tabela = new ArrayList();
            }
        }

        public void add(Formadepagamento formadepagamento, BigDecimal valor) {
            Init();
            int i = 0;
            while (i < Tabela.size() && !Tabela.get(i).getNome().getFormadepagamentoNome()
                    .equals(formadepagamento.getFormadepagamentoNome())) {
                i++;
            }
            if (i < Tabela.size()) {
                BigDecimal Valor = Tabela.get(i).getValor() != null
                        ? Tabela.get(i).getValor() : BigDecimal.ZERO;
                Tabela.get(i).setValor(Valor.add(valor));
            } else {
                Tabela.add(new NodeFormadePagamentoValor(formadepagamento, valor));
            }
        }

        public ArrayList<Object> get() {
            Init();
            ArrayList<Object> tabela = new ArrayList();
            tabela.addAll(Tabela);
            return tabela;
        }

        public class NodeFormadePagamentoValor {

            protected Formadepagamento Nome;
            protected BigDecimal Valor;

            public NodeFormadePagamentoValor(Formadepagamento Nome, BigDecimal Valor) {
                this.Nome = Nome;
                this.Valor = Valor;
            }

            /**
             * @return the Nome
             */
            public Formadepagamento getNome() {
                return Nome;
            }

            /**
             * @param Nome the Nome to set
             */
            public void setNome(Formadepagamento Nome) {
                this.Nome = Nome;
            }

            /**
             * @return the Valor
             */
            public BigDecimal getValor() {
                return Valor;
            }

            public String getOValor() {
                return FormatString.MonetaryPersistent(Valor);
            }

            /**
             * @param Valor the Valor to set
             */
            public void setValor(BigDecimal Valor) {
                this.Valor = Valor;
            }
        }
    }

    public Object getCaixaAbertoObject() {
        return CaixaAberto;
    }

    /**
     * @return the CaixaAberto
     */
    public boolean getCaixaAberto() {
        if (CaixaAberto != null) {
            Message = "O Caixa ja está Aberto";
            return true;
        }
        Message = "O Caixa Não está Aberto";
        return false;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Caixa.class);
    }

}
