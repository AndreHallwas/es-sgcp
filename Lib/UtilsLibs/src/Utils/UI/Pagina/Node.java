/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.UI.Pagina;

/**
 *
 * @author Raizen
 */
public class Node {
    private Node Proximo;
    private Node Anterior;
    private Acao Dados;

    public Node() {
    }

    public Node(Node Proximo, Node Anterior, Acao Dados) {
        this.Proximo = Proximo;
        this.Anterior = Anterior;
        this.Dados = Dados;
    }
    
    
    public boolean Seleciona(Object... Params){
        return getDados().Seleciona(Params);
    }
    
    public Node Proximo(Object... Params){
        if(getProximo() != null && getDados().Verifica(Params)){
            getProximo().Seleciona(Params);
            return getProximo();
        }
        return null;
    }
    /**
     * @return the Proximo
     */
    public Node getProximo() {
        return Proximo;
    }

    /**
     * @param Proximo the Proximo to set
     */
    public void setProximo(Node Proximo) {
        this.Proximo = Proximo;
    }

    /**
     * @return the Anterior
     */
    public Node getAnterior() {
        return Anterior;
    }

    /**
     * @param Anterior the Anterior to set
     */
    public void setAnterior(Node Anterior) {
        this.Anterior = Anterior;
    }

    /**
     * @return the Dados
     */
    public Acao getDados() {
        return Dados;
    }

    /**
     * @param Dados the Dados to set
     */
    public void setDados(Acao Dados) {
        this.Dados = Dados;
    }
}
