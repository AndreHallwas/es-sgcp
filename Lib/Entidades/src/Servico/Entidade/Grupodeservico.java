/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "grupodeservico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupodeservico.findAll", query = "SELECT g FROM Grupodeservico g")
    , @NamedQuery(name = "Grupodeservico.findByGrupodeservicoId", query = "SELECT g FROM Grupodeservico g WHERE g.grupodeservicoId = :grupodeservicoId")
    , @NamedQuery(name = "Grupodeservico.findByGrupodeservicoDescricao", query = "SELECT g FROM Grupodeservico g WHERE g.grupodeservicoDescricao = :grupodeservicoDescricao")})
public class Grupodeservico extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "grupodeservico_id")
    private Integer grupodeservicoId;
    @Basic(optional = false)
    @Column(name = "grupodeservico_descricao")
    private String grupodeservicoDescricao;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "grupodeservicoId")
    private Collection<Servico> servicoCollection = new ArrayList();

    public Grupodeservico() {
    }

    public Grupodeservico(Integer grupodeservicoId) {
        this.grupodeservicoId = grupodeservicoId;
    }

    public Grupodeservico(String grupodeservicoDescricao) {
        this.grupodeservicoDescricao = grupodeservicoDescricao;
    }

    public Grupodeservico(Integer grupodeservicoId, String grupodeservicoDescricao) {
        this.grupodeservicoId = grupodeservicoId;
        this.grupodeservicoDescricao = grupodeservicoDescricao;
    }

    public Integer getGrupodeservicoId() {
        return grupodeservicoId;
    }

    public void setGrupodeservicoId(Integer grupodeservicoId) {
        this.grupodeservicoId = grupodeservicoId;
    }

    public String getGrupodeservicoDescricao() {
        return grupodeservicoDescricao;
    }

    public void setGrupodeservicoDescricao(String grupodeservicoDescricao) {
        this.grupodeservicoDescricao = grupodeservicoDescricao;
    }

    @XmlTransient
    public Collection<Servico> getServicoCollection() {
        return servicoCollection;
    }

    public void setServicoCollection(Collection<Servico> servicoCollection) {
        this.servicoCollection = servicoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grupodeservicoId != null ? grupodeservicoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupodeservico)) {
            return false;
        }
        Grupodeservico other = (Grupodeservico) object;
        if ((this.grupodeservicoId == null && other.grupodeservicoId != null) || (this.grupodeservicoId != null && !this.grupodeservicoId.equals(other.grupodeservicoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return grupodeservicoDescricao;
    }
    
}
