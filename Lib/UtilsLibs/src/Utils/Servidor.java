package Geral.Servidor;

import java.net.ServerSocket;
import java.net.Socket;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Aluno
 */
public class Servidor 
{
    private static ServerSocket server;
    private static Socket sock;
    private static String mens;

    public static void run() 
    {
        try 
        {
            server = new ServerSocket(5000, 100); //esperando conexão na porta 5000
            System.out.println("177.131.36.185   Esperando conexão....");
            while (true) 
            {
                sock = server.accept(); // a espera de conexões (listen)
                new Thread(new Conexao(sock)).start();
            }
        }
        catch (Exception e) 
        {
            System.out.println("Erro: " + e);
        }
    }
}
