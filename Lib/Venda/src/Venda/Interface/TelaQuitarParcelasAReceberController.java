/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Interface;

import Venda.Controladora.CtrlVenda;
import Contaareceber.Controladora.CtrlContaareceber;
import Contaareceber.Controladora.CtrlParcelaareceber;
import Formadepagamento.Controladora.CtrlFormadepagamento;
import Utils.Controladora.CtrlUtils;
import Utils.FormatString;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaQuitarParcelasAReceberController implements Initializable, Tela {

    @FXML
    private VBox gpQuitarParcelaaReceber;
    @FXML
    private Pane gpDadosdaVenda;
    @FXML
    private JFXTextField txbValorToral;
    @FXML
    private JFXTextField txbParcelas;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Integer> tcParcela;
    @FXML
    private TableColumn<Object, Double> tcValor;
    @FXML
    private TableColumn<Object, String> tcDatadeVencimento;
    @FXML
    private TableColumn<Object, Boolean> tcStatus;
    @FXML
    private TableColumn<Object, String> tcDatadePagamento;
    @FXML
    private TableColumn<Object, Double> tcValorPago;
    @FXML
    private JFXTextField txbValordaParcela;
    @FXML
    private JFXTextField txbValoraReceber;
    @FXML
    private JFXTextField txbVenda;
    @FXML
    private Label lblStatus;
    @FXML
    private JFXButton btnRealizarPagamento;
    @FXML
    private JFXButton btnEstornarPagamento;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXComboBox<Object> cbFormadepagamento;
    @FXML
    private JFXTextField txbValorFaltante;

    private Object venda;
    private Object ItemSelecionado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcDatadeVencimento.setCellValueFactory(new PropertyValueFactory("DataVencimento"));
        tcParcela.setCellValueFactory(new PropertyValueFactory("ParrNumerodaparcela"));
        tcStatus.setCellValueFactory(new PropertyValueFactory("Situacao"));
        tcValor.setCellValueFactory(new PropertyValueFactory("Valor"));
        tcValorPago.setCellValueFactory(new PropertyValueFactory("ValorPago"));
        tcDatadePagamento.setCellValueFactory(new PropertyValueFactory("DataPagamento"));
        MaskFieldUtil.numericField(txbValoraReceber);
        estadoOriginal();
    }

    private void estadoOriginal() {
        gpDadosdaVenda.setDisable(true);
        CtrlUtils.clear(pndados.getChildren());
        lblStatus.setText("Status");
        btnEstornarPagamento.setDisable(true);
        btnRealizarPagamento.setDisable(true);
        tabela.getItems().clear();
        estadoOriginalCbParcelas();
    }

    @FXML
    private void HandleButtonRealizarPagamento(MouseEvent event) {
        btnEstornarPagamento.setDisable(true);
        btnRealizarPagamento.setDisable(true);
        if (venda != null) {
            if (!CtrlContaareceber.create().VerificaProximoVencimento(ItemSelecionado)) {
                if (Mensagem.ExibirConfimacao("Atenção: Este não é o Vencimento mais Próximo, "
                        + "Deseja prosseguir com o pagamento mesmo Assim?")) {
                    if (CtrlParcelaareceber.create().Receber(ItemSelecionado, txbValoraReceber.getText())) {
                        CtrlParcelaareceber.create().setFormadepagamento(ItemSelecionado,
                                cbFormadepagamento.getSelectionModel().getSelectedItem());
                        Mensagem.Exibir(CtrlParcelaareceber.create().getMsg(), 1);
                    } else {
                        Mensagem.Exibir(CtrlParcelaareceber.create().getMsg(), 2);
                    }
                    CarregaTabela(venda);
                }
            } else {
                if (CtrlParcelaareceber.create().Receber(ItemSelecionado, txbValoraReceber.getText())) {
                    CtrlParcelaareceber.create().setFormadepagamento(ItemSelecionado,
                            cbFormadepagamento.getSelectionModel().getSelectedItem());
                    Mensagem.Exibir(CtrlParcelaareceber.create().getMsg(), 1);
                } else {
                    Mensagem.Exibir(CtrlParcelaareceber.create().getMsg(), 2);
                }
                CarregaTabela(venda);
            }
        }
    }

    @FXML
    private void HandleButtonEstornarPagamento(MouseEvent event) {
        btnEstornarPagamento.setDisable(true);
        btnRealizarPagamento.setDisable(true);
        if (venda != null && ItemSelecionado != null) {
            if (!CtrlContaareceber.create().VerificaUltimoRecebimento(ItemSelecionado)) {
                if (Mensagem.ExibirConfimacao("Atenção: Este não é o ultimo pagamento, "
                        + "Deseja prosseguir com o estorno mesmo Assim?")) {
                    if (CtrlParcelaareceber.create().estornar(ItemSelecionado)) {
                        Mensagem.Exibir(CtrlParcelaareceber.create().getMsg(), 1);
                    } else {
                        Mensagem.Exibir(CtrlParcelaareceber.create().getMsg(), 2);
                    }
                    CarregaTabela(venda);
                }
            } else {
                if (CtrlParcelaareceber.create().estornar(ItemSelecionado)) {
                    Mensagem.Exibir(CtrlParcelaareceber.create().getMsg(), 1);
                } else {
                    Mensagem.Exibir(CtrlParcelaareceber.create().getMsg(), 2);
                }
                CarregaTabela(venda);
            }
        }
    }

    @FXML
    private void HandleButtonCancelar(MouseEvent event) {
        estadoOriginal();
    }

    @FXML
    private void HandleButtonPesquisarVenda(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Venda/Interface/TelaConsultaVenda.fxml",
                "Consulta de Venda", null);
        if (TelaConsultaVendaController.getVenda() != null) {
            venda = TelaConsultaVendaController.getVenda();
            txbVenda.setText(venda.toString());
            gpDadosdaVenda.setDisable(false);
            CtrlVenda.setCampos(venda, txbValorToral, txbParcelas);
            CarregaTabela(venda);
        } else {
            estadoOriginal();
        }
    }

    @FXML
    private void clicknatabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            Object f = tabela.getItems().get(lin);
            if (CtrlParcelaareceber.create().getSituacao(f) == true) {
                btnEstornarPagamento.setDisable(false);
                btnRealizarPagamento.setDisable(true);
            } else {
                btnRealizarPagamento.setDisable(false);
                btnEstornarPagamento.setDisable(true);
            }
            CtrlParcelaareceber.create().setCampos(f, txbValoraReceber, txbValordaParcela);
            cbFormadepagamento.getSelectionModel().clearSelection();
            Object aFormadepagamento = CtrlParcelaareceber.getFormadepagamento(f);
            if (aFormadepagamento != null) {
                cbFormadepagamento.getSelectionModel().select(aFormadepagamento);
            }
            ItemSelecionado = f;
        }
    }

    private void CarregaTabela(Object venda) {
        CtrlVenda ctm = CtrlVenda.create();
        CtrlParcelaareceber ccp = CtrlParcelaareceber.create();
        try {
            tabela.getItems().clear();
            if (venda != null) {
                tabela.setItems(FXCollections.observableList(
                        ccp.Pesquisar(ctm.getId(venda) + "", "venda")));
                VerificaStatus();
                txbValorFaltante.setText(FormatString.MonetaryPersistent(CtrlVenda.getValorFaltante(venda)));
            }

        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tabela");
        }
    }

    public void VerificaStatus() {
        CtrlVenda cc = CtrlVenda.create();
        boolean flag = false;
        if (ItemSelecionado != null) {
            Object auxVenda = CtrlParcelaareceber.getVenda(ItemSelecionado);
            if (auxVenda != null) {
                venda = auxVenda;
            } else {
                flag = true;
            }
        }
        if (flag) {
            ArrayList<Object> vendas = cc.Pesquisar(cc.getId(venda).toString(), "Id");
            if (vendas != null && vendas.size() > 0) {
                venda = vendas.get(0);
            }
        }
        if (venda != null) {
            String Resultado = cc.VerificaPagamento(venda);
            lblStatus.setText(Resultado);
        }

    }

    private void estadoOriginalCbParcelas() {
        cbFormadepagamento.getItems().clear();
        List<Object> ClimasdePagamento = CtrlFormadepagamento.create().Pesquisar("");
        if (ClimasdePagamento != null && !ClimasdePagamento.isEmpty()) {
            cbFormadepagamento.getItems().addAll(ClimasdePagamento);
        }
    }

}
