/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Entidade;

import Agendamento.Entidade.Agendamento;
import Atendimento.Entidade.Atendimento;
import JPA.Dao.JPADao;
import Venda.Entidade.Venda;
import Pessoa.Entidade.Cliente;
import Vacinacao.Entidade.Vacinacao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "animal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Animal.findAll", query = "SELECT a FROM Animal a")
    , @NamedQuery(name = "Animal.findByAnimalId", query = "SELECT a FROM Animal a WHERE a.animalId = :animalId")
    , @NamedQuery(name = "Animal.findByAnimalNome", query = "SELECT a FROM Animal a WHERE a.animalNome = :animalNome")
    , @NamedQuery(name = "Animal.findByAnimalPeso", query = "SELECT a FROM Animal a WHERE a.animalPeso = :animalPeso")
    , @NamedQuery(name = "Animal.findByAnimalCor", query = "SELECT a FROM Animal a WHERE a.animalCor = :animalCor")
    , @NamedQuery(name = "Animal.findByAnimalIdade", query = "SELECT a FROM Animal a WHERE a.animalIdade = :animalIdade")
    , @NamedQuery(name = "Animal.findByAnimalObs", query = "SELECT a FROM Animal a WHERE a.animalObs = :animalObs")
    , @NamedQuery(name = "Animal.findByAnimalGeral", query = "SELECT a FROM Animal a WHERE upper(a.animalNome) like upper(concat(:filtro,'%'))")})
public class Animal extends JPADao implements Serializable {

    @Lob
    @Column(name = "animal_imagem")
    private byte[] animalImagem;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "animalId")
    private Collection<Anotacao> anotacaoCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "animalId")
    private Collection<Atendimento> atendimentoCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "animalId")
    private Collection<Venda> vendaCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "animalId")
    private Collection<Agendamento> agendamentoCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "animal")
    private Collection<Vacinacao> vacinacaoCollection = new ArrayList();

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "animal_id")
    private Integer animalId;
    @Basic(optional = false)
    @Column(name = "animal_nome")
    private String animalNome;
    @Column(name = "animal_peso")
    private Integer animalPeso;
    @Column(name = "animal_cor")
    private String animalCor;
    @Column(name = "animal_idade")
    private Integer animalIdade;
    @Column(name = "animal_obs")
    private String animalObs;
    @Column(name = "animal_sexo")
    protected String animalSexo;
    @JoinColumn(name = "cli_id", referencedColumnName = "cli_id")
    @ManyToOne(optional = true)
    private Cliente cliId;
    @JoinColumn(name = "especie_id", referencedColumnName = "especie_id")
    @ManyToOne
    private Especie especieId;
    @JoinColumn(name = "pelagem_id", referencedColumnName = "pelagem_id")
    @ManyToOne
    private Pelagem pelagemId;
    @JoinColumn(name = "raca_id", referencedColumnName = "raca_id")
    @ManyToOne
    private Raca racaId;

    public Animal() {
    }

    public Animal(Integer animalId) {
        this.animalId = animalId;
    }

    public Animal(String animalNome, Integer animalPeso, String animalCor, Integer animalIdade,
            byte[] animalImagem, String animalObs, String animalSexo) {
        this.animalNome = animalNome;
        this.animalPeso = animalPeso;
        this.animalCor = animalCor;
        this.animalIdade = animalIdade;
        this.animalImagem = animalImagem;
        this.animalObs = animalObs;
        this.animalSexo = animalSexo;
    }

    public Animal(Integer animalId, String animalNome, Integer animalPeso, String animalCor,
            Integer animalIdade, byte[] animalImagem, String animalObs, String animalSexo) {
        this.animalId = animalId;
        this.animalNome = animalNome;
        this.animalPeso = animalPeso;
        this.animalCor = animalCor;
        this.animalIdade = animalIdade;
        this.animalImagem = animalImagem;
        this.animalObs = animalObs;
        this.animalSexo = animalSexo;
    }

    public Animal(Integer animalId, String animalNome) {
        this.animalId = animalId;
        this.animalNome = animalNome;
    }

    public Integer getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Integer animalId) {
        this.animalId = animalId;
    }

    public String getAnimalNome() {
        return animalNome;
    }

    public void setAnimalNome(String animalNome) {
        this.animalNome = animalNome;
    }

    public Integer getAnimalPeso() {
        return animalPeso;
    }

    public void setAnimalPeso(Integer animalPeso) {
        this.animalPeso = animalPeso;
    }

    public String getAnimalCor() {
        return animalCor;
    }

    public void setAnimalCor(String animalCor) {
        this.animalCor = animalCor;
    }

    public Integer getAnimalIdade() {
        return animalIdade;
    }

    public void setAnimalIdade(Integer animalIdade) {
        this.animalIdade = animalIdade;
    }

    public byte[] getAnimalImagem() {
        return animalImagem;
    }

    public void setAnimalImagem(byte[] animalImagem) {
        this.animalImagem = animalImagem;
    }

    public String getAnimalObs() {
        return animalObs;
    }

    public void setAnimalObs(String animalObs) {
        this.animalObs = animalObs;
    }

    public Cliente getCliId() {
        return cliId;
    }

    public void setCliId(Cliente cliId) {
        this.cliId = cliId;
    }

    public Especie getEspecieId() {
        return especieId;
    }

    public void setEspecieId(Especie especieId) {
        this.especieId = especieId;
    }

    public Pelagem getPelagemId() {
        return pelagemId;
    }

    public void setPelagemId(Pelagem pelagemId) {
        this.pelagemId = pelagemId;
    }

    public Raca getRacaId() {
        return racaId;
    }

    public void setRacaId(Raca racaId) {
        this.racaId = racaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (animalId != null ? animalId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Animal)) {
            return false;
        }
        Animal other = (Animal) object;
        if ((this.animalId == null && other.animalId != null) || (this.animalId != null && !this.animalId.equals(other.animalId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return animalNome;
    }

    @XmlTransient
    public Collection<Vacinacao> getVacinacaoCollection() {
        return vacinacaoCollection;
    }

    public void setVacinacaoCollection(Collection<Vacinacao> vacinacaoCollection) {
        this.vacinacaoCollection = vacinacaoCollection;
    }

    @XmlTransient
    public Collection<Agendamento> getAgendamentoCollection() {
        return agendamentoCollection;
    }

    public void setAgendamentoCollection(Collection<Agendamento> agendamentoCollection) {
        this.agendamentoCollection = agendamentoCollection;
    }

    @XmlTransient
    public Collection<Atendimento> getAtendimentoCollection() {
        return atendimentoCollection;
    }

    public void setAtendimentoCollection(Collection<Atendimento> atendimentoCollection) {
        this.atendimentoCollection = atendimentoCollection;
    }

    @XmlTransient
    public Collection<Venda> getVendaCollection() {
        return vendaCollection;
    }

    public void setVendaCollection(Collection<Venda> vendaCollection) {
        this.vendaCollection = vendaCollection;
    }

    /**
     * @return the animalSexo
     */
    public String getAnimalSexo() {
        return animalSexo;
    }

    /**
     * @param animalSexo the animalSexo to set
     */
    public void setAnimalSexo(String animalSexo) {
        this.animalSexo = animalSexo;
    }

    @XmlTransient
    public Collection<Anotacao> getAnotacaoCollection() {
        return anotacaoCollection;
    }

    public void setAnotacaoCollection(Collection<Anotacao> anotacaoCollection) {
        this.anotacaoCollection = anotacaoCollection;
    }
    
}
