/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Controladora;

import Controladora.Base.CtrlBase;
import Servico.Entidade.Grupodeservico;
import Servico.Entidade.Servico;
import Transacao.Transaction;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlServico extends CtrlBase {

    private static CtrlServico ctrlservico;

    public static CtrlServico create() {
        if (ctrlservico == null) {
            ctrlservico = new CtrlServico();
        }
        return ctrlservico;
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Servico) {
            return ((Servico) p).getServicoId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Servico) {
            txBusca.setText(((Servico) p).getServicoNome());
        }
    }

    public static void setCampos(Object p, JFXTextField txbNome, JFXTextField txbValor,
            JFXTextArea taDescricao, ComboBox cbGrupodeServico) {
         if (p != null && p instanceof Servico) {
             Servico sr = (Servico) p;
             txbNome.setText(sr.getServicoNome());
             txbValor.setText(sr.getServicoValor().toString());
             taDescricao.setText(sr.getServicoDescricao());
             cbGrupodeServico.getSelectionModel().select(sr.getGrupodeservicoId());
         }
    }

    public CtrlServico() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> servico = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Servico> ResultServico;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultServico = em.createNamedQuery("Servico.findAll", Servico.class)
                        .getResultList();
            } else {
                try {
                    ResultServico = em.createNamedQuery("Servico.findByServicoId", Servico.class)
                            .setParameter("servicoId", Integer.parseInt(Filtro)).getResultList();
                } catch (NumberFormatException ex) {
                    ResultServico = em.createNamedQuery("Servico.findByServicoGeral", Servico.class)
                            .setParameter("filtro", Filtro).getResultList();
                }
            }
            for (Servico servicos : ResultServico) {
                servico.add(servicos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return servico;
    }

    public Object Salvar(BigDecimal Valor, String Nome, String Descricao, Object grupodeServico) {
        Servico servico = new Servico(Valor, Nome, Descricao);
        if (grupodeServico instanceof Grupodeservico) {
            servico.setGrupodeservicoId((Grupodeservico) grupodeServico);
        }
        return super.Salvar(servico);
    }

    public Object Alterar(Object oServico, Integer Id, BigDecimal Valor, String Nome, String Descricao, Object grupodeServico) {
        Servico servico = null;
        if(oServico !=null && oServico instanceof Servico){
            servico = (Servico) oServico;
            servico.setServicoDescricao(Descricao);
            servico.setServicoNome(Nome);
            servico.setServicoValor(Valor);
            
            if (grupodeServico instanceof Grupodeservico) {
                servico.setGrupodeservicoId((Grupodeservico) grupodeServico);
            }
            
            servico = (Servico) super.Alterar(servico);
        }
        return servico;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Servico.class);
    }
    
}
