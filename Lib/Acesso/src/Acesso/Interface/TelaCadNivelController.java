/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Interface;

import Acesso.Controladora.Acesso;
import Acesso.Entidade.Componente;
import Utils.Acao.ButtonAcao;
import Utils.Acao.InterfaceFxmlAcao;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import Utils.Nodo;
import Utils.UI.Tela.IniciarTela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadNivelController implements Initializable, InterfaceFxmlAcao {

    @FXML
    private TextField txbNome;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private VBox pndados;
    @FXML
    private TextField txbIDGrupo;
    @FXML
    private TextField txbIDPermissao;
    @FXML
    private Label lbErroNomeGrupo;
    @FXML
    private TextArea taDescricao;
    @FXML
    private TableView<Object> tabelaComponente;
    @FXML
    private TableColumn<Object, Object> tcComponente;
    @FXML
    private TableColumn<Object, Object> tcAções;
    @FXML
    private JFXButton btnPesquisar;


    private ArrayList<Nodo> Componentes = new ArrayList();
    private int flag;
    private boolean EOriginal;
    private Object Instancia;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcAções.setCellValueFactory(new PropertyValueFactory("Crud"));
        tcComponente.setCellValueFactory(new PropertyValueFactory("ComponenteDescricao"));

        SetTelaAcao();
        estadoOriginal();
    }

    private void estadoOriginal() {
        EOriginal = true;
        setAllErro(false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);

        clear(pndados.getChildren());

        ArrayList<Object> oComponentes = Acesso.create().PesquisarComponente("");

        /////Adiciona os Componentes na Tabela;
        tabelaComponente.getItems().clear();
        tabelaComponente.getItems().addAll(FXCollections.observableList(oComponentes));

        /////Inicializa lista de Componentes sem Permissões;
        Componentes.clear();
        for (Object oComponente : oComponentes) {
            Componentes.add(new Nodo(oComponente, false));
        }

        clearPermissao();
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    public void clear(ObservableList<Node> pn) {
        ObservableList<Node> componentes = pn; //”limpa” os componentes
        for (Node n : componentes) {
            if (n instanceof Pane) {
                clear(((Pane) n).getChildren());
            }
            if (n instanceof TextInputControl) // textfield, textarea e htmleditor
            {
                ((TextInputControl) n).setText("");
            }
            if (n instanceof ComboBox) {
                ((ComboBox) n).getItems().clear();
            }
            if (n instanceof ImageView) {
                ((ImageView) n).setImage(null);
            }
        }
    }

    private void setAllErro(boolean EnomeGrupo) {
        lbErroNomeGrupo.setVisible(EnomeGrupo);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar,
            boolean cancelar, boolean pesquisar) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btnPesquisar.setDisable(pesquisar);
    }

    @FXML
    private void evtNovo(ActionEvent event) {
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(ActionEvent event) {
        Acesso acesso = Acesso.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            acesso.RemoverGrupo(Instancia);

        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        setAllErro(false);
        Acesso acesso = Acesso.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if (!acesso.ModificarGrupo(txbIDGrupo.getText(), txbNome.getText(),
                        taDescricao.getText(), txbIDPermissao.getText(), Nodo.getComponentes(Componentes))) {
                    Mensagem.Exibir(acesso.getMsg(), 2);
                } else {
                    Mensagem.Exibir(acesso.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if (!acesso.RegistraGrupo(txbNome.getText(), taDescricao.getText(), Nodo.getComponentes(Componentes))) {
                Mensagem.Exibir(acesso.getMsg(), 2);/////get msg
            } else {
                Mensagem.Exibir(acesso.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    private void EstadoConsulta(Object p) {
        Instancia = p;
        Acesso.setCampos(p, txbNome, taDescricao, txbIDGrupo, txbIDPermissao);
        ArrayList<Object> componentes = Acesso.getComponentes(p);

        clearPermissao();
        componentes.forEach((componente) -> {
            int Pos = BuscaComponente(componente);
            if (Pos != -1) {
                Componentes.get(Pos).setAtivo(true);
            } else {
                Mensagem.ExibirLog("Componente Não Existe");
            }
        });

        setButton(true, true, false, false, false, true);

        evtClickCampos(null);

        flag = 0;
        EOriginal = false;
    }

    private boolean validaCampos() {
        boolean fl = true;

        if (txbNome.getText().trim().isEmpty()) {
            setErro(lbErroNomeGrupo, "Campo Obrigatório Vazio");
            txbNome.setText("");
            txbNome.requestFocus();
            fl = false;
        }/*
        if (txbPermissao.getText().trim().isEmpty()){
            setErro(lbErroPermissao, "Campo Obrigatório Vazio");
            txbPermissao.setText("");
            txbPermissao.requestFocus();
            fl = false;
        }*/

        return fl;
    }

    private void evtClickCampos(Event event) {
        for (Nodo Componente : Componentes) {
            if (Componente.getObjeto() instanceof ButtonAcao) {
                ButtonAcao bt = (ButtonAcao) Componente.getObjeto();
                Object Obj = bt.findNode("ativ");
                if (Obj != null && Obj instanceof CheckBox) {
                    CheckBox oObj = (CheckBox) Obj;
                    oObj.setSelected(Componente.isAtivo());
                }
            }
        }

    }

    private void clearPermissao() {
        for (int i = 0; i < Componentes.size(); i++) {
            Componentes.get(i).setAtivo(false);
        }
    }

    private void AtivaComponente(Object Reference) {
        int Pos = BuscaComponente(Reference);
        if (Pos != -1) {
            Componentes.get(Pos).setAtivo(true);
        }
    }

    private void DesativaComponente(Object Reference) {
        int Pos = BuscaComponente(Reference);
        if (Pos != -1) {
            Componentes.get(Pos).setAtivo(false);
        }
    }

    private int BuscaComponente(Object Componente) {/*Pode ser Otimizado Retirando Comparação do Objeto*/
        int Pos = 0;
        Acesso acesso = Acesso.create();
        while (Pos < Componentes.size() && !acesso.ComparaComponente(Componentes.get(Pos).getObjeto(), Componente)) {
            Pos++;
        }
        return Pos < Componentes.size() ? Pos : -1;
    }

    @Override
    public void SelectEventAcao(Object Reference, String Action, Object element) {
        if (element != null && element instanceof CheckBox) {
            CheckBox oElemento = (CheckBox) element;
            if (Action.equalsIgnoreCase("ativ")) {
                if (oElemento.isSelected()) {
                    AtivaComponente(Reference);
                } else {
                    DesativaComponente(Reference);
                }
            }/* else if (Action.equalsIgnoreCase("visiv")) {
                
            }*/
        }
    }

    @Override
    public void SetTelaAcao() {
        Componente.setTelaAcao(this);
    }

    @FXML
    private void evtPesquisar(ActionEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Acesso/Interface/TelaConsultaNivel.fxml",
                "Consulta de Espécie", null);
        if (TelaConsultaNivelController.getNivel() != null) {
            EstadoConsulta(TelaConsultaNivelController.getNivel());
        }
    }

}
