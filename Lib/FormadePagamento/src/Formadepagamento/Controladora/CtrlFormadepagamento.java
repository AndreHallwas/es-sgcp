/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formadepagamento.Controladora;

import Controladora.Base.CtrlBase;
import FormadePagamento.Entidade.Formadepagamento;
import Formadepagamento.Interface.FormadePagamento;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlFormadepagamento extends CtrlBase {

    private static CtrlFormadepagamento ctrlformadepagamento;

    public static CtrlFormadepagamento create() {
        if (ctrlformadepagamento == null) {
            ctrlformadepagamento = new CtrlFormadepagamento();
        }
        return ctrlformadepagamento;
    }

    public CtrlFormadepagamento() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object formadepagamento, TextField txbNome, TextArea taObs,
            CheckBox cSimParcelado, CheckBox cNaoParcelado, CheckBox cSimParceladoaPrazo, CheckBox cNaoParceladoaPrazo) {
        if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
            Formadepagamento f = (Formadepagamento) formadepagamento;
            txbNome.setText(f.getFormadepagamentoNome());
            taObs.setText(f.getFormadepagamentoDesc());
            if(f.getFormadepagamentoParcelado()){
                cSimParcelado.setSelected(true);
                cNaoParcelado.setSelected(false);
            }else{
                cSimParcelado.setSelected(false);
                cNaoParcelado.setSelected(true);
            }
            if(f.getFormadepagamentoRecebimentoaprazo()){
                cSimParceladoaPrazo.setSelected(true);
                cNaoParceladoaPrazo.setSelected(false);
            }else{
                cSimParceladoaPrazo.setSelected(false);
                cNaoParceladoaPrazo.setSelected(true);
            }
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Formadepagamento) {
            return ((Formadepagamento) p).getFormadepagamentoId();
        }
        return -1;
    }

    public static Boolean getParcelado(Object p) {
        if (p != null && p instanceof Formadepagamento) {
            return ((Formadepagamento) p).getFormadepagamentoParcelado();
        }
        return null;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Formadepagamento) {
            txBusca.setText(((Formadepagamento) p).getFormadepagamentoNome());
        }
    }

    public Object getDebitonoCaixa() {
        EntityManager em = null;
        Object formadepagamento = null;
        List<Formadepagamento> ResultFormadepagamento;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ResultFormadepagamento = em.createNamedQuery("Formadepagamento.findByFormadepagamentoNome", Formadepagamento.class)
                        .setParameter("formadepagamentoNome", "Debito Automatico").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        
        if(ResultFormadepagamento != null && !ResultFormadepagamento.isEmpty()){
            formadepagamento = ResultFormadepagamento.get(0);
        }else{
            formadepagamento = Salvar("Debito Automatico", "Debito Automatico no caixa gerado"
                    + " pelo pagamento de contas.", false, false);
        }

        return formadepagamento;
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> formadepagamento = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Formadepagamento> ResultFormadepagamento;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultFormadepagamento = em.createNamedQuery("Formadepagamento.findAll", Formadepagamento.class)
                        .getResultList();
            } else {
                try {
                    ResultFormadepagamento = em.createNamedQuery("Formadepagamento.findByFormadepagamentoId", Formadepagamento.class)
                            .setParameter("formadepagamentoId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultFormadepagamento = em.createNamedQuery("Formadepagamento.findByFormadepagamentoNome", Formadepagamento.class)
                            .setParameter("formadepagamentoNome", Filtro).getResultList();
                }
            }
            for (Formadepagamento formadepagamentos : ResultFormadepagamento) {
                formadepagamento.add(formadepagamentos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return formadepagamento;
    }

    public Object Salvar(String Nome, String Descricao, boolean RecebimentoaPrazo, boolean Parcelado) {
        Formadepagamento formadepagamento = new Formadepagamento(Descricao, Nome, RecebimentoaPrazo, RecebimentoaPrazo);
        return super.Salvar(formadepagamento);
    }

    public Object Alterar(Object aDespesa, Integer Id, String Nome, String Descricao, boolean RecebimentoaPrazo, boolean Parcelado) {
        Formadepagamento formadepagamento = null;
        if (aDespesa != null && aDespesa instanceof Formadepagamento) {
            formadepagamento = (Formadepagamento) aDespesa;
            formadepagamento.setFormadepagamentoDesc(Descricao);
            formadepagamento.setFormadepagamentoNome(Nome);
            formadepagamento.setFormadepagamentoRecebimentoaprazo(RecebimentoaPrazo);
            formadepagamento.setFormadepagamentoParcelado(Parcelado);
            formadepagamento = (Formadepagamento) super.Alterar(formadepagamento);
        }
        return formadepagamento;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Formadepagamento.class);
    }

}
