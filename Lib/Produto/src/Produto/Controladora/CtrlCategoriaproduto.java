/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Controladora;

import Controladora.Base.CtrlBase;
import Produto.Entidade.Categoriaproduto;
import Produto.Entidade.CategoriaprodutoPK;
import Produto.Entidade.Tipoproduto;
import Transacao.Transaction;
import Utils.Imagem;
import com.jfoenix.controls.JFXTextField;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlCategoriaproduto extends CtrlBase {

    private static CtrlCategoriaproduto ctrlcategoriaproduto;

    public static CtrlCategoriaproduto create() {
        if (ctrlcategoriaproduto == null) {
            ctrlcategoriaproduto = new CtrlCategoriaproduto();
        }
        return ctrlcategoriaproduto;
    }

    public static void setCampos(Object p, TextField txbCategoria, TextArea taObservacoes, TextField txbTipo, 
            CheckBox cbMedicamento, CheckBox cbVacina) {
        if (p != null && p instanceof Categoriaproduto) {
            Categoriaproduto aCategoria = (Categoriaproduto) p;
            txbCategoria.setText(aCategoria.getCategoriaprodNome());
            taObservacoes.setText(aCategoria.getCategoriaprodObs());
            txbTipo.setText(aCategoria.getTipoproduto().getTipoNome());
            if(aCategoria.getCategoriaprodMedicamento() != null){
                cbMedicamento.setSelected(aCategoria.getCategoriaprodMedicamento());
            }
            if(aCategoria.getCategoriaprodVacina() != null){
                cbVacina.setSelected(aCategoria.getCategoriaprodVacina());
            }
        }
    }

    public static void setCampos(Object p, JFXTextField txbCategoria, JFXTextField txbTipo) {
        if (p != null && p instanceof Categoriaproduto) {
            Categoriaproduto aCategoria = (Categoriaproduto) p;
            txbCategoria.setText(aCategoria.getCategoriaprodNome());
            txbTipo.setText(aCategoria.getTipoproduto().getTipoNome());
        }
    }

    public CtrlCategoriaproduto() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object categoriaproduto, TextField txbNome, TextArea taObs) {
        if (categoriaproduto != null && categoriaproduto instanceof Categoriaproduto) {
            Categoriaproduto f = (Categoriaproduto) categoriaproduto;
            txbNome.setText(f.getCategoriaprodNome());
            taObs.setText(f.getCategoriaprodObs());
        }
    }

    public static Object getId(Object p) {
        if (p != null && p instanceof Categoriaproduto) {
            return ((Categoriaproduto) p).getCategoriaprodutoPK();
        }
        return null;
    }

    public static BufferedImage getImagem(Object p) {
        if (p != null && p instanceof Categoriaproduto) {
            return Imagem.ByteArrayToBufferedImage(((Categoriaproduto) p).getCategoriaprodImagem());
        }
        return null;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Categoriaproduto) {
            txBusca.setText(((Categoriaproduto) p).getCategoriaprodNome());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> categoriaproduto = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Categoriaproduto> ResultCategoriaproduto;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultCategoriaproduto = em.createNamedQuery("Categoriaproduto.findAll", Categoriaproduto.class)
                        .getResultList();
            } else {
                try {
                    ResultCategoriaproduto = em.createNamedQuery("Categoriaproduto.findByCategoriaprodId", Categoriaproduto.class)
                            .setParameter("categoriaprodId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultCategoriaproduto = em.createNamedQuery("Categoriaproduto.findByCategoriaprodNome", Categoriaproduto.class)
                            .setParameter("categoriaprodNome", Filtro).getResultList();
                }
            }
            for (Categoriaproduto categoriaprodutos : ResultCategoriaproduto) {
                categoriaproduto.add(categoriaprodutos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return categoriaproduto;
    }

    public Object Salvar(String Nome, String Obs, byte[] Imagem, Boolean Medicamento, Boolean Vacina,
            Object tipoproduto) {
        Categoriaproduto categoriaproduto = new Categoriaproduto(Nome, Obs, Imagem, Medicamento, Vacina);
        if (tipoproduto != null && tipoproduto instanceof Tipoproduto) {
            categoriaproduto.setTipoproduto((Tipoproduto) tipoproduto);
            Long IdCategoria = ((Long)getEntityManager().createNativeQuery("select nextval('categoriaproduto_id_seq')").getSingleResult());
            
            /////Set PK
            Integer IdTipo = categoriaproduto.getTipoproduto().getTipoprodutoId();
            CategoriaprodutoPK pk = new CategoriaprodutoPK(IdCategoria.intValue(),IdTipo);
            categoriaproduto.setCategoriaprodutoPK(pk);
        }
        return super.Salvar(categoriaproduto);
    }

    public Object Alterar(Object aCategoriadoProduto, Object ID, String Nome, String Obs,
            byte[] Imagem, Boolean Medicamento, Boolean Vacina, Object tipoproduto) {
        Categoriaproduto categoriaproduto = null;
        if(aCategoriadoProduto != null && aCategoriadoProduto instanceof Categoriaproduto
                && ID != null && ID instanceof CategoriaprodutoPK){
            categoriaproduto = (Categoriaproduto) aCategoriadoProduto;
            categoriaproduto.setCategoriaprodNome(Nome);
            categoriaproduto.setCategoriaprodObs(Obs);
            categoriaproduto.setCategoriaprodImagem(Imagem);
            categoriaproduto.setCategoriaprodMedicamento(Medicamento);
            categoriaproduto.setCategoriaprodVacina(Vacina);
            if (tipoproduto != null && tipoproduto instanceof Tipoproduto) {
                categoriaproduto.setTipoproduto((Tipoproduto) tipoproduto);
            }
            categoriaproduto = (Categoriaproduto) super.Alterar(categoriaproduto);
        }
        return categoriaproduto;
    }

    public boolean Remover(Object ID) {
        boolean flag = false;
        if (ID != null && ID instanceof CategoriaprodutoPK) {
            flag = super.Remover(ID) != null;
        }
        return flag;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Categoriaproduto.class);
    }
}
