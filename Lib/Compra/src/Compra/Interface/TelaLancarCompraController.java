package Compra.Interface;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Acesso.Sessao.Sessao;
import Compra.Controladora.CtrlCompra;
import ContaaPagar.Controladora.CtrlContaapagar;
import Fornecedor.Controladora.CtrlFornecedor;
import Fornecedor.Interface.TelaConsultaFornecedorController;
import Produto.Interface.TelaConsultaProdutoController;
import Utils.Acao.InterfaceFxmlAcao;
import Utils.Carrinho.Carrinho;
import Utils.Carrinho.Item;
import Utils.Carrinho.ItemCarrinho;
import Utils.Controladora.CtrlUtils;
import Utils.FormatString;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaLancarCompraController implements Initializable, Tela, InterfaceFxmlAcao {

    @FXML
    private Label lblCompra;
    @FXML
    private Label lblData;
    @FXML
    private Label lblValor;
    @FXML
    private JFXButton btnNovaCompra;
    @FXML
    private JFXButton btnAlterarCompra;
    @FXML
    private JFXButton btnExcluirCompra;
    @FXML
    private JFXTextField txbItem;
    @FXML
    private TableView<ItemCarrinho> tabela;
    @FXML
    private TableColumn<Object, String> tcCodigo;
    @FXML
    private TableColumn<Object, String> tcDescricao;
    @FXML
    private TableColumn<Object, String> tcQuantidade;
    @FXML
    private TableColumn<Object, String> tcValorUnitarito;
    @FXML
    private TableColumn<Object, String> tcSubtotal;
    @FXML
    private JFXButton btnContinuar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private VBox gpGeral;
    @FXML
    private VBox gpCompra;
    @FXML
    private HBox gpBuscarItem;
    @FXML
    private HBox gpBuscarFornecedor;
    @FXML
    private JFXTextField txbFornecedor;
    @FXML
    private JFXTextField txbStatus;
    @FXML
    private TableColumn<Object, Object> tcAcoes;
    @FXML
    private HBox gpBuscarItem1;
    @FXML
    private JFXTextField txbQuantidade;
    @FXML
    private JFXButton btnSalvar;
    
    private int flagNivelCancelar = 0;
    private int flag;
    private ItemCarrinho ItemSelecionado;
    private Carrinho oCarrinho;
    private Item Instancia;
    private int Quantidade;
    private Integer Id;
    private Object compra;
    private Object fornecedor;
    private TabelaParcelas Parcelas;
    private int flagConta;
    private boolean SalvarItemCarrinho;
    private double ValorTotaldaAlteracao;
    private double Total;
    @FXML
    private HBox gpPagamento;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SetTelaAcao();

        tcCodigo.setCellValueFactory(new PropertyValueFactory("Chave"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory("Quantidade"));
        tcValorUnitarito.setCellValueFactory(new PropertyValueFactory("PrecoUnitario"));
        tcSubtotal.setCellValueFactory(new PropertyValueFactory("SubTotal"));
        tcAcoes.setCellValueFactory(new PropertyValueFactory("Crud"));

        oCarrinho = new Carrinho();

        estadoOriginalCompra();

        MaskFieldUtil.numericField(txbQuantidade);
    }

    private void estadoOriginalCompra() {
        gpCompra.setDisable(true);
        compra = null;
        lblValor.setText("R$ 0.00");
        lblCompra.setText("Compra");
        lblData.setText(Variables.getData(new Date()));
        CtrlUtils.clear(gpGeral.getChildren());
        setButtonCompra(false, false, false);
        estadoOriginalCarrinho();
        tabela.getItems().clear();
        ValorTotaldaAlteracao = Double.MIN_VALUE;
        flagNivelCancelar = 0;
        if (oCarrinho != null) {
            oCarrinho.Limpar();
        }
        Parcelas = null;
        btnNovaCompra.setDisable(true);
        txbStatus.clear();
    }

    @FXML
    private void HandleButtonNovaCompra(MouseEvent event) {
        flag = 0;
        flagConta = 4;
        setButtonCompra(true, true, true);
        txbQuantidade.setText("1");
        estadoEdicaoCompra();
    }

    private void estadoEdicaoCompra() {
        gpCompra.setDisable(false);
        Total = -1;
        estadoOriginalCarrinho();
    }

    @FXML
    private void HandleButtonAlterarCompra(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Compra/Interface/TelaConsultaCompra.fxml",
                "Consulta de Compra", null);
        if (TelaConsultaCompraController.getCompra() != null) {
            compra = TelaConsultaCompraController.getCompra();
            /*txbCompra.setText(compra.toString());*/
            setButtonCompra(true, false, false);
            oCarrinho = CtrlCompra.create().setCarrinho(compra);
            CtrlCompra.create().setCampos(compra, lblValor, lblData);
            CtrlCompra.create().setCampos(compra, txbFornecedor);
            fornecedor = CtrlCompra.create().getFornecedor(compra);
            Id = CtrlCompra.create().getId(compra);
            estadoOriginalCarrinho();
            CarregaCarrinho();
            CarregaEstadoPagamento();

            flag = 1;
            setButtonCompra(true, true, true);
            ValorTotaldaAlteracao = oCarrinho.GeraValorTotal();
            estadoEdicaoCompra();
            if (Mensagem.ExibirConfimacao("Atenção: Ao Alterar qualquer item da compra "
                    + "será necessário gerar novamente as Contas a Pagar! Deseja prosseguir?")) {
                flag = 1;
                setButtonCompra(true, true, true);
                estadoEdicaoCompra();
            }
        }
    }

    @FXML
    private void HandleButtonExcluirCompra(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Compra/Interface/TelaConsultaCompra.fxml",
                "Consulta de Compra", null);
        if (TelaConsultaCompraController.getCompra() != null) {
            compra = TelaConsultaCompraController.getCompra();
            if (MensagensConta(false, "Excluir a Compra")) {
                if (Mensagem.ExibirConfimacao("Excluir uma Compra implica em apagar tambem todas as suas parcelas gerada,"
                        + " e reverter o estoque dos produtos ao estado anterior a compra. Deseja Realmente Excluir?")) {
                    if (CtrlCompra.create().RemoverCompra(Integer.parseInt(compra.toString()))) {
                        Mensagem.Exibir(CtrlCompra.create().getMsg(), 1);
                    } else {
                        Mensagem.Exibir(CtrlCompra.create().getMsg() + ": a Compra não foi removida com Sucesso", 1);
                    }
                }
            }
            setButtonCompra(true, true, true);
            estadoOriginalCompra();
        }
    }

    @FXML
    private void HandleButtonPesquisarItem(MouseEvent event) {
        if (MensagensConta(false, "Adicionar novos Itens")) {
            IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaProduto.fxml",
                    "Consulta do Produto", null);
            if (TelaConsultaProdutoController.getProduto() != null) {
                Instancia = (Item) TelaConsultaProdutoController.getProduto();
                txbItem.setText(Instancia.getNome());
                txbQuantidade.setDisable(false);
                btnSalvar.setDisable(false);
            }
        }
    }

    private void estadoOriginalCarrinho() {
        SalvarItemCarrinho = true;
        gpBuscarItem.setDisable(false);
        gpBuscarFornecedor.setDisable(false);
        txbQuantidade.setDisable(true);
        btnSalvar.setDisable(true);
        txbItem.setText("");
        tabela.setDisable(false);
        setButtonGeral(false, false);
        flagNivelCancelar = 1;
    }

    private void HandleButtonAlterarCarrinho(MouseEvent event) {
        AlterarCarrinho(ItemSelecionado);
    }

    private void AlterarCarrinho(ItemCarrinho oItem) {
        SalvarItemCarrinho = false;
        ItemSelecionado = oItem;
        Instancia = oItem.getoItem();
        Quantidade = oItem.getQuantidade();
        txbItem.setText(Instancia.getNome());
        txbQuantidade.setText(Quantidade + "");
        txbQuantidade.setDisable(false);
        txbQuantidade.requestFocus();
        btnSalvar.setDisable(false);
    }

    private void HandleButtonExcluirCarrinho(MouseEvent event) {
        oCarrinho.remove(ItemSelecionado);
        CarregaCarrinho();
        estadoOriginalCarrinho();
        gpBuscarItem.setDisable(false);
    }

    private void ExcluirCarrinho(ItemCarrinho oItem) {
        oCarrinho.remove(oItem);
        CarregaCarrinho();
        estadoOriginalCarrinho();
        gpBuscarItem.setDisable(false);
    }

    private void clicknatabela(MouseEvent event) {
        estadoOriginalCarrinho();
    }

    @FXML
    private void HandleButtonContinuar(MouseEvent event) {
        CtrlCompra cp = CtrlCompra.create();
        CtrlContaapagar cn = CtrlContaapagar.create();

        if (validaCampos()) {
            if (flag == 1)//alterar
            {
                if (Mensagem.ExibirConfimacao("Modificar uma Compra implica em apagar todas as suas parcelas geradas,"
                        + " e modificar o estoque dos produtos referenciados. Deseja Realmente Modificar?")) {
                    if ((compra = cp.ModificarCompraNovo(compra, new Date(), false, new BigDecimal(oCarrinho.GeraValorTotal()),
                            Parcelas.getQtdParcelas(), "", fornecedor, Sessao.getIndividuo(), Parcelas.getFormadePagamento(),
                            0, 0, 0, "", oCarrinho.getAllItems(), oCarrinho.getAllQuantidade(), Parcelas.getValores(),
                            Parcelas.getDatas())) == null) {
                        Mensagem.Exibir(cp.getMsg(), 2);
                    } else {
                        Mensagem.Exibir(cp.getMsg(), 1);
                        estadoOriginalCompra();
                    }
                }
            } else//cadastrar
            if ((compra = cp.RegistrarCompraNovo(new BigDecimal(oCarrinho.GeraValorTotal()), Parcelas.getQtdParcelas(),
                    "", fornecedor, Sessao.getIndividuo(), Parcelas.getFormadePagamento(), 0, 0, 0, "", oCarrinho.getAllItems(),
                    oCarrinho.getAllQuantidade(), Parcelas.getValores(), Parcelas.getDatas())) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginalCompra();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void HandleButtonCancelar(MouseEvent event) {
        if (CtrlUtils.CloseChildren(Variables._pndados,flagNivelCancelar == 0)) {
            estadoOriginalCompra();
        } else if (flagNivelCancelar == 1) {
            estadoOriginalCarrinho();
            flagNivelCancelar = 0;
        } else {
            estadoOriginalCompra();
        }
    }

    private void setButtonCompra(boolean Bnova, boolean Balterar, boolean Bexcluir) {
        btnAlterarCompra.setDisable(Balterar);
        btnExcluirCompra.setDisable(Bexcluir);
        btnNovaCompra.setDisable(Bnova);
    }

    private void setButtonGeral(boolean Bcontinuar, boolean Bcancelar) {
        btnContinuar.setDisable(Bcontinuar);
        btnCancelar.setDisable(Bcancelar);
    }

    @FXML
    private void clicknatabelaCarrinho(MouseEvent event) {
        if (MensagensConta(false, "Alterar o Carrinho")) {
            int lin = tabela.getSelectionModel().getSelectedIndex();
            if (lin > -1) {
                ItemCarrinho f = (ItemCarrinho) tabela.getItems().get(lin);
                estadoOriginalCarrinho();
                ItemSelecionado = f;
            }
        }
    }

    public void CarregaCarrinho() {
        try {
            tabela.getItems().clear();
            tabela.setItems(FXCollections.observableList(oCarrinho.getItems()));
            Double valor = oCarrinho.GeraValorTotal();
            lblValor.setText(FormatString.MonetaryPersistent(valor));
            Total = valor.doubleValue();
            if (flag == 1) {
                if (valor.compareTo(ValorTotaldaAlteracao) != 0) {
                    txbStatus.setText("Gerar Conta");
                } else if (txbStatus.getText().equalsIgnoreCase("Gerar Conta")) {
                    CarregaEstadoPagamento();
                }
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tabela");
        }
    }

    private boolean validaCampos() {
        return oCarrinho != null && !oCarrinho.isVazio()
                && !txbFornecedor.getText().trim().isEmpty()
                && txbStatus.getText().equalsIgnoreCase("Conta Gerada")
                && Parcelas != null;
    }

    @FXML
    private void HandleButtonPesquisarFornecedor(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Fornecedor/Interface/TelaConsultaFornecedor.fxml",
                "Consulta de Fornecedor", null);
        if (TelaConsultaFornecedorController.getFornecedor() != null) {
            fornecedor = TelaConsultaFornecedorController.getFornecedor();
            CtrlFornecedor.setCampoBusca(fornecedor, txbFornecedor);
            btnNovaCompra.setDisable(false);
        }
    }

    @FXML
    private void HandleButtonGerarContasaPagar(MouseEvent event) {
        if (oCarrinho == null || oCarrinho.isVazio()) {
            Mensagem.Exibir("O Carrinho está Vazio, portanto não é possivel gerar uma Conta a Pagar!", 2);
        } else if (MensagensConta(true, "Gerar Contas a Pagar")) {
            if (Parcelas != null) {
                if (Parcelas.getValorTotal() == Total) {
                    TelaQuitarContasAPagarController.setParcelas(Parcelas);
                } else {
                    TelaQuitarContasAPagarController.setParcelas(null);
                    TelaQuitarContasAPagarController.setValorTotal(Total);
                }
            } else {
                TelaQuitarContasAPagarController.setParcelas(null);
                if (compra != null && !txbStatus.getText().equalsIgnoreCase("Gerar Conta")) {
                    TelaQuitarContasAPagarController.setCompra(compra);
                } else {
                    TelaQuitarContasAPagarController.setValorTotal(Total);
                }
            }
            IniciarTela.loadWindow(this.getClass(), "/Compra/Interface/TelaQuitarContasAPagar.fxml",
                    "Lançar Conta a Pagar", null);
            Parcelas = TelaQuitarContasAPagarController.getParcelas();
            if (Parcelas != null) {
                txbStatus.setText("Conta Gerada");
            } else {
                txbStatus.setText("Conta Não Gerada");
            }
        }
    }

    public boolean MensagensConta(boolean Value, String Adc) {
        boolean flagC = true;
        if (flagConta == 1) {
            Mensagem.Exibir("Não é possivel " + Adc
                    + " pois esta conta ja está Paga!", 2);
            flagC = false;
        } else if (flagConta == 2) {
            Mensagem.Exibir("Não é possivel " + Adc
                    + " pois esta conta ja possui parcelas Pagas!", 2);
            flagC = false;
        } else if (flagConta == 3 && Value) {
            Mensagem.Exibir("Esta Conta ja possui Parcelas Geradas,"
                    + " ao prosseguir as Poderá editar elas.", 2);
        } else if (flagConta == 5) {
            Mensagem.Exibir("A Compra é Inválida", 2);
            flagC = false;
        }
        return flagC;
    }

    private void CarregaEstadoPagamento() {
        if (compra != null) {
            String Resultado = CtrlCompra.create().VerificaPagamento(compra);
            txbStatus.setText(Resultado);
            if (Resultado.equalsIgnoreCase("Compra Paga")) {
                flagConta = 1;
            } else if (Resultado.equalsIgnoreCase("Parcela Paga")) {
                flagConta = 2;
            } else if (Resultado.equalsIgnoreCase("Não Pago")) {
                flagConta = 3;
            } else if (Resultado.equalsIgnoreCase("Não Gerado")) {
                flagConta = 4;
            } else {
                flagConta = 5;
            }
        }
    }

    @Override
    public void SelectEventAcao(Object Reference, String Action, Object element) {
        if (Action.equalsIgnoreCase("mod")) {
            AlterarCarrinho((ItemCarrinho) Reference);
        } else if (Action.equalsIgnoreCase("rem")) {
            ExcluirCarrinho((ItemCarrinho) Reference);
        }
        System.out.println(Action + ": " + Reference);
    }

    @Override
    public void SetTelaAcao() {
        ItemCarrinho.setTelaAcao(this);
    }

    @FXML
    private void HandleButtonSalvar(MouseEvent event) {
        if (Instancia != null && getQuantidade()) {
            if (SalvarItemCarrinho) {
                if (oCarrinho.find(Instancia) != -1) {
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Selecione");
                    alert.setContentText("O Item a ser modificado já existe no carrinho,"
                            + " deseja acrescentar a Quantidade selecionada ao item?");

                    ButtonType Acrescentar = new ButtonType("Acrescentar");
                    ButtonType Cancelar = new ButtonType("Cancelar");

                    alert.getButtonTypes().clear();

                    alert.getButtonTypes().addAll(Acrescentar, Cancelar);
                    Optional<ButtonType> option = alert.showAndWait();

                    if (option.get() != null && option.get() == Acrescentar) {
                        oCarrinho.add(Instancia, Quantidade);
                    }
                } else {
                    oCarrinho.add(Instancia, Quantidade);
                }
            } else {
                if (ItemSelecionado != null && oCarrinho.find(Instancia) != -1) {

                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Selecione");
                    alert.setContentText("O Item a ser modificado já existe no carrinho,"
                            + " deseja acrescentar a Quantidade selecionada ao item,"
                            + " Substituir a Quantidade atual pela nova, ou cancelar a modificação?");

                    ButtonType Acrescentar = new ButtonType("Acrescentar");
                    ButtonType Substituir = new ButtonType("Substituir");
                    ButtonType Cancelar = new ButtonType("Cancelar");

                    alert.getButtonTypes().clear();

                    alert.getButtonTypes().addAll(Acrescentar, Substituir, Cancelar);
                    Optional<ButtonType> option = alert.showAndWait();

                    if (option.get() != null && option.get() != Cancelar) {

                        if (option.get() == Acrescentar) {
                            oCarrinho.changeAdd(ItemSelecionado, Instancia, Quantidade);
                        } else if (option.get() == Substituir) {
                            oCarrinho.change(ItemSelecionado, Instancia, Quantidade);
                        }
                    }
                } else {
                    oCarrinho.change(ItemSelecionado, Instancia, Quantidade);
                }

            }
            Instancia = null;
            Quantidade = 0;
            CarregaCarrinho();
            estadoOriginalCarrinho();
            gpBuscarItem.setDisable(false);
        }
    }

    private boolean getQuantidade() {
        if (txbQuantidade.getText().trim().isEmpty()) {
            Mensagem.Exibir("Informe a Quantidade", 2);
            txbQuantidade.requestFocus();
        } else if (Integer.parseInt(txbQuantidade.getText()) < 1) {
            Mensagem.Exibir("Informe uma Quantidade Acima de 1", 2);
            txbQuantidade.requestFocus();
        } else {
            Quantidade = Integer.parseInt(txbQuantidade.getText());
            return true;
        }
        return false;
    }

}
