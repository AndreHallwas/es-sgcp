/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Interface;

import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaServicoController implements Initializable {

    @FXML
    private VBox TabServico;
    @FXML
    private VBox TabGrupodeServico;
    
    private Tela TelaServico;
    private Tela TelaGrupodeServico;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TelaServico = IniciarTela.Create(this.getClass(), TabServico,
                "/Servico/Interface/TelaCadServico.fxml");
        TelaGrupodeServico = IniciarTela.Create(this.getClass(), TabGrupodeServico,
                "/Servico/Interface/TelaCadGrupodeServico.fxml");
    }    
    
}
