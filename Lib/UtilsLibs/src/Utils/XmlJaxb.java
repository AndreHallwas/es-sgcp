/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import Utils.Mensagem;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import static java.nio.charset.StandardCharsets.UTF_8;
import java.util.logging.Logger;
import static java.util.logging.Logger.getLogger;
import javax.xml.bind.JAXBContext;
import static javax.xml.bind.JAXBContext.newInstance;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Raizen
 */
public class XmlJaxb {

    private static final Logger LOG = getLogger(XmlJaxb.class.getName());

    /**
     *
     * @param Xml String Xml or File contents Xml example New File("/Xml") or String Xml;
     * @param obj Class to Transform Xml;
     * @return null in Erro, Object Contents the data of archive;
     */
    public static Object loadPersonDataFromFile(Object Xml, Class obj) {
        try {
            JAXBContext context = newInstance(obj);
            Unmarshaller um = context.createUnmarshaller();
            Object wrapper = null;
            if (Xml != null) {
                if (Xml instanceof String) {
                    wrapper = obj.cast(um.unmarshal(new ByteArrayInputStream(((String) Xml).getBytes(UTF_8.name()))));
                } else {
                    wrapper = obj.cast(um.unmarshal((File) Xml));
                }
            }
            return wrapper;

        } catch (UnsupportedEncodingException | JAXBException e) {
            Mensagem.ExibirException(e, "Erro ao Carregar XML");
        }
        return null;
    }

    /**
     *
     * @param Xml Object to Write in Xml Archive;
     * @param file File of Xml example new File("/Xml");
     */
    public static void savePersonDataToFile(Object Xml, File file) {
        try {
            JAXBContext context = newInstance(Xml.getClass());
            Marshaller m = context.createMarshaller();
            m.setProperty(JAXB_FORMATTED_OUTPUT, true);
            m.marshal(Xml, file);
        } catch (JAXBException e) {
            Mensagem.ExibirException(e, "Erro ao Salvar XML");
        }

    }

    private XmlJaxb() {
    }

}

/*
    * public static void main(String[] args) {
        personData.add(new Person("Hans", "Muster"));
        personData.add(new Person("Ruth", "Mueller"));
        personData.add(new Person("Heinz", "Kurz"));
        personData.add(new Person("Cornelia", "Meier"));
        personData.add(new Person("Werner", "Meyer"));
        personData.add(new Person("Lydia", "Kunz"));
        personData.add(new Person("Anna", "Best"));
        personData.add(new Person("Stefan", "Meier"));
        personData.add(new Person("Martin", "Mueller"));
        savePersonDataToFile(new File("d:\\pessoas.xml"));
    }
 *//*
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getBirthday() { return birthday;}
    class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

        @Override
        public LocalDate unmarshal(String v) throws Exception {
            return LocalDate.parse(v);
        }

        @Override
        public String marshal(LocalDate v) throws Exception {
            return v.toString();
        }
    }
 */
