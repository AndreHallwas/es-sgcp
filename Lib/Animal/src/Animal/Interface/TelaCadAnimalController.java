/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Interface;

import Animal.Controladora.CtrlAnimal;
import Animal.Controladora.CtrlEspecie;
import Animal.Controladora.CtrlPelagem;
import Animal.Controladora.CtrlRaca;
import Pessoa.Controladora.CtrlCliente;
import Pessoa.Interface.TelaConsultaClienteController;
import Utils.Controladora.CtrlUtils;
import Variables.Variables;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Imagem.TelaImagemFXMLController;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadAnimalController implements Initializable, Tela {

    @FXML
    private VBox pndados;
    @FXML
    private JFXTextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private JFXTextField txbIdade;
    @FXML
    private Label lbErroIdade;
    @FXML
    private JFXTextField txbPeso;
    @FXML
    private Label lbErroPeso;
    @FXML
    private JFXTextField txbCor;
    @FXML
    private Label lbErroCor;
    @FXML
    private JFXTextArea taObs;
    @FXML
    private Label lbErrobs;
    @FXML
    private JFXTextField txbEspecie;
    @FXML
    private Label lbErroEspecie;
    @FXML
    private JFXTextField txbRaca;
    @FXML
    private Label lbErroRaca;
    @FXML
    private JFXTextField txbPelagem;
    @FXML
    private Label lbErroPelagem;
    @FXML
    private JFXButton btnovo;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btapagar;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    @FXML
    private JFXTextField txbCliente;
    @FXML
    private Label lbErroCliente;
    @FXML
    private VBox gpImagem;
    @FXML
    private JFXButton btConsulta;
    @FXML
    private JFXTextField lbID;
    @FXML
    private JFXComboBox<String> cbSexo;

    private int flag;
    private Integer ID;
    private boolean EOriginal;
    private TelaImagemFXMLController Imagem;
    private Object pelagem;
    private Object raca;
    private Object especie;
    private Object cliente;
    private Object Instancia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Imagem = TelaImagemFXMLController.Create(this.getClass(), gpImagem);

        MaskFieldUtil.numericField(txbIdade);
        MaskFieldUtil.numericField(txbPeso);

        estadoOriginal();
    }

    private void setAllErro(boolean Ecliente, boolean Ecor, boolean Eespecie, boolean Eidade,
            boolean Enome, boolean Epelagem, boolean Epeso, boolean Eraca, boolean Eobs) {
        lbErroCliente.setVisible(Ecliente);
        lbErroCor.setVisible(Ecor);
        lbErroEspecie.setVisible(Eespecie);
        lbErroIdade.setVisible(Eidade);
        lbErroNome.setVisible(Enome);
        lbErroPelagem.setVisible(Epelagem);
        lbErroPeso.setVisible(Epeso);
        lbErroRaca.setVisible(Eraca);
        lbErrobs.setVisible(Eobs);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        if (txbNome.getText().trim().isEmpty()) {
            setErro(lbErroNome, "Campo Obrigatório Vazio");
            fl = false;
            txbNome.setText("");
            txbNome.requestFocus();
        }
        
        /**
         * Nome do animal pode se repetir.
         */
        /*CtrlAnimal cp = CtrlAnimal.create();
        ArrayList<Object> Pesquisa = cp.Pesquisar(txbNome.getText());
        if (txbNome.getText().trim().isEmpty() || !Pesquisa.isEmpty() || Pesquisa.isEmpty()) {
            if (txbNome.getText().trim().isEmpty()) {
                setErro(lbErroNome, "Campo Obrigatório Vazio");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            }else if (!Pesquisa.isEmpty() && flag == 1) {
                setErro(lbErroNome, "Nome Ja Existe");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            }
        }*/
        
        /**
         * Foi solicitada a remoção da validação.
         */
        /*if (txbCliente.getText().trim().isEmpty()) {
            setErro(lbErroCliente, "Campo Obrigatório Vazio");
            txbCliente.setText("");
            txbCliente.requestFocus();
            fl = false;
        }*/
        if (txbCor.getText().trim().isEmpty()) {
            /**
             * A Cor foi removida da interface por pedido. Devido a necessidade
             * dela em futuras modificações, Ela foi mantida no projeto do
             * software.
             */
            txbCor.setText("Indefinido");
        }
        return fl;
    }

    @FXML
    private void evtNovo(Event event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(Event event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(Event event) {
        CtrlAnimal cp = CtrlAnimal.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(ID);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(Event event) {
        setAllErro(false, false, false, false, false, false, false, false, false);
        CtrlAnimal cp = CtrlAnimal.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, ID, txbNome.getText(), txbPeso.getText().trim().isEmpty() ? null : Integer.parseInt(txbPeso.getText()),
                        txbCor.getText(), txbIdade.getText().trim().isEmpty() ? null : Integer.parseInt(txbIdade.getText()), taObs.getText(), cbSexo.getSelectionModel().getSelectedItem(),
                        Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()), pelagem, raca, especie, cliente)) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(txbNome.getText(), txbPeso.getText().trim().isEmpty() ? null : Integer.parseInt(txbPeso.getText()),
                    txbCor.getText(), txbIdade.getText().trim().isEmpty() ? null : Integer.parseInt(txbIdade.getText()), taObs.getText(), cbSexo.getSelectionModel().getSelectedItem(),
                    Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()), pelagem, raca, especie, cliente)) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(Event event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false, false, false, false, false, false, false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
        setComboBoxDefaut();
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false, false, false, false, false, false, false, false);
        setButton(false, true, true, true, false, false);

        pndados.setDisable(true);
        Imagem.estadoOriginal();

        clear(pndados.getChildren());

        /////ComboBox
        setComboBoxDefaut();
    }

    private void setComboBoxDefaut() {
        cbSexo.getSelectionModel().clearSelection();
        cbSexo.getItems().clear();
        cbSexo.getItems().addAll("Macho", "Femea", "Macho Castrado", "Femea Castrada");
        cbSexo.getSelectionModel().selectFirst();
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlAnimal.setCampos(p, txbNome, taObs, txbCliente, txbCor,
                    txbEspecie, txbIdade, txbPelagem, txbPeso, txbRaca);
            setComboBoxDefaut();
            cbSexo.getSelectionModel().select(CtrlAnimal.getSexo(p) + "");
            ID = CtrlAnimal.getId(p);
            lbID.setText(ID.toString());
            Imagem.setImagem(CtrlAnimal.getImagem(p));
        }
    }

    @FXML
    private void evtConsultaAvancada(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Animal/Interface/TelaConsultaAnimal.fxml",
                "Consulta de Animal", null);
        if (TelaConsultaAnimalController.getAnimal() != null) {
            EstadoConsulta(TelaConsultaAnimalController.getAnimal());
        }
    }

    @FXML
    private void evtConsultaCliente(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Pessoa/Interface/TelaConsultaCliente.fxml",
                "Consulta de Cliente", null);
        cliente = TelaConsultaClienteController.getCliente();
        setConsulta(cliente, txbCliente);
        CtrlCliente.setCampoBusca(TelaConsultaClienteController.getCliente(), txbCliente);
    }

    private void setConsulta(Object ob, TextField txb) {
        if (ob != null) {
            txb.setText(ob.toString());
        }
    }

    @FXML
    private void evtConsultaEspecie(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Animal/Interface/TelaConsultaEspecie.fxml",
                "Consulta de Espécie", null);
        especie = TelaConsultaEspecieController.getEspecie();
        setConsulta(especie, txbEspecie);
        CtrlEspecie.setCampoBusca(TelaConsultaEspecieController.getEspecie(), txbEspecie);
    }

    @FXML
    private void evtConsultaRaca(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Animal/Interface/TelaConsultaRaca.fxml",
                "Consulta de Serviço", null);
        raca = TelaConsultaRacaController.getRaca();
        setConsulta(raca, txbRaca);
        CtrlRaca.setCampoBusca(TelaConsultaRacaController.getRaca(), txbRaca);
    }

    @FXML
    private void evtConsultaPelagem(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Animal/Interface/TelaConsultaPelagem.fxml",
                "Consulta de Pelagem", null);
        pelagem = TelaConsultaPelagemController.getPelagem();
        setConsulta(pelagem, txbPelagem);
        CtrlPelagem.setCampoBusca(TelaConsultaPelagemController.getPelagem(), txbPelagem);
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }

    public void relatorio() {
        /*String sql = "select * from (select * from usuario where usr_login = '" + (String) (txbLogin.getText() != null ? txbLogin.getText() : txBusca.getText()) + "' ) as usuario inner join endereco on usuario.end_cep = endereco.end_cep inner join bairro on endereco.bai_cod = bairro.bai_cod inner join cidade on bairro.cid_cod = cidade.cid_cod inner join grupodeacesso on usuario.grupo_id = grupodeacesso.grupo_id";
        Relatorio.gerarRelatorio(sql, "Relatorios/MyReports/Rel_FichaUsuario.jasper", "Ficha do Usuario");*/
    }

}
