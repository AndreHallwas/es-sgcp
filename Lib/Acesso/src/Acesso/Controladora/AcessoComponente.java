package Acesso.Controladora;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Acesso.Entidade.Componente;
import Acesso.Entidade.Individuo;
import Acesso.Entidade.Permissaocomponente;
import Utils.Mensagem;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Raizen
 */
public abstract class AcessoComponente {

    private static ArrayList<Componente> Componentes = new ArrayList();
    private static HabilitaComponente HComponente;
    private static AcessoComponente cAcesso;

    /**
     * @param HComponente [0] is a instanceof HabilitaComponente;
     * @return
     */
    public static AcessoComponente create(HabilitaComponente... HComponente) {
        if (getcAcesso() == null) {
            if (HComponente != null && HComponente.length > 0) {
                setcAcesso(new AcessoComponente(HComponente[0]) {
                });
            } else {
                setcAcesso(null);
            }
        } else if (HComponente != null && HComponente.length > 0) {
            AcessoComponente.setHComponente(HComponente[0]);
        }
        return getcAcesso();
    }

    private AcessoComponente(HabilitaComponente HComponente) {
        setHComponente(HComponente);
    }

    public void GeraPermissao(Individuo individuo) {
        int Pos;
        Collection<Permissaocomponente> iComponentes = 
                individuo.getGrupo().getPermacessoId().getPermissaocomponenteCollection();
        getHComponente().Default(getComponentes());
        for (Permissaocomponente iComponente : iComponentes) {
            Pos = BuscaComponente(iComponente);
            if (Pos != -1) {
                getHComponente().Habilita(getComponentes().get(Pos));
            }
        }
    }

    public void Registra(Componente componente) {
        componente.Constroi();
        getComponentes().add(componente);
    }

    protected static int BuscaComponente(Permissaocomponente iComponente) {
        int Pos = 0;
        while (Pos < getComponentes().size()
                && !Componentes.get(Pos).getComponenteId().equalsIgnoreCase(iComponente.getComponenteId().getComponenteId())) {
            Pos++;
        }
        return Pos < getComponentes().size() ? Pos : -1;
    }

    /**
     * @return the Componentes
     */
    public static ArrayList<Componente> getComponentes() {
        return Componentes;
    }

    /**
     * @param aComponentes the Componentes to set
     */
    public static void setComponentes(ArrayList<Componente> aComponentes) {
        if (aComponentes == null) {
            Componentes = new ArrayList();
        } else {
            Componentes = aComponentes;
        }
    }

    /**
     * @return the HComponente
     */
    public static HabilitaComponente getHComponente() {
        return HComponente;
    }

    /**
     * @param aHComponente the HComponente to set
     */
    public static void setHComponente(HabilitaComponente aHComponente) {
        HComponente = aHComponente;
    }

    /**
     * @return the cAcesso
     */
    public static AcessoComponente getcAcesso() {
        return cAcesso;
    }

    /**
     * @param acAcesso the cAcesso to set
     */
    public static void setcAcesso(AcessoComponente acAcesso) {
        if (acAcesso == null) {
            setcAcesso(new AcessoComponente(new HabilitaComponente() {
                @Override
                public void Habilita(Componente componente) {
                    System.out.println("Componente Habilitado && Acesso ao Componente não Sobrescrito !!!!!  ");
                }

                @Override
                public void Default(ArrayList<Componente> Componentes) {
                    System.out.println("Default && Acesso ao Componente não Sobrescrito !!!!!  ");
                }
            }) {
            });
        } else {
            cAcesso = acAcesso;
        }
    }

    public void AdicionaObjeto(Object Objeto, String NomeRegistro) {
        int Pos = BuscaRegistro(NomeRegistro);
        if (Pos != -1) {
            Componentes.get(Pos).addObjeto(Objeto);
        } else {
            Mensagem.ExibirLog("Objeto Não Existe");
        }
    }

    private int BuscaRegistro(String NomeRegistro) {
        int i = 0;
        while (i < Componentes.size() && !Componentes.get(i).Compara(NomeRegistro)) {
            i++;
        }
        return i < Componentes.size() ? i : -1;
    }
    
    public void Default(){
        getHComponente().Default(getComponentes());
    }
}
