/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Produto.Controladora.CtrlDosagemproduto;
import Utils.Controladora.CtrlUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadDosagemprodutoController implements Initializable, Tela {

    @FXML
    private HBox pndados;
    @FXML
    private JFXTextArea taObs;
    @FXML
    private Label lbErroObs;
    @FXML
    private JFXButton btnovo;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btapagar;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    @FXML
    private JFXButton btConsulta;
    @FXML
    private JFXTextField txbIntervalo;
    @FXML
    private JFXTextField txbQuantidade;
    @FXML
    private Label lbErroQuantidade;
    @FXML
    private JFXTextField txbId;
    
    private int flag;
    private Integer ID;
    private boolean EOriginal;
    private Object Instancia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MaskFieldUtil.numericField(txbIntervalo);
        MaskFieldUtil.numericField(txbQuantidade);
        MaskFieldUtil.numericField(txbId);
        estadoOriginal();
    }

    private void setAllErro(boolean Edescricao, boolean Equantidade) {
        lbErroObs.setVisible(Edescricao);
        lbErroQuantidade.setVisible(Equantidade);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlDosagemproduto cp = CtrlDosagemproduto.create();
        if (txbQuantidade.getText().trim().isEmpty()) {
            setErro(lbErroQuantidade, "Campo Obrigatório Vazio");
            fl = false;
            txbQuantidade.setText("");
            txbQuantidade.requestFocus();
        }
        return fl;
    }

    @FXML
    private void evtNovo(ActionEvent event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(ActionEvent event) {
        CtrlDosagemproduto cp = CtrlDosagemproduto.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(ID);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        setAllErro(false, false);
        CtrlDosagemproduto cp = CtrlDosagemproduto.create();
        if (validaCampos()) {
            BigDecimal Quantidade = null;
            if (!txbQuantidade.getText().trim().isEmpty()) {
                Quantidade = new BigDecimal(txbQuantidade.getText());
            }
            BigDecimal Intervalo = null;
            if (!txbIntervalo.getText().trim().isEmpty()) {
                Intervalo = new BigDecimal(txbIntervalo.getText());
            }
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, ID, taObs.getText(), Quantidade, Intervalo)) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(taObs.getText(), Quantidade, Intervalo)) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
        txbIntervalo.setText("1");
        txbId.setDisable(true);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);
        txbId.setDisable(false);

        clear(pndados.getChildren());
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlDosagemproduto.setCampos(p, taObs, txbIntervalo, txbQuantidade, txbId);
            ID = CtrlDosagemproduto.getId(p);
        }
    }

    @FXML
    private void evtConsultaAvancada(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaDosagemproduto.fxml",
                "Consulta de Dosagem de Produto", null);
        if (TelaConsultaDosagemprodutoController.getDosagemproduto() != null) {
            EstadoConsulta(TelaConsultaDosagemprodutoController.getDosagemproduto());
        }
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }
}
