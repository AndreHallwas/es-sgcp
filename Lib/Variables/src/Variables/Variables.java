/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Variables;

import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Raizen
 */
public class Variables {

    public static Label _txUsuarioInterface;
    public static Label _txNivelInterface;
    public static Object _imgLogoUsuario;
    public static Stage _Stage;
    public static Scene _Scene;
    public static Pane _pndados;
    public static Object _Acc;
    public static SimpleDateFormat BR_Format_Data = new SimpleDateFormat("dd/MM/yyyy");
    public static SimpleDateFormat BR_Format_Data_Simple = new SimpleDateFormat("dd_MM_yyy-HH_mm_ss");
    
    public static Label _lblStatusCaixa;
    public static Label _lblCaixa;

    public static Object setStatusCaixa(boolean Status, String... Params) {
        if (_lblCaixa != null && _lblStatusCaixa != null) {
            _lblCaixa.setVisible(true);
            if (Status) {
                _lblStatusCaixa.setText("Aberto");
                _lblStatusCaixa.setTextFill(Color.GREEN);
            } else {
                _lblStatusCaixa.setText("Fechado");
                _lblStatusCaixa.setTextFill(Color.RED);
            }
        }
        return true;
    }

    public static String getData(Date data) {
        return data != null ? BR_Format_Data.format(data) : "Sem Data";
    }

    public static String getDataSimple(Date data) {
        return data != null ? BR_Format_Data_Simple.format(data) : "Sem Data";
    }

    public static String NotFoundDefaultMessage(Object obj) {
        return obj == null ? "Não Informado" : obj.toString();
    }
}
