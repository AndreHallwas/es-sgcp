/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "permissaocomponente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Permissaocomponente.findAll", query = "SELECT p FROM Permissaocomponente p")
    , @NamedQuery(name = "Permissaocomponente.findByPermcompId", query = "SELECT p FROM Permissaocomponente p WHERE p.permcompId = :permcompId")})
public class Permissaocomponente extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id/*
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="permissaocomponente_permcomp_id_seq")*/
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "permcomp_id")
    private Integer permcompId;
    @JoinColumn(name = "componente_id", referencedColumnName = "componente_id")
    @ManyToOne(optional = false)
    private Componente componenteId;
    @JoinColumn(name = "permacesso_id", referencedColumnName = "permacesso_id")
    @ManyToOne(optional = false)
    private Permissaodeacesso permacessoId;

    public Permissaocomponente() {
    }

    public Permissaocomponente(Integer permcompId) {
        this.permcompId = permcompId;
    }

    public Permissaocomponente(Integer permcompId, Componente componenteId) {
        this.permcompId = permcompId;
        this.componenteId = componenteId;
    }

    public Integer getPermcompId() {
        return permcompId;
    }

    public void setPermcompId(Integer permcompId) {
        this.permcompId = permcompId;
    }

    public Componente getComponenteId() {
        return componenteId;
    }

    public void setComponenteId(Componente componenteId) {
        this.componenteId = componenteId;
    }

    public Permissaodeacesso getPermacessoId() {
        return permacessoId;
    }

    public void setPermacessoId(Permissaodeacesso permacessoId) {
        this.permacessoId = permacessoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permcompId != null ? permcompId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permissaocomponente)) {
            return false;
        }
        Permissaocomponente other = (Permissaocomponente) object;
        if ((this.permcompId == null && other.permcompId != null) || (this.permcompId != null && !this.permcompId.equals(other.permcompId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Acesso.Permissaocomponente[ permcompId=" + permcompId + " ]";
    }
    
}
