/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacinacao.Entidade;

import Animal.Entidade.Animal;
import Venda.Entidade.Venda;
import Produto.Entidade.Produto;
import Utils.Acao.ButtonAcao;
import Utils.Acao.ButtonAcaoVacinacao;
import Utils.Carrinho.Item;
import Variables.Variables;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "vacinacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vacinacao.findAll", query = "SELECT v FROM Vacinacao v")
    , @NamedQuery(name = "Vacinacao.findByVacinacaoId", query = "SELECT v FROM Vacinacao v WHERE v.vacinacaoPK.vacinacaoId = :vacinacaoId")
    , @NamedQuery(name = "Vacinacao.findByVacinacaoData", query = "SELECT v FROM Vacinacao v WHERE v.vacinacaoPK.vacinacaoData = :vacinacaoData")
    , @NamedQuery(name = "Vacinacao.findByAnimalId", query = "SELECT v FROM Vacinacao v WHERE v.vacinacaoPK.animalId = :animalId")
    , @NamedQuery(name = "Vacinacao.findByAnimalIdVacinacao", query = "SELECT v FROM Vacinacao v WHERE "
            + "v.animal.cliId.cliId = :cliId and v.vacinacaoPK.vacinacaoId = :filtro "
            + "and v.vendaId is null and v.vacinacaoDataAplicacao is not null")
    , @NamedQuery(name = "Vacinacao.findByAnimalAllVacinacao", query = "SELECT v FROM Vacinacao v WHERE v.animal.cliId.cliId = :cliId and v.vendaId is null and v.vacinacaoDataAplicacao is not null")
    , @NamedQuery(name = "Vacinacao.findByProdutoId", query = "SELECT v FROM Vacinacao v WHERE v.vacinacaoPK.produtoId = :produtoId")
    , @NamedQuery(name = "Vacinacao.findByVacinacaoObs", query = "SELECT v FROM Vacinacao v WHERE v.vacinacaoObs = :vacinacaoObs")
    , @NamedQuery(name = "Vacinacao.findByVendaId", query = "SELECT v FROM Vacinacao v WHERE v.vendaId = :vendaId")
    , @NamedQuery(name = "Vacinacao.findByAnimalVacinacaoGeral", query = "SELECT v FROM Vacinacao v WHERE v.animal.cliId.cliId = :cliId and v.vendaId is null"
            + " and v.vacinacaoDataAplicacao is not null and v.produto is not null and upper(v.produto.produtoNome) like upper(concat(:filtro,'%'))"
            + " or v.animal.cliId.cliId = :cliId and v.vendaId is null and v.vacinacaoDataAplicacao is not null and v.produto is not null and upper(v.produto.produtoObs) like upper(concat(:filtro,'%'))")
    })
public class Vacinacao extends ButtonAcaoVacinacao implements Serializable, Item {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VacinacaoPK vacinacaoPK;
    @Column(name = "vacinacao_obs")
    private String vacinacaoObs;
    @JoinColumn(name = "venda_id", referencedColumnName = "venda_id")
    @ManyToOne(optional = true)
    private Venda vendaId;
    @JoinColumn(name = "animal_id", referencedColumnName = "animal_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Animal animal;
    @JoinColumn(name = "produto_id", referencedColumnName = "produto_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Produto produto;
    @Column(name = "vacinacao_data_aplicacao")
    @Temporal(TemporalType.DATE)
    private Date vacinacaoDataAplicacao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "vacinacao_peso")
    private BigDecimal vacinacaoPeso;
    @Column(name = "vacinacao_lancamento")
    private Boolean vacinacaoLancamento;

    public Vacinacao() {
    }

    public Vacinacao(VacinacaoPK vacinacaoPK) {
        this.vacinacaoPK = vacinacaoPK;
    }

    public Vacinacao(String vacinacaoObs) {
        this.vacinacaoObs = vacinacaoObs;
    }

    public Vacinacao(String vacinacaoObs, Date vacinacaoDataAplicacao,
            BigDecimal vacinacaoPeso, Boolean vacinacaoLancamento) {
        this.vacinacaoObs = vacinacaoObs;
        this.vacinacaoDataAplicacao = vacinacaoDataAplicacao;
        this.vacinacaoPeso = vacinacaoPeso;
        this.vacinacaoLancamento = vacinacaoLancamento;
    }

    public Vacinacao(VacinacaoPK vacinacaoPK, String vacinacaoObs, Date vacinacaoDataAplicacao,
            BigDecimal vacinacaoPeso, Boolean vacinacaoLancamento) {
        this.vacinacaoPK = vacinacaoPK;
        this.vacinacaoObs = vacinacaoObs;
        this.vacinacaoDataAplicacao = vacinacaoDataAplicacao;
        this.vacinacaoPeso = vacinacaoPeso;
        this.vacinacaoLancamento = vacinacaoLancamento;
    }

    public Vacinacao(int vacinacaoId, Date vacinacaoData, int animalId, int produtoId,
            String vacinacaoObs, Date vacinacaoDataAplicacao, BigDecimal vacinacaoPeso,
            Boolean vacinacaoLancamento) {
        this.vacinacaoPK = new VacinacaoPK(vacinacaoId, vacinacaoData, animalId, produtoId);
        this.vacinacaoObs = vacinacaoObs;
        this.vacinacaoDataAplicacao = vacinacaoDataAplicacao;
        this.vacinacaoPeso = vacinacaoPeso;
        this.vacinacaoLancamento = vacinacaoLancamento;
    }

    public Vacinacao(int vacinacaoId, Date vacinacaoData, int animalId, int produtoId) {
        this.vacinacaoPK = new VacinacaoPK(vacinacaoId, vacinacaoData, animalId, produtoId);
    }

    public VacinacaoPK getVacinacaoPK() {
        return vacinacaoPK;
    }

    public void setVacinacaoPK(VacinacaoPK vacinacaoPK) {
        this.vacinacaoPK = vacinacaoPK;
    }

    public String getVacinacaoObs() {
        return vacinacaoObs;
    }

    public void setVacinacaoObs(String vacinacaoObs) {
        this.vacinacaoObs = vacinacaoObs;
    }

    public Venda getVendaId() {
        return vendaId;
    }

    public Object getVenda() {
        return vacinacaoLancamento != null && vacinacaoLancamento
                ? vendaId != null ? vendaId : "Lançado" : "Não Vinculado";
    }

    public Object getId() {
        return vacinacaoPK != null ? vacinacaoPK.getVacinacaoId() : null;
    }

    public Object getData() {
        return vacinacaoPK != null ? Variables.getData(vacinacaoPK.getVacinacaoData()) : null;
    }

    public Object getDataAplicacao() {
        return Variables.getData(vacinacaoDataAplicacao);
    }

    public void setVendaId(Venda vendaId) {
        this.vendaId = vendaId;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vacinacaoPK != null ? vacinacaoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vacinacao)) {
            return false;
        }
        Vacinacao other = (Vacinacao) object;
        if ((this.vacinacaoPK == null && other.vacinacaoPK != null) || (this.vacinacaoPK != null && !this.vacinacaoPK.equals(other.vacinacaoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Vacinacao.Entidade.Vacinacao[ vacinacaoPK=" + vacinacaoPK + " ]";
    }

    @Override
    protected void AddNodes() {
        addNode("Button", "rem", "Remover");
        addNode("Button", "app", "Aplicar");
        addNode("Button", "capp", "Cancelar");
    }

    public Date getVacinacaoDataAplicacao() {
        return vacinacaoDataAplicacao;
    }

    public void setVacinacaoDataAplicacao(Date vacinacaoDataAplicacao) {
        this.vacinacaoDataAplicacao = vacinacaoDataAplicacao;
    }

    public BigDecimal getVacinacaoPeso() {
        return vacinacaoPeso;
    }

    public Object getPeso() {
        return vacinacaoPeso != null ? vacinacaoPeso : "Não Informado";
    }

    public void setVacinacaoPeso(BigDecimal vacinacaoPeso) {
        this.vacinacaoPeso = vacinacaoPeso;
    }

    public Boolean getVacinacaoLancamento() {
        return vacinacaoLancamento;
    }

    public void setVacinacaoLancamento(Boolean vacinacaoLancamento) {
        this.vacinacaoLancamento = vacinacaoLancamento;
    }

    @Override
    public double getValor() {
        return produto.getProdutoPrecoVenda().doubleValue();
    }

    @Override
    public String getNome() {
        return produto.getProdutoNome();
    }

    @Override
    public String getChave() {
        return vacinacaoPK.getVacinacaoId() + "";
    }

    @Override
    public String getPrecoUnitario() {
        return getValor() + "";
    }

    @Override
    public boolean Compara(Item oItem) {
        return equals(oItem);
    }

    @Override
    public String getTipo() {
        return "Vacina";
    }

}
