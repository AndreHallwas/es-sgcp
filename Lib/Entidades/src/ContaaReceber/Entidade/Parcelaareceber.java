/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContaaReceber.Entidade;

import Caixa.Entidade.Movimentacaoareceber;
import FormadePagamento.Entidade.Formadepagamento;
import JPA.Dao.JPADao;
import Utils.DateUtils;
import Utils.FormatString;
import Variables.Variables;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "parcelaareceber")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parcelaareceber.findAll", query = "SELECT p FROM Parcelaareceber p")
    , @NamedQuery(name = "Parcelaareceber.findByContaareceberId", query = "SELECT p FROM Parcelaareceber p WHERE p.parcelaareceberPK.contaareceberId = :contaareceberId")
    , @NamedQuery(name = "Parcelaareceber.findByParrNumerodaparcela", query = "SELECT p FROM Parcelaareceber p WHERE p.parcelaareceberPK.parrNumerodaparcela = :parrNumerodaparcela")
    , @NamedQuery(name = "Parcelaareceber.findByParcelaareceberPk", query = "SELECT p FROM Parcelaareceber p WHERE p.parcelaareceberPK.contaareceberId = :contaareceberId"
            + " and p.parcelaareceberPK.parrNumerodaparcela = :parrNumerodaparcela")
    , @NamedQuery(name = "Parcelaareceber.findByVendaId", query = "SELECT p FROM Parcelaareceber p WHERE p.contaareceber.vendaId = :vendaId"
            + " order by p.parcelaareceberPK.parrNumerodaparcela asc")
    , @NamedQuery(name = "Parcelaareceber.findByParrDataVencimento", query = "SELECT p FROM Parcelaareceber p WHERE p.parrDataVencimento = :parrDataVencimento")
    , @NamedQuery(name = "Parcelaareceber.findByParrDataPagamento", query = "SELECT p FROM Parcelaareceber p WHERE p.parrDataPagamento = :parrDataPagamento")
    , @NamedQuery(name = "Parcelaareceber.findByParrValor", query = "SELECT p FROM Parcelaareceber p WHERE p.parrValor = :parrValor")
    , @NamedQuery(name = "Parcelaareceber.findByParrSituacao", query = "SELECT p FROM Parcelaareceber p WHERE p.parrSituacao = :parrSituacao")
    , @NamedQuery(name = "Parcelaareceber.findByParrValorPago", query = "SELECT p FROM Parcelaareceber p WHERE p.parrValorPago = :parrValorPago")
    , @NamedQuery(name = "Parcelaareceber.findByParrTroco", query = "SELECT p FROM Parcelaareceber p WHERE p.parrTroco = :parrTroco")
    , @NamedQuery(name = "Parcelaareceber.findByFormadePagamento", query = "SELECT p FROM Parcelaareceber p WHERE p.formadepagamentoId.formadepagamentoId = :formadepagamentoId")
    , @NamedQuery(name = "Parcelaareceber.findByNextParcela", query = "SELECT p FROM Parcelaareceber p GROUP BY p HAVING MIN(p.parrDataPagamento) = p.parrDataPagamento"
            + " and p.parcelaareceberPK.contaareceberId = :contaareceberId")
    , @NamedQuery(name = "Parcelaareceber.findByAllGeral", query = "SELECT p FROM Parcelaareceber p WHERE p.parcelaareceberPK.parrNumerodaparcela = :parrNumerodaparcela"
            + " or p.parrValor = :parrValor"
            + " or p.parrValorPago = :parrValorPago")
    , @NamedQuery(name = "Parcelaareceber.findByAllData", query = "SELECT p FROM Parcelaareceber p WHERE p.parrDataVencimento = :parrDataVencimento"
            + " or p.parrDataPagamento = :parrDataPagamento")
    , @NamedQuery(name = "Parcelaareceber.findByBetweenVencimento", query = "SELECT p FROM Parcelaareceber p WHERE p.parrDataVencimento"
            + " between :datainicial and :datafinal")
    , @NamedQuery(name = "Parcelaareceber.findByBetweenPagamento", query = "SELECT p FROM Parcelaareceber p WHERE p.parrDataPagamento"
            + " between :datainicial and :datafinal")
    , @NamedQuery(name = "Parcelaareceber.findByAllBetween", query = "SELECT p FROM Parcelaareceber p WHERE p.parrDataPagamento"
            + " between :datainicial and :datafinal"
            + " or p.parrDataVencimento"
            + " between :datainicial and :datafinal")
    , @NamedQuery(name = "Parcelaareceber.findByDataVencimentoVenda", query = "SELECT p FROM Parcelaareceber p "
            + "WHERE p.parrDataVencimento = :parrDataVencimento"
            + " and p.contaareceber.vendaId = :vendaId")
    , @NamedQuery(name = "Parcelaareceber.findByParrDatadegeracao", query = "SELECT p FROM Parcelaareceber p WHERE p.parcelaareceberPK.parrDatadegeracao = :parrDatadegeracao")})
public class Parcelaareceber extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ParcelaareceberPK parcelaareceberPK;
    @Basic(optional = false)
    @Column(name = "parr_data_vencimento")
    @Temporal(TemporalType.DATE)
    private Date parrDataVencimento;
    @Column(name = "parr_data_pagamento")
    @Temporal(TemporalType.DATE)
    private Date parrDataPagamento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "parr_valor")
    private BigDecimal parrValor;
    @Column(name = "parr_situacao")
    private Boolean parrSituacao;
    @Column(name = "parr_valor_pago")
    private BigDecimal parrValorPago;
    @Column(name = "parr_troco")
    private BigDecimal parrTroco;
    @JoinColumn(name = "contaareceber_id", referencedColumnName = "contaareceber_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contaareceber contaareceber;
    @JoinColumn(name = "formadepagamento_id", referencedColumnName = "formadepagamento_id")
    @ManyToOne(optional = false)
    private Formadepagamento formadepagamentoId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parcelaareceber")
    private Collection<Movimentacaoareceber> movimentacaoareceberCollection;

    public Parcelaareceber() {
    }

    public Parcelaareceber(ParcelaareceberPK parcelaareceberPK) {
        this.parcelaareceberPK = parcelaareceberPK;
    }

    public Parcelaareceber(Integer ContaareceberID, Integer papNumerodaparcela, Date parrDataVencimento, Date parrDataPagamento,
            BigDecimal parrValor, Boolean parrSituacao, BigDecimal parrValorPago, BigDecimal parrTroco, Date parrDatadegeracao) {
        this.parcelaareceberPK = new ParcelaareceberPK(ContaareceberID, papNumerodaparcela, parrDatadegeracao);
        this.parrDataVencimento = parrDataVencimento;
        this.parrDataPagamento = parrDataPagamento;
        this.parrValor = parrValor;
        this.parrSituacao = parrSituacao;
        this.parrValorPago = parrValorPago;
        this.parrTroco = parrTroco;
    }

    public Parcelaareceber(ParcelaareceberPK parcelaareceberPK, Date parrDataVencimento, BigDecimal parrValor) {
        this.parcelaareceberPK = parcelaareceberPK;
        this.parrDataVencimento = parrDataVencimento;
        this.parrValor = parrValor;
    }

    public Parcelaareceber(int contaareceberId, int parrNumerodaparcela, Date parrDatadegeracao) {
        this.parcelaareceberPK = new ParcelaareceberPK(contaareceberId, parrNumerodaparcela, parrDatadegeracao);
    }

    public ParcelaareceberPK getParcelaareceberPK() {
        return parcelaareceberPK;
    }

    public void setParcelaareceberPK(ParcelaareceberPK parcelaareceberPK) {
        this.parcelaareceberPK = parcelaareceberPK;
    }

    public Date getParrDataVencimento() {
        return parrDataVencimento;
    }

    public String getDataVencimento() {
        return parrDataVencimento != null ? Variables.getData(parrDataVencimento) : "Aguardando Pagamento";
    }

    public void setParrDataVencimento(Date parrDataVencimento) {
        this.parrDataVencimento = parrDataVencimento;
    }

    public Date getParrDataPagamento() {
        return parrDataPagamento;
    }

    public String getDataPagamento() {
        return parrDataPagamento != null ? Variables.getData(parrDataPagamento) : "Aguardando Pagamento";
    }

    public void setParrDataPagamento(Date parrDataPagamento) {
        this.parrDataPagamento = parrDataPagamento;
    }

    public BigDecimal getParrValor() {
        return parrValor;
    }

    public String getValor() {
        return FormatString.MonetaryPersistent(parrValor);
    }

    public void setParrValor(BigDecimal parrValor) {
        this.parrValor = parrValor;
    }

    public Boolean getParrSituacao() {
        return parrSituacao;
    }

    public String getSituacao() {
        return parrSituacao ? "Pago" : "Pendente";
    }

    public void setParrSituacao(Boolean parrSituacao) {
        this.parrSituacao = parrSituacao;
    }

    public BigDecimal getParrValorPago() {
        return parrValorPago;
    }

    public String getValorPago() {
        return FormatString.MonetaryPersistent(parrValorPago);
    }

    public void setParrValorPago(BigDecimal parrValorPago) {
        this.parrValorPago = parrValorPago;
    }

    public BigDecimal getParrTroco() {
        return parrTroco;
    }

    public void setParrTroco(BigDecimal parrTroco) {
        this.parrTroco = parrTroco;
    }

    public Contaareceber getContaareceber() {
        return contaareceber;
    }

    public void setContaareceber(Contaareceber contaareceber) {
        this.contaareceber = contaareceber;
    }

    public Formadepagamento getFormadepagamentoId() {
        return formadepagamentoId;
    }

    public void setFormadepagamentoId(Formadepagamento formadepagamentoId) {
        this.formadepagamentoId = formadepagamentoId;
    }

    public Integer getParrNumerodaparcela() {
        return parcelaareceberPK != null ? parcelaareceberPK.getParrNumerodaparcela() : null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parcelaareceberPK != null ? parcelaareceberPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parcelaareceber)) {
            return false;
        }
        Parcelaareceber other = (Parcelaareceber) object;
        if ((this.parcelaareceberPK == null && other.parcelaareceberPK != null) || (this.parcelaareceberPK != null && !this.parcelaareceberPK.equals(other.parcelaareceberPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.Parcelaareceber[ parcelaareceberPK=" + parcelaareceberPK + " ]";
    }

    /////Trasient
    public String getDatadeVencimento() {
        if (parrDataVencimento != null) {
            return DateUtils.asLocalDate(parrDataVencimento).toString();
        } else {
            return "Sem Data de Vencimento";
        }
    }

    /////Trasient
    public String getDatadePagamento() {
        if (parrDataPagamento != null) {
            return DateUtils.asLocalDate(parrDataPagamento).toString();
        } else {
            return "Não foi Pago";
        }
    }

    @XmlTransient
    public Collection<Movimentacaoareceber> getMovimentacaoareceberCollection() {
        return movimentacaoareceberCollection;
    }

    public void setMovimentacaoareceberCollection(Collection<Movimentacaoareceber> movimentacaoareceberCollection) {
        this.movimentacaoareceberCollection = movimentacaoareceberCollection;
    }

}
