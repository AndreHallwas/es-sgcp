/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authenticator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Remote
 */
public class Auth {

    public static byte[] generate(String FileName) {
        try {
            final byte[] toBeHashed = readFile(FileName);
            final MessageDigest md = MessageDigest.getInstance("SHA-512");
            final byte[] hash = md.digest(toBeHashed);
            return hash;
        } catch (NoSuchAlgorithmException | FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public static String generateandread(String FileName) {
        return toHexFormat(generate(FileName));
    }

    public static String generateandwrite(String FileName) {
        try {
            final byte[] hash = generate(FileName);
            writeFileEnc(hash);
            return toHexFormat(hash);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static byte[] readFile(String Name) throws FileNotFoundException, IOException {
        File arq = new File(Name);
        return Files.readAllBytes(arq.toPath());
    }

    private static void writeFile(byte[] strToBytes) throws FileNotFoundException, IOException {
        File arq = null;
        FileOutputStream outputStream = new FileOutputStream("GeneratedKey.key");
        outputStream.write(strToBytes);
        outputStream.close();
    }

    private static void writeFileEnc(byte[] strToBytes) throws FileNotFoundException, IOException, Exception {
        writeFile(encrypt(strToBytes, "aaAaaAAAaaAaaaAa"));
    }

    private static String readFileEnc(String name) throws FileNotFoundException, Exception {
        return decrypt(readFile(name), "aaAaaAAAaaAaaaAa");
    }

    public static boolean authenticate(String KeyFile, String FileName) {
        String KeyProgram = generateandread(FileName);
        try {
            String KeyGenerated = readFileEnc(KeyFile);
            return KeyProgram.equals(KeyGenerated);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private static String toHexFormat(final byte[] bytes) {
        final StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    public static byte[] encrypt(byte[] textopuro, String chaveencriptacao) throws Exception {
        Cipher encripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
        SecretKeySpec key = new SecretKeySpec(chaveencriptacao.getBytes("UTF-8"), "AES");
        encripta.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec("AAAAAAaAAAAAAAAa".getBytes("UTF-8")));
        return encripta.doFinal(textopuro);
    }

    public static String decrypt(byte[] textoencriptado, String chaveencriptacao) throws Exception {
        Cipher decripta = Cipher.getInstance("AES/CBC/PKCS5Padding", "SunJCE");
        SecretKeySpec key = new SecretKeySpec(chaveencriptacao.getBytes("UTF-8"), "AES");
        decripta.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec("AAAAAAaAAAAAAAAa".getBytes("UTF-8")));
        return toHexFormat(decripta.doFinal(textoencriptado));
    }
}
