/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacina.Interface;

import Utils.Controladora.CtrlUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaAplicacaoVacinaController implements Initializable {

    @FXML
    private DatePicker dtpDatadeAplicacao;
    @FXML
    private JFXTextArea taDetalhes;
    @FXML
    private JFXCheckBox cbLancamento;
    @FXML
    private JFXTextField txbPeso;
    
    protected static LocalDate Data;
    protected static String Detalhes;
    protected static Boolean Lancamento;
    protected static Double Peso;
    protected static boolean EOriginal = true;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO"
        MaskFieldUtil.numericField(txbPeso);
        EstadoOriginal();
        
    }

    private void EstadoOriginal() {
        if (EOriginal) {
            dtpDatadeAplicacao.setValue(LocalDate.now());
            setData(null);
            setDetalhes(null);
            setLancamento(null);
            setPeso(null);
        } else {
            setEOriginal(true);
            txbPeso.setText(Peso+"");
            taDetalhes.setText(Detalhes);
            dtpDatadeAplicacao.setValue(Data);
            cbLancamento.setSelected(Lancamento != null ? Lancamento : false);
        }
    }

    @FXML
    private void HandleButtonConfirmar(MouseEvent event) {
        if (dtpDatadeAplicacao.getValue() != null) {
            setData(dtpDatadeAplicacao.getValue());
            setDetalhes(taDetalhes.getText());
            setLancamento((Boolean) cbLancamento.isSelected());
            if (!txbPeso.getText().trim().isEmpty()) {
                setPeso((Double) Double.parseDouble(txbPeso.getText()));
            } else {
                setPeso(null);
            }
            HandleButtonCancelar(event);
        } else {
            Mensagem.Exibir("Data Inválida!", 2);
        }
    }

    @FXML
    private void HandleButtonCancelar(MouseEvent event) {
        CtrlUtils.CloseStage(event);
    }

    /**
     * @return the Data
     */
    public static LocalDate getData() {
        return Data;
    }

    /**
     * @return the Detalhes
     */
    public static String getDetalhes() {
        return Detalhes;
    }

    /**
     * @return the Lancamento
     */
    public static Boolean getLancamento() {
        return Lancamento;
    }

    /**
     * @return the Peso
     */
    public static Double getPeso() {
        return Peso;
    }

    /**
     * @param aData the Data to set
     */
    public static void setData(LocalDate aData) {
        Data = aData;
    }

    /**
     * @param aDetalhes the Detalhes to set
     */
    public static void setDetalhes(String aDetalhes) {
        Detalhes = aDetalhes;
    }

    /**
     * @param aLancamento the Lancamento to set
     */
    public static void setLancamento(Boolean aLancamento) {
        Lancamento = aLancamento;
    }

    /**
     * @param aPeso the Peso to set
     */
    public static void setPeso(Double aPeso) {
        Peso = aPeso;
    }

    /**
     * @param aEOriginal the EOriginal to set
     */
    public static void setEOriginal(boolean aEOriginal) {
        EOriginal = aEOriginal;
    }

}
