/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authenticator;

import static java.lang.System.exit;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Remote
 */
public class Authenticator extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Tela.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setTitle("Auth");
        stage.setFullScreen(false);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static boolean main(String[] args) {
        if (args != null && args.length > 0) {
            if (args.length == 4 && args[0].equalsIgnoreCase("-auth")) {
                String keyfile = args[1];
                String file = args[3];
                return Auth.authenticate(keyfile, file);
            } else {
                System.out.println("type:");
                System.out.println("-auth: to set keyfile example: c://file");
                System.out.println("-local: to set file example: c://file");
            }
            exit(1);
        } else {
            launch(args);
        }
        return true;
    }

}
