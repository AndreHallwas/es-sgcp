/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Interface;

import Servico.Controladora.CtrlServico;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaServicoController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcValor;
    @FXML
    private TableColumn<Object, String> tcDescricao;
    @FXML
    private TableColumn<Object, String> tcGrupodeServico;
    @FXML
    private HBox btnGp;
    
    private static Object servico;
    private static boolean btnPainel = true;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("ServicoNome"));
        tcValor.setCellValueFactory(new PropertyValueFactory("ServValor"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("ServicoDescricao"));
        tcGrupodeServico.setCellValueFactory(new PropertyValueFactory("GrupodeservicoId"));
        CarregaTabela("");
        servico = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlServico ctm = CtrlServico.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            servico = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (servico != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhum Serviço Selecionado", 2);
        }
    }

    public static TelaConsultaServicoController Create(Class classe, Pane pndados) {
        TelaConsultaServicoController CServico = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Pessoa/Interface/TelaConsultaServicoFXML.fxml"));
            Parent root = Loader.load();
            CServico = (TelaConsultaServicoController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CServico;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaServicoController.btnPainel = btnPainel;
    }

    public static Object getServico() {
        return servico;
    }
}
