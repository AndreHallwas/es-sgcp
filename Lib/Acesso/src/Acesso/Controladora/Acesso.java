package Acesso.Controladora;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Acesso.Entidade.Componente;
import Acesso.Entidade.Grupodeacesso;
import Acesso.Entidade.Permissaocomponente;
import Acesso.Entidade.Permissaodeacesso;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;

/**
 *
 * @author Raizen
 */
public class Acesso {

    private static Acesso acesso;

    public static Acesso create() {
        if (acesso == null) {
            acesso = new Acesso();
        }
        return acesso;
    }

    public static void setCampos(Object Grupo, TextField txbNome, TextArea taDescricao, TextField txbIDGrupo, TextField txbIDPermissao) {
        Grupodeacesso oGrupo = (Grupodeacesso) Grupo;
        txbNome.setText(oGrupo.getGrupoNome());
        taDescricao.setText(oGrupo.getGrupoDescricao());
        txbIDGrupo.setText(Integer.toString(oGrupo.getGrupoId()));
        txbIDPermissao.setText(Integer.toString(oGrupo.getPermacessoId().getPermacessoId()));
        /////tgbVisivel tgbVisivel.setSelected((p.getVisivel() == '1'));
    }

    public static ArrayList<Object> setComponentes(Object Grupo, ComboBox<Object> cbComponentes) {
        Grupodeacesso oGrupo = (Grupodeacesso) Grupo;
        Collection<Permissaocomponente> Permissaoc = oGrupo.getPermacessoId().getPermissaocomponenteCollection();
        ArrayList<Permissaocomponente> PermissaoC = new ArrayList();
        for (Permissaocomponente permissaocomponente : Permissaoc) {
            PermissaoC.add(permissaocomponente);
        }
        ArrayList<Object> componentes = new ArrayList();
        PermissaoC.forEach(new ConsumerImpl(componentes));
        cbComponentes.getItems().clear();
        cbComponentes.getItems().addAll(componentes);
        return componentes;
    }

    public static ArrayList<Object> getComponentes(Object Grupo) {
        Grupodeacesso oGrupo = (Grupodeacesso) Grupo;
        Collection<Permissaocomponente> Permissaoc = oGrupo.getPermacessoId().getPermissaocomponenteCollection();
        ArrayList<Permissaocomponente> PermissaoC = new ArrayList();
        for (Permissaocomponente permissaocomponente : Permissaoc) {
            PermissaoC.add(permissaocomponente);
        }
        ArrayList<Object> componentes = new ArrayList();
        PermissaoC.forEach(new ConsumerImpl(componentes));
        return componentes;
    }
    private String Mensagem;

    private Acesso() {
    }

    public static void setCampos() {

    }

    private CtrlGrupodeacesso ac;
    private CtrlComponente cc;
    private CtrlPermissaodeacesso pa;
    private CtrlPermissaocomponente pc;
    private ArrayList<Componente> Componentes;

    public ArrayList<Object> PesquisarGrupo(String Filtro) {
        ac = CtrlGrupodeacesso.create();
        return ac.Pesquisar(Filtro);
    }

    public ArrayList<Object> PesquisarComponente(String Filtro) {
        cc = CtrlComponente.create();
        return cc.Pesquisar(Filtro);
    }

    public void addComponente(Componente componente) {
        if (Componentes == null) {
            Componentes = new ArrayList();
        }
        Componentes.add(componente);
    }

    public boolean RegistraComponentes() {
        boolean flag = true;
        cc = CtrlComponente.create();
        if (Componentes != null) {
            Componente oComponente;
            for (int i = 0; i < Componentes.size(); i++) {
                oComponente = Componentes.get(i);
                oComponente.Constroi();
                ArrayList<Object> osComponentes = cc.Pesquisar(oComponente.getComponenteId());
                if (!osComponentes.isEmpty()) {
                    flag = flag && cc.Alterar(osComponentes.get(0), oComponente.getComponenteId(),
                            oComponente.getComponenteDescricao()) != null;
                } else {
                    flag = flag && cc.Salvar(oComponente.getComponenteId(),
                            oComponente.getComponenteDescricao()) != null;
                }
            }
        }
        return flag;
    }

    protected boolean Erase_Componentes() {
        /*int Lin = Transaction.getEntityManager().createQuery("delete from Componente").executeUpdate();
        return Lin > 0;*/
        return true;
    }

    public boolean RegistraGrupo(String Nome, String Descricao, ArrayList<Object> Componentes) {
        boolean flag = true;
        ac = CtrlGrupodeacesso.create();
        pa = CtrlPermissaodeacesso.create();

        Grupodeacesso Gacesso = new Grupodeacesso(Nome, Descricao);

        Permissaodeacesso Pacesso = new Permissaodeacesso(Descricao);
        Gacesso.setPermacessoId(Pacesso);

        ArrayList<Permissaocomponente> Permissoes = new ArrayList();
        Permissaocomponente permissaocomponente;
        for (int i = 0; i < Componentes.size(); i++) {
            permissaocomponente = new Permissaocomponente();
            permissaocomponente.setComponenteId((Componente) Componentes.get(i));
            permissaocomponente.setPermacessoId(Pacesso);
            Permissoes.add(permissaocomponente);
        }
        Pacesso.setPermissaocomponenteCollection(Permissoes);
        Mensagem = (Gacesso = (Grupodeacesso) ac.Salvar(Gacesso)) != null
                ? "Salvo com Sucesso!"
                : "Erro ao Salvar:Acesso:211!";
        
        return Gacesso != null;
    }

    public boolean ModificarGrupo(String ID, String Nome, String Descricao, Object Permissao,
            ArrayList<Object> Componentes) {
        boolean flag = true;
        if (Permissao != null) {
            ac = CtrlGrupodeacesso.create();
            pa = CtrlPermissaodeacesso.create();
            pc = CtrlPermissaocomponente.create();
            if (Permissao instanceof String) {
                ArrayList<Object> AuxPermissao = pa.Pesquisar((String) Permissao);
                if (AuxPermissao != null && !AuxPermissao.isEmpty()) {
                    Permissao = AuxPermissao.get(0);
                }
            }
            Grupodeacesso grupo = null;
            ArrayList<Object> AuxGrupo = ac.Pesquisar((String) ID);
            if (AuxGrupo != null && !AuxGrupo.isEmpty()) {
                grupo = (Grupodeacesso) AuxGrupo.get(0);
            }
            Permissaodeacesso permissaodeacesso;
            Permissaocomponente permissaocomponente;

            if (Componentes != null && grupo != null && grupo.getPermacessoId() != null) {
                permissaodeacesso = grupo.getPermacessoId();
                Collection<Permissaocomponente> Permissaoc = permissaodeacesso.getPermissaocomponenteCollection();
                ArrayList<Permissaocomponente> aPermissaocomponente = new ArrayList();
                for (Permissaocomponente permissaocomponentes : Permissaoc) {
                    aPermissaocomponente.add(permissaocomponentes);
                }
                ArrayList<Permissaocomponente> Adicionados = new ArrayList();
                ArrayList<Permissaocomponente> NaoModificados = new ArrayList();
                ArrayList<Permissaocomponente> Removidos = new ArrayList();
                Removidos.addAll(aPermissaocomponente);
                for (Object oComponente : Componentes) {
                    int i = 0;
                    while (i < aPermissaocomponente.size() && !ComparaComponente(oComponente,
                            aPermissaocomponente.get(i).getComponenteId())) {
                        i++;
                    }
                    if (i < aPermissaocomponente.size()) {
                        NaoModificados.add(aPermissaocomponente.get(i));
                        Removidos.remove(aPermissaocomponente.get(i));
                    } else {
                        permissaocomponente = new Permissaocomponente();
                        permissaocomponente.setComponenteId((Componente) oComponente);
                        permissaocomponente.setPermacessoId(permissaodeacesso);
                        Adicionados.add(permissaocomponente);
                    }
                }
                /*for (Permissaocomponente Removido : Removidos) {
                    flag = flag && pc.Remover(Removido.getPermcompId());
                }
                for (Permissaocomponente Adicionado : Adicionados) {
                    flag = flag && pc.Salvar(Adicionado.getComponenteId(), Adicionado.getPermacessoId()) != null;
                }*/
                permissaodeacesso.setPermissaocomponenteCollection(new ArrayList());
                permissaodeacesso.getPermissaocomponenteCollection().addAll(NaoModificados);
                permissaodeacesso.getPermissaocomponenteCollection().addAll(Adicionados);
                grupo.setPermacessoId(permissaodeacesso);
                grupo.setGrupoNome(Nome);
                grupo.setGrupoDescricao(Descricao);
                
                Mensagem = (grupo = (Grupodeacesso) ac.Alterar(grupo)) != null
                        ? "Alterado com Sucesso!"
                        : "Erro ao Alterar:Acesso:211!";
            }

            return flag;
        }
        return false;
    }

    protected boolean SalvarGrupo(String Nome, String Descricao, Object Permissao) {
        ac = CtrlGrupodeacesso.create();
        return ac.Salvar(Nome, Descricao, Permissao) != null;
    }

    public boolean AlterarGrupo(Object oGrupo, String ID, String Nome, String Descricao, Object Permissao) {
        ac = CtrlGrupodeacesso.create();
        return ac.Alterar(oGrupo, Integer.parseInt(ID), Nome, Descricao, Permissao) != null;
    }

    public boolean RemoverGrupo(String ID) {
        ac = CtrlGrupodeacesso.create();
        return ac.Remover(Integer.parseInt(ID));
    }

    public boolean RemoverGrupo(Object Grupo) {
        if (Grupo != null && Grupo instanceof Grupodeacesso) {
            return ac.Remover(((Grupodeacesso) Grupo).getGrupoId());
        }
        return false;
    }

    public boolean SalvarComponente(String ID_Componente, String Descricao_Componente) {
        cc = CtrlComponente.create();
        return cc.Salvar(ID_Componente, Descricao_Componente) != null;
    }

    public boolean AlterarComponente(Object oComponente, String ID_Componente, String Descricao_Componente) {
        cc = CtrlComponente.create();
        return cc.Alterar(oComponente, ID_Componente, Descricao_Componente) != null;
    }

    public boolean RemoverComponente(String ID_Componente) {
        cc = CtrlComponente.create();
        return cc.Remover(ID_Componente);
    }

    protected boolean SalvarPermissaodeacesso(String Detalhes) {
        pa = CtrlPermissaodeacesso.create();
        return pa.Salvar(Detalhes) != null;
    }

    public boolean AlterarPermissaodeacesso(Object aPermissaodeacesso, String ID, String Detalhes) {
        pa = CtrlPermissaodeacesso.create();
        return pa.Alterar(aPermissaodeacesso, Integer.parseInt(ID), Detalhes) != null;
    }

    public boolean RemoverPermissaodeacesso(String ID) {
        pa = CtrlPermissaodeacesso.create();
        return pa.Remover(Integer.parseInt(ID));
    }

    protected boolean SalvarPermissaocomponente(Object Permissao, Componente componente) {
        pc = CtrlPermissaocomponente.create();
        return pc.Salvar(componente, Permissao) != null;
    }

    public boolean AlterarPermissaocomponente(Object aPermissaocomponente, Integer Id, String Permissao, Componente componente) {
        pc = CtrlPermissaocomponente.create();
        return pc.Alterar(aPermissaocomponente, Id, componente, Permissao) != null;
    }

    public boolean RemoverPermissaocomponente(Integer Id) {
        pc = CtrlPermissaocomponente.create();
        return pc.Remover(Id);
    }

    public ArrayList<Object> PesquisarPermissaodeacesso(String Filtro) {
        pa = CtrlPermissaodeacesso.create();
        return pa.Pesquisar(Filtro);
    }

    public ArrayList<String> PesquisarComponentePorID(String Filtro) {
        cc = CtrlComponente.create();
        ArrayList<Object> osComponentes = cc.Pesquisar(Filtro);
        ArrayList<String> Nomes = new ArrayList();
        osComponentes.forEach((oComponente) -> {
            Nomes.add(((Componente) oComponente).getComponenteId());
        });
        return Nomes;
    }

    /**
     * True is Equal
     *
     * @param componente1
     * @param componente2
     * @return
     */
    public boolean ComparaComponente(Object componente1, Object componente2) {
        return ((Componente) componente1).getComponenteId().
                equalsIgnoreCase(((Componente) componente2).getComponenteId());
    }

    public String getMsg() {
        return Mensagem != null && !Mensagem.trim().isEmpty() ? Mensagem : "Realizado";
    }

    private static class ConsumerImpl implements Consumer<Permissaocomponente> {

        private final ArrayList<Object> componentes;

        public ConsumerImpl(ArrayList<Object> componentes) {
            this.componentes = componentes;
        }

        @Override
        public void accept(Permissaocomponente permissaoComponente) {
            componentes.add(permissaoComponente.getComponenteId());
        }
    }
}
