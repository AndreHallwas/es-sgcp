/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Interface;

import Animal.Controladora.CtrlAnimal;
import Animal.Controladora.CtrlAnotacao;
import Animal.Entidade.Anotacao;
import Transacao.Backup.nodeTabela;
import Utils.Acao.InterfaceFxmlAcao;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaCadAnotacaoController implements Initializable, Tela, InterfaceFxmlAcao {

    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Object> tcId;
    @FXML
    private TableColumn<Object, Object> tcData;
    @FXML
    private TableColumn<Object, Object> tcDescricao;
    @FXML
    private TableColumn<Object, Object> tcOpcoes;

    private Object oAnimal;
    @FXML
    private JFXTextField txbAnimal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        SetTelaAcao();
        tcId.setCellValueFactory(new PropertyValueFactory("AnotacaoId"));
        tcData.setCellValueFactory(new PropertyValueFactory("DatadaAnotacao"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("AnotacaoDescricao"));
        tcOpcoes.setCellValueFactory(new PropertyValueFactory("Crud"));
    }

    private void CarregaTabela(String filtro) {
        CtrlAnotacao ctm = CtrlAnotacao.create();
        try {
            tabela.getItems().clear();
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, "AnimalId")));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Animal/Interface/TelaConsultaAnimal.fxml",
                "Consulta de Animal", null);
        if (TelaConsultaAnimalController.getAnimal() != null) {
            EstadoConsulta(TelaConsultaAnimalController.getAnimal());
        }
    }

    private void EstadoConsulta(Object animal) {
        tabela.getItems().clear();
        tabela.setItems(FXCollections.observableList(CtrlAnimal.getAnotacoes(animal)));
        CarregaTabela(CtrlAnimal.getId(animal) + "");
        CtrlAnimal.setCampoBusca(animal, txbAnimal);
        oAnimal = animal;
    }

    @FXML
    private void evtAdicionar(MouseEvent event) {
        TelaDetalhesAnotacaoController.setFlag(1);
        TelaDetalhesAnotacaoController.setAnimal(oAnimal);
        IniciarTela.loadWindow(this.getClass(), "/Animal/Interface/TelaDetalhesAnotacao.fxml",
                "Detalhes da Anotação", null);
        EstadoOriginalAnimal();
    }

    private void evtModificar(MouseEvent event, Object Reference) {
        TelaDetalhesAnotacaoController.setFlag(0);
        TelaDetalhesAnotacaoController.setAnimal(oAnimal);
        TelaDetalhesAnotacaoController.setAnotacao(Reference);
        IniciarTela.loadWindow(this.getClass(), "/Animal/Interface/TelaDetalhesAnotacao.fxml",
                "Detalhes da Anotação", null);
        EstadoOriginalAnimal();
    }

    private void evtRemover(MouseEvent event, Object Reference) {
        CtrlAnotacao ca = CtrlAnotacao.create();
        if (!ca.Remover(CtrlAnotacao.getId(Reference))) {
            Mensagem.Exibir(ca.create().getMsg(), 2);
        } else {
            EstadoOriginalAnimal();
        }
    }

    @Override
    public void SelectEventAcao(Object Reference, String Action, Object element) {
        if (Action.equalsIgnoreCase("mod")) {
            evtModificar(null, Reference);
        } else if (Action.equalsIgnoreCase("rem")) {
            evtRemover(null, Reference);
        }
    }

    @Override
    public void SetTelaAcao() {
        Anotacao.setTelaAcao(this);
    }

    private void EstadoOriginalAnimal() {
        if (oAnimal != null) {
            CarregaTabela(CtrlAnimal.getId(oAnimal) + "");
        }
    }

}
