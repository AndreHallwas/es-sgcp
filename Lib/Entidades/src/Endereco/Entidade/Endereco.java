/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import JPA.Dao.JPADao;
import java.util.ArrayList;
import javax.persistence.FetchType;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "endereco")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Endereco.findAll", query = "SELECT e FROM Endereco e")
    , @NamedQuery(name = "Endereco.findByEndCep", query = "SELECT e FROM Endereco e WHERE e.endCep = :endCep")
    , @NamedQuery(name = "Endereco.findByEndLocal", query = "SELECT e FROM Endereco e WHERE e.endLocal = :endLocal")})
public class Endereco extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "end_cep")
    private String endCep;
    @Basic(optional = false)
    @Column(name = "end_local")
    private String endLocal;
    @JoinColumn(name = "bai_id", referencedColumnName = "bai_id")
    @ManyToOne(optional = false)
    private Bairro baiId;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "endCep")
    private Collection<Complementoendereco> complementoenderecoCollection = new ArrayList();

    public Endereco() {
    }

    public Endereco(String endCep) {
        this.endCep = endCep;
    }

    public Endereco(String endCep, String endLocal) {
        this.endCep = endCep;
        this.endLocal = endLocal;
    }

    public String getEndCep() {
        return endCep;
    }

    public void setEndCep(String endCep) {
        this.endCep = endCep;
    }

    public String getEndLocal() {
        return endLocal;
    }

    public void setEndLocal(String endLocal) {
        this.endLocal = endLocal;
    }

    public Bairro getBaiId() {
        return baiId;
    }

    public void setBaiId(Bairro baiId) {
        this.baiId = baiId;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (endCep != null ? endCep.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Endereco)) {
            return false;
        }
        Endereco other = (Endereco) object;
        if ((this.endCep == null && other.endCep != null) || (this.endCep != null && !this.endCep.equals(other.endCep))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        /*return "testedb2.Endereco[ endCep=" + endCep + " ]";*/
        return endLocal;
    }
    
    @XmlTransient
    public Collection<Complementoendereco> getComplementoenderecoCollection() {
        return complementoenderecoCollection;
    }

    public void setComplementoenderecoCollection(Collection<Complementoendereco> complementoenderecoCollection) {
        this.complementoenderecoCollection = complementoenderecoCollection;
    }
    
}
