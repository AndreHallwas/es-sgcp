/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacina.Relatorio;

import Transacao.Transaction;
import Utils.GerarRelatorios;
import Utils.Mensagem;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaRelatorioVacinacaoController implements Initializable {

    @FXML
    private DatePicker dtpDataInicial;
    @FXML
    private DatePicker dtpDataFinal;
    @FXML
    private JFXButton btnBuscar;
    @FXML
    private ScrollPane spdado;
    @FXML
    private HBox dado;

    private static final String Caminho = "Relatorios/Petshop/Vacinacao.jasper";
    private static final String Tela = "Relatório de Vacinações";
    private SwingNode sn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dtpDataInicial.setValue(LocalDate.now());
        dtpDataFinal.setValue(null);
        btnBuscar.setVisible(false);/*Busca Simples*/
    }

    @FXML
    private void btnTelaCheia(MouseEvent event) {
        try {
            if (dtpDataInicial.getValue() != null && dtpDataFinal.getValue() != null) {
                GerarRelatorios.gerarRelatorio(SqlFiltro(), Caminho, Tela, Transaction.getEntityManager());
            } else {
                GerarRelatorios.gerarRelatorio(SqlDefault(), Caminho, Tela, Transaction.getEntityManager());
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Não Há Dados Eminentes Nesta Consulta");
            dtpDataInicial.setValue(LocalDate.now());
            dtpDataFinal.setValue(null);
            Default();
        }
    }

    @FXML
    private void evtBuscar(MouseEvent event) {
        if (dtpDataInicial.getValue() != null && dtpDataFinal.getValue() != null) {
            setRelatorio(SqlFiltro(), Caminho);
        } else {
            Default();
        }
    }

    private void Default() {
        setRelatorio(SqlDefault(), Caminho);
    }

    private void setRelatorio(String Sql, String Caminho) {
        try {
            sn = GerarRelatorios.gerarRelatorio(Sql, Caminho, Transaction.getEntityManager());
            dado.getChildren().clear();
            dado.getChildren().add(sn);
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Não Há Dados Resultantes Desta Consulta");
            dtpDataInicial.setValue(LocalDate.now());
            dtpDataFinal.setValue(null);
            Default();
        }
    }

    private String SqlDefault() {
        return "SELECT * FROM cliente" +
        " inner join animal ON animal.cli_id = cliente.cli_id" 
        + " inner join vacinacao on vacinacao.animal_id = animal.animal_id" 
        + " inner join produto on vacinacao.produto_id = produto.produto_id" 
        + " where vacinacao.vacinacao_data_aplicacao is null ";
    }

    private String SqlFiltro() {
        String DataInicial = dtpDataInicial.getValue().toString();
        String DataFinal = dtpDataFinal.getValue().toString();
        return "SELECT * FROM cliente" +
        " inner join animal ON animal.cli_id = cliente.cli_id" 
        + " inner join vacinacao on vacinacao.animal_id = animal.animal_id" 
        + " inner join produto on vacinacao.produto_id = produto.produto_id" 
        + " where  vacinacao.vacinacao_data_aplicacao is null "
        + " and vacinacao.vacinacao_data between '" + DataInicial + "' and '" + DataFinal + "'";
    }

}
