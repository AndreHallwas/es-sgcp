/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacina.Interface;

import Animal.Controladora.CtrlAnimal;
import Pessoa.Controladora.CtrlCliente;
import Pessoa.Interface.TelaConsultaClienteController;
import Utils.Acao.InterfaceFxmlAcao;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Vacina.Controladora.CtrlVacina;
import Vacina.Interface.TabelaVacinas.nodeTabela;
import Vacinacao.Entidade.Vacinacao;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaVacinaController implements Initializable, InterfaceFxmlAcao, Tela {

    @FXML
    private JFXTextField txbCliente;
    @FXML
    private Label lblErroCliente;
    @FXML
    private JFXComboBox<Object> cbAnimal;
    @FXML
    private Label lblErroAnimal;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Object> tcId;
    @FXML
    private TableColumn<Object, Object> tcVenda;
    @FXML
    private TableColumn<Object, Object> tcAcoes;
    @FXML
    private TableColumn<Object, Object> tcVacina;
    @FXML
    private TableColumn<Object, Object> tcPeso;
    @FXML
    private TableColumn<Object, Object> tcDataPrevista;
    @FXML
    private TableColumn<Object, Object> tcDatadeAplicação;
    @FXML
    private HBox gpAdicionar;
    @FXML
    private JFXButton btnBuscar;
    
    private Object Cliente;
    private Object Instancia;
    private Object Animal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcId.setCellValueFactory(new PropertyValueFactory("Id"));
        tcDataPrevista.setCellValueFactory(new PropertyValueFactory("Data"));
        tcDatadeAplicação.setCellValueFactory(new PropertyValueFactory("DataAplicacao"));
        tcVacina.setCellValueFactory(new PropertyValueFactory("Produto"));
        tcPeso.setCellValueFactory(new PropertyValueFactory("Peso"));
        tcVenda.setCellValueFactory(new PropertyValueFactory("Venda"));
        tcAcoes.setCellValueFactory(new PropertyValueFactory("Crud"));

        SetTelaAcao();
        
        EstadoOriginal();
    }

    @FXML
    private void HandleButtonPesquisarCliente(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Pessoa/Interface/TelaConsultaCliente.fxml",
                "Consulta de Cliente", null);
        if (TelaConsultaClienteController.getCliente() != null) {
            EstadoOriginal();
            Cliente = TelaConsultaClienteController.getCliente();
            ArrayList<Object> Animais = CtrlCliente.getAnimais(Cliente);
            if(Animais != null && !Animais.isEmpty()){
                
                CtrlCliente.setCampoBusca(Cliente, txbCliente);
                gpAdicionar.setDisable(false);
                btnBuscar.setDisable(false);
                cbAnimal.getItems().clear();
                cbAnimal.setItems(FXCollections.observableList(Animais));
                cbAnimal.getSelectionModel().selectFirst();
                Animal = cbAnimal.getSelectionModel().getSelectedItem();
                
                if(Animais.size() == 1){
                    CarregaTabela(CtrlAnimal.getId(Animal)+"");
                }
            }else{
                Mensagem.Exibir("O Cliente: "+CtrlCliente.getNome(Cliente)+" não Possui Animais Registrados.", 2);
                cbAnimal.setDisable(true);
                Animal = null;
            }
        }
    }

    @FXML
    private void HandleCBAnimalChanged(Event event) {
        if (cbAnimal.getSelectionModel().getSelectedItem() != null) {
            Animal = cbAnimal.getSelectionModel().getSelectedItem();
        }
    }

    @FXML
    private void HandleButtonAdicionar(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Vacina/Interface/TelaAdicionarVacina.fxml",
                "Adicionar Vacina", null);
        TabelaVacinas Tabela = TelaAdicionarVacinaController.getTabeladeVacinas();
        setAllErro(false, false);
        CtrlVacina cv = CtrlVacina.create();
        if (validaCampos() && Tabela != null) {
            List<nodeTabela> nodes = Tabela.get();
            if ((Instancia = cv.Salvar(nodes, Animal)) == null) {
                Mensagem.Exibir(cv.getMsg(), 1);
            }
            CarregaTabela(CtrlAnimal.getId(Animal)+"");
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    private void EstadoOriginal() {
        tabela.getItems().clear();
        cbAnimal.getItems().clear();
        Cliente = null;
        txbCliente.setText("");
        gpAdicionar.setDisable(true);
        btnBuscar.setDisable(true);
        cbAnimal.setDisable(false);
        setAllErro(false, false);
    }

    private void setAllErro(boolean animal, boolean cliente) {
        lblErroAnimal.setVisible(animal);
        lblErroCliente.setVisible(cliente);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    private boolean validaCampos() {
        boolean fl = true;
        StringBuilder Result = new StringBuilder();

        if (Cliente == null) {
            setErro(lblErroCliente, "Campo Obrigatório Vazio");
            txbCliente.setText("");
            txbCliente.requestFocus();
            fl = false;
        }
        if (Animal == null) {
            setErro(lblErroAnimal, "Campo Inválido");
            fl = false;
        }

        return fl;
    }

    public void evtAplicar(Object Vacina) {

        if (CtrlVacina.isAplicada(Vacina)) {
            TelaAplicacaoVacinaController.setEOriginal(false);
            TelaAplicacaoVacinaController.setLancamento(CtrlVacina.getLancamento(Vacina));
            TelaAplicacaoVacinaController.setPeso(CtrlVacina.getPeso(Vacina));
            TelaAplicacaoVacinaController.setData(CtrlVacina.getDataAplicacao(Vacina));
        }
        
        TelaAplicacaoVacinaController.setDetalhes(CtrlVacina.getObs(Vacina));

        IniciarTela.loadWindow(this.getClass(), "/Vacina/Interface/TelaAplicacaoVacina.fxml",
                "Aplicar Vacina", null);

        LocalDate Data = TelaAplicacaoVacinaController.getData();
        if (Data != null) {
            Boolean Lancamento = TelaAplicacaoVacinaController.getLancamento();
            String Obs = TelaAplicacaoVacinaController.getDetalhes();

            BigDecimal Peso = null;
            if (TelaAplicacaoVacinaController.getPeso() != null) {
                Peso = new BigDecimal(TelaAplicacaoVacinaController.getPeso());
            }

            if ((Instancia = Vacina = CtrlVacina.create().Aplicar(Vacina, Data, Obs, Lancamento, Peso)) == null) {
                Mensagem.Exibir(CtrlVacina.create().getMsg(), 2);
            }
            
            CarregaTabela(CtrlAnimal.getId(Animal)+"");

        }

    }

    public void evtRemover(Object Vacina) {
        if (!CtrlVacina.create().VerificaComprado(Vacina)) {
            if (!CtrlVacina.create().Remover(CtrlVacina.getId(Vacina))) {
                Mensagem.Exibir(CtrlVacina.create().getMsg(), 2);
            }
            CarregaTabela(CtrlAnimal.getId(Animal)+"");
        }else{
            Mensagem.Exibir("Não é Possivel remover uma Vacina Lançada em uma Compra."
                    + " Para efetuar a Remoção remova a vacina da Compra referente."
                    + " ID da Compra: "+CtrlVacina.create().getCompraId(Vacina)+".", 2);
        }
    }

    private void CarregaTabela(String filtro) {
        CtrlVacina ctm = CtrlVacina.create();
        try {
            tabela.getItems().clear();
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, "Geral")));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao arregar Tabela");
        }
    }

    public void evtCancelarAplicacao(Object Vacina) {
        if (!CtrlVacina.create().VerificaComprado(Vacina)) {
            if ((Instancia = Vacina = CtrlVacina.create().CancelarAplicao(Vacina))== null) {
                Mensagem.Exibir(CtrlVacina.create().getMsg(), 2);
            }
            CarregaTabela(CtrlAnimal.getId(Animal)+"");
        }else{
            Mensagem.Exibir("Não é Possivel Cancelar uma Aplicação de uma Vacina Lançada em uma Compra."
                    + " Para efetuar a Remoção remova a vacina da Compra referente."
                    + " ID da Compra: "+CtrlVacina.create().getCompraId(Vacina)+".", 2);
        }
    }

    @Override
    public void SelectEventAcao(Object Reference, String Action, Object element) {
        if (Action.equalsIgnoreCase("app")) {
            evtAplicar(Reference);
        } else if (Action.equalsIgnoreCase("capp")) {
            evtCancelarAplicacao(Reference);
        } else if (Action.equalsIgnoreCase("rem")) {
            evtRemover(Reference);
        }
    }

    @Override
    public void SetTelaAcao() {
        Vacinacao.setTelaAcao(this);
    }

    private void AtualizaTabela(Object oAnimal) {
        if (Cliente != null && oAnimal != null) {
            tabela.getItems().clear();
            tabela.setItems(FXCollections.observableList(CtrlVacina.getVacinas(oAnimal)));
        }
    }

    @FXML
    private void HandleButtonBuscar(Event event) {
        if(Animal != null){
            CarregaTabela(CtrlAnimal.getId(Animal)+"");
        }
    }

}
