/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "pais")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pais.findAll", query = "SELECT p FROM Pais p")
    , @NamedQuery(name = "Pais.findByPaisId", query = "SELECT p FROM Pais p WHERE p.paisId = :paisId")
    , @NamedQuery(name = "Pais.findByPaisSgl", query = "SELECT p FROM Pais p WHERE p.paisSgl = :paisSgl")
    , @NamedQuery(name = "Pais.findByPaisNome", query = "SELECT p FROM Pais p WHERE p.paisNome = :paisNome")})
public class Pais extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pais_id")
    private Integer paisId;
    @Basic(optional = false)
    @Column(name = "pais_sgl")
    private String paisSgl;
    @Basic(optional = false)
    @Column(name = "pais_nome")
    private String paisNome;
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "paisId")
    private Collection<Estado> estadoCollection = new ArrayList();;

    public Pais() {
    }

    public Pais(Integer paisId) {
        this.paisId = paisId;
    }

    public Pais(String paisSgl, String paisNome) {
        this.paisSgl = paisSgl;
        this.paisNome = paisNome;
    }

    public Pais(Integer paisId, String paisSgl, String paisNome) {
        this.paisId = paisId;
        this.paisSgl = paisSgl;
        this.paisNome = paisNome;
    }

    public Integer getPaisId() {
        return paisId;
    }

    public void setPaisId(Integer paisId) {
        this.paisId = paisId;
    }

    public String getPaisSgl() {
        return paisSgl;
    }

    public void setPaisSgl(String paisSgl) {
        this.paisSgl = paisSgl;
    }

    public String getPaisNome() {
        return paisNome;
    }

    public void setPaisNome(String paisNome) {
        this.paisNome = paisNome;
    }

    @XmlTransient
    public Collection<Estado> getEstadoCollection() {
        return estadoCollection;
    }

    public void setEstadoCollection(Collection<Estado> estadoCollection) {
        this.estadoCollection = estadoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paisId != null ? paisId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.paisId == null && other.paisId != null) || (this.paisId != null && !this.paisId.equals(other.paisId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testedb2.Pais[ paisId=" + paisId + " ]";
    }
    
}
