/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "categoriaproduto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoriaproduto.findAll", query = "SELECT c FROM Categoriaproduto c")
    , @NamedQuery(name = "Categoriaproduto.findByCategoriaprodId", query = "SELECT c FROM Categoriaproduto c WHERE c.categoriaprodutoPK.categoriaprodId = :categoriaprodId")
    , @NamedQuery(name = "Categoriaproduto.findByTipoprodutoId", query = "SELECT c FROM Categoriaproduto c WHERE c.categoriaprodutoPK.tipoprodutoId = :tipoprodutoId")
    , @NamedQuery(name = "Categoriaproduto.findByCategoriaprodNome", query = "SELECT c FROM Categoriaproduto c WHERE c.categoriaprodNome = :categoriaprodNome")
    , @NamedQuery(name = "Categoriaproduto.findByCategoriaprodObs", query = "SELECT c FROM Categoriaproduto c WHERE c.categoriaprodObs = :categoriaprodObs")})
public class Categoriaproduto extends JPADao implements Serializable {

    @Lob
    @Column(name = "categoriaprod_imagem")
    private byte[] categoriaprodImagem;

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CategoriaprodutoPK categoriaprodutoPK;
    @Column(name = "categoriaprod_nome")
    private String categoriaprodNome;
    @Column(name = "categoriaprod_obs")
    private String categoriaprodObs;
    @Column(name = "categoriaprod_medicamento")
    private Boolean categoriaprodMedicamento;
    @Column(name = "categoriaprod_vacina")
    private Boolean categoriaprodVacina;
    
    @JoinColumn(name = "tipoproduto_id", referencedColumnName = "tipoproduto_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Tipoproduto tipoproduto;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "categoriaproduto")
    private Collection<Produto> produtoCollection = new ArrayList();

    public Categoriaproduto() {
    }

    public Categoriaproduto(CategoriaprodutoPK categoriaprodutoPK) {
        this.categoriaprodutoPK = categoriaprodutoPK;
    }

    public Categoriaproduto(String categoriaprodNome, String categoriaprodObs, byte[] categoriaprodImagem,
            Boolean categoriaprodMedicamento, Boolean categoriaprodVacina) {
        this.categoriaprodNome = categoriaprodNome;
        this.categoriaprodObs = categoriaprodObs;
        this.categoriaprodImagem = categoriaprodImagem;
        this.categoriaprodMedicamento = categoriaprodMedicamento;
        this.categoriaprodVacina = categoriaprodVacina;
    }

    public Categoriaproduto(CategoriaprodutoPK categoriaprodutoPK, String categoriaprodNome, String categoriaprodObs,
            byte[] categoriaprodImagem, Boolean categoriaprodMedicamento, Boolean categoriaprodVacina) {
        this.categoriaprodutoPK = categoriaprodutoPK;
        this.categoriaprodNome = categoriaprodNome;
        this.categoriaprodObs = categoriaprodObs;
        this.categoriaprodImagem = categoriaprodImagem;
        this.categoriaprodMedicamento = categoriaprodMedicamento;
        this.categoriaprodVacina = categoriaprodVacina;
    }

    public Categoriaproduto(int categoriaprodId, int tipoprodutoId, String categoriaprodNome, String categoriaprodObs, byte[] categoriaprodImagem) {
        this.categoriaprodutoPK = new CategoriaprodutoPK(categoriaprodId, tipoprodutoId);
        this.categoriaprodNome = categoriaprodNome;
        this.categoriaprodObs = categoriaprodObs;
        this.categoriaprodImagem = categoriaprodImagem;
    }

    public Categoriaproduto(int categoriaprodId, int tipoprodutoId) {
        this.categoriaprodutoPK = new CategoriaprodutoPK(categoriaprodId, tipoprodutoId);
    }

    public CategoriaprodutoPK getCategoriaprodutoPK() {
        return categoriaprodutoPK;
    }

    public void setCategoriaprodutoPK(CategoriaprodutoPK categoriaprodutoPK) {
        this.categoriaprodutoPK = categoriaprodutoPK;
    }

    public String getCategoriaprodNome() {
        return categoriaprodNome;
    }

    public void setCategoriaprodNome(String categoriaprodNome) {
        this.categoriaprodNome = categoriaprodNome;
    }

    public String getCategoriaprodObs() {
        return categoriaprodObs;
    }

    public void setCategoriaprodObs(String categoriaprodObs) {
        this.categoriaprodObs = categoriaprodObs;
    }

    public byte[] getCategoriaprodImagem() {
        return categoriaprodImagem;
    }

    public void setCategoriaprodImagem(byte[] categoriaprodImagem) {
        this.categoriaprodImagem = categoriaprodImagem;
    }

    public Tipoproduto getTipoproduto() {
        return tipoproduto;
    }

    public void setTipoproduto(Tipoproduto tipoproduto) {
        this.tipoproduto = tipoproduto;
    }

    @XmlTransient
    public Collection<Produto> getProdutoCollection() {
        return produtoCollection;
    }

    public void setProdutoCollection(Collection<Produto> produtoCollection) {
        this.produtoCollection = produtoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoriaprodutoPK != null ? categoriaprodutoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoriaproduto)) {
            return false;
        }
        Categoriaproduto other = (Categoriaproduto) object;
        if ((this.categoriaprodutoPK == null && other.categoriaprodutoPK != null) || (this.categoriaprodutoPK != null && !this.categoriaprodutoPK.equals(other.categoriaprodutoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return categoriaprodNome;
    }

    public Boolean getCategoriaprodMedicamento() {
        return categoriaprodMedicamento;
    }

    public void setCategoriaprodMedicamento(Boolean categoriaprodMedicamento) {
        this.categoriaprodMedicamento = categoriaprodMedicamento;
    }

    public Boolean getCategoriaprodVacina() {
        return categoriaprodVacina;
    }

    public void setCategoriaprodVacina(Boolean categoriaprodVacina) {
        this.categoriaprodVacina = categoriaprodVacina;
    }
    
}
