package Agendamento.Interface;

import Acesso.Sessao.Sessao;
import Agendamento.Controladora.CtrlAgendamento;
import Pessoa.Controladora.CtrlCliente;
import Pessoa.Interface.TelaConsultaClienteController;
import Utils.Controladora.CtrlUtils;
import Variables.Variables;
import Utils.DateUtils;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public class TelaCadAgendamentoController implements Initializable, Tela {

    @FXML
    private VBox pnAgendamento;
    @FXML
    private DatePicker dtpData;
    @FXML
    private TableView<Object> Tabela;
    @FXML
    private TableColumn<Object, Date> tcHorario;
    @FXML
    private TableColumn<Object, String> tcAnimal;
    @FXML
    private TableColumn<Object, String> tcCliente;
    @FXML
    private TableColumn<Object, String> tcTelefone;
    @FXML
    private Button btnAgendar;
    @FXML
    private Button btnDesmarcar;
    @FXML
    private JFXComboBox<Object> cbAnimal;
    @FXML
    private Button btnFechar;
    @FXML
    private JFXTextField txbCliente;
    @FXML
    private Label lblErroCliente;
    @FXML
    private Label lblErroAnimal;
    @FXML
    private Label lblErroData;
    private Object oAgendamento;
    private int EstadoConsulta;
    private Object oCliente;
    private boolean EstadoEdicao;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tcHorario.setCellValueFactory(new PropertyValueFactory("AgendamentoHorario"));
        tcCliente.setCellValueFactory(new PropertyValueFactory("ClienteId"));
        tcAnimal.setCellValueFactory(new PropertyValueFactory("AnimalId"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("Telefone"));
        EstadoOriginal();
    }

    public void EstadoOriginal() {
        EstadoEdicao = false;
        LocalDate aData = dtpData.getValue() == null ? LocalDate.now() : dtpData.getValue();
        
        Tabela.getItems().clear();
        Tabela.setItems(FXCollections.observableArrayList(
                FXCollections.observableArrayList(
                        CtrlAgendamento.create().PesquisaAgenda(
                                DateUtils.asDate(aData)))));
        
        btnAgendar.setDisable(true);
        btnDesmarcar.setDisable(true);
        EstadoConsulta = 0;

        lblErroAnimal.setVisible(false);
        lblErroCliente.setVisible(false);
        lblErroData.setVisible(false);

        dtpData.setValue(aData);

        oAgendamento = null;

        cbAnimal.getItems().clear();

        CtrlUtils.clear(pnAgendamento.getChildren());
    }

    @FXML
    private void clicknatabela(MouseEvent event) {
        int lin = Tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            Object p = Tabela.getItems().get(lin);

            EstadoConsulta(p);
        }
    }

    private void EstadoConsulta(Object p) {
        btnAgendar.setDisable(false);
        btnDesmarcar.setDisable(false);
        if (!CtrlAgendamento.setCampos(p, txbCliente, cbAnimal)) {
            CtrlUtils.clear(pnAgendamento.getChildren());
            EstadoEdicao = false;
        } else {
            EstadoEdicao = true;
        }
        oAgendamento = p;
        EstadoConsulta = 1;
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        /*CtrlAgendamento ca = CtrlAgendamento.create();
        ArrayList<Object> Pesquisa = ca.Pesquisar(DateUtils.asDate(dtpData.getValue()).toString());*/
        if (dtpData.getValue() == null) {
            setErro(lblErroData, "Campo Obrigatório Vazio");
            dtpData.requestFocus();
            fl = false;
        }
        if (DateUtils.asDate(dtpData.getValue()).before(DateUtils.asDate(LocalDate.now()))) {
            setErro(lblErroData, "Data Anterior ao Dia de Hoje");
            dtpData.requestFocus();
            fl = false;
        }
        if (txbCliente.getText().trim().isEmpty()) {
            setErro(lblErroCliente, "Campo Obrigatório Vazio");
            txbCliente.setText("");
            txbCliente.requestFocus();
            fl = false;
        }
        if (cbAnimal.getItems().isEmpty()) {
            setErro(lblErroAnimal, "Selecione um Animal");
            cbAnimal.requestFocus();
            fl = false;
        }
        return fl;
    }

    @FXML
    private void HandleButtonAgendar(ActionEvent event) {
        if (validaCampos()) {
            if (EstadoEdicao) {
                if ((oAgendamento = CtrlAgendamento.create().Alterar(oAgendamento, CtrlAgendamento.getId(oAgendamento),
                        CtrlAgendamento.getData(oAgendamento), "Vazio",
                        Sessao.getIndividuo(), cbAnimal.getSelectionModel().getSelectedItem(),
                        null)) == null) {
                    Mensagem.ExibirLog(CtrlAgendamento.create().getMsg());
                } else {
                    Mensagem.ExibirLog(CtrlAgendamento.create().getMsg());
                }
            }else{
                if ((oAgendamento = CtrlAgendamento.create().Salvar(CtrlAgendamento.getData(oAgendamento), "Vazio",
                        Sessao.getIndividuo(), cbAnimal.getSelectionModel().getSelectedItem(),
                        null)) == null) {
                    Mensagem.ExibirLog(CtrlAgendamento.create().getMsg());
                } else {
                    Mensagem.ExibirLog(CtrlAgendamento.create().getMsg());
                }
            }
            EstadoOriginal();
        }
    }

    @FXML
    private void HandleButtonDesmarcar(ActionEvent event) {
        if (oAgendamento != null) {
            if (CtrlAgendamento.create().Desmarcar(oAgendamento)) {
                Mensagem.ExibirLog(CtrlAgendamento.create().getMsg());
            } else {
                Mensagem.ExibirLog(CtrlAgendamento.create().getMsg());
            }
            EstadoOriginal();
        }
    }

    @FXML
    private void HandleButtonFechar(ActionEvent event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EstadoConsulta == 1)) {
            EstadoOriginal();
        }
    }

    @FXML
    private void HandleButtonPesquisarCliente(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Pessoa/Interface/TelaConsultaCliente.fxml",
                "Consulta de Cliente", null);
        oCliente = TelaConsultaClienteController.getCliente();
        CtrlCliente.setCampoBusca(oCliente, txbCliente);
        cbAnimal.getItems().clear();
        cbAnimal.getItems().addAll(CtrlCliente.getAnimais(oCliente));
        cbAnimal.getSelectionModel().selectFirst();
    }

    @FXML
    private void HandleDateTimePickerDataChanged(Event event) {
        if (dtpData.getValue() != null) {
            Tabela.setItems(FXCollections.observableArrayList(
                    CtrlAgendamento.create().PesquisaAgenda(DateUtils.asDate(dtpData.getValue()))));
        }
    }

}
