/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caixa.Entidade;

import JPA.Dao.JPADao;
import Pessoa.Entidade.Usuario;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "caixa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Caixa.findAll", query = "SELECT c FROM Caixa c")
    , @NamedQuery(name = "Caixa.findByCaixaId", query = "SELECT c FROM Caixa c WHERE c.caixaId = :caixaId")
    , @NamedQuery(name = "Caixa.findByCaixaData", query = "SELECT c FROM Caixa c WHERE c.caixaData = :caixaData")
    , @NamedQuery(name = "Caixa.findByCaixaValorFinal", query = "SELECT c FROM Caixa c WHERE c.caixaValorFinal = :caixaValorFinal")
    , @NamedQuery(name = "Caixa.findByCaixaValorInicial", query = "SELECT c FROM Caixa c WHERE c.caixaValorInicial = :caixaValorInicial")
    , @NamedQuery(name = "Caixa.findByCaixaDataFechamento", query = "SELECT c FROM Caixa c WHERE c.caixaDataFechamento = :caixaDataFechamento")})
public class Caixa extends JPADao implements Serializable {

    @JoinColumn(name = "usr_id", referencedColumnName = "usr_id")
    @ManyToOne(optional = false)
    private Usuario usrId;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "caixa_id")
    private Integer caixaId;
    @Basic(optional = false)
    @Column(name = "caixa_data")
    @Temporal(TemporalType.DATE)
    private Date caixaData;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "caixa_valor_final")
    private BigDecimal caixaValorFinal;
    @Basic(optional = false)
    @Column(name = "caixa_valor_inicial")
    private BigDecimal caixaValorInicial;
    @Column(name = "caixa_data_fechamento")
    @Temporal(TemporalType.DATE)
    private Date caixaDataFechamento;

    @OneToMany(mappedBy = "caixaId")
    private Collection<Movimentacaoapagar> movimentacaoapagarCollection;
    @OneToMany(mappedBy = "caixaId")
    private Collection<Movimentacaoareceber> movimentacaoareceberCollection;

    public Caixa() {
    }

    public Caixa(Integer caixaId) {
        this.caixaId = caixaId;
    }

    public Caixa(Date caixaData, BigDecimal caixaValorInicial) {
        this.caixaData = caixaData;
        this.caixaValorInicial = caixaValorInicial;
    }

    public Caixa(Integer caixaId, BigDecimal caixaValorFinal, Date caixaDataFechamento) {
        this.caixaId = caixaId;
        this.caixaValorFinal = caixaValorFinal;
        this.caixaDataFechamento = caixaDataFechamento;
    }

    public Integer getCaixaId() {
        return caixaId;
    }

    public void setCaixaId(Integer caixaId) {
        this.caixaId = caixaId;
    }

    public Date getCaixaData() {
        return caixaData;
    }

    public void setCaixaData(Date caixaData) {
        this.caixaData = caixaData;
    }

    public BigDecimal getCaixaValorFinal() {
        return caixaValorFinal;
    }

    public void setCaixaValorFinal(BigDecimal caixaValorFinal) {
        this.caixaValorFinal = caixaValorFinal;
    }

    public BigDecimal getCaixaValorInicial() {
        return caixaValorInicial;
    }

    public void setCaixaValorInicial(BigDecimal caixaValorInicial) {
        this.caixaValorInicial = caixaValorInicial;
    }

    public Date getCaixaDataFechamento() {
        return caixaDataFechamento;
    }

    public void setCaixaDataFechamento(Date caixaDataFechamento) {
        this.caixaDataFechamento = caixaDataFechamento;
    }

    public Usuario getUsrId() {
        return usrId;
    }

    public void setUsrId(Usuario usrId) {
        this.usrId = usrId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (caixaId != null ? caixaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Caixa)) {
            return false;
        }
        Caixa other = (Caixa) object;
        if ((this.caixaId == null && other.caixaId != null) || (this.caixaId != null && !this.caixaId.equals(other.caixaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Caixa.Entidade.Caixa[ caixaId=" + caixaId + " ]";
    }

    @XmlTransient
    public Collection<Movimentacaoapagar> getMovimentacaoapagarCollection() {
        return movimentacaoapagarCollection;
    }

    public void setMovimentacaoapagarCollection(Collection<Movimentacaoapagar> movimentacaoapagarCollection) {
        this.movimentacaoapagarCollection = movimentacaoapagarCollection;
    }

    @XmlTransient
    public Collection<Movimentacaoareceber> getMovimentacaoareceberCollection() {
        return movimentacaoareceberCollection;
    }

    public void setMovimentacaoareceberCollection(Collection<Movimentacaoareceber> movimentacaoareceberCollection) {
        this.movimentacaoareceberCollection = movimentacaoareceberCollection;
    }
    
}
