/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Interface;

import Endereco.Controladora.CtrlComplementoendereco;
import Endereco.Controladora.CtrlEndereco;
import Endereco.Entidade.Endereco;
import Utils.Controladora.CtrlUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Raizen
 */
public class TelaEnderecoController implements Initializable, Tela {

    @FXML
    private Label lblErroCep;
    @FXML
    private JFXTextField txbCep;
    @FXML
    private JFXButton btnConsultarCep;
    @FXML
    private JFXTextField cbPais;
    @FXML
    private JFXTextField cbEstado;
    @FXML
    private JFXTextField cbCidade;
    @FXML
    private JFXTextField cbBairro;
    @FXML
    private JFXTextField cbRua;
    @FXML
    private VBox pnEndereco;
    @FXML
    private Label lblErroComplemento;
    @FXML
    private Label lblErroNumero;
    @FXML
    private JFXTextField cbComplemento;
    @FXML
    private JFXTextField cbNumero;
    private static Object Endereco;
    private Object Complemento;
    private boolean EOriginal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MaskFieldUtil.numericField(txbCep);
        MaskFieldUtil.maxField(txbCep, 10);

        estadoOriginal();
    }

    @FXML
    private void evtConsultaCep(ActionEvent event) {
        lblErroCep.setVisible(false);
        if (txbCep.getText().length() == 8) {
            CtrlEndereco ce = new CtrlEndereco();
            List endereco = ce.Pesquisar(txbCep.getText());
            if (!endereco.isEmpty()) {
                Object en = endereco.get(0);
                CtrlEndereco.setCampos(en, txbCep, cbRua, cbBairro, cbCidade, cbEstado, cbPais);
                Endereco = en;
            } else {
                estadoOriginal();
                setErro(lblErroCep, "Cep Não Encontrado");
                txbCep.setText("");
                txbCep.requestFocus();
            }
        } else {
            estadoOriginal();
            setErro(lblErroCep, "Cep Inválido");
            txbCep.setText("");
            txbCep.requestFocus();
        }
        EOriginal = false;
    }

    public boolean valida() //Validações de banco
    {
        return TelaEnderecoController.valida(txbCep, cbNumero, lblErroCep, lblErroNumero);
    }

    public void estadoOriginal() {
        lblErroCep.setVisible(false);
        lblErroNumero.setVisible(false);
        Complemento = null;
        CtrlUtils.clear(pnEndereco.getChildren());
    }

    public static boolean valida(TextField txbCep, TextField cbNumero, Label lblErroCep, Label lblErroNumero) //Validações de banco
    {
        boolean fl = true;

        if (txbCep.getText().length() == 8) {
            CtrlEndereco ce = new CtrlEndereco();
            if (ce.Pesquisar(txbCep.getText()).isEmpty()) {
                setErro(lblErroCep, "Cep Não Existe");
                txbCep.setText("");
                txbCep.requestFocus();
                fl = false;
            }
        } else {
            setErro(lblErroCep, "Cep Inválido");
            txbCep.setText("");
            txbCep.requestFocus();
            fl = false;
        }
        if (cbNumero.getText().trim().isEmpty()) {
            setErro(lblErroNumero, "Numero Inválido");
            cbNumero.setText("");
            cbNumero.requestFocus();
            fl = false;
        }

        return fl;
    }

    private static void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public static Boolean ConsultaCep(String filtro, TextField txbCep, Label lblErroCep,
            TextField cbBairro, TextField cbRua, TextField cbCidade, TextField cbEstado, TextField cbPais) {
        return buscaEndereco(filtro, txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
    }

    public static Boolean buscaEndereco(String filtro, TextField txbCep, Label lblErroCep,
            TextField cbBairro, TextField cbRua, TextField cbCidade, TextField cbEstado, TextField cbPais) {
        boolean flag = true;
        if (filtro.length() == 8) {
            CtrlEndereco ce = new CtrlEndereco();
            List endereco = ce.Pesquisar(filtro);
            if (!endereco.isEmpty()) {
                Object en = endereco.get(0);
                CtrlEndereco.setCampos(en, txbCep, cbRua, cbBairro, cbCidade, cbEstado, cbPais);
                Endereco = en;
            } else {
                setErro(lblErroCep, "Cep Não Encontrado");
                txbCep.setText("");
                txbCep.requestFocus();
                flag = false;
            }
        } else {
            setErro(lblErroCep, "Cep Inválido");
            txbCep.setText("");
            txbCep.requestFocus();
            flag = false;
        }
        return flag;
    }

    public static void ConsultaCep(String filtro, TextField txbCep, Label lblErroCep,
            ComboBox cbBairro, ComboBox cbRua, ComboBox cbCidade, ComboBox cbEstado, ComboBox cbPais) {
        buscaEndereco(filtro, txbCep, lblErroCep, cbBairro, cbRua, cbCidade, cbEstado, cbPais);
    }

    public static void buscaEndereco(String filtro, TextField txbCep, Label lblErroCep,
            ComboBox cbBairro, ComboBox cbRua, ComboBox cbCidade, ComboBox cbEstado, ComboBox cbPais) {
        if (filtro.length() == 8) {
            CtrlEndereco ce = new CtrlEndereco();
            List endereco = ce.Pesquisar(filtro);
            if (!endereco.isEmpty()) {
                cbBairro.getItems().clear();
                cbRua.getItems().clear();
                cbBairro.getItems().clear();
                cbCidade.getItems().clear();
                cbEstado.getItems().clear();
                cbPais.getItems().clear();
                Endereco en = (Endereco) endereco.get(0);
                txbCep.setText(en.getEndCep());
                cbRua.getItems().add(en);
                cbRua.getSelectionModel().select(en);
                cbBairro.getItems().add(en.getBaiId().getBaiNome());
                cbBairro.getSelectionModel().select(en.getBaiId().getBaiNome());
                cbCidade.getItems().add(en.getBaiId().getCidId().getCidNome());
                cbCidade.getSelectionModel().select(en.getBaiId().getCidId().getCidNome());
                cbEstado.getItems().add(en.getBaiId().getCidId().getEstId().getEstNome());
                cbEstado.getSelectionModel().select(en.getBaiId().getCidId().getEstId().getEstNome());
                cbPais.getItems().add(en.getBaiId().getCidId().getEstId().getPaisId().getPaisNome());
                cbPais.getSelectionModel().select(en.getBaiId().getCidId().getEstId()
                        .getPaisId().getPaisNome());
            } else {
                setErro(lblErroCep, "Cep Não Encontrado");
                txbCep.setText("");
                txbCep.requestFocus();
            }
        } else {
            setErro(lblErroCep, "Cep Inválido");
            txbCep.setText("");
            txbCep.requestFocus();
        }
    }

    public static TelaEnderecoController Create(Class classe, Pane pndados) {
        TelaEnderecoController CEndereco = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Endereco/Interface/TelaEndereco.fxml"));
            Parent root = Loader.load();
            CEndereco = (TelaEnderecoController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CEndereco;
    }

    @Deprecated
    public Object getEndereco() {
        String Cep = txbCep.getText();
        Object oEndereco = null;
        if (Cep != null && !Cep.isEmpty()) {
            ArrayList<Object> Ceps = CtrlEndereco.create().Pesquisar(Cep);
            if (Ceps != null && !Ceps.isEmpty()) {
                oEndereco = Ceps.get(0);
            }
        }
        return oEndereco;
    }

    @Deprecated
    public void setEndereco(Object oEndereco) {
        CtrlEndereco.setCampos(oEndereco, txbCep, cbRua, cbBairro, cbCidade, cbEstado, cbPais);
        Endereco = oEndereco;
    }

    public Object getComplementoEndereco() {
        if (EOriginal && Endereco != null && Complemento != null) {
            EOriginal = CtrlComplementoendereco.Compara(Complemento, Endereco,
                    cbComplemento.getText(), cbNumero.getText());

        }
        if (!EOriginal) {
            Complemento = null;
        }
        if (Endereco != null && Complemento == null) {
            CtrlComplementoendereco ce = CtrlComplementoendereco.create();
            ArrayList<Object> Complementos = ce.Pesquisar(CtrlEndereco.getCep(Endereco)
                    + "#:#" + cbNumero.getText() + "#:#"
                    + cbComplemento.getText(), "CepNumeroComplemento");
            if (Complementos.isEmpty()) {
                Complemento = ce.Salvar(cbComplemento.getText(), cbNumero.getText(), Endereco);
            } else {
                Complemento = Complementos.get(0);
            }
        }
        return Complemento;
    }

    public void setComplementoEndereco(Object oComplementodoEndereco) {
        if (CtrlComplementoendereco.getEndereco(oComplementodoEndereco) != null) {
            CtrlComplementoendereco.setCampos(oComplementodoEndereco, txbCep, cbRua, cbBairro, cbCidade,
                    cbEstado, cbPais, cbNumero, cbComplemento);
            Complemento = oComplementodoEndereco;
            Endereco = CtrlComplementoendereco.getEndereco(oComplementodoEndereco);
            EOriginal = true;
        }
    }

}
