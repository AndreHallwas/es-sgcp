/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

import Empresa.Entidade.Empresa;
import Fornecedor.Entidade.Fornecedor;
import JPA.Dao.JPADao;
import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "complementoendereco")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Complementoendereco.findAll", query = "SELECT c FROM Complementoendereco c")
    , @NamedQuery(name = "Complementoendereco.findByEndId", query = "SELECT c FROM Complementoendereco c WHERE c.endId = :endId")
    , @NamedQuery(name = "Complementoendereco.findByEndNumero", query = "SELECT c FROM Complementoendereco c WHERE c.endNumero = :endNumero")
    , @NamedQuery(name = "Complementoendereco.findByEndComplemento", query = "SELECT c FROM Complementoendereco c WHERE c.endComplemento = :endComplemento")
    , @NamedQuery(name = "Complementoendereco.findByEndCep_Numero_Complemento", query = "SELECT c FROM Complementoendereco c WHERE c.endCep.endCep = :endCep"
            + " AND c.endNumero = :endNumero AND c.endComplemento = :endComplemento ")
    , @NamedQuery(name = "Complementoendereco.findByEndsLocal_Numero_Bairro_Complemento", query = "SELECT c FROM Complementoendereco c WHERE c.endsId.endLocal = :endLocal"
            + " AND c.endNumero = :endNumero AND c.endComplemento = :endComplemento AND c.endsId.endBairro = :endBairro ")})
public class Complementoendereco extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "end_id")
    private Integer endId;
    @Basic(optional = false)
    @Column(name = "end_numero")
    private String endNumero;
    @Column(name = "end_complemento")
    private String endComplemento;
    @JoinColumn(name = "end_cep", referencedColumnName = "end_cep")
    @ManyToOne
    private Endereco endCep;
    @JoinColumn(name = "ends_id", referencedColumnName = "ends_id")
    @ManyToOne
    private Enderecosimples endsId;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "endId")
    private Collection<Cliente> clienteCollection = new ArrayList();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "endId")
    private Collection<Usuario> usuarioCollection = new ArrayList();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "endId")
    private Collection<Fornecedor> fornecedorCollection = new ArrayList();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "endId")
    private Collection<Empresa> empresaCollection = new ArrayList();

    public Complementoendereco() {
    }

    public Complementoendereco(Integer endId) {
        this.endId = endId;
    }

    public Complementoendereco(String endNumero, String endComplemento) {
        this.endNumero = endNumero;
        this.endComplemento = endComplemento;
    }

    public Complementoendereco(Integer endId, String endNumero, String endComplemento) {
        this.endId = endId;
        this.endNumero = endNumero;
        this.endComplemento = endComplemento;
    }

    public Complementoendereco(Integer endId, String endNumero) {
        this.endId = endId;
        this.endNumero = endNumero;
    }

    public Integer getEndId() {
        return endId;
    }

    public void setEndId(Integer endId) {
        this.endId = endId;
    }

    public String getEndNumero() {
        return endNumero;
    }

    public void setEndNumero(String endNumero) {
        this.endNumero = endNumero;
    }

    public String getEndComplemento() {
        return endComplemento;
    }

    public void setEndComplemento(String endComplemento) {
        this.endComplemento = endComplemento;
    }

    @XmlTransient
    public Collection<Cliente> getClienteCollection() {
        return clienteCollection;
    }

    public void setClienteCollection(Collection<Cliente> clienteCollection) {
        this.clienteCollection = clienteCollection;
    }

    public Endereco getEndCep() {
        return endCep;
    }

    public void setEndCep(Endereco endCep) {
        this.endsId = null;
        this.endCep = endCep;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    @XmlTransient
    public Collection<Fornecedor> getFornecedorCollection() {
        return fornecedorCollection;
    }

    public void setFornecedorCollection(Collection<Fornecedor> fornecedorCollection) {
        this.fornecedorCollection = fornecedorCollection;
    }

    @XmlTransient
    public Collection<Empresa> getEmpresaCollection() {
        return empresaCollection;
    }

    public void setEmpresaCollection(Collection<Empresa> empresaCollection) {
        this.empresaCollection = empresaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (endId != null ? endId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Complementoendereco)) {
            return false;
        }
        Complementoendereco other = (Complementoendereco) object;
        if ((this.endId == null && other.endId != null) || (this.endId != null && !this.endId.equals(other.endId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return endCep != null ? endCep.toString() : endsId != null ? endsId.toString() : "Sem Endereço";
    }

    public Enderecosimples getEndsId() {
        return endsId;
    }

    public void setEndsId(Enderecosimples endsId) {
        this.endCep = null;
        this.endsId = endsId;
    }

}
