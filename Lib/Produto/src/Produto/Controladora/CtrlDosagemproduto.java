/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Controladora;

import Controladora.Base.CtrlBase;
import Produto.Entidade.Dosagemproduto;
import Transacao.Transaction;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlDosagemproduto extends CtrlBase {

    private static CtrlDosagemproduto ctrldosagemproduto;

    public static CtrlDosagemproduto create() {
        if (ctrldosagemproduto == null) {
            ctrldosagemproduto = new CtrlDosagemproduto();
        }
        return ctrldosagemproduto;
    }

    public CtrlDosagemproduto() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object dosagemproduto, TextArea taObs,
            TextField txbIntervalo, TextField txbQuantidade, TextField txbId) {
        if (dosagemproduto != null && dosagemproduto instanceof Dosagemproduto) {
            Dosagemproduto f = (Dosagemproduto) dosagemproduto;
            taObs.setText(f.getDosagemDesc());
            txbIntervalo.setText(f.getDosagemIntervalo() != null ? f.getDosagemIntervalo()+"" : "0");
            txbQuantidade.setText(f.getDosagemQuantidade()+"");
            txbId.setText(f.getDosagemprodutoId()!= null ? f.getDosagemprodutoId()+"" : "0");
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Dosagemproduto) {
            return ((Dosagemproduto) p).getDosagemprodutoId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Dosagemproduto) {
            txBusca.setText(((Dosagemproduto) p).getDosagemDesc());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> dosagemproduto = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Dosagemproduto> ResultDosagemproduto;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultDosagemproduto = em.createNamedQuery("Dosagemproduto.findAll", Dosagemproduto.class)
                        .getResultList();
            } else {
                try {
                    ResultDosagemproduto = em.createNamedQuery("Dosagemproduto.findByDosagemprodutoId", Dosagemproduto.class)
                            .setParameter("dosagemprodutoId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultDosagemproduto = em.createNamedQuery("Dosagemproduto.findByDosagemDesc", Dosagemproduto.class)
                            .setParameter("dosagemDesc", Filtro).getResultList();
                }
            }
            for (Dosagemproduto dosagemprodutos : ResultDosagemproduto) {
                dosagemproduto.add(dosagemprodutos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return dosagemproduto;
    }

    public Object Salvar(String Descricao, BigDecimal Quantidade, BigDecimal Intervalo) {
        Dosagemproduto dosagemproduto = new Dosagemproduto(Descricao, Quantidade, Intervalo);
        return super.Salvar(dosagemproduto);
    }

    public Object Alterar(Object aDosagem, Integer Id, String Descricao, BigDecimal Quantidade, BigDecimal Intervalo) {
        Dosagemproduto dosagemproduto = null;
        if(aDosagem != null && aDosagem instanceof Dosagemproduto){
            dosagemproduto = (Dosagemproduto) aDosagem;
            dosagemproduto.setDosagemDesc(Descricao);
            dosagemproduto.setDosagemQuantidade(Quantidade);
            dosagemproduto.setDosagemIntervalo(Intervalo);
            dosagemproduto = (Dosagemproduto) super.Alterar(dosagemproduto);
        }
        return dosagemproduto;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Dosagemproduto.class);
    }
}
