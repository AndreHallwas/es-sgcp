/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Atendimento.Entidade;

import Venda.Entidade.Venda;
import Agendamento.Entidade.Agendamento;
import Animal.Entidade.Animal;
import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "atendimento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Atendimento.findAll", query = "SELECT a FROM Atendimento a")
    , @NamedQuery(name = "Atendimento.findByAtendimentoId", query = "SELECT a FROM Atendimento a WHERE a.atendimentoId = :atendimentoId")
    , @NamedQuery(name = "Atendimento.findByAtendimentoDescricaoGeral", query = "SELECT a FROM Atendimento a WHERE a.atendimentoDescricaoGeral = :atendimentoDescricaoGeral")
    , @NamedQuery(name = "Atendimento.findByAtendimentoDescricaoVeterinario", query = "SELECT a FROM Atendimento a WHERE a.atendimentoDescricaoVeterinario = :atendimentoDescricaoVeterinario")
    , @NamedQuery(name = "Atendimento.findByAtendimentoSintomas", query = "SELECT a FROM Atendimento a WHERE a.atendimentoSintomas = :atendimentoSintomas")
    , @NamedQuery(name = "Atendimento.findByAtendimentoObs", query = "SELECT a FROM Atendimento a WHERE a.atendimentoObs = :atendimentoObs")
    , @NamedQuery(name = "Atendimento.findByAtendimentoData", query = "SELECT a FROM Atendimento a WHERE a.atendimentoData = :atendimentoData")
    , @NamedQuery(name = "Atendimento.findByAnimalId", query = "SELECT a FROM Atendimento a WHERE a.animalId.animalId = :animalId")
    , @NamedQuery(name = "Atendimento.findByAnimalIdAndData", query = "SELECT a FROM Atendimento a WHERE a.animalId.animalId = :animalId"
            + " and a.atendimentoData = :atendimentoData")})
public class Atendimento extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "atendimento_id")
    private Integer atendimentoId;
    @Basic(optional = false)
    @Column(name = "atendimento_descricao_geral")
    private String atendimentoDescricaoGeral;
    @Column(name = "atendimento_descricao_veterinario")
    private String atendimentoDescricaoVeterinario;
    @Column(name = "atendimento_sintomas")
    private String atendimentoSintomas;
    @Column(name = "atendimento_obs")
    private String atendimentoObs;
    @Basic(optional = false)
    @Column(name = "atendimento_data")
    @Temporal(TemporalType.DATE)
    private Date atendimentoData;
    @JoinColumn(name = "agendamento_id", referencedColumnName = "agendamento_id")
    @ManyToOne
    private Agendamento agendamentoId;
    @JoinColumn(name = "animal_id", referencedColumnName = "animal_id")
    @ManyToOne(optional = false)
    private Animal animalId;
    @OneToMany(mappedBy = "atendimentoId")
    private Collection<Venda> vendaCollection = new ArrayList();

    public Atendimento() {
    }

    public Atendimento(Integer atendimentoId) {
        this.atendimentoId = atendimentoId;
    }

    public Atendimento(String atendimentoDescricaoGeral, String atendimentoDescricaoVeterinario, String atendimentoSintomas, String atendimentoObs, Date atendimentoData) {
        this.atendimentoDescricaoGeral = atendimentoDescricaoGeral;
        this.atendimentoDescricaoVeterinario = atendimentoDescricaoVeterinario;
        this.atendimentoSintomas = atendimentoSintomas;
        this.atendimentoObs = atendimentoObs;
        this.atendimentoData = atendimentoData;
    }

    public Atendimento(Integer atendimentoId, String atendimentoDescricaoGeral, String atendimentoDescricaoVeterinario, String atendimentoSintomas, String atendimentoObs, Date atendimentoData) {
        this.atendimentoId = atendimentoId;
        this.atendimentoDescricaoGeral = atendimentoDescricaoGeral;
        this.atendimentoDescricaoVeterinario = atendimentoDescricaoVeterinario;
        this.atendimentoSintomas = atendimentoSintomas;
        this.atendimentoObs = atendimentoObs;
        this.atendimentoData = atendimentoData;
    }

    public Atendimento(Integer atendimentoId, String atendimentoDescricaoGeral, Date atendimentoData) {
        this.atendimentoId = atendimentoId;
        this.atendimentoDescricaoGeral = atendimentoDescricaoGeral;
        this.atendimentoData = atendimentoData;
    }

    public Integer getAtendimentoId() {
        return atendimentoId;
    }

    public void setAtendimentoId(Integer atendimentoId) {
        this.atendimentoId = atendimentoId;
    }

    public String getAtendimentoDescricaoGeral() {
        return atendimentoDescricaoGeral;
    }

    public void setAtendimentoDescricaoGeral(String atendimentoDescricaoGeral) {
        this.atendimentoDescricaoGeral = atendimentoDescricaoGeral;
    }

    public String getAtendimentoDescricaoVeterinario() {
        return atendimentoDescricaoVeterinario;
    }

    public void setAtendimentoDescricaoVeterinario(String atendimentoDescricaoVeterinario) {
        this.atendimentoDescricaoVeterinario = atendimentoDescricaoVeterinario;
    }

    public String getAtendimentoSintomas() {
        return atendimentoSintomas;
    }

    public void setAtendimentoSintomas(String atendimentoSintomas) {
        this.atendimentoSintomas = atendimentoSintomas;
    }

    public String getAtendimentoObs() {
        return atendimentoObs;
    }

    public void setAtendimentoObs(String atendimentoObs) {
        this.atendimentoObs = atendimentoObs;
    }

    public Date getAtendimentoData() {
        return atendimentoData;
    }

    public void setAtendimentoData(Date atendimentoData) {
        this.atendimentoData = atendimentoData;
    }

    public Agendamento getAgendamentoId() {
        return agendamentoId;
    }

    public void setAgendamentoId(Agendamento agendamentoId) {
        this.agendamentoId = agendamentoId;
    }

    public Animal getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Animal animalId) {
        this.animalId = animalId;
    }

    @XmlTransient
    public Collection<Venda> getVendaCollection() {
        return vendaCollection;
    }

    public void setVendaCollection(Collection<Venda> vendaCollection) {
        this.vendaCollection = vendaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (atendimentoId != null ? atendimentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Atendimento)) {
            return false;
        }
        Atendimento other = (Atendimento) object;
        if ((this.atendimentoId == null && other.atendimentoId != null) || (this.atendimentoId != null && !this.atendimentoId.equals(other.atendimentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.Atendimento[ atendimentoId=" + atendimentoId + " ]";
    }
    
}
