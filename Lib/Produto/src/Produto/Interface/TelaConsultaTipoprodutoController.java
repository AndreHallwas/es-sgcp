/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Produto.Controladora.CtrlTipoproduto;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaTipoprodutoController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcDescricao;
    
    private static Object tipoproduto;
    private static boolean btnPainel = true;
    @FXML
    private HBox btnGp;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("TipoNome"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("TipoDesc"));
        CarregaTabela("");
        tipoproduto = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlTipoproduto ctm = CtrlTipoproduto.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            tipoproduto = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (tipoproduto != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhum Tipo de Produto Selecionado", 2);
        }
    }

    public static TelaConsultaTipoprodutoController Create(Class classe, Pane pndados) {
        TelaConsultaTipoprodutoController CTipoproduto = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Produto/Interface/TelaConsultaTipoproduto.fxml"));
            Parent root = Loader.load();
            CTipoproduto = (TelaConsultaTipoprodutoController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CTipoproduto;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaTipoprodutoController.btnPainel = btnPainel;
    }

    public static Object getTipoproduto() {
        return tipoproduto;
    }
}
