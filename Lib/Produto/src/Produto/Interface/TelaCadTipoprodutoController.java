/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import Produto.Controladora.CtrlTipoproduto;
import Variables.Variables;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadTipoprodutoController implements Initializable, Tela {

    @FXML
    private HBox pndados;
    @FXML
    private Label lbErroNome;
    @FXML
    private Label lbErroObs;
    @FXML
    private JFXButton btnovo;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btapagar;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    @FXML
    private JFXTextField txbNome;
    @FXML
    private JFXTextArea taObs;
    private int flag;
    private Integer ID;
    private boolean EOriginal;
    private Object Instancia;
    @FXML
    private JFXButton btConsulta;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        estadoOriginal();
    }

    private void setAllErro(boolean Enome, boolean Edescricao) {
        lbErroObs.setVisible(Edescricao);
        lbErroNome.setVisible(Enome);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlTipoproduto cp = CtrlTipoproduto.create();
        ArrayList<Object> Pesquisa = cp.Pesquisar(txbNome.getText());
        if (txbNome.getText().trim().isEmpty() || !Pesquisa.isEmpty() || Pesquisa.isEmpty()) {
            if (txbNome.getText().trim().isEmpty()) {
                setErro(lbErroNome, "Campo Obrigatório Vazio");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            } else if (!Pesquisa.isEmpty() && flag == 1) {
                setErro(lbErroNome, "Nome Ja Existe");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            }
        }
        return fl;
    }

    @FXML
    private void evtNovo(ActionEvent event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(ActionEvent event) {
        CtrlTipoproduto cp = CtrlTipoproduto.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(ID);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        setAllErro(false, false);
        CtrlTipoproduto cp = CtrlTipoproduto.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, ID, txbNome.getText(), taObs.getText())) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(txbNome.getText(), taObs.getText())) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);

        clear(pndados.getChildren());
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlTipoproduto.setCampos(p, txbNome, taObs);
            ID = CtrlTipoproduto.getId(p);
        }
    }

    @FXML
    private void evtConsultaAvancada(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaTipoproduto.fxml",
                "Consulta de Tipo de Produto", null);
        if (TelaConsultaTipoprodutoController.getTipoproduto() != null) {
            EstadoConsulta(TelaConsultaTipoprodutoController.getTipoproduto());
        }
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }
}
