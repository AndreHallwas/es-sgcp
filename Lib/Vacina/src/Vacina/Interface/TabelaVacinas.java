/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacina.Interface;

import Utils.Acao.ButtonAcaoAbstract;
import Utils.DateUtils;
import Variables.Variables;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Raizen
 */
public final class TabelaVacinas{

    protected IDSeq seq = new IDSeq();
    protected ArrayList<nodeTabela> nodes;
    protected Object Data;
    protected Object Vacina;

    public TabelaVacinas() {
        Inicializa();
    }

    public TabelaVacinas(Object Data, Object Vacina) {
        this.Data = Data;
        this.Vacina = Vacina;
        Inicializa();
    }

    public void Inicializa() {
        if (nodes == null) {
            nodes = new ArrayList();
        }
    }

    public void add(Object Data, Object Vacina, String Observacoes) {
        Inicializa();
        nodes.add(new nodeTabela(seq.getNextId(), Data, Vacina, Observacoes));
    }

    public void change(int ID, Date Data, Object Vacina, String Observacoes) {
        Inicializa();
        int Pos = 0;
        while (Pos < nodes.size() && nodes.get(Pos).getId() != ID) {
            Pos++;
        }
        if (Pos < nodes.size()) {
            nodes.get(Pos).setData(Data);
            nodes.get(Pos).setVacina(Vacina);
            nodes.get(Pos).setObservacoes(Observacoes);
        }
    }

    public void remove(Integer ID) {
        Inicializa();
        int Pos = 0;
        while (Pos < nodes.size() && nodes.get(Pos).getId() != ID) {
            Pos++;
        }
        if (Pos < nodes.size()) {
            nodes.remove(Pos);
            ReordenaIndices();
        }
    }

    public ArrayList<nodeTabela> get() {
        return nodes;
    }

    public ArrayList<Object> getObject() {
        ArrayList<Object> osNodes = new ArrayList();
        osNodes.addAll(nodes);
        return osNodes;
    }

    public ArrayList<Object> getVacinas() {
        ArrayList<Object> Vacinas = new ArrayList();
        for (nodeTabela node : nodes) {
            Vacinas.add(node.getVacina());
        }
        return Vacinas;
    }

    public ArrayList<Object> getDatas() {
        ArrayList<Object> Datas = new ArrayList();
        for (nodeTabela node : nodes) {
            Datas.add(node.getData());
        }
        return Datas;
    }

    public ArrayList<Object> getObservacoes() {
        ArrayList<Object> Observacoes = new ArrayList();
        for (nodeTabela node : nodes) {
            Observacoes.add(node.getObservacoes());
        }
        return Observacoes;
    }
    
    public void Zerar(){
        nodes = new ArrayList();
    }

    private void ReordenaIndices() {
        seq.Restart();
        for (nodeTabela node : nodes) {
            node.setId(seq.getNextId());
        }
    }

    public class nodeTabela extends ButtonAcaoAbstract<nodeTabela> {

        protected int Id;
        protected Object Data;
        protected Object Vacina;
        protected String Observacoes;

        public nodeTabela(int Id, Object Data, Object Vacina, String Observacoes) {
            this.Id = Id;
            this.Data = Data;
            this.Vacina = Vacina;
            this.Observacoes = Observacoes;
        }

        /**
         * @return the Id
         */
        public int getId() {
            return Id;
        }

        /**
         * @return the Data
         */
        public Object getData() {
            return Data;
        }
        
        public Object getDataFormatada() {
            if(Data != null && Data instanceof LocalDate){
                return Variables.getData(DateUtils.asDate((LocalDate) Data));
            }
            return Data+"";
        }

        /**
         * @return the Vacina
         */
        public Object getVacina() {
            return Vacina;
        }

        /**
         * @param Id the Id to set
         */
        public void setId(int Id) {
            this.Id = Id;
        }

        /**
         * @param Data the Data to set
         */
        public void setData(Date Data) {
            this.Data = Data;
        }

        /**
         * @param Vacina the Valor to set
         */
        public void setVacina(Object Vacina) {
            this.Vacina = Vacina;
        }

        /**
         * @return the Observacoes
         */
        public String getObservacoes() {
            return Observacoes;
        }

        /**
         * @param Observacoes the Observacoes to set
         */
        public void setObservacoes(String Observacoes) {
            this.Observacoes = Observacoes;
        }

        @Override
        protected void AddNodes() {
            addNode("Button", "rem", "Remover");
        }
        
    }

    public class IDSeq {

        private int IdCurrent = 0;

        /**
         * @return the IdCurrent
         */
        public int getCurrentId() {
            return IdCurrent;
        }

        public int getNextId() {
            return ++IdCurrent;
        }
        
        public void Restart(){
            IdCurrent = 0;
        }

    }

}
