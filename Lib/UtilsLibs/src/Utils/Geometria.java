/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raizen
 */
public class Geometria {

    private int TV = 3;
    private int TN = 4;
    private double ver = 0;

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////FUNÇÕES NECESSÁRIAS/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
    void func_reqgeraldoplano(double[] v) {
        int i = 0;
        char opcao = 75, opcao1 = 75;
        v[i] = v[i + 1] = v[i + 2] = v[i + 3] = 0;
        while (opcao1 != '1') {
            i = 0;
            do {
                if (opcao1 == '2') {
                    System.console().flush();
                } else if (i > 0) {
                    System.console().flush();
                } else {
                    System.out.printf("\n");
                }
                System.out.printf("\nDigite os Componentes da Equação Geral do Plano \nNo Seguinte Formato: x + y + z + & e Pressione [[Enter]] \n\n");
                System.out.printf("\nDigite os Componentes um por um e Pressione [enter]\n");
                System.out.printf("\nDigite o %dº Componente\n", i + 1);

                if (i == 0) {
                    System.out.printf("Plano= ");
                }
                if (i == 1) {
                    System.out.printf("(%0.1fx) +  ", v[0]);
                }
                if (i == 2) {
                    System.out.printf("(%0.1fx) + (%0.1fy) + ", v[0], v[1]);
                }
                if (i == 3) {
                    System.out.printf("(%0.1fx) + (%0.1fy) + (%0.1fz) + ", v[0], v[1], v[2]);
                }

                v[i] = (double) scanf("%f");
                i++;
            } while (i < 4);
            opcao1 = 75;
            i = 0;
            while (opcao1 != '1' && opcao1 != '2') {
                System.console().flush();
                System.out.printf("\n\n\t\tPlano= (%0.1fx) + (%0.1fy) + (%0.1fz) + (%0.1f) \n\n", v[0], v[1], v[2], v[3]);
                System.out.printf("\n\t\t\t Escolha a Opção Desejada: \n");
                System.out.printf("\n\n--Pressione o botão [1] Para Prosseguir; ");
                System.out.printf("\n--Pressione o botão [2] Para Alterar os Componentes;\n\n");
                if (opcao1 != '1' && opcao1 != '2' && i > 0) {
                    System.out.printf("\n\tOpção inválida,Digite Novamente a Opção Desejada:\n");
                }
                opcao1 = new Scanner(System.in).next().charAt(0);
                i++;
            }
        }
        System.console().flush();
        System.out.printf("\n\nPlano= (%0.1fx) + (%0.1fy) + (%0.1fz) + (%0.1f) \n\n", v[0], v[1], v[2], v[3]);
    }
    
    

    void func_levetoreponto(double[] a, double[] pa, double[] b, double[] pb) {
        int i;
        System.console().flush();
        System.out.printf("\tInsira as Coordenadas do 1ªVetor\n\n");
        func_digitavetor(a);
        System.console().flush();
        System.out.printf("\tInsira as Coordenadas do Ponto\n\n");
        func_digitaponto(pa);
        System.console().flush();
        System.out.printf("\n\tX=(%0.1f , %0.1f , %0.1f) Vetor1=(%0.1f , %0.1f , %0.1f)\n\n", pa[0], pa[1], pa[2], a[0], a[1], a[2]);
        System.out.printf("\tInsira as Coordenadas do 2ªVetor\n\n");
        func_digitavetor(b);
        System.console().flush();
        System.out.printf("\tInsira as Coordenadas do Ponto\n\n");
        func_digitaponto(pb);
        System.console().flush();
        System.out.printf("\n\tX2=(%0.1f , %0.1f , %0.1f) Vetor2=(%0.1f , %0.1f , %0.1f)\n\n", pb[0], pb[1], pb[2], b[0], b[1], b[2]);
        func_prosseguir();
        System.console().flush();
        System.out.printf("\n\t X=(%0.1f , %0.1f , %0.1f) Vetor1=(%0.1f , %0.1f , %0.1f)\n", pa[0], pa[1], pa[2], a[0], a[1], a[2]);
        System.out.printf("\n\t\t\t\t e \n");
        System.out.printf("\n\t X2=(%0.1f , %0.1f , %0.1f) Vetor2=(%0.1f , %0.1f , %0.1f)\n\n", pb[0], pb[1], pb[2], b[0], b[1], b[2]);
    }

    double func_lamb(double pb, double a, double pa) {
        double lambida = 0;
        if (a == 0) {
            if (pa == pb) {
                lambida = pa;
            } else {
                ver = 1;
            }
        } else {
            lambida = (pb - pa) / a;
        }
        return lambida;
    }

    int func_pontonareta(double[] pb, double[] a, double[] pa) {
        double lambx, lamby, lambz;
        ver = 0;
        lambx = func_lamb(pb[0], a[0], pa[0]);
        lamby = func_lamb(pb[1], a[1], pa[1]);
        lambz = func_lamb(pb[2], a[2], pa[2]);
        if (lambx == lamby && lambx == lambz &ver == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    void func_Vetorial(double[] a, double[] b, double[] c) {
        double i = 0, j = 0, k = 0;
        i += a[1] * b[2];
        j += a[2] * b[0];
        k += a[0] * b[1];
        i += (a[0] * b[2]) * (-1);
        j += (a[2] * b[1]) * (-1);
        k += (a[1] * b[0]) * (-1);
        c[0] = i;
        c[1] = j;
        c[2] = k;
    }

    int func_dl(double[] v, double[] v1) {
        double[] v3 = new double[3];
        double aux = 0;
        int veri = 0;
        if (v1[0] != 0 &v[0] != 0) {
            aux = v[0] / v1[0];
            veri = 1;
        } else if (v1[1] != 0 &v[1] != 0) {
            aux = v[1] / v1[1];
            veri = 1;
        } else if (v1[2] != 0 &v[2] != 0) {
            aux = v[2] / v1[2];
            veri = 1;
        }
        v3[0] = aux * v1[0];
        v3[1] = aux * v1[1];
        v3[2] = aux * v1[2];
        if (v3[0] == v[0] &v3[1] == v[1] &v3[2] == v[2] &veri == 1) {
            //System.out.printf("\n\nÉ LD\n\n");
            return 1;
        } else {
            //System.out.printf("\n\nÉ LI\n\n");
            return 0;
        }
    }

    double func_ProdMisto(double[] a, double[] b, double[] ab) {
        double mis = 0;
        mis += (a[0] * b[1] * ab[2]);
        mis += (a[1] * b[2] * ab[0]);
        mis += (a[2] * b[0] * ab[1]);
        mis += ((ab[0] * b[1] * a[2]) * (-1));
        mis += ((ab[1] * b[2] * a[0]) * (-1));
        mis += ((ab[2] * b[0] * a[1]) * (-1));
        return mis;
    }

    void func_levet(double[] v, double[] pv) {
        int i;
        System.console().flush();
        System.out.printf("\tInsira os Valores do Vetor Diretor da Reta: \n\n");
        func_digitavetor(v);
        System.console().flush();
        System.out.printf("\tInsira os Valores do Ponto da Reta: \n\n");
        func_digitaponto(pv);
        System.console().flush();
        System.out.printf("\n\tX=(%0.1f , %0.1f , %0.1f) Vetor1=(%0.1f , %0.1f , %0.1f)\n\n", pv[0], pv[1], pv[2], v[0], v[1], v[2]);
    }

    double func_ProdEScalar(double[] a, double[] b) {
        double pr;
        pr = (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]);
        return pr;
    }

    int func_pontonaEqGeral(double[] a, double[] b) {
        ver = a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + b[TN];
        if (ver == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    double divisao(double a, double b) {
        double r;
        r = a / b;
        return r;
    }

    int func_LDouLIplan(double[] N1, double[] N2) {
        double[] v3 = new double[TN];
        v3[0] = divisao(N1[0], N2[0]);
        v3[1] = divisao(N1[1], N2[1]);
        v3[2] = divisao(N1[2], N2[2]);
        v3[3] = divisao(N1[3], N2[3]);
        if (v3[0] == v3[1] &v3[1] == v3[2] &v3[2] == v3[3]) {
            return 45;
        } else if (v3[0] == v3[1] &v3[1] == v3[2] &v3[2] != v3[3]) {
            return 47;
        } else if (v3[0] != v3[1] || v3[1] != v3[2] || v3[2] != v3[1]) {
            return 49;
        }
        return 0;
    }

    void func_sair() {
        System.out.printf("\n\n Pressione [Enter] PARA SAIR\n\n");
        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(Geometria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void func_prosseguir() {
        System.out.printf("\n\n Pressione [Enter] PARA Prosseguir\n\n");
        try {            Thread.sleep(500);        } catch (InterruptedException ex) {            Logger.getLogger(Geometria.class.getName()).log(Level.SEVERE, null, ex);        }
    }

    double func_modulo(double[] v) {
        double m;
        m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2) + Math.pow(v[2], 2));
        return m;
    }

    double func_det(double[][] m) {
        double det;
        det = +((m[0][0] * m[1][1] * m[2][2]) + (m[0][1] * m[1][2] * m[2][0]) + (m[0][2] * m[1][0] * m[2][1])) - ((m[2][0] * m[1][1] * m[0][2]) + (m[2][1] * m[1][2] * m[0][0]) + (m[2][2] * m[1][0] * m[0][1]));
        System.out.printf("\n O Determinante é :%0.1f \n", det);
        return det;
    }

    void func_pescalar(double[] v, double[] v1) {
        if ((v[0] * v1[0] + v[1] * v1[1] + v[2] * v1[2]) == 0) {
            System.out.printf("\n São Ortogonais!!\n");
        } else {
            System.out.printf("\n Não são Ortogonais!!\n");
        }
    }

    void func_digitaponto(double[] v) {
        int i = 0;
        char opcao = 75, opcao1 = 75;
        v[i] = v[i + 1] = v[i + 2] = 0;
        while (opcao1 != '1') {
            i = 0;
            do {
                if (opcao1 == '2') {
                    System.console().flush();
                } else if (i > 0) {
                    System.console().flush();
                } else {
                    System.out.printf("\n");
                }
                System.out.printf("\nDigite os Componentes no seguinte formato: (x , y , z) e pressione [Enter] \n\n");
                System.out.printf("\nDigite os Componentes um por um e Pressione [enter]\n");
                System.out.printf("\nDigite o %dº Componente\n", i + 1);

                if (i == 0) {
                    System.out.printf("(");
                }
                if (i == 1) {
                    System.out.printf("(%0.1f ,", v[0]);
                }
                if (i == 2) {
                    System.out.printf("(%0.1f , %0.1f ,", v[0], v[1]);
                }

                v[i] = (double) scanf("%f");
                i++;
            } while (i < TV);
            opcao1 = 75;
            i = 0;
            while (opcao1 != '1' && opcao1 != '2') {
                System.console().flush();
                System.out.printf("\n\n\t\t\tComponentes= (%0.1f , %0.1f ,%0.1f)\n\n", v[0], v[1], v[2]);
                System.out.printf("\n\t\t\t Escolha a Opção Desejada: \n");
                System.out.printf("\n\n--Pressione o botão [1] Para Prosseguir; ");
                System.out.printf("\n--Pressione o botão [2] Para Alterar os Componentes;\n\n");
                if (opcao1 != '1' && opcao1 != '2' && i > 0) {
                    System.out.printf("\n\tOpção inválida,Digite Novamente a Opção Desejada:\n");
                }
                
                opcao1 = new Scanner(System.in).next().charAt(0);
                i++;
            }
        }
        System.console().flush();
        System.out.printf("\n\nComponentes= (%0.1f , %0.1f ,%0.1f)\n\n", v[0], v[1], v[2]);
    }

    void func_digitavetor(double[] v) {
        int i = 0, z = 0;
        char opcao = 75, opcao1 = 75;
        v[i] = v[i + 1] = v[i + 2] = 0;
        while (opcao1 != '1') {
            i = 0;
            do {
                if (opcao1 == '2') {
                    System.console().flush();
                } else if (i > 0) {
                    System.console().flush();
                } else {
                    System.out.printf("\n");
                }
                System.out.printf("\nDigite os Componentes no seguinte formato: (x , y , z) e pressione [Enter] \n\n");
                System.out.printf("\nDigite os Componentes um por um e Pressione [enter]\n");
                System.out.printf("\nDigite o %dº Componente\n", i + 1);

                if (i == 0) {
                    System.out.printf("(");
                }
                if (i == 1) {
                    System.out.printf("(%0.1f ,", v[0]);
                }
                if (i == 2) {
                    System.out.printf("(%0.1f , %0.1f ,", v[0], v[1]);
                }

                v[i] = (double) scanf("%f");
                i++;
            } while (i < TV);
            opcao1 = 75;
            i = 0;
            while (opcao1 != '1' && opcao1 != '2') {
                System.console().flush();
                System.out.printf("\n\n\t\t\tComponentes= (%0.1f , %0.1f ,%0.1f)\n\n", v[0], v[1], v[2]);
                System.out.printf("\n\t\t\t Escolha a Opção Desejada: \n");
                System.out.printf("\n\n--Pressione o botão [1] Para Prosseguir; ");
                System.out.printf("\n--Pressione o botão [2] Para Alterar os Componentes;\n\n");
                if (v[0] == 0 &v[1] == 0 &v[2] == 0) {
                    System.out.printf("\n\tOBS:Voçê Digitou um Vetor Nulo,Pessione [2] para alterar,ou continue\n");
                }
                if (opcao1 != '1' && opcao1 != '2' && i > 0) {
                    System.out.printf("\n\tOpção inválida,Digite Novamente a Opção Desejada:\n");
                }
                
                opcao1 = new Scanner(System.in).next().charAt(0);
                i++;
            }
        }
        System.console().flush();
        System.out.printf("\n\nComponentes= (%0.1f , %0.1f ,%0.1f)\n\n", v[0], v[1], v[2]);
    }

    double func_escalar(double[] v, double[] v1) {
        double e;
        e = (v[0] * v1[0] + v[1] * v1[1] + v[2] * v1[2]);
        return e;
    }

    void func_vetdiretor(double[] p, double[] p1, double[] vd) {
        int i = 0;
        for (i = 0; i < 3; i++) {
            vd[i] = p1[i] - p[i];
        }
    }

    void func_vetnormal(double[] v, double[] v1, double[] vn) {
        double[][] m = new double[3][3];
        int i;
        for (i = 0; i < 3; i++) {
            m[0][i] = v[i];
            m[1][i] = v1[i];
        }
        //////////////Produto Vetorial/////////////
        vn[0] = +(m[0][1] * m[1][2]) - (m[0][2] * m[1][1]);
        vn[1] = +(m[0][2] * m[1][0]) - (m[1][2] * m[0][0]);
        vn[2] = +(m[0][0] * m[1][1]) - (m[1][0] * m[0][1]);
    }

    double func_vetdir(double[] vn) {/////
        double[] p, p1;
        p = new double[TV];
        p1 = new double[TV];
        System.out.printf("\n Digite os Valores dos Pontos :\n");
        System.out.printf("\n Digite os valores do ponto 1\n");
        func_digitavetor(p);
        System.out.printf("\n Digite os valores do ponto 2\n");
        func_digitavetor(p1);
        func_vetdiretor(p, p1, vn);
        return 0;
    }

    double func_vetnor(double[] vn) {/////
        double[] p, p1;
        p = new double[TV];
        p1 = new double[TV];
        System.out.printf("\n Digite os Valores dos vetores diretores do plano :\n");
        System.out.printf("\n Digite os valores do vetor diretor 1\n");
        func_digitavetor(p);
        System.out.printf("\n Digite os valores do vetor diretor 2\n");
        func_digitavetor(p1);
        func_vetnormal(p, p1, vn);
        return 0;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////MENUS///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    char menu_main() {
        int i = 0;
        char o = 0;
        while (o != '1' && o != '2' && o != '3' && o != 27) {
            System.console().flush();
            System.out.printf("\t\tPressione o número da Opção Escolhida\n\n");
            System.out.printf("\t\t\t[ESC] Para Sair\n");
            System.out.printf("\t\t\t[1] - Posição Relativa\n");
            System.out.printf("\t\t\t[2] - Ângulo\n");
            System.out.printf("\t\t\t[3] - Distância\n");
            if (o != '1' && o != '2' && o != '3' && o != 27 && i > 0) {
                System.out.printf("\n\tOpção inválida,Digite Novamente a Opção Desejada:\n");
            }
            o = new Scanner(System.in).next().charAt(0);
            i++;
        }
        System.console().flush();
        return o;
    }

    char menu_rp() {
        int i = 0;
        char o = 0;
        while (o != '1' && o != '2' && o != '3' && o != 27) {
            System.console().flush();
            System.out.printf("\t\tPressione o número da Opção Escolhida\n\n");
            System.out.printf("\t\t\t[ESC] Para Sair\n");
            System.out.printf("\t\t\t[1] - Entre 2 Retas\n");
            System.out.printf("\t\t\t[2] - Entre 1 Reta e 1 Plano\n");
            System.out.printf("\t\t\t[3] - Entre 2 Planos\n");
            if (o != '1' && o != '2' && o != '3' && o != 27 && i > 0) {
                System.out.printf("\n\tOpção inválida,Digite Novamente a Opção Desejada:\n");
            }
            o = new Scanner(System.in).next().charAt(0);
            i++;
        }
        System.console().flush();
        return o;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////iNTEGRAÇÃO DE MENUS/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    void executa_geral() {
        char o, o2;
        double[] v1, v2;
        do {
            
            o = menu_main();
            switch (o) {
                case '1':
                    
                    o2 = menu_rp();
                    switch (o2) {
                        case '1':
                            double[] p1,
                             p2;
                            v1 = new double[3];
                            p1 = new double[3];
                            v2 = new double[3];
                            p2 = new double[3];
                            func_levetoreponto(v1, p1, v2, p2);
                            main_posrelativa_rr(v1, p1, v2, p2);//posição relativa entre retas
                            break;

                        case '2':
                            double[] v,
                             pv,
                             N;
                            v = new double[3];
                            pv = new double[3];
                            N = new double[TN];
                            func_reqgeraldoplano(N);
                            func_levet(v, pv);
                            main_posrelativa_rp(v, pv, N);
                            break;

                        case '3':
                            double[] N1,
                             N2;
                            N1 = new double[TN];
                            N2 = new double[TN];
                            System.console().flush();
                            System.out.printf("\t\t1ªPlano\n\n");
                            func_reqgeraldoplano(N1);
                            System.out.printf("\t\t2ªPlano\n\n");
                            func_reqgeraldoplano(N2);
                            main_posrelativa_pp(N1, N2);
                            break;

                        case 27:
                            System.console().flush();
                            break;
                    }
                    break;

                case '2':
                    
                    o2 = menu_rp();
                    switch (o2) {
                        case '1':
                            main_angulo_rr();
                            break;

                        case '2':
                            main_angulo_rp();
                            break;

                        case '3':
                            main_angulo_pp();
                            break;
                        case 27:
                            System.console().flush();
                            break;
                    }
                    break;

                case '3':
                    
                    o2 = menu_rp();
                    switch (o2) {
                        case '1':
                            double[] p1,
                             p2;
                            double escalar;
                            v1 = new double[3];
                            p1 = new double[3];
                            v2 = new double[3];
                            p2 = new double[3];
                            func_levetoreponto(v1, p1, v2, p2);
                            main_distancia_rr(v1, p1, v2, p2);
                            break;

                        case '2':
                            main_distancia_rp();
                            break;

                        case '3':
                            main_distancia_pp();
                            break;

                        case 27:
                            System.console().flush();
                            break;
                    }

                case 27:
                    System.console().flush();
                    break;

            }

        } while (o != 27);
    }

    int main() {
        executa_geral();
        return 0;
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////POSIÇÃO RELATIVA////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
    void main_posrelativa_rr(double[] a, double[] pa, double[] b, double[] pb) {
        double[] ab;
        ab = new double[3];
        if (func_dl(a, b) == 1) {
            if (func_pontonareta(pb, a, pa) == 1) {
                System.out.printf("As Retas R e S São Iguais!!!\n\n");
            } else {
                System.out.printf("As Retas R e S São Paralelas!!!\n\n");
            }
        } else {
            func_vetdiretor(pa, pb, ab);
            if (func_ProdMisto(a, b, ab) == 0) {
                System.out.printf("As Retas R e S São Concorrentes!!!\n\n");
            } else {
                System.out.printf("As Retas R e S São Reversas!!!\n\n");
            }
        }
        func_sair();
    }

    void main_posrelativa_rp(double[] v, double[] pv, double[] N) {
        if (func_ProdEScalar(v, N) != 0) {
            System.out.printf("A reta é Transversal ao Plano\n\n");
        } else if (func_pontonaEqGeral(pv, N) == 1) {
            System.out.printf("A Reta Esta Contida no Plano\n\n");
        } else {
            System.out.printf("A reta é Paralela ao Plano\n\n");
        }
        func_sair();
    }

    void main_posrelativa_pp(double[] N1, double[] N2) {
        double[] v3;
        int d;
        d = func_LDouLIplan(N1, N2);
        if (d == 45) {
            System.out.printf("O 1ªPlano é Igual ao 2ªPlano\n\n");
        }
        if (d == 47) {
            System.out.printf("O 1ªPlano é Paralelo ao 2ªPlano\n\n");
        }
        if (d == 49) {
            System.out.printf("O 1ªPlano é Transversal ao 2ªPlano\n\n");
        }
        func_sair();
    }

    int main_aut_posrelativa_rr(double[] a, double[] pa, double[] b, double[] pb) {
        double[] ab;
        ab = new double[3];
        if (func_dl(a, b) == 1) {
            if (func_pontonareta(pb, a, pa) == 1) {
                return 61;
            } else {
                return 65;
            }
        } else {
            func_vetdiretor(pa, pb, ab);
            if (func_ProdMisto(a, b, ab) == 0) {
                return 67;
            } else {
                return 69;
            }
        }
    }

    int main_aut_posrelativa_rp(double[] v, double[] pv, double[] N) {
        if (func_ProdEScalar(v, N) != 0) {
            return 55;
        } else if (func_pontonaEqGeral(pv, N) == 1) {
            return 57;
        } else {
            return 59;
        }
    }

    int main_aut_posrelativa_pp(double[] N1, double[] N2) {
        double[] v3;
        v3 = new double[TN];
        v3[0] = divisao(N1[0], N2[0]);
        v3[1] = divisao(N1[1], N2[1]);
        v3[2] = divisao(N1[2], N2[2]);
        v3[3] = divisao(N1[3], N2[3]);
        if (v3[0] == v3[1] &v3[1] == v3[2] &v3[2] == v3[3]) {
            return 45;
        } else if (v3[0] == v3[1] &v3[1] == v3[2] &v3[2] != v3[3]) {
            return 47;
        } else if (v3[0] != v3[1] || v3[1] != v3[2] || v3[2] != v3[1]) {
            return 49;
        }
        return 0;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////DISTÂNCIA////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    void main_distancia_rr(double[] v1, double[] p1, double[] v2, double[] p2) {
        int i = 0;
        double[] v1Xv2, ab, v3;
        double escalar;
        v1Xv2 = new double[TV];
        ab = new double[TV];
        v3 = new double[TV];
        i = main_aut_posrelativa_rr(v1, p1, v2, p2);
        if (i == 61 || i == 65) {
            main_aut_distancia_por(p2, p1, v2);
        } else if (i == 69) {
            func_Vetorial(v1, v2, v1Xv2);
            main_aut_distancia_pop(p1, p2, v1Xv2);
        } else if (i == 67) {
            System.out.printf("\n\tDISTÂNCIA= 0\n");
        }
        func_sair();
    }

    void main_distancia_rp() {
        double[] v, v1, p, p1, vn, vd, vd1, vn1;
        v = new double[TV];
        v1 = new double[TV];
        p = new double[TV];
        p1 = new double[TV];
        vn = new double[TV];
        vd = new double[TV];
        vd1 = new double[TV];
        vn1 = new double[TV];
        int i = 0, j = 0, z = 0;
        System.out.printf("\nDigite um ponto do plano\n");
        func_digitaponto(p1);
        System.out.printf("\nDigite a equação geral do plano\n");
        func_reqgeraldoplano(vn);
        System.out.printf("\nDigite um ponto da reta\n");
        func_digitaponto(p);
        System.out.printf("\nDigite o vetor diretor da reta\n");
        func_digitavetor(vd);
        i = main_aut_posrelativa_rp(vd, p, vn);
        System.console().flush();
        System.out.printf("\n Distância entre a reta= (%0.1f,%0.1f,%0.1f) ", vd[0], vd[1], vd[2]);
        System.out.printf(" e o plano= ");
        for (z = 0; z < 4; z++) {
            if (vn[z] >= 0) {
                System.out.printf("+%0.1f", vn[z]);
            } else {
                System.out.printf("%0.1f", vn[z]);
            }
        }
        System.out.printf(" é:\n");
        if (i == 55 || i == 57) {
            System.out.printf("\n\tDISTÂNCIA= 0\n");
        } else if (i == 59) {
            for (j = 0; j < TV; j++) {
                vn1[j] = vn[j];
            }
            main_aut_distancia_pop(p, p1, vn1);
        }
        func_sair();
    }

    void main_distancia_pp() {
        double[] p, p1, vn, va, va1, vn1;
        p = new double[TV];
        p1 = new double[TV];
        vn = new double[TV];
        va = new double[TN];
        va1 = new double[TN];
        vn1 = new double[TV];
        int i = 0, j = 0, z = 0;
        System.out.printf("\nDigite um ponto do primeiro plano\n");
        func_digitaponto(p);
        System.out.printf("\nDigite a equação geral do plano\n");
        func_reqgeraldoplano(va);
        System.out.printf("\nDigite um ponto do outro plano\n");
        func_digitaponto(p1);
        System.out.printf("\nDigite a equação geral do outro plano\n");
        func_reqgeraldoplano(va1);
        i = main_aut_posrelativa_pp(va, va1);
        for (j = 0; j < TV; j++) {
            vn[j] = va[j];
            vn1[j] = va1[j];
        }
        System.console().flush();
        System.out.printf("\nDistância entre o plano 1= ");
        for (z = 0; z < 4; z++) {
            if (va[z] >= 0) {
                System.out.printf("+%0.1f", va[z]);
            } else {
                System.out.printf("%0.1f", va[z]);
            }
        }
        System.out.printf(" e o plano 2= ");
        for (z = 0; z < 4; z++) {
            if (va1[z] >= 0) {
                System.out.printf("+%0.1f", va1[z]);
            } else {
                System.out.printf("%0.1f", va1[z]);
            }
        }
        System.out.printf(" é:\n");
        if (i == 45 || i == 49) {//problemas
            System.out.printf("\n\tDISTÂNCIA= 0\n");
        } else if (i == 47) {
            main_aut_distancia_pop(p, p1, vn1);
        }
        func_sair();
    }

    void main_aut_distancia_por(double[] p, double[] p1, double[] vd1) {
        double[] vdp, vd2;
        vdp = new double[TV];
        vd2 = new double[TV];
        double aux, aux1;
        func_vetdiretor(p1, p, vdp);//talves de erro
        func_vetnormal(vdp, vd1, vd2);//produto vetorial
        aux = func_modulo(vd2);
        if (aux < 0) {
            aux = aux * (-1);
        }
        aux1 = func_modulo(vd1);
        if (aux1 < 0) {
            aux1 = aux1 * (-1);
        }
        System.out.printf("\n A DISTANCIA É %f\n", aux / aux1);
    }

    void main_aut_distancia_pop(double[] p, double[] p1, double[] vn) {
        double aux = 0;
        double[] vd1 = new double[TV];
        func_vetdiretor(p1, p, vd1);
        aux = func_escalar(vd1, vn);
        if (aux < 0) {
            aux = aux * (-1);
        }
        System.out.printf("\n A DISTANCIA É %f\n", aux / func_modulo(vn));
    }
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////ÂNGULOS/////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

    void main_angulo_rr() {
        int opcao;
        double[] v, v1;
        double aux = 0;
        v = new double[TV];
        v1 = new double[TV];
        System.out.printf("\n Forneça os vetores diretores\n");
        System.out.printf("\n Digite os valores do vetor 1\n");
        func_digitavetor(v);
        System.out.printf("\n Digite os valores do vetor 2\n");
        func_digitavetor(v1);
        aux = func_escalar(v, v1) / (func_modulo(v) * func_modulo(v1));
        if (aux < 0) {
            aux = aux * (-1);
        }
        System.console().flush();
        System.out.printf("\n\tÂngulo entre a Reta 1=(%0.1f,%0.1f,%0.1f) e a Reta 2=(%0.1f,%0.1f,%0.1f) é:\n", v[0], v[1], v[2], v1[0], v1[1], v1[2]);
        System.out.printf("\n\n O= COS-¹ %f RAD", aux);
        func_sair();
    }

    void main_angulo_rp() {
        int opcao;
        double[] v, v1;
        double aux = 0;
        v = new double[TV];
        v1 = new double[TV];
        System.out.printf("\n Forneça o vetor diretor\n");
        System.out.printf("\n Digite os valores do vetor diretor da reta\n");
        func_digitavetor(v);
        System.out.printf("\n Forneça o vetor normal\n");
        System.out.printf("\n Digite os valores do vetor normal ao plano\n");
        func_digitavetor(v1);
        aux = func_escalar(v, v1) / (func_modulo(v) * func_modulo(v1));
        if (aux < 0) {
            aux = aux * (-1);
        }
        System.console().flush();
        System.out.printf("\n\tÂngulo entre a Reta =(%0.1f,%0.1f,%0.1f) e o Plano =(%0.1f,%0.1f,%0.1f) é:\n", v[0], v[1], v[2], v1[0], v1[1], v1[2]);
        System.out.printf("\n\n O= SIN-¹ %f ", aux);
        func_sair();
    }

    void main_angulo_pp() {
        int opcao;
        double[] v, v1;
        double aux = 0;
        v = new double[TV];
        v1 = new double[TV];
        System.out.printf("\n Forneça os vetores normais\n");
        System.out.printf("\n Digite os valores do vetor do plano 1\n");
        func_digitavetor(v);
        System.out.printf("\n Digite os valores do vetor do plano 2\n");
        func_digitavetor(v1);
        aux = func_escalar(v, v1) / (func_modulo(v) * func_modulo(v1));
        if (aux < 0) {
            aux = aux * (-1);
        }
        System.console().flush();
        System.out.printf("\n\tÂngulo entre o plano 1=(%0.1f,%0.1f,%0.1f) e o plano 2=(%0.1f,%0.1f,%0.1f) é:\n", v[0], v[1], v[2], v1[0], v1[1], v1[2]);
        System.out.printf("\n\n\t\t\t O= COS-¹ %f rad", aux);
        func_sair();
    }

    private Object scanf(String f) {
        return f.equals("%f") ? new Scanner(System.in).nextDouble() : (Object)new Scanner(System.in).next();
    }

}
