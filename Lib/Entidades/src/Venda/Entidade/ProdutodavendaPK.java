/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Entidade;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Raizen
 */
@Embeddable
public class ProdutodavendaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "venda_id")
    private int vendaId;
    @Basic(optional = false)
    @Column(name = "produto_id")
    private int produtoId;

    public ProdutodavendaPK() {
    }

    public ProdutodavendaPK(int vendaId, int produtoId) {
        this.vendaId = vendaId;
        this.produtoId = produtoId;
    }

    public int getVendaId() {
        return vendaId;
    }

    public void setVendaId(int vendaId) {
        this.vendaId = vendaId;
    }

    public int getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(int produtoId) {
        this.produtoId = produtoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) vendaId;
        hash += (int) produtoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProdutodavendaPK)) {
            return false;
        }
        ProdutodavendaPK other = (ProdutodavendaPK) object;
        if (this.vendaId != other.vendaId) {
            return false;
        }
        if (this.produtoId != other.produtoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.ProdutodavendaPK[ vendaId=" + vendaId + ", produtoId=" + produtoId + " ]";
    }
    
}
