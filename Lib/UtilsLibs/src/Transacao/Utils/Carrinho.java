/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao.Utils;

import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class Carrinho {

    private ArrayList<ItemCarrinho> Items;
    private float ValorTotal;
    private int QuantidadeTotal;
    private final IDSeq Id = new IDSeq();

    public double GeraValorTotal() {
        double ValorAux = 0;
        for (ItemCarrinho Item : Items) {
            ValorAux = ValorAux + (Item.getoItem().getValor() * Item.getQuantidade());
        }
        return ValorTotal = (float) ValorAux;
    }
    
    public int GeraQuantidadeTotal(){
        int ValorAux = 0;
        for (ItemCarrinho Item : Items) {
            ValorAux = ValorAux + Item.getQuantidade();
        }
        return QuantidadeTotal = ValorAux;
    }

    public boolean add(Item oItem, int Quantidade) {
        if (inicializa()) {
            int Pos = find(oItem);
            ItemCarrinho Item;
            if (Pos == -1) {
                Item = new ItemCarrinho(oItem, Quantidade, Id.getNextId());
                Items.add(Item);
            } else {
                Item = Items.get(Pos);
                Item.setQuantidade(Item.getQuantidade() + Quantidade);
                /**
                 * A Implementar Controle de Quantidade
                 */
            }
            return true;
        }
        return false;
    }

    public void remove(Item oItem) {
        if (inicializa()) {
            int Pos = find(oItem);
            if (Pos != -1) {
                Items.remove(Pos);
            }
        }
    }

    public void remove(ItemCarrinho Item) {
        if (inicializa()) {
            int Pos = find(Item.getId());
            if (Pos != -1) {
                Items.remove(Pos);
            }
        }
    }

    public void remove(int Id) {
        if (inicializa()) {
            int Pos = find(Id);
            if (Pos != -1) {
                Items.remove(Pos);
            }
        }
    }

    public void change(Item oItem, int Quantidade) {
        if (inicializa()) {
            int Pos = find(oItem);
            ItemCarrinho Item;
            if (Pos != -1) {
                Item = Items.get(Pos);
                Item.setQuantidade(Quantidade);
            }
        }
    }

    public void change(ItemCarrinho Item) {
        if (inicializa()) {
            int Pos = find(Item.getId());
            int Pos2 = find(Item.getoItem());
            if (Pos != -1) {
                if (Pos == Pos2 || Pos2 == -1) {
                    Item = Items.set(Pos, Item);
                }else{
                    Item = Items.set(Pos2, Item);
                    Items.remove(Pos);
                }
            }
        }
    }

    public void change(int Id, int Quantidade, Item oItem) {
        if (inicializa()) {
            ItemCarrinho Item;
            int Pos = find(Id);
            if (Pos != -1) {
                Item = Items.get(Pos);
                Item.setQuantidade(Quantidade);
                Item.setoItem(oItem);
            }
        }
    }

    private int find(int Id) {

        if (inicializa()) {
            int i = 0;
            while (i < Items.size() && Items.get(i).getId() != Id) {
                i++;
            }
            return i < Items.size() ? i : -1;
        }
        return -1;
    }

    public int find(Item oItem) {
        if (inicializa()) {
            int i = 0;
            while (i < Items.size() && !Items.get(i).getoItem().Compara(oItem)) {
                i++;
            }
            return i < Items.size() ? i : -1;
        }
        return -1;
    }

    private boolean inicializa() {
        if (Items == null) {
            Items = new ArrayList();
        }
        return !(Items == null);
    }

    /**
     * @return the Items
     */
    public ArrayList<ItemCarrinho> getItems() {
        ArrayList<ItemCarrinho> Aux = new ArrayList();
        Aux.addAll(Items);
        return Aux;
    }

    /**
     * @return the ValorTotal
     */
    public float getValorTotal() {
        return ValorTotal;
    }

    public boolean Vazio() {
        return inicializa() && !Items.isEmpty();
    }

    public void Limpar() {
        Items = null;
        inicializa();
    }

    public class IDSeq {

        private int IdCurrent = 0;

        /**
         * @return the IdCurrent
         */
        public int getCurrentId() {
            return IdCurrent;
        }

        public int getNextId() {
            return ++IdCurrent;
        }

    }

}
