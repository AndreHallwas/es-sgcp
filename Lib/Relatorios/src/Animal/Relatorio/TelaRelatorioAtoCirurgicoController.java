/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Relatorio;

import Animal.Controladora.CtrlAnimal;
import Pessoa.Controladora.CtrlCliente;
import Pessoa.Interface.TelaConsultaClienteController;
import Transacao.Transaction;
import Utils.GerarRelatorios;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaRelatorioAtoCirurgicoController implements Initializable {

    @FXML
    private JFXTextField txbusca;
    @FXML
    private JFXComboBox<Object> cbAnimal;
    @FXML
    private ScrollPane spdado;
    @FXML
    private HBox dado;
    @FXML
    private JFXButton btnBuscar;
    
    private static final String Caminho = "Relatorios/Petshop/AtoCirurgico.jasper";
    private static final String Tela = "Termo de Responsabilidade para Ato Cirurgico";
    private SwingNode sn;
    private Object oCliente;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnBuscar.setVisible(false);/*Busca Simples*/
    }    


    @FXML
    private void btnTelaCheia(MouseEvent event) {
        try
        {
            if (!txbusca.getText().isEmpty())
                GerarRelatorios.gerarRelatorio(SqlFiltro(), Caminho, Tela, Transaction.getEntityManager());
            else
                GerarRelatorios.gerarRelatorio(SqlDefault(), Caminho, Tela, Transaction.getEntityManager());
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Não Há Dados Eminentes Nesta Consulta");
            txbusca.setText("");
            Default();
        }
    }

    @FXML
    private void evtBuscar(MouseEvent event) {
        if (!txbusca.getText().isEmpty()){
            setRelatorio(SqlFiltro(), Caminho);
        } else{
            Default();
        }
    }

    private void Default()
    {
        setRelatorio(SqlDefault(), Caminho);
    }

    private void setRelatorio(String Sql, String Caminho) {
        try
        {
            sn = GerarRelatorios.gerarRelatorio(Sql, Caminho, Transaction.getEntityManager());
            dado.getChildren().clear();
            dado.getChildren().add(sn);
        } catch (Exception ex)
        {
            Mensagem.ExibirException(ex, "Não Há Dados Resultantes Desta Consulta");
            txbusca.setText("");
            Default();
        }
    }
    
    private String SqlDefault(){
        return "SELECT * FROM animal\n" +
                " inner join cliente ON animal.cli_id = cliente.cli_id\n" +
                " inner join complementoendereco on cliente.end_id = complementoendereco.end_id\n" +
                " inner join endereco on complementoendereco.end_cep = endereco.end_cep\n" +
                " inner join especie on especie.especie_id = animal.especie_id\n" +
                " inner join pelagem on pelagem.pelagem_id = animal.pelagem_id\n" +
                " inner join raca on raca.raca_id = animal.raca_id";
    }
    private String SqlFiltro(){
        String Animal = CtrlAnimal.getId(cbAnimal.getSelectionModel().getSelectedItem()).toString();
        String Cliente = CtrlCliente.getID(oCliente).toString();
        return "SELECT * FROM "+ 
                "(select * from animal where animal.cli_id = "+Cliente+" and animal.animal_id = "+Animal+") as animal\n" +
                " inner join cliente ON animal.cli_id = cliente.cli_id\n" +
                " inner join complementoendereco on cliente.end_id = complementoendereco.end_id\n" +
                " inner join endereco on complementoendereco.end_cep = endereco.end_cep\n" +
                " inner join especie on especie.especie_id = animal.especie_id\n" +
                " inner join pelagem on pelagem.pelagem_id = animal.pelagem_id\n" +
                " inner join raca on raca.raca_id = animal.raca_id";
    }

    @FXML
    private void HandleEventPesquisarCliente(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Pessoa/Interface/TelaConsultaCliente.fxml",
                "Consulta de Cliente", null);
        oCliente = TelaConsultaClienteController.getCliente();
        CtrlCliente.setCampoBusca(oCliente, txbusca);
        cbAnimal.getItems().clear();
        cbAnimal.getItems().addAll(CtrlCliente.getAnimais(oCliente));
        cbAnimal.getSelectionModel().selectFirst();
    }
    
}
