/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Remote
 */
@Entity
@Table(name = "enderecosimples")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Enderecosimples.findAll", query = "SELECT e FROM Enderecosimples e")
    , @NamedQuery(name = "Enderecosimples.findByEndsId", query = "SELECT e FROM Enderecosimples e WHERE e.endsId = :endsId")
    , @NamedQuery(name = "Enderecosimples.findByEndLocal", query = "SELECT e FROM Enderecosimples e WHERE e.endLocal = :endLocal")
    , @NamedQuery(name = "Enderecosimples.findByEndBairro", query = "SELECT e FROM Enderecosimples e WHERE e.endBairro = :endBairro")})
public class Enderecosimples extends JPADao implements Serializable {

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "endsId")
    private Collection<Complementoendereco> complementoenderecoCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ends_id")
    private Integer endsId;
    @Basic(optional = false)
    @Column(name = "end_local")
    private String endLocal;
    @Column(name = "end_bairro")
    private String endBairro;

    public Enderecosimples() {
    }

    public Enderecosimples(String endLocal, String endBairro) {
        this.endLocal = endLocal;
        this.endBairro = endBairro;
    }

    public Enderecosimples(Integer endsId) {
        this.endsId = endsId;
    }

    public Integer getEndsId() {
        return endsId;
    }

    public void setEndsId(Integer endsId) {
        this.endsId = endsId;
    }

    public String getEndLocal() {
        return endLocal;
    }

    public void setEndLocal(String endLocal) {
        this.endLocal = endLocal;
    }
    public String getEndBairro() {
        return endBairro;
    }

    public void setEndBairro(String endBairro) {
        this.endBairro = endBairro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (endsId != null ? endsId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enderecosimples)) {
            return false;
        }
        Enderecosimples other = (Enderecosimples) object;
        if ((this.endsId == null && other.endsId != null) || (this.endsId != null && !this.endsId.equals(other.endsId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return endLocal;
    }

    @XmlTransient
    public Collection<Complementoendereco> getComplementoenderecoCollection() {
        return complementoenderecoCollection;
    }

    public void setComplementoenderecoCollection(Collection<Complementoendereco> complementoenderecoCollection) {
        this.complementoenderecoCollection = complementoenderecoCollection;
    }
    
}
