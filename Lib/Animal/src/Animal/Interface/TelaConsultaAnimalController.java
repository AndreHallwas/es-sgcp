/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Interface;

import Animal.Controladora.CtrlAnimal;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaAnimalController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcIdade;
    @FXML
    private TableColumn<Object, String> tcPeso;
    @FXML
    private TableColumn<Object, String> tcCor;
    @FXML
    private TableColumn<Object, String> tcCliente;
    @FXML
    private TableColumn<Object, String> tcDescricao;
    @FXML
    private TableColumn<Object, String> tcID;
    
    private static Object animal;
    private static boolean btnPainel = true;
    @FXML
    private HBox btnGp;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("AnimalNome"));
        tcIdade.setCellValueFactory(new PropertyValueFactory("AnimalIdade"));
        /*tcDescricao.setCellValueFactory(new PropertyValueFactory("AnimalObs"));*/
        tcPeso.setCellValueFactory(new PropertyValueFactory("AnimalPeso"));
        tcCor.setCellValueFactory(new PropertyValueFactory("AnimalCor"));
        tcCliente.setCellValueFactory(new PropertyValueFactory("CliId"));
        tcID.setCellValueFactory(new PropertyValueFactory("AnimalId"));
        
        CarregaTabela("");
        animal = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlAnimal ctm = CtrlAnimal.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            animal = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (animal != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhum Animal Selecionado", 2);
        }
    }

    public static TelaConsultaAnimalController Create(Class classe, Pane pndados) {
        TelaConsultaAnimalController CAnimal = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Animal/Interface/TelaConsultaAnimal.fxml"));
            Parent root = Loader.load();
            CAnimal = (TelaConsultaAnimalController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CAnimal;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaAnimalController.btnPainel = btnPainel;
    }

    public static Object getAnimal() {
        return animal;
    }
}
