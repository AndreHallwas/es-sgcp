/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Interface;

import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaAnimalController implements Initializable {

    @FXML
    private VBox tabAnimal;
    @FXML
    private VBox tabEspecie;
    @FXML
    private VBox tabRaca;
    @FXML
    private VBox tabPelagem;
    private Tela TelaAnimal;
    private Tela TelaEspecie;
    private Tela TelaPelagem;
    private Tela TelaRaca;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TelaAnimal = IniciarTela.Create(this.getClass(), tabAnimal,
                "/Animal/Interface/TelaCadAnimal.fxml");
        TelaEspecie = IniciarTela.Create(this.getClass(), tabEspecie,
                "/Animal/Interface/TelaCadEspecie.fxml");
        TelaPelagem = IniciarTela.Create(this.getClass(), tabPelagem,
                "/Animal/Interface/TelaCadPelagem.fxml");
        TelaRaca = IniciarTela.Create(this.getClass(), tabRaca,
                "/Animal/Interface/TelaCadRaca.fxml");
    }

}
