/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacina.Controladora;

import Animal.Entidade.Animal;
import Controladora.Base.CtrlBase;
import Produto.Entidade.Produto;
import Transacao.Transacao;
import Transacao.Transaction;
import Utils.DateUtils;
import Vacina.Interface.TabelaVacinas.nodeTabela;
import Vacinacao.Entidade.Vacinacao;
import Vacinacao.Entidade.VacinacaoPK;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlVacina extends CtrlBase {

    private static CtrlVacina ctrlvacina;

    public static CtrlVacina create() {
        if (ctrlvacina == null) {
            ctrlvacina = new CtrlVacina();
        }
        return ctrlvacina;
    }

    public static void setCampos(Object aVacina, TextField txbCep, TextField cbRua, TextField cbBairro,
            TextField cbCidade, TextField cbEstado, TextField cbPais) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
        }
    }

    public static Object getId(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getVacinacaoPK();
        }
        return null;
    }

    public static boolean isAplicada(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getVacinacaoDataAplicacao() != null;
        }
        return false;
    }

    public static String getObs(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getVacinacaoObs();
        }
        return null;
    }

    public static Boolean getLancamento(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getVacinacaoLancamento();
        }
        return false;
    }

    public static LocalDate getDataAplicacao(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return DateUtils.asLocalDate(en.getVacinacaoDataAplicacao());
        }
        return null;
    }

    public static double getPeso(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getVacinacaoPeso() != null ? en.getVacinacaoPeso().doubleValue() : 0;
        }
        return 0;
    }

    public static Object getAnimal(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getAnimal();
        }
        return null;
    }

    public static Object getProduto(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getProduto();
        }
        return null;
    }

    public static ArrayList<Object> getVacinas(Object oAnimal) {
        ArrayList<Object> Vacinas = new ArrayList();
        if (oAnimal != null && oAnimal instanceof Animal) {
            Animal en = (Animal) oAnimal;
            Vacinas.addAll(en.getVacinacaoCollection());
        }
        return Vacinas;
    }

    public static ArrayList<LocalDate> getIntervalo(Object oProduto, Date DataInicial) {
        ArrayList<LocalDate> Datas = new ArrayList();
        if (oProduto != null && oProduto instanceof Produto) {
            Produto en = (Produto) oProduto;
            if (en.getDosagemprodutoId() != null && en.getDosagemprodutoId().getDosagemQuantidade() != null
                    && en.getDosagemprodutoId().getDosagemIntervalo() != null) {
                for (int i = 1; i <= en.getDosagemprodutoId().getDosagemQuantidade().intValue(); i++) {
                    Datas.add(DateUtils.SumDays(DataInicial,
                            i * en.getDosagemprodutoId().getDosagemIntervalo().intValue()));
                }
            }
        }
        return Datas;
    }

    public static boolean isVacina(Object aVacina) {
        return aVacina != null && aVacina instanceof Vacinacao;
    }

    public CtrlVacina() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Param) {
        ArrayList<Object> vacina = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            boolean flag = false, flagVacina = false;
            List<Vacinacao> ResultVacina = new ArrayList();
            if (Param != null && !Param.trim().isEmpty()) {
                if (Param.equalsIgnoreCase("Venda")) {
                    vacina = PesquisarVacina(Filtro);
                    flagVacina = true;
                }
                flag = true;
            }
            if (!flagVacina) {
                if (Filtro != null && Filtro.trim().isEmpty()) {
                    ResultVacina = em.createNamedQuery("Vacinacao.findAll", Vacinacao.class).getResultList();
                } else {
                    try {
                        ResultVacina = em.createNamedQuery("Vacinacao.findByAnimalId", Vacinacao.class)
                                .setParameter("animalId", Integer.parseInt(Filtro)).getResultList();
                    } catch (NumberFormatException ex) {
                        ResultVacina = em.createNamedQuery("Vacinacao.findByVacinacaoObs", Vacinacao.class)
                                .setParameter("vacinacaoObs", Filtro).getResultList();
                    }
                }
                for (Vacinacao vacinas : ResultVacina) {
                    vacina.add(vacinas);
                }
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return vacina;
    }

    private ArrayList<Object> PesquisarVacina(String Filtro) {
        ArrayList<Object> vacina = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Vacinacao> ResultVacina = null;
            String[] auxFiltro = Filtro.split("#:#");
            if (auxFiltro != null) {
                if (auxFiltro.length > 0 && auxFiltro.length < 2) {
                    ResultVacina = em.createNamedQuery("Vacinacao.findByAnimalAllVacinacao", Vacinacao.class)
                        .setParameter("cliId", Integer.parseInt(auxFiltro[0])).getResultList();
                } else {
                    try {
                        ResultVacina = em.createNamedQuery("Vacinacao.findByAnimalIdVacinacao", Vacinacao.class)
                                .setParameter("cliId", Integer.parseInt(auxFiltro[0]))
                                .setParameter("filtro", Integer.parseInt(auxFiltro[1])).getResultList();

                    } catch (Exception ex) {
                        ResultVacina = em.createNamedQuery("Vacinacao.findByAnimalVacinacaoGeral", Vacinacao.class)
                                .setParameter("cliId", Integer.parseInt(auxFiltro[0]))
                                .setParameter("filtro", auxFiltro[1]).getResultList();
                    }
                }

                for (Vacinacao vacinas : ResultVacina) {
                    vacina.add(vacinas);
                }
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return vacina;
    }

    public Object Salvar(List<nodeTabela> Tabela, Object oAnimal) {
        Object result = new Transacao() {
            @Override
            public Object Transacao(Object... Params) {
                if (Tabela != null && !Tabela.isEmpty()) {
                    Boolean flag = true;
                    nodeTabela node;
                    Vacinacao vacina;
                    for (int i = 0; i < Tabela.size(); i++) {
                        node = Tabela.get(i);
                        vacina = (Vacinacao) Salvar(DateUtils.asDate((LocalDate) node.getData()),
                                node.getObservacoes(), oAnimal, node.getVacina(), em);
                        em.persist(vacina);
                    }
                }
                return Tabela;
            }
        }.Realiza();
        Message = result != null ? "Realizado!" : "Não Realizado";
        return result;
    }

    public boolean VerificaComprado(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getVendaId() != null;
        }
        return false;
    }

    public String getCompraId(Object aVacina) {
        if (aVacina != null && aVacina instanceof Vacinacao) {
            Vacinacao en = (Vacinacao) aVacina;
            return en.getVendaId() != null ? en.getVendaId() + "" : "Não Vinculado";
        }
        return "Venda Inválida";
    }

    public Object Salvar(Date Data, String Obs, Object oAnimal, Object oProduto, EntityManager em) {
        Object Vacina = null;
        if (em != null) {
            Vacina = Salvar(Data, Obs, oAnimal, oProduto);
        } else {
            Vacina = super.Salvar(Salvar(Data, Obs, oAnimal, oProduto));
        }
        return Vacina;
    }

    protected Vacinacao Salvar(Date Data, String Obs, Object oAnimal, Object oProduto) {
        Vacinacao vacina = new Vacinacao(Obs);
        Animal animal = null;
        Produto produto = null;
        if (Data != null && Data instanceof Date) {
            VacinacaoPK pk = new VacinacaoPK();
            if (oAnimal != null && oAnimal instanceof Animal) {
                animal = (Animal) oAnimal;
                vacina.setAnimal(animal);
                pk.setAnimalId(animal.getAnimalId());
            }
            if (oProduto != null && oProduto instanceof Produto) {
                produto = (Produto) oProduto;
                vacina.setProduto(produto);
                pk.setProdutoId(produto.getProdutoId());
            }
            pk.setVacinacaoData(Data);
            Long Id = ((Long) getEntityManager().createNativeQuery("select nextval('vacinacao_id_seq')").getSingleResult());
            pk.setVacinacaoId(Id.intValue());

            vacina.setVacinacaoPK(pk);
        }
        return vacina;
    }

    public Object Alterar(Object aVacina, Object Id, Date Data, Date DataAplicacao, String Obs,
            Boolean Lancamento, BigDecimal Peso, Object oAnimal, Object oProduto) {
        Vacinacao vacina = null;
        if (aVacina != null && aVacina instanceof Vacinacao) {
            vacina = (Vacinacao) aVacina;
            Animal animal = null;
            Produto produto = null;
            if (Data != null && Data instanceof Date) {
                if (Id != null && Id instanceof VacinacaoPK) {
                    VacinacaoPK pk = (VacinacaoPK) Id;
                    if (oAnimal != null && oAnimal instanceof Animal) {
                        animal = (Animal) oAnimal;
                        vacina.setAnimal(animal);
                        pk.setAnimalId(animal.getAnimalId());
                    }
                    if (oProduto != null && oProduto instanceof Produto) {
                        produto = (Produto) oProduto;
                        vacina.setProduto(produto);
                        pk.setProdutoId(produto.getProdutoId());
                    }
                    pk.setVacinacaoData(Data);
                    /*pk.setVacinacaoId()*/
                    vacina.setVacinacaoPK(pk);
                }
            }
            vacina.setVacinacaoObs(Obs);
            vacina.setVacinacaoDataAplicacao(DataAplicacao);
            vacina.setVacinacaoLancamento(Lancamento);
            vacina.setVacinacaoPeso(Peso);
            vacina = (Vacinacao) super.Alterar(vacina);
        }
        return vacina;
    }

    public Object Aplicar(Object aVacina, LocalDate DataAplicacao, String Obs, Boolean Lancamento,
            BigDecimal Peso) {
        Vacinacao vacina = null;
        if (aVacina != null && aVacina instanceof Vacinacao) {
            vacina = (Vacinacao) aVacina;
            return Alterar(vacina, vacina.getId(), vacina.getVacinacaoPK().getVacinacaoData(),
                    DateUtils.asDate(DataAplicacao), Obs, Lancamento, Peso, vacina.getAnimal(),
                    vacina.getProduto());
        }
        return vacina;
    }

    public Object CancelarAplicao(Object aVacina) {
        Vacinacao vacina = null;
        if (aVacina != null && aVacina instanceof Vacinacao) {
            vacina = (Vacinacao) aVacina;
            return Alterar(vacina, vacina.getId(), vacina.getVacinacaoPK().getVacinacaoData(),
                    null, vacina.getVacinacaoObs(), Boolean.FALSE, null, vacina.getAnimal(),
                    vacina.getProduto());
        }
        return vacina;
    }

    public boolean Remover(Object Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Vacinacao.class);
    }

}
