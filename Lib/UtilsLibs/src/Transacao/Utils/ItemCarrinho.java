/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao.Utils;

/**
 *
 * @author Raizen
 */
public class ItemCarrinho {

    private Item oItem;
    private int Quantidade;
    private int Id;

    public ItemCarrinho(Item oItem, int Quantidade, int Id) {
        this.oItem = oItem;
        this.Quantidade = Quantidade;
        this.Id = Id;
    }
    
    public String getNome(){
        return getoItem().getNome();
    }
    
    public String getChave(){
        return getoItem().getChave();
    }
    
    public String getPrecoUnitario(){
        return getoItem().getPrecoUnitario();
    }
    
    public String getSubTotal(){
        return getQuantidade()*Double.parseDouble(getPrecoUnitario()) + "";
    }

    /**
     * @return the oItem
     */
    public Item getoItem() {
        return oItem;
    }

    /**
     * @param oItem the oItem to set
     */
    public void setoItem(Item oItem) {
        this.oItem = oItem;
    }

    /**
     * @return the Quantidade
     */
    public int getQuantidade() {
        return Quantidade;
    }

    /**
     * @param Quantidade the Quantidade to set
     */
    public void setQuantidade(int Quantidade) {
        this.Quantidade = Quantidade;
    }

    /**
     * @return the Id
     */
    public int getId() {
        return Id;
    }

    /**
     * @param Id the Id to set
     */
    public void setId(int Id) {
        this.Id = Id;
    }
}
