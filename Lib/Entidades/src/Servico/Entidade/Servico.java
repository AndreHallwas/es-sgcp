package Servico.Entidade;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Venda.Entidade.Servicodavenda;
import JPA.Dao.JPADao;
import Utils.Carrinho.Item;
import Utils.FormatString;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "servico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servico.findAll", query = "SELECT s FROM Servico s")
    , @NamedQuery(name = "Servico.findByServicoId", query = "SELECT s FROM Servico s WHERE s.servicoId = :servicoId")
    , @NamedQuery(name = "Servico.findByServicoValor", query = "SELECT s FROM Servico s WHERE s.servicoValor = :servicoValor")
    , @NamedQuery(name = "Servico.findByServicoNome", query = "SELECT s FROM Servico s WHERE s.servicoNome = :servicoNome")
    , @NamedQuery(name = "Servico.findByServicoDescricao", query = "SELECT s FROM Servico s WHERE s.servicoDescricao = :servicoDescricao")
    , @NamedQuery(name = "Servico.findByServicoGeral", query = "SELECT s FROM Servico s WHERE upper(s.servicoNome) like upper(concat(:filtro,'%'))")})
public class Servico extends JPADao implements Serializable, Item {

    @OneToMany(fetch=FetchType.LAZY, mappedBy = "servico")
    private Collection<Servicodavenda> servicodavendaCollection = new ArrayList();

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "servico_id")
    private Integer servicoId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "servico_valor")
    private BigDecimal servicoValor;
    @Basic(optional = false)
    @Column(name = "servico_nome")
    private String servicoNome;
    @Column(name = "servico_descricao")
    private String servicoDescricao;
    @JoinColumn(name = "grupodeservico_id", referencedColumnName = "grupodeservico_id")
    @ManyToOne(optional = false)
    private Grupodeservico grupodeservicoId;

    public Servico() {
    }

    public Servico(Integer servicoId) {
        this.servicoId = servicoId;
    }

    public Servico(BigDecimal servicoValor, String servicoNome, String servicoDescricao) {
        this.servicoValor = servicoValor;
        this.servicoNome = servicoNome;
        this.servicoDescricao = servicoDescricao;
    }

    public Servico(Integer servicoId, BigDecimal servicoValor, String servicoNome, String servicoDescricao) {
        this.servicoId = servicoId;
        this.servicoValor = servicoValor;
        this.servicoNome = servicoNome;
        this.servicoDescricao = servicoDescricao;
    }

    public Servico(Integer servicoId, BigDecimal servicoValor, String servicoNome) {
        this.servicoId = servicoId;
        this.servicoValor = servicoValor;
        this.servicoNome = servicoNome;
    }

    public Integer getServicoId() {
        return servicoId;
    }

    public void setServicoId(Integer servicoId) {
        this.servicoId = servicoId;
    }

    public BigDecimal getServicoValor() {
        return servicoValor;
    }

    public String getServValor() {
        return FormatString.MonetaryPersistent(servicoValor);
    }


    public void setServicoValor(BigDecimal servicoValor) {
        this.servicoValor = servicoValor;
    }

    public String getServicoNome() {
        return servicoNome;
    }

    public void setServicoNome(String servicoNome) {
        this.servicoNome = servicoNome;
    }

    public String getServicoDescricao() {
        return servicoDescricao;
    }

    public void setServicoDescricao(String servicoDescricao) {
        this.servicoDescricao = servicoDescricao;
    }

    public Grupodeservico getGrupodeservicoId() {
        return grupodeservicoId;
    }

    public void setGrupodeservicoId(Grupodeservico grupodeservicoId) {
        this.grupodeservicoId = grupodeservicoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (servicoId != null ? servicoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servico)) {
            return false;
        }
        Servico other = (Servico) object;
        if ((this.servicoId == null && other.servicoId != null) || (this.servicoId != null && !this.servicoId.equals(other.servicoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return servicoNome;
    }

    @XmlTransient
    public Collection<Servicodavenda> getServicodavendaCollection() {
        return servicodavendaCollection;
    }

    public void setServicodavendaCollection(Collection<Servicodavenda> servicodavendaCollection) {
        this.servicodavendaCollection = servicodavendaCollection;
    }

    @Override
    public boolean Compara(Item oItem) {
        return equals(oItem);
    }

    @Override
    public double getValor() {
        return getServicoValor().doubleValue();
    }

    @Override
    public String getNome() {
        return getServicoNome();
    }

    @Override
    public String getChave() {
        return getServicoId()+"";
    }

    @Override
    public String getPrecoUnitario() {
        return getValor()+"";
    }

    @Override
    public String getTipo() {
        return "Serviço";
    }
    
}
