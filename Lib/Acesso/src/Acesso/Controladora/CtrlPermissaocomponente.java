/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Controladora;

import Acesso.Entidade.Componente;
import Acesso.Entidade.Permissaocomponente;
import Acesso.Entidade.Permissaodeacesso;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlPermissaocomponente extends CtrlBase{
    

    private static CtrlPermissaocomponente ctrlpermissaocomponente;

    public static CtrlPermissaocomponente create() {
        if (ctrlpermissaocomponente == null) {
            ctrlpermissaocomponente = new CtrlPermissaocomponente();
        }
        return ctrlpermissaocomponente;
    }

    public CtrlPermissaocomponente() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> permissaocomponente = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            
            List<Permissaocomponente> ResultPermissaocomponente = em.createNamedQuery("Permissaocomponente.findByPermcompId", Permissaocomponente.class)
            .setParameter("permcompId", Integer.parseInt(Filtro)).getResultList();
            
            for (Permissaocomponente permissaocomponentes : ResultPermissaocomponente) {
                permissaocomponente.add(permissaocomponentes);
            }
            
        } finally {
            if (em != null) {
                em.close();
            }
        }
        
        return permissaocomponente;
    }

    public Object Salvar(Object componente, Object permissaodeacesso) {
        Permissaocomponente permissaocomponente = new Permissaocomponente();
        if(componente != null && componente instanceof Componente){
            permissaocomponente.setComponenteId((Componente) componente);
        }
        if(permissaodeacesso != null && permissaodeacesso instanceof Permissaodeacesso){
            permissaocomponente.setPermacessoId((Permissaodeacesso) permissaodeacesso);
        }
        return super.Salvar(permissaocomponente);
    }

    public Object Alterar(Object aPermissaodeComponente, Integer Id, Object componente, Object permissaodeacesso) {
        Permissaocomponente permissaocomponente = null;
        if(aPermissaodeComponente != null && aPermissaodeComponente instanceof Permissaocomponente){
            permissaocomponente = (Permissaocomponente) aPermissaodeComponente;
            if (componente != null && componente instanceof Componente) {
                permissaocomponente.setComponenteId((Componente) componente);
            }
            if (permissaodeacesso != null && permissaodeacesso instanceof Permissaodeacesso) {
                permissaocomponente.setPermacessoId((Permissaodeacesso) permissaodeacesso);
            }
            permissaocomponente = (Permissaocomponente) super.Alterar(permissaocomponente);
        }
        return permissaocomponente;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Permissaocomponente.class);
    }
}
