/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Interface;

import Venda.Controladora.CtrlVenda;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaVendaController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcId;
    @FXML
    private TableColumn<Object, String> tcValorTotal;
    @FXML
    private TableColumn<Object, String> tcParcelas;
    @FXML
    private TableColumn<Object, String> tcData;
    @FXML
    private TableColumn<Object, String> tcSituacao;
    @FXML
    private TableColumn<Object, String> tcObs;
    @FXML
    private HBox btnGp;

    private static Object Venda;
    private static boolean btnPainel = true;
    protected static int Quantidade;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcData.setCellValueFactory(new PropertyValueFactory("DatadaVenda"));
        tcId.setCellValueFactory(new PropertyValueFactory("VendaId"));
        tcObs.setCellValueFactory(new PropertyValueFactory("VendaObs"));
        tcParcelas.setCellValueFactory(new PropertyValueFactory("Parcelas"));
        tcSituacao.setCellValueFactory(new PropertyValueFactory("Situacao"));
        tcValorTotal.setCellValueFactory(new PropertyValueFactory("ValorTotal"));
        CarregaTabela("");
        Venda = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlVenda ctm = CtrlVenda.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, "Geral")));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            Venda = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        try {
            if (Venda != null) {
                evtCancelar(event);
            } else {
                Mensagem.Exibir("Nenhuma Venda Selecionada", 2);
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex);
        }
    }

    public static TelaConsultaVendaController Create(Class classe, Pane pndados) {
        TelaConsultaVendaController CVenda = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Interfaces/TelaConsultaVenda.fxml"));
            Parent root = Loader.load();
            CVenda = (TelaConsultaVendaController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CVenda;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaVendaController.btnPainel = btnPainel;
    }

    public static Object getVenda() {
        return Venda;
    }

}
