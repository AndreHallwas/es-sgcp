/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Interface;

import Endereco.Controladora.CtrlComplementoendereco;
import Pessoa.Entidade.Cliente;
import Endereco.Interface.TelaEnderecoController;
import Endereco.Interface.TelaEnderecoSimplesController;
import Pessoa.Controladora.CtrlCliente;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Imagem.TelaImagemFXMLController;
import Utils.Controladora.CtrlUtils;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Utils.Validar;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTabPane;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadClienteController implements Initializable, Tela {

    @FXML
    private HBox pndados;
    @FXML
    private TextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private TextField txbRG;
    @FXML
    private Label lbErroRG;
    @FXML
    private TextField txbCPF;
    @FXML
    private Label lbErroCPF;
    @FXML
    private TextField txbTelefone;
    @FXML
    private Label lbErroTelefone;
    @FXML
    private TextField txbEmail;
    @FXML
    private Label lbErroEMail;
    @FXML
    private TextArea taObs;
    @FXML
    private Label lbErroObs;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private VBox gpImagem;
    @FXML
    private JFXButton btConsulta;
    @FXML
    private VBox gpEndereco;
    @FXML
    private VBox gpEnderecoSimples;
    @FXML
    private JFXTabPane TabEndereco;

    private int flag;
    private static Cliente cliente;
    private boolean EOriginal;
    private Integer ID;
    private Object Instancia;
    private static TelaImagemFXMLController Imagem;
    private static TelaEnderecoController Endereco;
    private static TelaEnderecoSimplesController EnderecoSimples;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*flag = 2;*/

        Imagem = TelaImagemFXMLController.Create(this.getClass(), gpImagem);
        Endereco = TelaEnderecoController.Create(this.getClass(), gpEndereco);
        EnderecoSimples = TelaEnderecoSimplesController.Create(this.getClass(), gpEnderecoSimples);

        estadoOriginal();

        MaskFieldUtil.foneField(txbTelefone);
        MaskFieldUtil.cpfField(txbCPF);
        MaskFieldUtil.numericField(txbRG);
        MaskFieldUtil.maxField(txbRG, 10);

        /*Start.getAcessoComponente().AdicionaObjeto(gpTabela, "Tabela de Clientes");*/
    }

    private void setAllErro(boolean Enome, boolean Erg, boolean Ecpf, boolean Etelefone, boolean Eemail, boolean Eobs) {
        lbErroNome.setVisible(Enome);
        lbErroRG.setVisible(Erg);
        lbErroCPF.setVisible(Ecpf);
        lbErroTelefone.setVisible(Etelefone);
        lbErroEMail.setVisible(Eemail);
        lbErroObs.setVisible(Eobs);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }
    
    public void selectDefaultTabEndereco(){
        Platform.runLater(() -> {
            TabEndereco.getSelectionModel().selectFirst();
        });
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlCliente cp = CtrlCliente.create();
        ArrayList<Object> Pesquisa = cp.Pesquisar(MaskFieldUtil.onlyAlfaNumericValue(txbCPF), "CPF");
        if (!txbCPF.getText().trim().isEmpty() && !Validar.isValidCPF(txbCPF.getText())) {
            setErro(lbErroCPF, "CPF Inválido");
            txbCPF.setText("");
            txbCPF.requestFocus();
            fl = false;
        }
        if (!Pesquisa.isEmpty() && !txbCPF.getText().trim().isEmpty()) {
            String CPFAtual = CtrlCliente.getCPF(Instancia);
            if (flag == 0 && !CPFAtual.equals(MaskFieldUtil.onlyAlfaNumericValue(txbCPF))) {
                setErro(lbErroCPF, "CPF Ja Existe");
                fl = false;
                txbCPF.setText("");
                txbCPF.requestFocus();
            } else if (flag == 1) {
                setErro(lbErroCPF, "CPF Ja Existe");
                fl = false;
                txbCPF.setText("");
                txbCPF.requestFocus();
            }
        }
        if (txbNome.getText().trim().isEmpty()) {
            setErro(lbErroNome, "Campo Obrigatório Vazio");
            txbNome.setText("");
            txbNome.requestFocus();
            fl = false;
        }
        fl = fl && (TabEndereco.getSelectionModel().getSelectedIndex() == 0 ? EnderecoSimples.valida() :
                Endereco.valida());
        return fl;
    }

    @FXML
    private void evtNovo(Event event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        txbCPF.setDisable(false);
        flag = 1;
    }

    @FXML
    private void evtAlterar(Event event) {
        EstadoEdicao();
        txbCPF.setDisable(false);
        flag = 0;
    }

    @FXML
    private void evtApagar(Event event) {
        CtrlCliente cp = CtrlCliente.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(ID);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(Event event) {

        setAllErro(false, false, false, false, false, false);
        CtrlCliente cp = CtrlCliente.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, ID, MaskFieldUtil.onlyAlfaNumericValue(txbCPF), txbRG.getText(),
                        Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()),
                        txbNome.getText(), txbTelefone.getText(), txbEmail.getText(),
                        taObs.getText(), TabEndereco.getSelectionModel().getSelectedIndex() == 0 ? EnderecoSimples.getComplementoEndereco()
                                : Endereco.getComplementoEndereco())) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(MaskFieldUtil.onlyAlfaNumericValue(txbCPF), txbRG.getText(),
                    Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()),
                    txbNome.getText(), txbTelefone.getText(), txbEmail.getText(),
                    taObs.getText(), TabEndereco.getSelectionModel().getSelectedIndex() == 0 ? EnderecoSimples.getComplementoEndereco()
                                : Endereco.getComplementoEndereco())) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }

    }

    @FXML
    private void evtCancelar(Event event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false, false, false, false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false, false, false, false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);
        Imagem.estadoOriginal();
        Endereco.estadoOriginal();
        EnderecoSimples.estadoOriginal();
        selectDefaultTabEndereco();

        clear(pndados.getChildren());
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlCliente.setCliente(p, txbNome, txbCPF, txbEmail, txbRG, txbTelefone, taObs);
            Imagem.setImagem(CtrlCliente.getImagem(p));
            ID = CtrlCliente.getID(p);
            Endereco.setComplementoEndereco(CtrlCliente.getEndereco(p));
            EnderecoSimples.setComplementoEndereco(CtrlCliente.getEndereco(p));
            CtrlComplementoendereco.setTabEndereco(CtrlCliente.getEndereco(p), TabEndereco);
        }
    }

    @FXML
    private void evtConsultaAvancada(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Pessoa/Interface/TelaConsultaCliente.fxml",
                "Consulta de Cliente", null);
        if (TelaConsultaClienteController.getCliente() != null) {
            EstadoConsulta(TelaConsultaClienteController.getCliente());
        }
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }

    public static Cliente getCliente() {
        return cliente;
    }

}
