/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa.Controladora;

import Controladora.Base.CtrlBase;
import Empresa.Entidade.Empresa;
import Endereco.Entidade.Complementoendereco;
import Transacao.Transaction;
import Utils.Imagem;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlEmpresa extends CtrlBase{

    protected static CtrlEmpresa ctrlempresa = null;

    public static BufferedImage getImagem(Object empresa) {
        return Imagem.ByteArrayToBufferedImage(((Empresa) empresa).getEmpLogo());
    }

    public static void setCampos(Object empresa, TextField txbNome, TextField txbCNPJ, TextField txbFone, TextField txbRazao2) {
        Empresa p = (Empresa) empresa;
        txbNome.setText(p.getEmpNome());
        txbCNPJ.setText(p.getEmpCnpj());
        txbFone.setText(p.getEmpTelefone());
        txbRazao2.setText(p.getEmpRazaosocial());
    }

    public static CtrlEmpresa create() {
        if (ctrlempresa == null) {
            ctrlempresa = new CtrlEmpresa();
        }
        return ctrlempresa;
    }

    public static Object getEndereco(Object p) {
        if(p != null && p instanceof Empresa){
            Empresa empresa = (Empresa) p;
            return empresa.getEndId();
        }
        return null;
    }

    public CtrlEmpresa() {
        super(Transaction.getEntityManagerFactory());
    }

    public Object Pesquisar() {
        ArrayList<Object> empresa = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            List<Empresa> ResultEmpresa = em.createNamedQuery("Empresa.findByEmpId", Empresa.class)
                    .setParameter("empId", 1).getResultList();

            for (Empresa empresas : ResultEmpresa) {
                empresa.add(empresas);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return empresa.isEmpty() ? null : empresa.get(0);
    }

    public Object Salvar(String Nome, String CNPJ, Object endereco, String Fone,
            String RazaoSocial, BufferedImage Imagem) {
        Empresa empresa = new Empresa(RazaoSocial, Fone, Nome, CNPJ, Utils.Imagem.BufferedImageToByteArray(Imagem));
        if (endereco != null && endereco instanceof Complementoendereco) {
            empresa.setEndId((Complementoendereco) endereco);
        }
        return super.Salvar(empresa);
    }

    public Object Alterar(Object aEmpresa, String Nome, String CNPJ, Object endereco, String Fone,
            String RazaoSocial, BufferedImage Imagem) {
        Empresa empresa = null;
        if (aEmpresa != null && aEmpresa instanceof Empresa) {
            empresa = (Empresa) aEmpresa;
            empresa.setEmpCnpj(CNPJ);
            empresa.setEmpLogo(Utils.Imagem.BufferedImageToByteArray(Imagem));
            empresa.setEmpNome(Nome);
            empresa.setEmpRazaosocial(RazaoSocial);
            empresa.setEmpTelefone(Fone);
            /*empresa.setEndId(CNPJ);*/
            if (endereco != null && endereco instanceof Complementoendereco) {
                empresa.setEndId((Complementoendereco) endereco);
            }
            empresa = (Empresa) super.Alterar(empresa);
        }
        return empresa;
    }

    public boolean Remover() {
        return super.Remover(1) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Empresa.class);
    }
}
