/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formadepagamento.Interface;

import Formadepagamento.Controladora.CtrlFormadepagamento;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaFormadepagamentoController implements Initializable {


    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcId;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcDescricao;
    @FXML
    private HBox btnGp;
    
    private static boolean btnPainel = true;
    private static Object formadepagamento;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("FormadepagamentoNome"));
        tcId.setCellValueFactory(new PropertyValueFactory("FormadepagamentoId"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("FormadepagamentoDesc"));
        CarregaTabela("");
        formadepagamento = null;
        btnGp.setVisible(btnPainel);
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        if (formadepagamento != null) {
            evtCancelar(event);
        }
    }

    private void CarregaTabela(String filtro) {
        CtrlFormadepagamento ctm = CtrlFormadepagamento.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        CtrlUtils.CloseStage(event);
    }

    @FXML
    private void evtBuscar() {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            formadepagamento = tabela.getItems().get(lin);
        }
    }

    public static Object getFormadepagamento() {
        return formadepagamento;
    }

    /**
     * @param aBtnPainel the btnPainel to set
     */
    public static void setBtnPainel(boolean aBtnPainel) {
        btnPainel = aBtnPainel;
    }

}
