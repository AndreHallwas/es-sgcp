/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP;

import Acesso.Sessao.Sessao;
import Empresa.Controladora.CtrlEmpresa;
import Utils.Arquivo;
import Utils.Encryptor;
import Utils.Imagem;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import com.jfoenix.controls.JFXCheckBox;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaLoginFXMLController implements Initializable {

    private static Stage _login;

    @FXML
    private TextField txbSenha;
    @FXML
    private TextField txbLogin;
    @FXML
    private ImageView imgEmpresa;
    @FXML
    private JFXCheckBox cbRelembrarSenha;

    private static boolean Retorno;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CtrlEmpresa ce = CtrlEmpresa.create();
        if (ce.Pesquisar() != null) {
            imgEmpresa.setImage(Imagem.BufferedImageToImage(CtrlEmpresa.getImagem(ce.Pesquisar())));
        }
        Retorno = false;

        loadUser();
    }

    private void saveUser(String User, String Pass) {
        writeLastUser(Encryptor.encrypt("Last_User_Logged",
                "RandomVectorInit", User + "#ÿ#" + Pass));
    }

    private void saveUser(String User) {
        writeLastUser(Encryptor.encrypt("Last_User_Logged",
                "RandomVectorInit", User));
    }

    private void saveUser() {
        if (cbRelembrarSenha.isSelected()) {
            saveUser(txbLogin.getText(), txbSenha.getText());
        } else {
            saveUser(txbLogin.getText());
        }
    }

    private void loadUser() {
        String LastUserEnc = readLastUser();
        if (LastUserEnc != null && !LastUserEnc.trim().isEmpty()) {
            String LastUserDec = Encryptor.decrypt("Last_User_Logged",
                    "RandomVectorInit", LastUserEnc);
            String[] Ret = LastUserDec.split("#ÿ#");
            txbLogin.setText(Ret[0]);

            if (Ret.length > 1) {
                txbSenha.setText(Ret[1]);
                cbRelembrarSenha.setSelected(true);
                Platform.runLater(() -> txbLogin.requestFocus());
            } else {
                cbRelembrarSenha.setSelected(false);
                Platform.runLater(() -> txbSenha.requestFocus());
            }
        }
    }

    private void writeLastUser(String Text) {
        ArrayList<String> Data = new ArrayList();
        Data.add(Text);
        Arquivo.gravaArquivoDeStringUTF("LastUser.db", Data);
    }

    private String readLastUser() {
        ArrayList<String> Data = null;
        Data = Arquivo.leArquivoDeStringUTF("LastUser.db", 1);
        return Data != null && !Data.isEmpty() ? Data.get(0) : "";
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        if (txbLogin.getText().trim().isEmpty()) {
            txbLogin.getStyleClass().add("wrong-credentials");
            txbLogin.setText("");
            txbLogin.requestFocus();
            fl = false;
        } else {
            txbLogin.getStyleClass().remove("wrong-credentials");
        }
        if (txbSenha.getText().trim().isEmpty()) {
            txbSenha.getStyleClass().add("wrong-credentials");
            txbSenha.setText("");
            txbSenha.requestFocus();
            fl = false;
        } else {
            txbSenha.getStyleClass().remove("wrong-credentials");
        }
        return fl;
    }

    public static boolean isRetorno() {
        return Retorno;
    }

    @FXML
    private void handleLoginButtonAction(ActionEvent event) {
        if (validaCampos()) {
            if (Sessao.CriarSessao(txbLogin.getText(), txbSenha.getText())) {
                saveUser();

                handleCancelButtonAction(event);

                Retorno = true;

            } else {
                txbLogin.getStyleClass().add("wrong-credentials");
                txbSenha.getStyleClass().add("wrong-credentials");
                txbLogin.requestFocus();
            }
        }
    }

    @FXML
    private void handleCancelButtonAction(ActionEvent event) {
        _login.close();
    }

    public static boolean Create(Class Classe) {
        Parent root = null;
        try {
            Stage stage = IniciarTela.newStage();
            stage.setTitle("Login");
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            root = FXMLLoader.load(Classe.getResource("TelaLoginFXML.fxml"));
            Scene scene = IniciarTela.newScene(root);
            stage.setScene(scene);
            _login = stage;
            stage.showAndWait();
            return TelaLoginFXMLController.isRetorno();
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Login");
        }
        return false;
    }

    @FXML
    private void evtAjuda(MouseEvent event) {
        MainMenuOptions.evtAjuda(event, this.getClass());
    }

}
