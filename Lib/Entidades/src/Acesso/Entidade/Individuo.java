/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Entidade;
import Acesso.Entidade.Grupodeacesso;

/**
 *
 * @author Raizen
 */
public interface Individuo {
    
    /**
     * @return the Acesso
     */
    public Grupodeacesso getGrupo();

    /**
     * @param Grupo
     */
    public void setGrupo(Grupodeacesso Grupo);
    
    
}
