/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Entidade;

import Animal.Entidade.Animal;
import Atendimento.Entidade.Atendimento;
import ContaaReceber.Entidade.Contaareceber;
import ContaaReceber.Entidade.Parcelaareceber;
import JPA.Dao.JPADao;
import Pessoa.Entidade.Cliente;
import Pessoa.Entidade.Usuario;
import Utils.FormatString;
import Vacinacao.Entidade.Vacinacao;
import Variables.Variables;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "venda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Venda.findAll", query = "SELECT v FROM Venda v")
    , @NamedQuery(name = "Venda.findByVendaId", query = "SELECT v FROM Venda v WHERE v.vendaId = :vendaId")
    , @NamedQuery(name = "Venda.findByVendaValorTotal", query = "SELECT v FROM Venda v WHERE v.vendaValorTotal = :vendaValorTotal")
    , @NamedQuery(name = "Venda.findByVendaData", query = "SELECT v FROM Venda v WHERE v.vendaData = :vendaData")
    , @NamedQuery(name = "Venda.findByVendaSituacao", query = "SELECT v FROM Venda v WHERE v.vendaSituacao = :vendaSituacao")
    , @NamedQuery(name = "Venda.findByVendaObs", query = "SELECT v FROM Venda v WHERE v.vendaObs = :vendaObs")
    , @NamedQuery(name = "Venda.findByBetween", query = "SELECT v FROM Venda v WHERE v.vendaData between :datainicial and :datafinal")
    , @NamedQuery(name = "Venda.findByAllGeral", query = "SELECT v FROM Venda v WHERE v.vendaId = :vendaId"
            + " or v.vendaValorTotal = :vendaValorTotal")})
public class Venda extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "venda_id")
    private Integer vendaId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "venda_valor_total")
    private BigDecimal vendaValorTotal;
    @Basic(optional = false)
    @Column(name = "venda_data")
    @Temporal(TemporalType.DATE)
    private Date vendaData;
    @Column(name = "venda_situacao")
    private Boolean vendaSituacao;
    @Column(name = "venda_obs")
    private String vendaObs;
    @JoinColumn(name = "animal_id", referencedColumnName = "animal_id")
    @ManyToOne
    private Animal animalId;
    @JoinColumn(name = "atendimento_id", referencedColumnName = "atendimento_id")
    @ManyToOne
    private Atendimento atendimentoId;
    @JoinColumn(name = "cli_id", referencedColumnName = "cli_id")
    @ManyToOne(optional = false)
    private Cliente cliId;
    @JoinColumn(name = "usr_id", referencedColumnName = "usr_id")
    @ManyToOne(optional = false)
    private Usuario usrId;
    
    @OneToMany(mappedBy = "vendaId")
    private Collection<Vacinacao> vacinacaoCollection = new ArrayList();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendaId")
    private Collection<Contaareceber> contaareceberCollection = new ArrayList();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "venda")
    private Collection<Servicodavenda> servicodavendaCollection = new ArrayList();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "venda")
    private Collection<Produtodavenda> produtodavendaCollection = new ArrayList();

    public Venda() {
    }

    public Venda(Integer vendaId) {
        this.vendaId = vendaId;
    }

    public Venda(BigDecimal vendaValorTotal, Date vendaData, String vendaObs, Boolean vendaSituacao) {
        this.vendaValorTotal = vendaValorTotal;
        this.vendaData = vendaData;
        this.vendaObs = vendaObs;
        this.vendaSituacao = vendaSituacao;
    }

    public Venda(Integer vendaId, BigDecimal vendaValorTotal, Date vendaData, String vendaObs, Boolean vendaSituacao) {
        this.vendaId = vendaId;
        this.vendaValorTotal = vendaValorTotal;
        this.vendaData = vendaData;
        this.vendaObs = vendaObs;
        this.vendaSituacao = vendaSituacao;
    }

    public Venda(Integer vendaId, BigDecimal vendaValorTotal, Date vendaData) {
        this.vendaId = vendaId;
        this.vendaValorTotal = vendaValorTotal;
        this.vendaData = vendaData;
    }

    public Integer getVendaId() {
        return vendaId;
    }

    public void setVendaId(Integer vendaId) {
        this.vendaId = vendaId;
    }

    public BigDecimal getVendaValorTotal() {
        return vendaValorTotal;
    }
    
    public String getValorTotal(){
        return FormatString.MonetaryPersistent(vendaValorTotal);
    }

    public void setVendaValorTotal(BigDecimal vendaValorTotal) {
        this.vendaValorTotal = vendaValorTotal;
    }

    public Date getVendaData() {
        return vendaData;
    }
    public String getDatadaVenda() {
        return Variables.getData(vendaData);
    }

    public void setVendaData(Date vendaData) {
        this.vendaData = vendaData;
    }

    public Boolean getVendaSituacao() {
        return vendaSituacao;
    }

    public void setVendaSituacao(Boolean vendaSituacao) {
        this.vendaSituacao = vendaSituacao;
    }

    public String getVendaObs() {
        return vendaObs;
    }

    public void setVendaObs(String vendaObs) {
        this.vendaObs = vendaObs;
    }

    @XmlTransient
    public Collection<Vacinacao> getVacinacaoCollection() {
        return vacinacaoCollection;
    }

    public void setVacinacaoCollection(Collection<Vacinacao> vacinacaoCollection) {
        this.vacinacaoCollection = vacinacaoCollection;
    }

    @XmlTransient
    public Collection<Contaareceber> getContaareceberCollection() {
        return contaareceberCollection;
    }

    public void setContaareceberCollection(Collection<Contaareceber> contaareceberCollection) {
        this.contaareceberCollection = contaareceberCollection;
    }

    @XmlTransient
    public Collection<Servicodavenda> getServicodavendaCollection() {
        return servicodavendaCollection;
    }

    public void setServicodavendaCollection(Collection<Servicodavenda> servicodavendaCollection) {
        this.servicodavendaCollection = servicodavendaCollection;
    }

    @XmlTransient
    public Collection<Produtodavenda> getProdutodavendaCollection() {
        return produtodavendaCollection;
    }

    public void setProdutodavendaCollection(Collection<Produtodavenda> produtodavendaCollection) {
        this.produtodavendaCollection = produtodavendaCollection;
    }

    public Animal getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Animal animalId) {
        this.animalId = animalId;
    }

    public Atendimento getAtendimentoId() {
        return atendimentoId;
    }

    public void setAtendimentoId(Atendimento atendimentoId) {
        this.atendimentoId = atendimentoId;
    }

    public Cliente getCliId() {
        return cliId;
    }

    public void setCliId(Cliente cliId) {
        this.cliId = cliId;
    }

    public Usuario getUsrId() {
        return usrId;
    }

    public void setUsrId(Usuario usrId) {
        this.usrId = usrId;
    }

    public Object getParcelas() {
        Object Parcelas = null;
        if (contaareceberCollection != null && !contaareceberCollection.isEmpty()) {
            for (Contaareceber contaareceber : contaareceberCollection) {
                if (contaareceber.getParcelaareceberCollection() != null
                        && !contaareceber.getParcelaareceberCollection().isEmpty()) {
                    Parcelas = contaareceber.getContaareceberParcelas();
                } else {
                    Parcelas = "Sem Parcelas";
                }
            }
        } else {
            Parcelas = "Conta Não Gerada";
        }
        return Parcelas;
    }

    public Object getSituacao() {
        Object Parcelas = null;
        if (contaareceberCollection != null && !contaareceberCollection.isEmpty()) {
            for (Contaareceber contaareceber : contaareceberCollection) {
                if (contaareceber.getParcelaareceberCollection() != null
                        && !contaareceber.getParcelaareceberCollection().isEmpty()) {
                    boolean result = true;
                    for (Parcelaareceber parcelaareceber : contaareceber.getParcelaareceberCollection()) {
                        result = parcelaareceber.getParrSituacao() && result;
                    }
                    if(result){
                        Parcelas = "Pago";
                    }else{
                        Parcelas = "Pendente";
                    }
                } else {
                    Parcelas = "Gerar Parcelas";
                }
            }
        } else {
            Parcelas = "Gerar Conta";
        }
        return Parcelas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vendaId != null ? vendaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Venda)) {
            return false;
        }
        Venda other = (Venda) object;
        if ((this.vendaId == null && other.vendaId != null) || (this.vendaId != null && !this.vendaId.equals(other.vendaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return vendaId + "";
    }

}
