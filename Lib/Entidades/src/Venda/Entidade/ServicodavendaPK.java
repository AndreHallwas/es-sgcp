/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Entidade;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Raizen
 */
@Embeddable
public class ServicodavendaPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "venda_id")
    private int vendaId;
    @Basic(optional = false)
    @Column(name = "servico_id")
    private int servicoId;

    public ServicodavendaPK() {
    }

    public ServicodavendaPK(int vendaId, int servicoId) {
        this.vendaId = vendaId;
        this.servicoId = servicoId;
    }

    public int getVendaId() {
        return vendaId;
    }

    public void setVendaId(int vendaId) {
        this.vendaId = vendaId;
    }

    public int getServicoId() {
        return servicoId;
    }

    public void setServicoId(int servicoId) {
        this.servicoId = servicoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) vendaId;
        hash += (int) servicoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ServicodavendaPK)) {
            return false;
        }
        ServicodavendaPK other = (ServicodavendaPK) object;
        if (this.vendaId != other.vendaId) {
            return false;
        }
        if (this.servicoId != other.servicoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.ServicodavendaPK[ vendaId=" + vendaId + ", servicoId=" + servicoId + " ]";
    }
    
}
