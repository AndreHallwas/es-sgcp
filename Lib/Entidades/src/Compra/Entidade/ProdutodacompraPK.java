/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Entidade;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Raizen
 */
@Embeddable
public class ProdutodacompraPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "produto_id")
    private int produtoId;
    @Basic(optional = false)
    @Column(name = "compra_id")
    private int compraId;

    public ProdutodacompraPK() {
    }

    public ProdutodacompraPK(int produtoId, int compraId) {
        this.produtoId = produtoId;
        this.compraId = compraId;
    }

    public int getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(int produtoId) {
        this.produtoId = produtoId;
    }

    public int getCompraId() {
        return compraId;
    }

    public void setCompraId(int compraId) {
        this.compraId = compraId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) produtoId;
        hash += (int) compraId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProdutodacompraPK)) {
            return false;
        }
        ProdutodacompraPK other = (ProdutodacompraPK) object;
        if (this.produtoId != other.produtoId) {
            return false;
        }
        if (this.compraId != other.compraId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.ProdutodacompraPK[ produtoId=" + produtoId + ", compraId=" + compraId + " ]";
    }
    
}
