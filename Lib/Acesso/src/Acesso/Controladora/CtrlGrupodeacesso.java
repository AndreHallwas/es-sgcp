/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Controladora;

import Acesso.Entidade.Grupodeacesso;
import Acesso.Entidade.Permissaodeacesso;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlGrupodeacesso extends CtrlBase {

    private static CtrlGrupodeacesso ctrlgrupodeacesso;

    public static CtrlGrupodeacesso create() {
        if (ctrlgrupodeacesso == null) {
            ctrlgrupodeacesso = new CtrlGrupodeacesso();
        }
        return ctrlgrupodeacesso;
    }

    public CtrlGrupodeacesso() {
        super(Transaction.getEntityManagerFactory());
    }

    public static String getNome(Object p) {
        if (p != null && p instanceof Grupodeacesso) {
            return ((Grupodeacesso) p).getGrupoNome();
        }
        return null;
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> grupodeacesso = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Grupodeacesso> ResultGrupodeacesso;
            if (Filtro == null || Filtro.trim().isEmpty()) {
                ResultGrupodeacesso = em.createNamedQuery("Grupodeacesso.findAll", Grupodeacesso.class).getResultList();
            } else {
                try {
                    ResultGrupodeacesso = em.createNamedQuery("Grupodeacesso.findByGrupoId", Grupodeacesso.class)
                            .setParameter("grupoId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultGrupodeacesso = em.createNamedQuery("Grupodeacesso.findByGrupoNome", Grupodeacesso.class)
                            .setParameter("grupoNome", Filtro).getResultList();
                }
            }
            for (Grupodeacesso grupodeacessos : ResultGrupodeacesso) {
                grupodeacesso.add(grupodeacessos);
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return grupodeacesso;
    }

    public Object Salvar(Object oGrupodeAcesso) {
        Grupodeacesso grupodeacesso = null;
        if (oGrupodeAcesso != null && oGrupodeAcesso instanceof Grupodeacesso) {
            grupodeacesso = (Grupodeacesso) oGrupodeAcesso;
        }
        return super.Salvar(grupodeacesso);
    }

    public Object Salvar(String grupoNome, String grupoDescricao, Object PermissaodeAcesso) {
        Grupodeacesso grupodeacesso = new Grupodeacesso(grupoNome, grupoDescricao);
        if (PermissaodeAcesso != null && PermissaodeAcesso instanceof Permissaodeacesso) {
            grupodeacesso.setPermacessoId((Permissaodeacesso) PermissaodeAcesso);
        }
        return super.Salvar(grupodeacesso);
    }

    public Object Alterar(Object oGrupodeAcesso) {
        Grupodeacesso grupodeacesso = null;
        if (oGrupodeAcesso != null && oGrupodeAcesso instanceof Grupodeacesso) {
            grupodeacesso = (Grupodeacesso) oGrupodeAcesso;
        }
        return super.Alterar(grupodeacesso);
    }

    public Object Alterar(Object oGrupodeacesso, Integer Id, String grupoNome, String grupoDescricao, Object PermissaodeAcesso) {
        Grupodeacesso grupodeacesso = null;
        if (oGrupodeacesso != null && oGrupodeacesso instanceof Grupodeacesso) {
            grupodeacesso = (Grupodeacesso) oGrupodeacesso;
            grupodeacesso.setGrupoDescricao(grupoDescricao);
            grupodeacesso.setGrupoNome(grupoNome);
            if (PermissaodeAcesso != null && PermissaodeAcesso instanceof Permissaodeacesso) {
                grupodeacesso.setPermacessoId((Permissaodeacesso) PermissaodeAcesso);
            }
            grupodeacesso = (Grupodeacesso) super.Alterar(grupodeacesso);
        }
        return grupodeacesso;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Grupodeacesso.class);
    }

}
