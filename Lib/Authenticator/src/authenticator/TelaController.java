/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authenticator;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author Remote
 */
public class TelaController implements Initializable {

    @FXML
    private Label lblDefault;
    @FXML
    private Label lblKey;
    @FXML
    private Label lblGenerate;
    private File KeyFile;
    private File DefaultFile;
    private File GenerateFile;
    private Alert alert;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void openDefault(MouseEvent event) {
        File file = openfile();
        if (file != null) {
            DefaultFile = file;
            lblDefault.setText(file.getAbsolutePath());
        }
    }

    @FXML
    private void openKey(MouseEvent event) {
        File file = openfile();
        if (file != null) {
            KeyFile = file;
            lblKey.setText(file.getAbsolutePath());
        }
    }

    @FXML
    private void auth(MouseEvent event) {
        if (Auth.authenticate(KeyFile.getPath(), DefaultFile.getPath())) {
            sendMessage("Arquivo Autentico");
        }
    }

    @FXML
    private void openGenerate(MouseEvent event) {
        File file = openfile();
        if (file != null) {
            GenerateFile = file;
            lblGenerate.setText(file.getAbsolutePath());
        }
    }

    @FXML
    private void Gen(MouseEvent event) {
        String key = Auth.generateandwrite(GenerateFile.getPath());
        sendMessage("Chave Gerada: " + key);
    }

    private File openfile() {
        FileChooser fc = new FileChooser();
        File file = fc.showOpenDialog(null);
        return file;
    }

    private void sendMessage(String message) {
        if (alert == null) {
            alert = new Alert(AlertType.INFORMATION);
        }
        alert.setContentText(message);
        alert.showAndWait();
    }

}
