/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacinacao.Entidade;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Raizen
 */
@Embeddable
public class VacinacaoPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "vacinacao_id")
    private int vacinacaoId;
    @Basic(optional = false)
    @Column(name = "vacinacao_data")
    @Temporal(TemporalType.DATE)
    private Date vacinacaoData;
    @Basic(optional = false)
    @Column(name = "animal_id")
    private int animalId;
    @Basic(optional = false)
    @Column(name = "produto_id")
    private int produtoId;

    public VacinacaoPK() {
    }

    public VacinacaoPK(Date vacinacaoData, int animalId, int produtoId) {
        this.vacinacaoData = vacinacaoData;
        this.animalId = animalId;
        this.produtoId = produtoId;
    }

    public VacinacaoPK(int vacinacaoId, Date vacinacaoData, int animalId, int produtoId) {
        this.vacinacaoId = vacinacaoId;
        this.vacinacaoData = vacinacaoData;
        this.animalId = animalId;
        this.produtoId = produtoId;
    }

    public int getVacinacaoId() {
        return vacinacaoId;
    }

    public void setVacinacaoId(int vacinacaoId) {
        this.vacinacaoId = vacinacaoId;
    }

    public Date getVacinacaoData() {
        return vacinacaoData;
    }

    public void setVacinacaoData(Date vacinacaoData) {
        this.vacinacaoData = vacinacaoData;
    }

    public int getAnimalId() {
        return animalId;
    }

    public void setAnimalId(int animalId) {
        this.animalId = animalId;
    }

    public int getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(int produtoId) {
        this.produtoId = produtoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) vacinacaoId;
        hash += (vacinacaoData != null ? vacinacaoData.hashCode() : 0);
        hash += (int) animalId;
        hash += (int) produtoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VacinacaoPK)) {
            return false;
        }
        VacinacaoPK other = (VacinacaoPK) object;
        if (this.vacinacaoId != other.vacinacaoId) {
            return false;
        }
        if ((this.vacinacaoData == null && other.vacinacaoData != null) || (this.vacinacaoData != null && !this.vacinacaoData.equals(other.vacinacaoData))) {
            return false;
        }
        if (this.animalId != other.animalId) {
            return false;
        }
        if (this.produtoId != other.produtoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Vacinacao.Entidade.VacinacaoPK[ vacinacaoId=" + vacinacaoId + ", vacinacaoData=" + vacinacaoData + ", animalId=" + animalId + ", produtoId=" + produtoId + " ]";
    }
    
}
