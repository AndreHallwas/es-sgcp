/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.UI.Pagina;

/**
 *
 * @author Raizen
 */
public interface Acao {
    
    public boolean Seleciona(Object... Params);
    public boolean Proximo(Object... Params);
    public boolean Verifica(Object... Params);
    
}
