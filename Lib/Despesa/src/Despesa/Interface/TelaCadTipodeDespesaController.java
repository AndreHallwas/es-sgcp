/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Despesa.Interface;

import Despesa.Controladora.CtrlTipodedespesa;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadTipodeDespesaController implements Initializable {

    @FXML
    private HBox pndados;
    @FXML
    private JFXTextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private JFXTextArea taObs;
    @FXML
    private Label lbErroObs;
    @FXML
    private JFXButton btnovo;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btapagar;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    @FXML
    private JFXButton btConsulta;

    private int flag;
    private Integer ID;
    private boolean EOriginal;
    private Object Instancia;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        estadoOriginal();
    }

    private void setAllErro(boolean Enome, boolean Eobs) {
        lbErroNome.setVisible(Enome);
        lbErroObs.setVisible(Eobs);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    @FXML
    private void evtNovo(Event event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(Event event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(Event event) {
        CtrlTipodedespesa cf = CtrlTipodedespesa.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cf.Remover(ID);
        }
        estadoOriginal();
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlTipodedespesa cf = CtrlTipodedespesa.create();
        ArrayList<Object> Resultado = cf.Pesquisar(txbNome.getText());
        if (txbNome.getText().trim().isEmpty() || !Resultado.isEmpty()) {
            if (txbNome.getText().trim().isEmpty()) {
                setErro(lbErroNome, "Campo Obrigatório Vazio");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            }/* else if (Resultado.isEmpty()) {
                if (flag == 0) {
                    setErro(lbErroNome, "Nome Não Existente");
                    fl = false;
                    txbNome.setText("");
                    txbNome.requestFocus();
                }
            } */else if (flag == 1) {
                setErro(lbErroNome, "Nome Ja Existe");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            }
        }
        return fl;
    }

    @FXML
    private void evtConfirmar(Event event) {
        setAllErro(false, false);
        CtrlTipodedespesa cf = CtrlTipodedespesa.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cf.Alterar(Instancia, ID, txbNome.getText(), taObs.getText())) == null) {
                    Mensagem.Exibir(cf.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cf.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cf.Salvar(txbNome.getText(), taObs.getText())) == null) {
                Mensagem.Exibir(cf.getMsg(), 2);
            } else {
                Mensagem.Exibir(cf.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(Event event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);

        clear(pndados.getChildren());
    }

    private void setCampos(Object f) {
        if (f != null) {
            CtrlTipodedespesa.setCampos(f, txbNome, taObs);
            ID = CtrlTipodedespesa.getId(f);
        }
    }

    @FXML
    private void evtConsultaAvancada(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Despesa/Interface/TelaConsultaTipodeDespesa.fxml",
                "Consulta de Tipodedespesa", null);
        if (TelaConsultaTipodeDespesaController.getTipodedespesa() != null) {
            EstadoConsulta(TelaConsultaTipodeDespesaController.getTipodedespesa());
        }
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }

}
