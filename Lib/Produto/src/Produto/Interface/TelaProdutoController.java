/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaProdutoController implements Initializable {

    @FXML
    private VBox tabCategoria;
    @FXML
    private VBox tabTipo;
    @FXML
    private VBox tabDosagem;
    @FXML
    private VBox tabProduto;
    private Tela TelaProduto;
    private Tela TelaDosagemproduto;
    private Tela TelaTipoproduto;
    private Tela TelaCategoriaproduto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TelaProduto = IniciarTela.Create(this.getClass(), tabProduto,
                "/Produto/Interface/TelaCadProduto.fxml");
        TelaDosagemproduto = IniciarTela.Create(this.getClass(), tabDosagem,
                "/Produto/Interface/TelaCadDosagemproduto.fxml");
        TelaTipoproduto = IniciarTela.Create(this.getClass(), tabTipo,
                "/Produto/Interface/TelaCadTipoproduto.fxml");
        TelaCategoriaproduto = IniciarTela.Create(this.getClass(), tabCategoria,
                "/Produto/Interface/TelaCadCategoriaproduto.fxml");
    }

}
