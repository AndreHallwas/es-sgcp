/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Entidade;

import Utils.Hash;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class ComponenteObjeto extends Componente{
    private String Nome;

    public ComponenteObjeto() {
        super();
    }

    public ComponenteObjeto(Object Objeto, String Nome) {
        super();
        Objetos = new ArrayList();
        Objetos.add(Objeto);
        this.Nome = Nome;
    }
    
    @Override
    public void Constroi(){
        setComponenteId(Hash.stringHexa(Hash.gerarHash(getNome(), "MD5")));
        setComponenteDescricao(getNome());
    }

    /**
     * @return the Nome
     */
    public String getNome() {
        return Nome;
    }

    /**
     * @param Nome the Nome to set
     */
    public void setNome(String Nome) {
        this.Nome = Nome;
    }
}
