/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Interface;

import Acesso.Controladora.Acesso;
import Endereco.Interface.TelaEnderecoController;
import Pessoa.Controladora.CtrlUsuario;
import Utils.Controladora.CtrlUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.GerarRelatorios;
import Utils.UI.Imagem.TelaImagemFXMLController;
import Utils.UI.Tela.IniciarTela;
import Utils.Validar;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */

public class TelaCadUsuarioController implements Initializable {

    @FXML
    private TextField txbNome;
    @FXML
    private TextField txbRG;
    @FXML
    private TextField txbCPF;
    @FXML
    private TextField txbTelefone;
    @FXML
    private TextField txbEmail;
    @FXML
    private TextField txbSalario;
    @FXML
    private ComboBox<Object> cbNivel;
    @FXML
    private TextArea taObs;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private HBox pndados;
    @FXML
    private PasswordField txbSenha;
    @FXML
    private TextField txbLogin;
    @FXML
    private Label lbErroLogin;
    @FXML
    private Label lbErroSenha;
    @FXML
    private Label lbErroNome;
    @FXML
    private Label lbErroRG;
    @FXML
    private Label lbErroCPF;
    @FXML
    private Label lbErroTelefone;
    @FXML
    private Label lbErroEMail;
    @FXML
    private Label lbErroSalario;
    @FXML
    private Label lbErroObs;
    @FXML
    private Button btnFichadoUsuario;
    @FXML
    private Group gpNivel;
    @FXML
    private VBox gpImagem;
    @FXML
    private JFXButton btConsulta;
    @FXML
    private VBox gpEndereco;

    private char flag;
    private boolean EOriginal;
    private Integer Id;
    private Object Instancia;
    private static TelaImagemFXMLController Imagem;
    private static TelaEnderecoController Endereco;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Imagem = TelaImagemFXMLController.Create(this.getClass(), gpImagem);
        Endereco = TelaEnderecoController.Create(this.getClass(), gpEndereco);
        
        estadoOriginal();

        MaskFieldUtil.foneField(txbTelefone);
        MaskFieldUtil.cpfField(txbCPF);
        MaskFieldUtil.numericField(txbSalario);
        MaskFieldUtil.numericField(txbRG);
        MaskFieldUtil.maxField(txbRG, 10);
        MaskFieldUtil.maxField(txbSalario, 8);

        /*Start.getAcessoComponente().AdicionaObjeto(gpNivel, "Nivel Usuario");
        Start.getAcessoComponente().AdicionaObjeto(gpTabela, "Tabela de Usuarios");*/
    }

    private void setAllErro(boolean Elogin, boolean Esenha, boolean Enome, boolean Erg, boolean Ecpf, boolean Etelefone, boolean Eemail, boolean Esalario, boolean Eobs) {
        lbErroLogin.setVisible(Elogin);
        lbErroSenha.setVisible(Esenha);
        lbErroNome.setVisible(Enome);
        lbErroRG.setVisible(Erg);
        lbErroCPF.setVisible(Ecpf);
        lbErroTelefone.setVisible(Etelefone);
        lbErroEMail.setVisible(Eemail);
        lbErroSalario.setVisible(Esalario);
        lbErroObs.setVisible(Eobs);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlUsuario cp = CtrlUsuario.create();
        ArrayList<Object> Pesquisa = cp.Pesquisar(txbLogin.getText());
        if (txbLogin.getText().trim().isEmpty() || !Pesquisa.isEmpty() || Pesquisa.isEmpty()) {
            if (txbLogin.getText().trim().isEmpty()) {
                setErro(lbErroLogin, "Campo Obrigatório Vazio");
                fl = false;
                txbLogin.setText("");
                txbLogin.requestFocus();
            }/* else if (Pesquisa.isEmpty()) {
                if (flag == 0) {
                    setErro(lbErroLogin, "Login Não Existente");
                    fl = false;
                    txbLogin.setText("");
                    txbLogin.requestFocus();
                }
            } */else if (flag == 1) {
                setErro(lbErroLogin, "Login Ja Existe");
                fl = false;
                txbLogin.setText("");
                txbLogin.requestFocus();
            }
        }
        if (txbRG.getText().trim().isEmpty()) {
            setErro(lbErroRG, "Campo Obrigatório Vazio");
            txbRG.setText("");
            txbRG.requestFocus();
            fl = false;
        }
        if (txbCPF.getText().trim().isEmpty() || !Validar.isValidCPF(txbCPF.getText())) {
            if (txbCPF.getText().trim().isEmpty()) {
                setErro(lbErroCPF, "Campo Obrigatório Vazio");
            } else {
                setErro(lbErroCPF, "CPF Inválido");
            }
            txbCPF.setText("");
            txbCPF.requestFocus();
            fl = false;
        }
        if (txbNome.getText().trim().isEmpty()) {
            setErro(lbErroNome, "Campo Obrigatório Vazio");
            txbNome.setText("");
            txbNome.requestFocus();
            fl = false;
        }
        if (txbSalario.getText().trim().isEmpty()) {
            setErro(lbErroSalario, "Campo Obrigatório Vazio");
            txbSalario.setText("");
            txbSalario.requestFocus();
            fl = false;
        }
        if (txbSenha.getText().trim().isEmpty()) {
            setErro(lbErroSenha, "Campo Obrigatório Vazio");
            txbSenha.setText("");
            txbSenha.requestFocus();
            fl = false;
        }
        fl = fl && Endereco.valida();
        return fl;
    }

    @FXML
    private void evtNovo(Event event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        txbLogin.setDisable(false);
        flag = 1;
    }

    @FXML
    private void evtAlterar(Event event) {
        EstadoEdicao();
        txbLogin.setDisable(false);
        flag = 0;
    }

    @FXML
    private void evtApagar(Event event) {
        CtrlUsuario cp = CtrlUsuario.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(Id);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(Event event) {
        setAllErro(false, false, false, false, false, false, false, false, false);
        CtrlUsuario cp = CtrlUsuario.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, Id, txbLogin.getText(), Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()),
                        txbEmail.getText(), txbRG.getText(), MaskFieldUtil.onlyAlfaNumericValue(txbCPF), txbNome.getText(), taObs.getText(),
                        txbTelefone.getText(), new BigDecimal(Double.parseDouble(txbSalario.getText())), txbSenha.getText(),
                        Endereco.getComplementoEndereco(), cbNivel.getSelectionModel().getSelectedItem())) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(txbLogin.getText(), Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()),
                    txbEmail.getText(), txbRG.getText(), MaskFieldUtil.onlyAlfaNumericValue(txbCPF), txbNome.getText(), taObs.getText(),
                    txbTelefone.getText(), new BigDecimal(Double.parseDouble(txbSalario.getText())), txbSenha.getText(),
                    Endereco.getComplementoEndereco(), cbNivel.getSelectionModel().getSelectedItem())) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(Event event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false, false, false, false, false, false, false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false, false, false, false, false, false, false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);
        Imagem.estadoOriginal();
        Endereco.estadoOriginal();

        clear(pndados.getChildren());
        cbNivel.getItems().clear();
        cbNivel.getItems().addAll(Acesso.create().PesquisarGrupo(""));
        cbNivel.getSelectionModel().select(0);
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlUsuario.setCampos(p, txbNome, txbCPF, txbEmail, txbRG, txbTelefone,
                    taObs, txbSalario, txbSenha, txbLogin, cbNivel);
            Id = CtrlUsuario.getId(p);
            Imagem.setImagem(CtrlUsuario.getImagem(p));
            Endereco.setComplementoEndereco(CtrlUsuario.getEndereco(p));
            
        }
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    @FXML
    private void evtConsultaAvancada(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Pessoa/Interface/TelaConsultaUsuario.fxml",
                "Consulta de Usuario", null);
        if (TelaConsultaUsuarioController.getUsuario() != null) {
            EstadoConsulta(TelaConsultaUsuarioController.getUsuario());
        }
    }

    @FXML
    private void evtFichadoUsuario(Event event) {
        /*String sql = "select * from (select * from usuario where usr_login = '" + (String) (txbLogin.getText()
        != null ? txbLogin.getText() : txBusca.getText()) + "' ) as usuario inner join endereco on usuario.end_cep
        = endereco.end_cep inner join bairro on endereco.bai_cod = bairro.bai_cod inner join cidade on bairro.cid_cod
        = cidade.cid_cod inner join grupodeacesso on usuario.grupo_id = grupodeacesso.grupo_id";
        Relatorio.gerarRelatorio(sql, "Relatorios/MyReports/Rel_FichaUsuario.jasper", "Ficha do Usuario");*/
    }
    
}
