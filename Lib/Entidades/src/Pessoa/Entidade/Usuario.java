/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Entidade;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import Acesso.Entidade.Grupodeacesso;
import Acesso.Entidade.Individuo;
import Acesso.Entidade.Permissaodeacesso;
import Agendamento.Entidade.Agendamento;
import Caixa.Entidade.Caixa;
import Endereco.Entidade.Complementoendereco;
import Compra.Entidade.Compra;
import JPA.Dao.JPADao;
import Venda.Entidade.Venda;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByUsrId", query = "SELECT u FROM Usuario u WHERE u.usrId = :usrId")
    , @NamedQuery(name = "Usuario.findByUsrLogin", query = "SELECT u FROM Usuario u WHERE u.usrLogin = :usrLogin")
    , @NamedQuery(name = "Usuario.findByUsrEmail", query = "SELECT u FROM Usuario u WHERE u.usrEmail = :usrEmail")
    , @NamedQuery(name = "Usuario.findByUsrRg", query = "SELECT u FROM Usuario u WHERE u.usrRg = :usrRg")
    , @NamedQuery(name = "Usuario.findByUsrCpf", query = "SELECT u FROM Usuario u WHERE u.usrCpf = :usrCpf")
    , @NamedQuery(name = "Usuario.findByUsrNome", query = "SELECT u FROM Usuario u WHERE u.usrNome = :usrNome")
    , @NamedQuery(name = "Usuario.findByUsrObs", query = "SELECT u FROM Usuario u WHERE u.usrObs = :usrObs")
    , @NamedQuery(name = "Usuario.findByUsrTelefone", query = "SELECT u FROM Usuario u WHERE u.usrTelefone = :usrTelefone")
    , @NamedQuery(name = "Usuario.findByUsrSalario", query = "SELECT u FROM Usuario u WHERE u.usrSalario = :usrSalario")
    , @NamedQuery(name = "Usuario.findByUsrDatacadastro", query = "SELECT u FROM Usuario u WHERE u.usrDatacadastro = :usrDatacadastro")
    , @NamedQuery(name = "Usuario.findByUsrSenha", query = "SELECT u FROM Usuario u WHERE u.usrSenha = :usrSenha")})
public class Usuario extends JPADao implements Serializable, Pessoa, Individuo {

    @Lob
    @Column(name = "usr_foto")
    private byte[] usrFoto;
    @JoinColumn(name = "end_id", referencedColumnName = "end_id")
    @ManyToOne(optional = false)
    private Complementoendereco endId;
    @JoinColumn(name = "permacesso_id", referencedColumnName = "permacesso_id")
    @ManyToOne
    private Permissaodeacesso permacessoId;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "usr_id")
    private Integer usrId;
    @Basic(optional = false)
    @Column(name = "usr_login")
    private String usrLogin;
    @Column(name = "usr_email")
    private String usrEmail;
    @Basic(optional = false)
    @Column(name = "usr_rg")
    private String usrRg;
    @Basic(optional = false)
    @Column(name = "usr_cpf")
    private String usrCpf;
    @Basic(optional = false)
    @Column(name = "usr_nome")
    private String usrNome;
    @Column(name = "usr_obs")
    private String usrObs;
    @Column(name = "usr_telefone")
    private String usrTelefone;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "usr_salario")
    private BigDecimal usrSalario;
    @Column(name = "usr_datacadastro")
    @Temporal(TemporalType.DATE)
    private Date usrDatacadastro;
    @Column(name = "usr_senha")
    private String usrSenha;
    @JoinColumn(name = "grupo_id", referencedColumnName = "grupo_id")
    @ManyToOne(optional = false)
    private Grupodeacesso grupoId;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usrId")
    private Collection<Compra> compraCollection = new ArrayList();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usrId")
    private Collection<Caixa> caixaCollection = new ArrayList();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usrId")
    private Collection<Venda> vendaCollection = new ArrayList();
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usrId")
    private Collection<Agendamento> agendamentoCollection = new ArrayList();

    public Usuario() {
    }

    public Usuario(Integer usrId) {
        this.usrId = usrId;
    }

    public Usuario(String login, String senha, byte[] foto, String email, String rg, String cpf,
            String nome, String obs, String telefone, BigDecimal salario) {
        this.usrLogin = login;
        this.usrSenha = senha;
        this.usrFoto = foto;
        this.usrEmail = email;
        this.usrRg = rg;
        this.usrCpf = cpf;
        this.usrNome = nome;
        this.usrObs = obs;
        this.usrTelefone = telefone;
        this.usrSalario = salario;
    }

    public Usuario(Integer usrId, String login, String senha, byte[] foto, String email, String rg,
            String cpf, String nome, String obs, String telefone, BigDecimal salario) {
        this.usrId = usrId;
        this.usrLogin = login;
        this.usrSenha = senha;
        this.usrFoto = foto;
        this.usrEmail = email;
        this.usrRg = rg;
        this.usrCpf = cpf;
        this.usrNome = nome;
        this.usrObs = obs;
        this.usrTelefone = telefone;
        this.usrSalario = salario;
    }

    public Usuario(Integer usrId, String usrLogin, String usrRg, String usrCpf, String usrNome, BigDecimal usrSalario) {
        this.usrId = usrId;
        this.usrLogin = usrLogin;
        this.usrRg = usrRg;
        this.usrCpf = usrCpf;
        this.usrNome = usrNome;
        this.usrSalario = usrSalario;
    }

    public Integer getUsrId() {
        return usrId;
    }

    public void setUsrId(Integer usrId) {
        this.usrId = usrId;
    }

    public String getUsrLogin() {
        return usrLogin;
    }

    public void setUsrLogin(String usrLogin) {
        this.usrLogin = usrLogin;
    }

    public byte[] getUsrFoto() {
        return usrFoto;
    }

    public void setUsrFoto(byte[] usrFoto) {
        this.usrFoto = usrFoto;
    }

    public String getUsrEmail() {
        return usrEmail;
    }

    public void setUsrEmail(String usrEmail) {
        this.usrEmail = usrEmail;
    }

    public String getUsrRg() {
        return usrRg;
    }

    public void setUsrRg(String usrRg) {
        this.usrRg = usrRg;
    }

    public String getUsrCpf() {
        return usrCpf;
    }

    public void setUsrCpf(String usrCpf) {
        this.usrCpf = usrCpf;
    }

    public String getUsrNome() {
        return usrNome;
    }

    public void setUsrNome(String usrNome) {
        this.usrNome = usrNome;
    }

    public String getUsrObs() {
        return usrObs;
    }

    public void setUsrObs(String usrObs) {
        this.usrObs = usrObs;
    }

    public String getUsrTelefone() {
        return usrTelefone;
    }

    public void setUsrTelefone(String usrTelefone) {
        this.usrTelefone = usrTelefone;
    }

    public BigDecimal getUsrSalario() {
        return usrSalario;
    }

    public void setUsrSalario(BigDecimal usrSalario) {
        this.usrSalario = usrSalario;
    }

    public Date getUsrDatacadastro() {
        return usrDatacadastro;
    }

    public void setUsrDatacadastro(Date usrDatacadastro) {
        this.usrDatacadastro = usrDatacadastro;
    }

    public String getUsrSenha() {
        return usrSenha;
    }

    public void setUsrSenha(String usrSenha) {
        this.usrSenha = usrSenha;
    }

    public Grupodeacesso getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Grupodeacesso grupoId) {
        this.grupoId = grupoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usrId != null ? usrId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usrId == null && other.usrId != null) || (this.usrId != null && !this.usrId.equals(other.usrId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return usrNome;
    }

    @Override
    public Grupodeacesso getGrupo() {
        return grupoId;
    }

    @Override
    public void setGrupo(Grupodeacesso Grupo) {
        this.grupoId = Grupo;
    }

    @XmlTransient
    public Collection<Agendamento> getAgendamentoCollection() {
        return agendamentoCollection;
    }

    public void setAgendamentoCollection(Collection<Agendamento> agendamentoCollection) {
        this.agendamentoCollection = agendamentoCollection;
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection() {
        return compraCollection;
    }

    public void setCompraCollection(Collection<Compra> compraCollection) {
        this.compraCollection = compraCollection;
    }

    @XmlTransient
    public Collection<Caixa> getCaixaCollection() {
        return caixaCollection;
    }

    public void setCaixaCollection(Collection<Caixa> caixaCollection) {
        this.caixaCollection = caixaCollection;
    }

    @XmlTransient
    public Collection<Venda> getVendaCollection() {
        return vendaCollection;
    }

    public void setVendaCollection(Collection<Venda> vendaCollection) {
        this.vendaCollection = vendaCollection;
    }

    public Complementoendereco getEndId() {
        return endId;
    }

    public void setEndId(Complementoendereco endId) {
        this.endId = endId;
    }

    public Permissaodeacesso getPermacessoId() {
        return permacessoId;
    }

    public void setPermacessoId(Permissaodeacesso permacessoId) {
        this.permacessoId = permacessoId;
    }

}
