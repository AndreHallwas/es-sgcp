/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Interface;

import Compra.Controladora.CtrlCompra;
import ContaaPagar.Controladora.CtrlContaapagar;
import ContaaPagar.Controladora.CtrlParcelaapagar;
import Formadepagamento.Controladora.CtrlFormadepagamento;
import Utils.Controladora.CtrlUtils;
import Utils.FormatString;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaQuitarParcelasAPagarController implements Initializable, Tela {

    @FXML
    private VBox gpQuitarParcelaaPagar;
    @FXML
    private Pane gpDadosdaCompra;
    @FXML
    private JFXTextField txbValorToral;
    @FXML
    private JFXTextField txbParcelas;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Integer> tcParcela;
    @FXML
    private TableColumn<Object, Double> tcValor;
    @FXML
    private TableColumn<Object, String> tcDatadeVencimento;
    @FXML
    private TableColumn<Object, Boolean> tcStatus;
    @FXML
    private TableColumn<Object, String> tcDatadePagamento;
    @FXML
    private TableColumn<Object, Double> tcValorPago;
    @FXML
    private JFXTextField txbValordaParcela;
    @FXML
    private JFXTextField txbValoraPagar;
    @FXML
    private JFXTextField txbCompra;
    @FXML
    private Label lblStatus;
    @FXML
    private JFXButton btnRealizarPagamento;
    @FXML
    private JFXButton btnEstornarPagamento;
    @FXML
    private AnchorPane pndados;
    @FXML
    private JFXComboBox<Object> cbFormadePagamento;
    @FXML
    private JFXTextField txbValorFaltante;

    private Object compra;
    private Object ItemSelecionado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcDatadeVencimento.setCellValueFactory(new PropertyValueFactory("DataVencimento"));
        tcParcela.setCellValueFactory(new PropertyValueFactory("ParpNumerodaparcela"));
        tcStatus.setCellValueFactory(new PropertyValueFactory("Situacao"));
        tcValor.setCellValueFactory(new PropertyValueFactory("Valor"));
        tcValorPago.setCellValueFactory(new PropertyValueFactory("ValorPago"));
        tcDatadePagamento.setCellValueFactory(new PropertyValueFactory("DataPagamento"));
        MaskFieldUtil.numericField(txbValoraPagar);
        estadoOriginal();
    }

    private void estadoOriginal() {
        gpDadosdaCompra.setDisable(true);
        CtrlUtils.clear(pndados.getChildren());
        lblStatus.setText("Status");
        btnEstornarPagamento.setDisable(true);
        btnRealizarPagamento.setDisable(true);
        tabela.getItems().clear();
        estadoOriginalCbParcelas();
    }

    @FXML
    private void HandleButtonRealizarPagamento(MouseEvent event) {
        btnEstornarPagamento.setDisable(true);
        btnRealizarPagamento.setDisable(true);
        if (compra != null) {
            if (!CtrlContaapagar.create().VerificaProximoVencimento(ItemSelecionado)) {
                if (Mensagem.ExibirConfimacao("Atenção: Este não é o Vencimento mais Próximo, "
                        + "Deseja prosseguir com o pagamento mesmo Assim?")) {
                    if (CtrlParcelaapagar.create().Pagar(ItemSelecionado, txbValoraPagar.getText())) {
                        CtrlParcelaapagar.create().setFormadepagamento(ItemSelecionado,
                                cbFormadePagamento.getSelectionModel().getSelectedItem());
                        Mensagem.Exibir(CtrlParcelaapagar.create().getMsg(), 1);
                    } else {
                        Mensagem.Exibir(CtrlParcelaapagar.create().getMsg(), 2);
                    }
                    CarregaTabela(compra);
                }
            } else {
                if (CtrlParcelaapagar.create().Pagar(ItemSelecionado, txbValoraPagar.getText())) {
                    CtrlParcelaapagar.create().setFormadepagamento(ItemSelecionado,
                            cbFormadePagamento.getSelectionModel().getSelectedItem());
                    Mensagem.Exibir(CtrlParcelaapagar.create().getMsg(), 1);
                } else {
                    Mensagem.Exibir(CtrlParcelaapagar.create().getMsg(), 2);
                }
                CarregaTabela(compra);
            }
        }
    }

    @FXML
    private void HandleButtonEstornarPagamento(MouseEvent event) {
        btnEstornarPagamento.setDisable(true);
        btnRealizarPagamento.setDisable(true);
        if (compra != null && ItemSelecionado != null) {
            if (!CtrlContaapagar.create().VerificaUltimoPagamento(ItemSelecionado)) {
                if (Mensagem.ExibirConfimacao("Atenção: Este não é o ultimo pagamento, "
                        + "Deseja prosseguir com o estorno mesmo Assim?")) {
                    if (CtrlParcelaapagar.create().estornar(ItemSelecionado)) {
                        Mensagem.Exibir(CtrlParcelaapagar.create().getMsg(), 1);
                    } else {
                        Mensagem.Exibir(CtrlParcelaapagar.create().getMsg(), 2);
                    }
                    CarregaTabela(compra);
                }
            } else {
                if (CtrlParcelaapagar.create().estornar(ItemSelecionado)) {
                    Mensagem.Exibir(CtrlParcelaapagar.create().getMsg(), 1);
                } else {
                    Mensagem.Exibir(CtrlParcelaapagar.create().getMsg(), 2);
                }
                CarregaTabela(compra);
            }
        }
    }

    @FXML
    private void HandleButtonCancelar(MouseEvent event) {
        estadoOriginal();
    }

    @FXML
    private void HandleButtonPesquisarCompra(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Compra/Interface/TelaConsultaCompra.fxml",
                "Consulta de Compra", null);
        if (TelaConsultaCompraController.getCompra() != null) {
            compra = TelaConsultaCompraController.getCompra();
            txbCompra.setText(compra.toString());
            gpDadosdaCompra.setDisable(false);
            CtrlCompra.setCampos(compra, txbValorToral, txbParcelas);
            CarregaTabela(compra);
        } else {
            estadoOriginal();
        }
    }

    @FXML
    private void clicknatabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            Object f = tabela.getItems().get(lin);
            if (CtrlParcelaapagar.create().getSituacao(f) == true) {
                btnEstornarPagamento.setDisable(false);
                btnRealizarPagamento.setDisable(true);
            } else {
                btnRealizarPagamento.setDisable(false);
                btnEstornarPagamento.setDisable(true);
            }
            CtrlParcelaapagar.create().setCampos(f, txbValoraPagar, txbValordaParcela);
            cbFormadePagamento.getSelectionModel().clearSelection();
            Object aFormadepagamento = CtrlParcelaapagar.getFormadepagamento(f);
            if (aFormadepagamento != null) {
                cbFormadePagamento.getSelectionModel().select(aFormadepagamento);
            }
            ItemSelecionado = f;
        }
    }

    private void CarregaTabela(Object compra) {
        CtrlCompra ctm = CtrlCompra.create();
        CtrlParcelaapagar ccp = CtrlParcelaapagar.create();
        try {
            tabela.getItems().clear();
            if (compra != null) {
                tabela.setItems(FXCollections.observableList(
                        ccp.Pesquisar(ctm.getId(compra) + "", "compra")));
                VerificaStatus();
                txbValorFaltante.setText(FormatString.MonetaryPersistent(CtrlCompra.getValorFaltante(compra)));
            }

        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tabela");
        }
    }

    public void VerificaStatus() {
        CtrlCompra cc = CtrlCompra.create();
        boolean flag = false;
        if (ItemSelecionado != null) {
            Object auxCompra = CtrlParcelaapagar.getCompra(ItemSelecionado);
            if (auxCompra != null) {
                compra = auxCompra;
            } else {
                flag = true;
            }
        }
        if (flag) {
            ArrayList<Object> compras = cc.Pesquisar(cc.getId(compra).toString(), "Id");
            if (compras != null && compras.size() > 0) {
                compra = compras.get(0);
            }
        }
        if (compra != null) {
            String Resultado = cc.VerificaPagamento(compra);
            lblStatus.setText(Resultado);
        }

    }

    private void estadoOriginalCbParcelas() {
        cbFormadePagamento.getItems().clear();
        List<Object> FormasdePagamento = CtrlFormadepagamento.create().Pesquisar("");
        if (FormasdePagamento != null && !FormasdePagamento.isEmpty()) {
            cbFormadePagamento.getItems().addAll(FormasdePagamento);
        }
    }

}
