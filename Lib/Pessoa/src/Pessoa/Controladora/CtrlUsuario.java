/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Controladora;

import Acesso.Entidade.Grupodeacesso;
import Controladora.Base.CtrlBase;
import Endereco.Entidade.Complementoendereco;
import Pessoa.Entidade.Usuario;
import Transacao.Transaction;
import Utils.Imagem;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlUsuario extends CtrlBase {

    private static CtrlUsuario ctrlusuario;

    public static CtrlUsuario create() {
        if (ctrlusuario == null) {
            ctrlusuario = new CtrlUsuario();
        }
        return ctrlusuario;
    }

    public static void setCampos(Object pessoa, TextField txbNome, TextField txbCPF,
            TextField txbEmail, TextField txbRG, TextField txbTelefone, TextArea taObs,
            TextField txbSalario, PasswordField txbSenha, TextField txbLogin, ComboBox<Object> cbNivel) {
        if (pessoa != null && pessoa instanceof Usuario) {
            Usuario p = (Usuario) pessoa;
            txbSalario.setText(p.getUsrSalario().toString());
            txbSenha.setText(p.getUsrSenha());
            txbLogin.setText(p.getUsrLogin());
            cbNivel.getSelectionModel().select(p.getGrupoId());
            txbNome.setText(p.getUsrNome());
            txbCPF.setText(p.getUsrCpf());
            txbEmail.setText(p.getUsrEmail());
            txbRG.setText(p.getUsrRg());
            txbTelefone.setText(p.getUsrTelefone());
            taObs.setText(p.getUsrObs());
        }
    }

    public static BufferedImage getImagem(Object p) {
        if (p != null && p instanceof Usuario) {
            return Imagem.ByteArrayToBufferedImage(((Usuario) p).getUsrFoto());
        }
        return null;
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Usuario) {
            return ((Usuario) p).getUsrId();
        }
        return -1;
    }

    public static Object getEndereco(Object p) {
        if (p != null && p instanceof Usuario) {
            return ((Usuario) p).getEndId();
        }
        return null;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Usuario) {
            txBusca.setText(((Usuario) p).getUsrNome());
        }
    }

    public static String getLogin(Object p) {
        if (p != null && p instanceof Usuario) {
            return ((Usuario) p).getUsrLogin();
        }
        return null;
    }

    public static Object getGrupo(Object p) {
        if (p != null && p instanceof Usuario) {
            return ((Usuario) p).getGrupoId();
        }
        return null;
    }

    public CtrlUsuario() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> usuario = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            List<Usuario> ResultUsuario;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultUsuario = em.createNamedQuery("Usuario.findAll", Usuario.class)
                        .getResultList();
            } else {
                ResultUsuario = em.createNamedQuery("Usuario.findByUsrLogin", Usuario.class)
                        .setParameter("usrLogin", Filtro).getResultList();
            }

            for (Usuario usuarios : ResultUsuario) {
                usuario.add(usuarios);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return usuario;
    }

    public Object Salvar(String Login, byte[] Foto, String Email, String Rg, String Cpf,
            String Nome, String Obs, String Telefone, BigDecimal Salario,
            String Senha, Object oEndereco, Object grupo) {
        Usuario usuario = new Usuario(Login, Senha, Foto, Email, Rg, Cpf,
                Nome, Obs, Telefone, Salario);
        usuario.setUsrDatacadastro(new Date());
        if (grupo != null && grupo instanceof Grupodeacesso) {
            usuario.setGrupoId((Grupodeacesso) grupo);
        }
        if (oEndereco != null && oEndereco instanceof Complementoendereco) {
            usuario.setEndId((Complementoendereco) oEndereco);
        }

        return super.Salvar(usuario);
    }

    public Object Alterar(Object oUsuario, Integer Id, String Login, byte[] Foto, String Email, String Rg, String Cpf,
            String Nome, String Obs, String Telefone, BigDecimal Salario,
            String Senha, Object oEndereco, Object grupo) {
        Usuario usuario = null;
        if (oUsuario != null && oUsuario instanceof Usuario) {
            usuario = (Usuario) oUsuario;
            usuario.setUsrCpf(Cpf);
            usuario.setUsrEmail(Email);
            usuario.setUsrFoto(Foto);
            usuario.setUsrLogin(Login);
            usuario.setUsrNome(Nome);
            usuario.setUsrObs(Obs);
            usuario.setUsrRg(Rg);
            usuario.setUsrSalario(Salario);
            usuario.setUsrSenha(Senha);
            usuario.setUsrTelefone(Telefone);

            if (grupo != null && grupo instanceof Grupodeacesso) {
                usuario.setGrupoId((Grupodeacesso) grupo);
            }
            if (oEndereco != null && oEndereco instanceof Complementoendereco) {
                usuario.setEndId((Complementoendereco) oEndereco);
            }
            usuario = (Usuario) super.Alterar(usuario);
        }
        return usuario;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Usuario.class);
    }
}
