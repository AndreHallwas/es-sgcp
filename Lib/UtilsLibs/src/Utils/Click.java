/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.awt.event.InputEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Raizen
 */
public class Click {
     public static void click(Pane control) {
        java.awt.Point originalLocation = java.awt.MouseInfo.getPointerInfo().getLocation();
        javafx.geometry.Point2D buttonLocation = control.localToScreen(control.getLayoutBounds().getMinX(), control.getLayoutBounds().getMinY());
        try {
            java.awt.Robot robot = new java.awt.Robot();
            robot.mouseMove((int) buttonLocation.getX(), (int) buttonLocation.getY());
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            robot.mouseMove((int) originalLocation.getX(), (int) originalLocation.getY());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
