/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Controladora;

import Controladora.Base.CtrlBase;
import Endereco.Entidade.Complementoendereco;
import Pessoa.Entidade.Cliente;
import Transacao.Transaction;
import Utils.Imagem;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlCliente extends CtrlBase {

    private static CtrlCliente ctrlcliente;

    public static CtrlCliente create() {
        if (ctrlcliente == null) {
            ctrlcliente = new CtrlCliente();
        }
        return ctrlcliente;
    }

    public static void setCliente(Object pessoa, TextField txbNome, TextField txbCPF,
            TextField txbEmail, TextField txbRG, TextField txbTelefone, TextArea taObs) {
        if (pessoa != null && pessoa instanceof Cliente) {
            Cliente p = (Cliente) pessoa;
            txbNome.setText(p.getCliNome());
            txbCPF.setText(p.getCliCpf());
            txbEmail.setText(p.getCliEmail());
            txbRG.setText(p.getCliRg());
            txbTelefone.setText(p.getCliTelefone());
            taObs.setText(p.getCliObs());
        }
    }

    public static BufferedImage getImagem(Object p) {
        if (p != null && p instanceof Cliente) {
            return Imagem.ByteArrayToBufferedImage(((Cliente) p).getCliFoto());
        }
        return null;
    }

    public static Object getEndereco(Object p) {
        if (p != null && p instanceof Cliente) {
            return ((Cliente) p).getEndId();
        }
        return null;
    }

    public static Integer getID(Object p) {
        if (p != null && p instanceof Cliente) {
            return ((Cliente) p).getCliId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Cliente) {
            txBusca.setText(((Cliente) p).getCliNome());
        }
    }

    public static ArrayList<Object> getAnimais(Object oCliente) {
        ArrayList<Object> osAnimais = new ArrayList();
        if (oCliente != null && oCliente instanceof Cliente) {
            osAnimais.addAll(((Cliente) oCliente).getAnimalCollection());
        }
        return osAnimais;
    }

    public static String getNome(Object p) {
        if (p != null && p instanceof Cliente) {
            return ((Cliente) p).getCliNome() != null ? ((Cliente) p).getCliNome() : "Sem Nome";
        }
        return "Sem Nome";
    }

    public static String getCPF(Object p) {
        if (p != null && p instanceof Cliente) {
            return ((Cliente) p).getCliCpf();
        }
        return "";
    }

    public CtrlCliente() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Param) {
        ArrayList<Object> cliente = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            List<Cliente> ResultCliente = null;
            try {
                if (Filtro != null && Filtro.isEmpty()) {
                    if (Param != null && !Param.equalsIgnoreCase("CPF")) {
                        ResultCliente = em.createNamedQuery("Cliente.findAll", Cliente.class)
                                .getResultList();
                    }
                } else if (Param != null) {
                    if (Param.equalsIgnoreCase("Id")) {
                        ResultCliente = em.createNamedQuery("Cliente.findByCliId", Cliente.class)
                                .setParameter("cliId", Integer.parseInt(Filtro)).getResultList();
                    } else if (Param.equalsIgnoreCase("CPF")) {
                        ResultCliente = em.createNamedQuery("Cliente.findByCliCpf", Cliente.class)
                                .setParameter("cliCpf", Filtro).getResultList();
                    } else if (Param.equalsIgnoreCase("Geral")) {
                        ResultCliente = em.createNamedQuery("Cliente.findByCliGeral", Cliente.class)
                                .setParameter("filtro", Filtro).getResultList();
                    }
                }

            } catch (Exception ex) {
                ResultCliente = em.createNamedQuery("Cliente.findByCliCpf", Cliente.class)
                        .setParameter("cliCpf", Filtro).getResultList();
            }
            if (ResultCliente != null) {
                for (Cliente clientes : ResultCliente) {
                    cliente.add(clientes);
                }
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return cliente;
    }

    public Object Salvar(String Cpf, String Rg, byte[] Foto, String Nome, String Telefone,
            String Email, String Obs, Object oEndereco) {
        Cliente cliente = new Cliente(Cpf, Rg, Foto, Nome, Telefone, Email, Obs);
        cliente.setCliDatacadastro(new Date());
        if (oEndereco != null && oEndereco instanceof Complementoendereco) {
            cliente.setEndId((Complementoendereco) oEndereco);
        }

        return super.Salvar(cliente);
    }

    public Object Alterar(Object oCliente, Integer Id, String Cpf, String Rg, byte[] Foto, String Nome, String Telefone,
            String Email, String Obs, Object oEndereco) {
        Cliente cliente = null;
        if (oCliente != null && oCliente instanceof Cliente) {
            cliente = (Cliente) oCliente;
            cliente.setCliCpf(Cpf);
            cliente.setCliEmail(Email);
            cliente.setCliFoto(Foto);
            cliente.setCliNome(Nome);
            cliente.setCliObs(Obs);
            cliente.setCliRg(Rg);
            cliente.setCliTelefone(Telefone);

            if (oEndereco != null && oEndereco instanceof Complementoendereco) {
                cliente.setEndId((Complementoendereco) oEndereco);
            }
            cliente = (Cliente) super.Alterar(cliente);
        }
        return cliente;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Cliente.class);
    }

}
