/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Entidade;

import JPA.Dao.JPADao;
import Servico.Entidade.Servico;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "servicodavenda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servicodavenda.findAll", query = "SELECT s FROM Servicodavenda s")
    , @NamedQuery(name = "Servicodavenda.findByVendaId", query = "SELECT s FROM Servicodavenda s WHERE s.servicodavendaPK.vendaId = :vendaId")
    , @NamedQuery(name = "Servicodavenda.findByServicoId", query = "SELECT s FROM Servicodavenda s WHERE s.servicodavendaPK.servicoId = :servicoId")
    , @NamedQuery(name = "Servicodavenda.findByServvQuantidade", query = "SELECT s FROM Servicodavenda s WHERE s.servvQuantidade = :servvQuantidade")
    , @NamedQuery(name = "Servicodavenda.findByServvValor", query = "SELECT s FROM Servicodavenda s WHERE s.servvValor = :servvValor")})
public class Servicodavenda extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ServicodavendaPK servicodavendaPK;
    @Basic(optional = false)
    @Column(name = "servv_quantidade")
    private int servvQuantidade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "servv_valor")
    private BigDecimal servvValor;
    @JoinColumn(name = "servico_id", referencedColumnName = "servico_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Servico servico;
    @JoinColumn(name = "venda_id", referencedColumnName = "venda_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Venda venda;

    public Servicodavenda() {
    }

    public Servicodavenda(ServicodavendaPK servicodavendaPK) {
        this.servicodavendaPK = servicodavendaPK;
    }

    public Servicodavenda(ServicodavendaPK servicodavendaPK, int servvQuantidade, BigDecimal servvValor) {
        this.servicodavendaPK = servicodavendaPK;
        this.servvQuantidade = servvQuantidade;
        this.servvValor = servvValor;
    }

    public Servicodavenda(int servicoId, int vendaId, int servvQuantidade, BigDecimal servvValor) {
        this.servicodavendaPK = new ServicodavendaPK(vendaId, servicoId);
        this.servvQuantidade = servvQuantidade;
        this.servvValor = servvValor;
    }

    public Servicodavenda(int vendaId, int servicoId) {
        this.servicodavendaPK = new ServicodavendaPK(vendaId, servicoId);
    }

    public ServicodavendaPK getServicodavendaPK() {
        return servicodavendaPK;
    }

    public void setServicodavendaPK(ServicodavendaPK servicodavendaPK) {
        this.servicodavendaPK = servicodavendaPK;
    }

    public int getServvQuantidade() {
        return servvQuantidade;
    }

    public void setServvQuantidade(int servvQuantidade) {
        this.servvQuantidade = servvQuantidade;
    }

    public BigDecimal getServvValor() {
        return servvValor;
    }

    public void setServvValor(BigDecimal servvValor) {
        this.servvValor = servvValor;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (servicodavendaPK != null ? servicodavendaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicodavenda)) {
            return false;
        }
        Servicodavenda other = (Servicodavenda) object;
        if ((this.servicodavendaPK == null && other.servicodavendaPK != null) || (this.servicodavendaPK != null && !this.servicodavendaPK.equals(other.servicodavendaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.Servicodavenda[ servicodavendaPK=" + servicodavendaPK + " ]";
    }
    
}
