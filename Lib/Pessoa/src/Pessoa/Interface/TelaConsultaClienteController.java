/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Interface;

import Pessoa.Controladora.CtrlCliente;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import Utils.UI.Tela.Tela;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaClienteController implements Initializable, Tela {

    private static Object cliente;
    private static boolean btnPainel = true;

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcRG;
    @FXML
    private TableColumn<Object, String> tcCPF;
    @FXML
    private TableColumn<Object, String> tcTelefone;
    @FXML
    private TableColumn<Object, String> tcEndereco;
    @FXML
    private TableColumn<Object, String> tcEmail;
    @FXML
    private TableColumn<Object, String> tcObs;
    @FXML
    private HBox btnGp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("CliNome"));
        tcEmail.setCellValueFactory(new PropertyValueFactory("CliEmail"));
        tcEndereco.setCellValueFactory(new PropertyValueFactory("EndId"));
        tcCPF.setCellValueFactory(new PropertyValueFactory("CliCpf"));
        tcObs.setCellValueFactory(new PropertyValueFactory("CliObs"));
        tcRG.setCellValueFactory(new PropertyValueFactory("CliRg"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("CliTelefone"));
        CarregaTabela("");
        setCliente(null);
        btnGp.setVisible(btnPainel);
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        if (getCliente() != null) {
            evtCancelar(event);
        }
    }

    private void CarregaTabela(String filtro) {
        CtrlCliente ctm = CtrlCliente.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, "Geral")));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex);
        }
    }

    public static TelaConsultaClienteController Create(Class classe, Pane pndados) {
        TelaConsultaClienteController CCliente = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Pessoa/Interface/TelaConsultaCliente.FXML.fxml"));
            Parent root = Loader.load();
            CCliente = (TelaConsultaClienteController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CCliente;
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        CtrlUtils.CloseStage(event);
    }

    @FXML
    private void evtBuscar() {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            TelaConsultaClienteController.setCliente(tabela.getItems().get(lin));
        }
    }

    /**
     * @return the cliente
     */
    public static Object getCliente() {
        return cliente;
    }

    /**
     * @return the btnPainel
     */
    public static boolean isBtnPainel() {
        return btnPainel;
    }

    /**
     * @param aBtnPainel the btnPainel to set
     */
    public static void setBtnPainel(boolean aBtnPainel) {
        btnPainel = aBtnPainel;
    }

    /**
     * @param aCliente the cliente to set
     */
    public static void setCliente(Object aCliente) {
        cliente = aCliente;
    }

}
