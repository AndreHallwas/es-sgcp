/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Controladora;

import Compra.Entidade.Compra;
import Compra.Entidade.Produtodacompra;
import ContaaPagar.Entidade.Contaapagar;
import ContaaPagar.Entidade.Parcelaapagar;
import Controladora.Base.CtrlBase;
import FormadePagamento.Entidade.Formadepagamento;
import Fornecedor.Entidade.Fornecedor;
import Pessoa.Entidade.Usuario;
import Produto.Entidade.Produto;
import Transacao.Transacao;
import Transacao.Transaction;
import Utils.Carrinho.Carrinho;
import Utils.DateUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXComboBox;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlCompra extends CtrlBase {

    private static CtrlCompra ctrlcompra;

    public static CtrlCompra create() {
        if (ctrlcompra == null) {
            ctrlcompra = new CtrlCompra();
        }
        return ctrlcompra;
    }

    public CtrlCompra() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> compra = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Compra> ResultCompra = null;
            if (Filtro == null || Filtro.isEmpty()) {
                ResultCompra = em.createNamedQuery("Compra.findAll", Compra.class)
                        .getResultList();
            } else if (Tipo != null && !Tipo.trim().isEmpty()) {
                try {
                    if (Tipo.equalsIgnoreCase("Id")) {
                        ResultCompra = em.createNamedQuery("Compra.findByCompraId", Compra.class)
                                .setParameter("compraId", Integer.parseInt(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("Periodo")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultCompra = em.createNamedQuery("Compra.findByBetween", Compra.class)
                                    .setParameter("datainicial", new Date(auxFiltro[0]))
                                    .setParameter("datafinal", new Date(auxFiltro[1])).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("Geral")) {
                        try {
                            ResultCompra = em.createNamedQuery("Compra.findByCompraData", Compra.class)
                                    .setParameter("compraData", new Date(Filtro)).getResultList();
                        } catch (Exception ex) {
                            System.out.println(ex.getMessage());
                            ResultCompra = em.createNamedQuery("Compra.findByAllGeral", Compra.class)
                                    .setParameter("compraId", Integer.parseInt(Filtro))
                                    .setParameter("compraValorTotal", Double.parseDouble(Filtro)).getResultList();
                        }
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    try {
                        ResultCompra = em.createNamedQuery("Compra.findByCompraSituacao", Compra.class)
                                .setParameter("compraSituacao", Boolean.parseBoolean(Filtro)).getResultList();
                    } catch (Exception exc) {
                        ResultCompra = em.createNamedQuery("Compra.findByCompraObs", Compra.class)
                                .setParameter("compraObs", Filtro).getResultList();
                        System.out.println(ex.getMessage());
                    }
                }
            }

            for (Compra compras : ResultCompra) {
                compra.add(compras);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return compra;
    }

    public Object RegistrarCompraNovo(BigDecimal valorTotal, Integer Parcelas, String Obs, Object fornecedor,
            Object usuario, Object formadepagamento, Integer Acrescimo, Integer Desconto, Integer Juros,
            String Descricao, ArrayList<Object> produtos, ArrayList<Integer> quantidade,
            ArrayList<Object> valores, ArrayList<Object> datas) {

        Compra compra = null;
        EntityManager em = null;

        try {
            /////Inicia a Transacao
            em = getEntityManager();
            em.getTransaction().begin();

            boolean SituacaoGeral = false;

            /////Gera a Compra
            compra = new Compra(valorTotal, new Date(), Obs, SituacaoGeral);
            if (fornecedor != null && fornecedor instanceof Fornecedor) {
                compra.setForId((Fornecedor) fornecedor);
            }
            if (usuario != null && usuario instanceof Usuario) {
                compra.setUsrId((Usuario) usuario);
            }

            /////Persiste a Compra para gerar o Id
            em.persist(compra);
            em.flush();

            /////Gera os Produtos da Compra
            Produto produto;
            Produtodacompra produtodacompra;
            ArrayList<Produtodacompra> Produtosdacompra = new ArrayList();
            for (int i = 0; i < produtos.size(); i++) {
                produto = (Produto) produtos.get(i);
                /////Gerar Produto da Compra
                produtodacompra = new Produtodacompra(produto.getProdutoId(), compra.getCompraId(), quantidade.get(i), produto.getProdutoPreco().multiply(new BigDecimal(quantidade.get(i).intValue())));/////Quantidade
                produtodacompra.setCompra(compra);
                produtodacompra.setProduto(produto);
                /////Adicionar ao Estoque
                produto.setProdutoQuantidade(produto.getProdutoQuantidade() + quantidade.get(i));/////transacao
                produto = em.merge(produto);
                /////Adiciona na Lista
                Produtosdacompra.add(produtodacompra);
            }
            /////Adicionar Produtosdacompra na Compra;
            compra.setProdutodacompraCollection(Produtosdacompra);

            /////Gera a Conta a Pagar
            Contaapagar contaapagar = new Contaapagar(valorTotal, SituacaoGeral, Acrescimo,
                    Desconto, Juros, new Date(), Descricao, Parcelas);
            if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
                contaapagar.setFormadepagamentoId((Formadepagamento) formadepagamento);
            }
            contaapagar.setTipodedespesaId(null);
            contaapagar.setCompraId(compra);

            /////Adicionar a Conta a Pagar a Compra
            ArrayList<Contaapagar> Contasapagar = new ArrayList();
            Contasapagar.add(contaapagar);
            compra.setContaapagarCollection(Contasapagar);

            /////Persiste a conta para gerar o Id
            em.persist(contaapagar);
            em.flush();

            /////Gerar Parcelas a Pagar
            BigDecimal ValorParcela;
            Date DataVencimentoParcela;
            Parcelaapagar parcelaaPagar;
            ArrayList<Parcelaapagar> Parcelasapagar = new ArrayList();
            for (int i = 0, NumerodaParcela = 1; i < Parcelas; i++, NumerodaParcela++) {
                ValorParcela = new BigDecimal((Double) valores.get(i));
                DataVencimentoParcela = (Date) datas.get(i);

                parcelaaPagar = new Parcelaapagar(contaapagar.getContaapagarId(), NumerodaParcela,
                        DataVencimentoParcela, null, ValorParcela, SituacaoGeral, BigDecimal.ZERO, BigDecimal.ZERO, new Date());
                parcelaaPagar.setFormadepagamentoId(contaapagar.getFormadepagamentoId());/////Old contaapagar.getFormadepagamentoId()
                parcelaaPagar.setContaapagar(contaapagar);
                /////Adiciona na Lista de Parcelas a Pagar
                Parcelasapagar.add(parcelaaPagar);
            }
            /////Adicionar as Parcelas a Pagar a Conta a Pagar;
            contaapagar.setParcelaapagarCollection(Parcelasapagar);

            /////Finaliza a Compra
            compra = em.merge(compra);

            /////Finaliza a Transação
            em.getTransaction().commit();

            Message = "Compra Realizada com Sucesso";
        } catch (Exception ex) {
            if (em != null) {
                Mensagem.ExibirException(ex, ":CtrlCompra:284");
                em.getTransaction().rollback();
            }
            Message = "Erro, a Compra não Realizada";
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return compra;
    }

    public Compra ModificarCompraNovo(Object aCompra, Date Data, boolean SituacaoCompra, BigDecimal valorTotal, Integer Parcelas, String Obs, Object fornecedor,
            Object usuario, Object formadepagamento, Integer Acrescimo, Integer Desconto, Integer Juros,
            String Descricao, ArrayList<Object> produtos, ArrayList<Integer> quantidade,
            ArrayList<Object> valores, ArrayList<Object> datas) {

        Compra compra = null;
        EntityManager em = null;

        try {
            if (aCompra != null && aCompra instanceof Compra) {
                compra = (Compra) aCompra;
                /////Inicia a Transacao
                em = getEntityManager();
                em.getTransaction().begin();

                boolean SituacaoGeral = false;
                /////Altera a Compra
                compra.setCompraValorTotal(valorTotal);
                compra.setCompraData(Data);
                compra.setCompraObs(Obs);
                compra.setCompraSituacao(SituacaoCompra);

                if (fornecedor != null && fornecedor instanceof Fornecedor) {
                    compra.setForId((Fornecedor) fornecedor);
                }
                if (usuario != null && usuario instanceof Usuario) {
                    compra.setUsrId((Usuario) usuario);
                }

                /////Alterar os Produtos da Compra
                if (compra.getProdutodacompraCollection() != null) {
                    Produtodacompra produtodacompra;
                    Collection<Produtodacompra> Produtoc = compra.getProdutodacompraCollection();
                    ArrayList<Produtodacompra> oProdutodacompra = new ArrayList();
                    for (Produtodacompra produtodacompras : Produtoc) {
                        oProdutodacompra.add(produtodacompras);
                    }
                    ArrayList<Produtodacompra> Adicionados = new ArrayList();
                    ArrayList<Produtodacompra> NaoModificados = new ArrayList();
                    ArrayList<Produtodacompra> Removidos = new ArrayList();
                    Removidos.addAll(oProdutodacompra);
                    for (Object oProduto : produtos) {
                        int i = 0;
                        while (i < oProdutodacompra.size() && !ComparaProdutodacompra((Produto) oProduto,
                                oProdutodacompra.get(i).getProdutodacompraPK().getProdutoId())) {
                            i++;
                        }
                        if (i < oProdutodacompra.size()) {
                            produtodacompra = oProdutodacompra.get(i);
                            /////Altera o Estoque
                            produtodacompra.getProduto().setProdutoQuantidade((produtodacompra.getProduto().getProdutoQuantidade() - produtodacompra.getProdcQuantidade())
                                    + quantidade.get(i));
                            produtodacompra.setProduto(em.merge(produtodacompra.getProduto()));
                            /////Altera o Produto da Compra
                            produtodacompra.setProdcQuantidade(quantidade.get(i));
                            produtodacompra.setProdcValor(((Produto) oProduto).getProdutoPreco().multiply(new BigDecimal(quantidade.get(i).intValue())));
                            /////Adiciona na Lista de Não Modificados
                            NaoModificados.add(oProdutodacompra.get(i));
                            Removidos.remove(oProdutodacompra.get(i));
                        } else {
                            /////Gerar Produto da Compra
                            produtodacompra = new Produtodacompra(((Produto) oProduto).getProdutoId(), compra.getCompraId(), quantidade.get(i),
                                    ((Produto) oProduto).getProdutoPreco().multiply(new BigDecimal(quantidade.get(i).intValue())));
                            produtodacompra.setCompra(compra);
                            produtodacompra.setProduto((Produto) oProduto);
                            /////Adicionar a Lista de Novos
                            Adicionados.add(produtodacompra);
                        }
                    }
                    for (Produtodacompra Removido : Removidos) {
                        /////Subtrair Do Estoque
                        Removido.getProduto().setProdutoQuantidade(Removido.getProduto().getProdutoQuantidade() - Removido.getProdcQuantidade());
                        Removido.setProduto(em.merge(Removido.getProduto()));
                    }
                    for (Produtodacompra Adicionado : Adicionados) {
                        /////Adicionar Ao Estoque
                        Adicionado.getProduto().setProdutoQuantidade(Adicionado.getProduto().getProdutoQuantidade() + Adicionado.getProdcQuantidade());
                        Adicionado.setProduto(em.merge(Adicionado.getProduto()));
                    }
                    /////Adicionar Alterações a Compra
                    compra.setProdutodacompraCollection(new ArrayList());
                    compra.getProdutodacompraCollection().addAll(NaoModificados);
                    compra.getProdutodacompraCollection().addAll(Adicionados);
                }

                /////Guarda as Contas a Remover
                Collection<Contaapagar> Contasaremover = compra.getContaapagarCollection();
                /////Limpa as Contas da Compra
                compra.setContaapagarCollection(new ArrayList());
                /////Altera a Compra para Remover as Contas
                compra = em.merge(compra);
                em.flush();

                ///Remover as Parcelas da Compra
                if (Contasaremover != null) {
                    for (Contaapagar contaapagar : Contasaremover) {
                        em.remove(em.find(Contaapagar.class, contaapagar.getContaapagarId()));
                    }
                }

                /////Gera a Nova Conta a Pagar
                Contaapagar contaapagar = new Contaapagar(valorTotal, SituacaoGeral, Acrescimo,
                        Desconto, Juros, new Date(), Descricao, Parcelas);
                if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
                    contaapagar.setFormadepagamentoId((Formadepagamento) formadepagamento);
                }
                contaapagar.setTipodedespesaId(null);
                contaapagar.setCompraId(compra);

                /////Adicionar a Conta a Pagar a Compra
                ArrayList<Contaapagar> Contasapagar = new ArrayList();
                Contasapagar.add(contaapagar);
                compra.setContaapagarCollection(Contasapagar);

                /////Persiste a conta para gerar o Id
                em.persist(contaapagar);
                em.flush();

                /////Gerar Parcelas a Pagar
                BigDecimal ValorParcela;
                Date DataVencimentoParcela;
                Parcelaapagar parcelaaPagar;
                ArrayList<Parcelaapagar> Parcelasapagar = new ArrayList();
                for (int i = 0, NumerodaParcela = 1; i < Parcelas; i++, NumerodaParcela++) {
                    ValorParcela = new BigDecimal((Double) valores.get(i));
                    DataVencimentoParcela = (Date) datas.get(i);

                    parcelaaPagar = new Parcelaapagar(contaapagar.getContaapagarId(), NumerodaParcela,
                            DataVencimentoParcela, null, ValorParcela, SituacaoGeral, BigDecimal.ZERO, BigDecimal.ZERO, new Date());
                    parcelaaPagar.setFormadepagamentoId(contaapagar.getFormadepagamentoId());/////Old contaapagar.getFormadepagamentoId()
                    parcelaaPagar.setContaapagar(contaapagar);
                    /////Adiciona na Lista de Parcelas a Pagar
                    Parcelasapagar.add(parcelaaPagar);
                }
                /////Adicionar as Parcelas a Pagar a Conta a Pagar;
                contaapagar.setParcelaapagarCollection(Parcelasapagar);

                /////Finaliza a Compra
                compra = em.merge(compra);

                /////Finaliza a Transação
                em.getTransaction().commit();

                Message = "Compra Realizada com Sucesso";
            } else {
                Message = "Compra Inválida!";
            }

        } catch (Exception ex) {
            if (em != null) {
                Mensagem.ExibirException(ex, ":CtrlCompra:434");
                if (em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
            }
            Message = "Erro, a Compra não Realizada";
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return compra;
    }

    private boolean ComparaProdutodacompra(Produto oProduto, Integer produtodacompra) {
        return oProduto.getProdutoId().equals(produtodacompra);
    }

    public boolean RemoverCompra(Integer Id) {
        boolean flag = true;

        Compra compra = null;
        EntityManager em = null;

        try {
            if (Id != null) {
                /////Inicia a Transacao
                em = getEntityManager();
                em.getTransaction().begin();

                compra = em.find(Compra.class, Id);
                if (compra != null) {
                    /////Subtrai do estoque
                    for (Produtodacompra produtodacompra : compra.getProdutodacompraCollection()) {
                        produtodacompra.getProduto().setProdutoQuantidade(produtodacompra.getProduto().getProdutoQuantidade() - produtodacompra.getProdcQuantidade());
                        produtodacompra.setProduto(em.merge(produtodacompra.getProduto()));
                    }
                    /////Remove a Compra
                    em.remove(compra);
                }

                /////Finaliza a Transação
                em.getTransaction().commit();
            }
        } catch (Exception ex) {
            if (em != null) {
                Mensagem.ExibirException(ex, ":CtrlCompra:625");
                em.getTransaction().rollback();
            }
            flag = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return flag;
    }

    public static void setCampos(Object compra, TextField txbValorTotal, TextField txbParcelas) {
        if (compra != null && compra instanceof Compra) {
            Compra auxCompra = (Compra) compra;
            txbValorTotal.setText(auxCompra.getCompraValorTotal() + "");
            if (txbParcelas != null && auxCompra.getContaapagarCollection() != null
                    && !auxCompra.getContaapagarCollection().isEmpty()) {
                Contaapagar conta = (Contaapagar) new ArrayList(auxCompra.getContaapagarCollection()).get(0);
                txbParcelas.setText(conta.getContaapagarParcelas() + "");
            }
        }
    }

    public static void setCampos(Object compra, Label lblValor, Label lblData, TextField txbCompra, TextField txbTotal) {
        if (compra != null && compra instanceof Compra) {
            Compra auxCompra = (Compra) compra;
            lblValor.setText(auxCompra.getCompraValorTotal() + "");
            lblData.setText(DateUtils.asLocalDate(auxCompra.getCompraData()).toString());
            txbCompra.setText(auxCompra.getCompraId() + "");
            txbTotal.setText(auxCompra.getCompraValorTotal() + "");
        }
    }

    public static void setCampos(Object compra, Label lblValor, Label lblData) {
        if (compra != null && compra instanceof Compra) {
            Compra auxCompra = (Compra) compra;
            lblValor.setText(auxCompra.getCompraValorTotal() + "");
            lblData.setText(DateUtils.asLocalDate(auxCompra.getCompraData()).toString());
        }
    }

    public static void setCampos(Object compra, TextField txbFornecedor) {
        if (compra != null && compra instanceof Compra) {
            Compra auxCompra = (Compra) compra;
            if (auxCompra.getForId() != null) {
                txbFornecedor.setText(auxCompra.getForId().getForNome());
            }
        }
    }

    public static void setCampos(Object compra, TextField txbValorTotal, JFXComboBox<Object> cbFormadePagamento,
            TextField txbParcelas, DatePicker txbVencimento, TextField txbDiasEntreParcelas) {
        if (compra != null && compra instanceof Compra) {
            try {
                Compra aCompra = (Compra) compra;
                txbValorTotal.setText(aCompra.getCompraValorTotal().toString());
                if (aCompra.getContaapagarCollection() != null && aCompra.getContaapagarCollection().size() > 0) {
                    ArrayList<Contaapagar> Contas = new ArrayList(aCompra.getContaapagarCollection());
                    if (Contas.size() > 0) {
                        txbParcelas.setText(Contas.get(0).getContaapagarParcelas() + "");
                        ArrayList<Parcelaapagar> Parcelas = new ArrayList(Contas.get(0).getParcelaapagarCollection());
                        txbVencimento.setValue(DateUtils.asLocalDate(Parcelas.get(0).getParpDataVencimento()));
                        cbFormadePagamento.getSelectionModel().select(Parcelas.get(0).getFormadepagamentoId());
                        if (Contas.size() > 1) {
                            txbDiasEntreParcelas.setText(DateUtils.DiferenceDays(Parcelas.get(0).getParpDataVencimento(),
                                    Parcelas.get(1).getParpDataVencimento()) + "");
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    public static Carrinho setCarrinho(Object compra) {
        Carrinho c = new Carrinho();
        if (compra != null && compra instanceof Compra) {
            Compra auxCompra = (Compra) compra;
            if (auxCompra.getProdutodacompraCollection() != null) {
                for (Produtodacompra produtodacompra : auxCompra.getProdutodacompraCollection()) {
                    c.add(produtodacompra.getProduto(), produtodacompra.getProdcQuantidade());
                }
            }
        }
        return c;
    }

    public static Integer getId(Object compra) {
        if (compra != null && compra instanceof Compra) {
            return ((Compra) compra).getCompraId();
        }
        return -1;
    }

    public static Double getValorTotal(Object compra) {
        if (compra != null && compra instanceof Compra) {
            return ((Compra) compra).getCompraValorTotal().doubleValue();
        }
        return -1.0;
    }

    public static BigDecimal getValorFaltante(Object compra) {
        BigDecimal valor = new BigDecimal(0);
        if (compra != null && compra instanceof Compra) {
            BigDecimal ValorPago = new BigDecimal(0);
            BigDecimal ValorTotal = new BigDecimal(0);
            Compra aCompra = (Compra) compra;
            for (Contaapagar contaapagar : aCompra.getContaapagarCollection()) {
                for (Parcelaapagar parcelaapagar : contaapagar.getParcelaapagarCollection()) {
                    if (parcelaapagar.getParpSituacao() != null && parcelaapagar.getParpSituacao()) {
                        ValorPago = ValorPago.add(parcelaapagar.getParpValorPago());
                    }
                    ValorTotal = ValorTotal.add(parcelaapagar.getParpValor());
                }
            }
            valor = ValorTotal.subtract(ValorPago);
        }
        return valor;
    }

    public static ArrayList<Object> getParcelas(Object compra) {
        ArrayList<Object> Retorno = new ArrayList();
        if (compra != null && compra instanceof Compra) {
            Compra auxCompra = (Compra) compra;
            if (auxCompra.getContaapagarCollection() != null) {
                for (Contaapagar contaapagar : auxCompra.getContaapagarCollection()) {
                    Retorno.add(contaapagar);
                }
            }
        }
        return Retorno;
    }

    public static ArrayList<Object> getValorParcelas(Object compra) {//
        ArrayList<Object> Parcelas = new ArrayList();
        if (compra != null && compra instanceof Compra) {
            Compra aCompra = (Compra) compra;
            if (aCompra.getContaapagarCollection() != null && !aCompra.getContaapagarCollection().isEmpty()) {
                for (Contaapagar contaapagar : aCompra.getContaapagarCollection()) {
                    for (Parcelaapagar parcelaapagar : contaapagar.getParcelaapagarCollection()) {
                        Parcelas.add(parcelaapagar.getParpValor().doubleValue());
                    }
                }
            }
        }
        return Parcelas;
    }

    public static ArrayList<Object> getDataParcelas(Object compra) {//
        ArrayList<Object> Parcelas = new ArrayList();
        if (compra != null && compra instanceof Compra) {
            Compra aCompra = (Compra) compra;
            if (aCompra.getContaapagarCollection() != null && !aCompra.getContaapagarCollection().isEmpty()) {
                for (Contaapagar contaapagar : aCompra.getContaapagarCollection()) {
                    for (Parcelaapagar parcelaapagar : contaapagar.getParcelaapagarCollection()) {
                        Parcelas.add(parcelaapagar.getParpDataVencimento());
                    }
                }
            }
        }
        return Parcelas;
    }

    public static Object getFornecedor(Object compra) {
        if (compra != null && compra instanceof Compra) {
            Compra aCompra = (Compra) compra;
            return aCompra.getForId();
        }
        return null;
    }

    public String VerificaPagamento(Object compra) {

        String Resultado = "";
        Contaapagar aConta = null;
        Boolean Situacao = null;

        if (compra != null && compra instanceof Compra) {
            Compra aCompra = (Compra) compra;
            ArrayList<Contaapagar> Contas = new ArrayList(aCompra.getContaapagarCollection());
            if (Contas.size() > 0) {
                aConta = Contas.get(0);
                ArrayList<Parcelaapagar> Parcelas = new ArrayList(aConta.getParcelaapagarCollection());
                if (Parcelas.size() > 0) {
                    int Count = 0;
                    for (Parcelaapagar parcelaapagar : Parcelas) {
                        if (parcelaapagar.getParpSituacao() == true) {
                            Count++;
                        }
                    }
                    if (Count >= Parcelas.size()) {
                        Resultado = "Compra Paga";
                        if (aConta.getContaapagarSituacao() == false) {
                            Situacao = true;
                        }
                    } else {
                        if (Count == 0) {
                            Resultado = "Não Pago";
                        } else if (Count < Parcelas.size()) {
                            Resultado = "Parcela Paga";
                        }
                        if (aConta.getContaapagarSituacao() == true) {
                            Situacao = false;
                        }
                    }
                }
            } else {
                Resultado = "Não Gerado";
            }
        } else {
            Resultado = "Compra Inválida!";
        }
        
        if (aConta != null && Situacao != null) {
            final Object Objeto = aConta;
            Transacao T = new Transacao() {
                @Override
                public Object Transacao(Object... Params) {
                    return em.merge(Objeto);
                }
            };
            aConta = (Contaapagar) T.Realiza();
        }
        return Resultado;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Compra.class);
    }

}
/////Inserir/excluir parcelas e aumentar contadores e valores V

/////Alterar Situação da Compra ao pagar todas as parcelas V
/////pagamento parcial, Remover pagamento parcial ao estornar V
/////verificar se já está pago ou não. V
/////só pagar a proxima a vencer V
