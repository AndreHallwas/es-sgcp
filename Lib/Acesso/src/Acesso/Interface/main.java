/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Interface;

import Utils.Mensagem;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Raizen
 */
public class main extends Application {
  

    private static Stage _stage;

    @Override
    public void start(Stage stage) throws Exception {
        try {
            
            Parent root = FXMLLoader.load(getClass().getResource("/Acesso/Interface/TelaCadNivel.fxml"));

            Scene scene = new Scene(root);
            scene.getStylesheets().add("/Estilos/DarkTheme.css");
            stage.setScene(scene);
            stage.show();
        } catch (Exception ex) {
            Mensagem.Exibir(ex.getMessage(), 2);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
