/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Entidade;

import JPA.Dao.JPADao;
import Utils.Acao.ButtonAcao;
import Utils.Hash;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "componente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Componente.findAll", query = "SELECT c FROM Componente c")
    , @NamedQuery(name = "Componente.findByComponenteId", query = "SELECT c FROM Componente c WHERE c.componenteId = :componenteId")
    , @NamedQuery(name = "Componente.findByComponenteDescricao", query = "SELECT c FROM Componente c WHERE c.componenteDescricao = :componenteDescricao")})
public class Componente extends ButtonAcao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "componente_id")
    protected String componenteId;
    @Basic(optional = false)
    @Column(name = "componente_nome")
    private String componenteNome;
    @Column(name = "componente_descricao")
    protected String componenteDescricao;
    @Transient
    protected ArrayList<Object> Objetos;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "componenteId")
    private Collection<Permissaocomponente> permissaocomponenteCollection = new ArrayList();

    public Componente() {
    }

    public Componente(String componenteId) {
        this.componenteId = componenteId;
        Objetos = new ArrayList();
        Objetos.add(this);
    }

    public Componente(String componenteId, String componenteDescricao) {
        this.componenteId = componenteId;
        this.componenteDescricao = componenteDescricao;
        Objetos = new ArrayList();
        Objetos.add(this);
    }
    
    public void Constroi(){
        setComponenteId(Hash.stringHexa(Hash.gerarHash(this.getClass().getName(), "MD5")));
        setComponenteDescricao(this.getClass().getName());
    }

    public boolean Compara(String NomeRegistro) {
        return Hash.stringHexa(Hash.gerarHash(NomeRegistro, "MD5")).equalsIgnoreCase(componenteId);
    }

    public String getComponenteId() {
        return componenteId;
    }

    public void setComponenteId(String componenteId) {
        this.componenteId = componenteId;
    }

    public String getComponenteDescricao() {
        return componenteDescricao;
    }

    public void setComponenteDescricao(String componenteDescricao) {
        this.componenteDescricao = componenteDescricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (componenteId != null ? componenteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Componente)) {
            return false;
        }
        Componente other = (Componente) object;
        if ((this.componenteId == null && other.componenteId != null) || (this.componenteId != null && !this.componenteId.equals(other.componenteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        /*return "testedb2.Componente[ componenteId=" + componenteId + " ]";*/
        return componenteDescricao;
    }

    @XmlTransient
    public Collection<Permissaocomponente> getPermissaocomponenteCollection() {
        return permissaocomponenteCollection;
    }

    public void setPermissaocomponenteCollection(Collection<Permissaocomponente> permissaocomponenteCollection) {
        this.permissaocomponenteCollection = permissaocomponenteCollection;
    }

    public String getComponenteNome() {
        return componenteNome;
    }

    public void setComponenteNome(String componenteNome) {
        this.componenteNome = componenteNome;
    }

    /**
     * @return the Objetos
     */
    public ArrayList<Object> getObjetos() {
        return Objetos;
    }

    /**
     * @param Objetos the Objetos to set
     */
    public void setObjetos(ArrayList<Object> Objetos) {
        this.Objetos = Objetos;
    }

    public void addObjeto(Object Objeto) {
        if(Objetos == null){
            Objetos = new ArrayList();
        }
        Objetos.add(Objeto);
    }

    @Override
    protected void AddNodes() {
        addNode("CheckBox", "ativ", "Ativar");
        /*addNode("CheckBox", "visiv", "Visivel");*/
    }
    
}
