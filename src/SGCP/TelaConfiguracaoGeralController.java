/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP;

import Transacao.Transaction;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConfiguracaoGeralController implements Initializable {

    private static boolean Next;
    private static Node root;

    public static boolean Conecta() {
        return Transaction.isStarted() && Transaction.isCreated();
    }

    @FXML
    private JFXComboBox<String> cbConexao;
    @FXML
    private Label lblStatusConexao;
    @FXML
    private Label lblStatusBanco;
    @FXML
    private JFXTextArea taBanco;
    @FXML
    private JFXButton btnConfirmar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private JFXButton btnGerarBanco;
    private boolean StatusConexão;
    private boolean StatusBanco;
    private final String CacheComboBox = "";

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        EstadoOriginal();
        TestarConexao();
    }

    private void EstadoOriginal() {
        cbConexao.getItems().clear();
        cbConexao.getItems().addAll(Transaction.getPersistenceUs());
        cbConexao.getSelectionModel().selectFirst();
        lblStatusBanco.setVisible(false);
        lblStatusConexao.setVisible(false);
        btnConfirmar.setDisable(true);
        btnGerarBanco.setDisable(false);
        btnCancelar.setDisable(false);
        taBanco.clear();
    }

    private boolean TestarConexao() {
        StatusConexão = Transaction.isStarted();
        lblStatusConexao.setVisible(true);
        if (StatusConexão) {
            lblStatusConexao.setText("OK");
            lblStatusBanco.setVisible(true);
            TestarBanco();
        } else {
            EstadoOriginal();
            lblStatusConexao.setText("Erro");
            lblStatusConexao.setVisible(true);
        }
        return StatusConexão;
    }

    private boolean TestarBanco() {
        StatusBanco = Transaction.isCreated();
        if (!StatusBanco) {
            btnGerarBanco.setDisable(false);
            lblStatusBanco.setText("Criar");
        } else {
            lblStatusBanco.setText("OK");
            btnConfirmar.setDisable(false);
        }
        return StatusBanco;
    }

    public static void setRoot(Node root) {
        TelaConfiguracaoGeralController.root = root;
    }

    @FXML
    private void HandleCBConexão(Event event) {
        if (!cbConexao.getSelectionModel().getSelectedItem().equals(CacheComboBox)) {
            Transaction.setPU(cbConexao.getSelectionModel().getSelectedItem());
            Transaction.Connect();
            TestarConexao();
        }
    }

    @FXML
    private void HandleButtonGerarBanco(ActionEvent event) {
        /*Start.FirstStart();*/
        TestarBanco();
    }

    @FXML
    private void HandleButtonConfirmar(ActionEvent event) {
        if (Next = StatusConexão == StatusBanco && StatusConexão == true) {
            HandleButtonCancelar(event);
        }
    }

    @FXML
    private void HandleButtonCancelar(Event event) {
        Pane n = (Pane) root;
        n.getChildren().clear();
    }

    public static boolean isNext() {
        return Next;
    }

}
