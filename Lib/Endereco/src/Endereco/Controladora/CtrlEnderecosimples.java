/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Controladora;

import Controladora.Base.CtrlBase;
import Endereco.Entidade.Enderecosimples;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Remote
 */
public class CtrlEnderecosimples extends CtrlBase {

    private static CtrlEnderecosimples ctrlenderecosimples;

    public static CtrlEnderecosimples create() {
        if (ctrlenderecosimples == null) {
            ctrlenderecosimples = new CtrlEnderecosimples();
        }
        return ctrlenderecosimples;
    }
    
    public static Integer getId(Object in){
        if(in != null && in instanceof Enderecosimples){
            return ((Enderecosimples)in).getEndsId();
        }
        return -1;
    }
    public static String getLocal(Object in){
        if(in != null && in instanceof Enderecosimples){
            return ((Enderecosimples)in).getEndLocal();
        }
        return null;
    }

    public static void setCampos(Object oEnderecosimples, TextField cbRua, TextField cbBairro) {
        if (oEnderecosimples != null && oEnderecosimples instanceof Enderecosimples) {
            Enderecosimples en = (Enderecosimples) oEnderecosimples;
            cbRua.setText(en.getEndLocal()+"");
            cbBairro.setText(en.getEndBairro()+"");
        }
    }

    public CtrlEnderecosimples() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> enderecosimples = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            List<Enderecosimples> ResultEnderecosimples = em.createNamedQuery("Enderecosimples.findByEndsId", Enderecosimples.class)
                    .setParameter("endsId", Filtro).getResultList();

            for (Enderecosimples enderecosimpless : ResultEnderecosimples) {
                enderecosimples.add(enderecosimpless);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return enderecosimples;
    }

    public Object Salvar(String Local, String bairro) {
        Enderecosimples enderecosimples = new Enderecosimples(Local, bairro);
        return super.Salvar(enderecosimples);
    }

    public Object Alterar(Object oEnderecosimples, String Local, String bairro) {
        Enderecosimples enderecosimples = null;
        if (oEnderecosimples != null && oEnderecosimples instanceof Enderecosimples) {
            enderecosimples = (Enderecosimples) oEnderecosimples;
            enderecosimples.setEndBairro(bairro);
            enderecosimples.setEndLocal(Local);
            enderecosimples = (Enderecosimples) super.Alterar(enderecosimples);
        }
        return enderecosimples;
    }

    public boolean Remover(String Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Enderecosimples.class);
    }
    
}
