/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendamento.Controladora;

import Agendamento.Entidade.Agendamento;
import Animal.Entidade.Animal;
import Controladora.Base.CtrlBase;
import Pessoa.Controladora.CtrlCliente;
import Pessoa.Entidade.Usuario;
import Produto.Entidade.Produto;
import Transacao.Transaction;
import Utils.Mensagem;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlAgendamento extends CtrlBase {

    private static CtrlAgendamento ctrlagendamento;

    public static CtrlAgendamento create() {
        if (ctrlagendamento == null) {
            ctrlagendamento = new CtrlAgendamento();
        }
        return ctrlagendamento;
    }

    public static Date getData(Object oAgendamento) {
        if (oAgendamento != null && oAgendamento instanceof Agendamento) {
            return ((Agendamento) oAgendamento).getAgendamentoData();
        }
        return null;
    }

    public CtrlAgendamento() {
        super(Transaction.getEntityManagerFactory());
    }

    public static boolean setCampos(Object agendamento, TextField txbCliente, ComboBox cbAnimal) {
        try {
            if (agendamento != null && agendamento instanceof Agendamento) {
                Agendamento f = (Agendamento) agendamento;
                if (f.getAgendamentoId() != null) {
                    txbCliente.setText(f.getAnimalId().getCliId().getCliNome());
                    cbAnimal.getItems().clear();
                    cbAnimal.getItems().addAll(CtrlCliente.getAnimais(f.getClienteId()));
                    cbAnimal.getSelectionModel().select(f.getAnimalId());
                } else {
                    return false;
                }
            }
            return true;
        } catch (Exception ex) {
            Mensagem.ExibirException(ex);
        }
        return false;
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Agendamento) {
            return ((Agendamento) p).getAgendamentoId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Agendamento) {
            txBusca.setText(((Agendamento) p).getAgendamentoData().toString());
        }
    }

    public ArrayList<Object> Pesquisar(Object oFiltro) {
        ArrayList<Object> agendamento = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Agendamento> ResultAgendamento = new ArrayList();
            if (oFiltro instanceof String) {
                String Filtro = (String) oFiltro;
                if (Filtro != null && Filtro.isEmpty()) {
                    ResultAgendamento = em.createNamedQuery("Agendamento.findAll", Agendamento.class)
                            .getResultList();
                } else {
                    try {
                        ResultAgendamento = em.createNamedQuery("Agendamento.findByAgendamentoId", Agendamento.class)
                                .setParameter("agendamentoId", Integer.parseInt(Filtro)).getResultList();
                    } catch (Exception ex) {
                        ResultAgendamento = em.createNamedQuery("Agendamento.findByAgendamentoDetalhes", Agendamento.class)
                                .setParameter("agendamentoDetalhes", Filtro).getResultList();
                    }
                }
            } else if (oFiltro instanceof Date) {
                Date Filtro = (Date) oFiltro;
                ResultAgendamento = em.createNamedQuery("Agendamento.findByAgendamentoData", Agendamento.class)
                        .setParameter("agendamentoData", Filtro).getResultList();
            }
            for (Agendamento agendamentos : ResultAgendamento) {
                agendamento.add(agendamentos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return agendamento;
    }

    public ArrayList<Object> GeraAgendaDefault(Date Data) {
        int Minutos = 60;
        Data.setHours(8);
        ArrayList<Object> aAgenda = new ArrayList();
        for (int i = 0; i < 9; i++) {
            Agendamento agendamento = new Agendamento(Data);
            aAgenda.add(agendamento);
            Data = new Date(Data.getTime() + Minutos * 60 * 1000); // soma 60 minutos
        }
        return aAgenda;
    }

    public ArrayList<Object> PesquisaAgenda2(Date Data) {
        ArrayList<Object> asDatas = Pesquisar(Data.toString());
        ArrayList<Object> Datas = GeraAgendaDefault(Data);
        for (Object aData : asDatas) {
            Agendamento adata = (Agendamento) aData;
            int Pos = 0;
            while (Pos < Datas.size() && !((Agendamento) Datas.get(Pos)).getAgendamentoHorario()
                    .equalsIgnoreCase(adata.getAgendamentoHorario())) {
                Pos++;
            }
            if (Pos < Datas.size()) {
                Datas.set(Pos, aData);
            }
        }
        return asDatas;
    }

    public ArrayList<Object> PesquisaAgenda(Date Data) {
        ArrayList<Object> asDatas = GeraAgendaDefault(Data);
        ArrayList<Object> Datas;
        ArrayList<Object> NovasDatas = new ArrayList();
        for (Object aData : asDatas) {
            Agendamento adata = (Agendamento) aData;
            Datas = Pesquisar(adata.getAgendamentoData());
            if (!Datas.isEmpty()) {
                NovasDatas.add(Datas.get(0));
            } else {
                NovasDatas.add(aData);
            }
        }
        return NovasDatas;
    }

    public Object Salvar(Date Data, String Detalhes, Object usuario,
            Object animal, Object produto) {
        Agendamento agendamento = new Agendamento(Data, Detalhes);
        if (usuario != null && usuario instanceof Usuario) {
            agendamento.setUsrId((Usuario) usuario);
        }
        if (animal != null && animal instanceof Animal) {
            agendamento.setAnimalId((Animal) animal);
        }
        if (produto != null && produto instanceof Produto) {
            agendamento.setProdutoId((Produto) produto);
        }
        return super.Salvar(agendamento);
    }

    public Object Alterar(Object oAgendamento, Integer Id, Date Data, String Detalhes,
            Object usuario, Object animal, Object produto) {
        Agendamento agendamento = null;
        if (oAgendamento != null && oAgendamento instanceof Agendamento) {
            agendamento = (Agendamento) oAgendamento;
            agendamento.setAgendamentoData(Data);
            agendamento.setAgendamentoDetalhes(Detalhes);
            if (usuario != null && usuario instanceof Usuario) {
                agendamento.setUsrId((Usuario) usuario);
            }
            if (animal != null && animal instanceof Animal) {
                agendamento.setAnimalId((Animal) animal);
            }
            if (produto != null && produto instanceof Produto) {
                agendamento.setProdutoId((Produto) produto);
            }
            agendamento = (Agendamento) super.Alterar(agendamento);
        }
        return agendamento;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    public boolean Desmarcar(Object oAgendamento) {
        if (oAgendamento != null && oAgendamento instanceof Agendamento) {
            Agendamento aux = (Agendamento) oAgendamento;
            ArrayList<Object> Agendamentos = Pesquisar(aux.getAgendamentoData());
            if (Agendamentos != null && !Agendamentos.isEmpty()) {
                Agendamento agendamento = (Agendamento) Agendamentos.get(0);
                return Remover(agendamento.getAgendamentoId());
            }
        }
        return false;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Agendamento.class);
    }

}
