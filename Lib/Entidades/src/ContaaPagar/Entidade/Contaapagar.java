/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContaaPagar.Entidade;

import Utils.Acao.ButtonAcao;
import Compra.Entidade.Compra;
import Despesa.Entidade.Tipodedespesa;
import FormadePagamento.Entidade.Formadepagamento;
import Utils.FormatString;
import Variables.Variables;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "contaapagar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contaapagar.findAll", query = "SELECT c FROM Contaapagar c")
    , @NamedQuery(name = "Contaapagar.findByContaapagarId", query = "SELECT c FROM Contaapagar c WHERE c.contaapagarId = :contaapagarId")
    , @NamedQuery(name = "Contaapagar.findByContaapagarValortotal", query = "SELECT c FROM Contaapagar c WHERE c.contaapagarValortotal = :contaapagarValortotal")
    , @NamedQuery(name = "Contaapagar.findByContaapagarSituacao", query = "SELECT c FROM Contaapagar c WHERE c.contaapagarSituacao = :contaapagarSituacao")
    , @NamedQuery(name = "Contaapagar.findByContaapagarAcrescimo", query = "SELECT c FROM Contaapagar c WHERE c.contaapagarAcrescimo = :contaapagarAcrescimo")
    , @NamedQuery(name = "Contaapagar.findByContaapagarDesconto", query = "SELECT c FROM Contaapagar c WHERE c.contaapagarDesconto = :contaapagarDesconto")
    , @NamedQuery(name = "Contaapagar.findByContaapagarJuros", query = "SELECT c FROM Contaapagar c WHERE c.contaapagarJuros = :contaapagarJuros")
    , @NamedQuery(name = "Contaapagar.findByContaapagarParcelas", query = "SELECT c FROM Contaapagar c WHERE c.contaapagarParcelas = :contaapagarParcelas")
    , @NamedQuery(name = "Contaapagar.findByContaapagarData", query = "SELECT c FROM Contaapagar c WHERE c.contaapagarData = :contaapagarData")})
public class Contaapagar extends ButtonAcao<Contaapagar> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "contaapagar_id")
    private Integer contaapagarId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "contaapagar_valortotal")
    private BigDecimal contaapagarValortotal;
    @Column(name = "contaapagar_situacao")
    private Boolean contaapagarSituacao;
    @Column(name = "contaapagar_acrescimo")
    private Integer contaapagarAcrescimo;
    @Column(name = "contaapagar_desconto")
    private Integer contaapagarDesconto;
    @Column(name = "contaapagar_juros")
    private Integer contaapagarJuros;
    @Column(name = "contaapagar_data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contaapagarData;
    @Column(name = "contaapagar_descricao")
    private String contaapagarDescricao;
    @Basic(optional = false)
    @Column(name = "contaapagar_parcelas")
    private int contaapagarParcelas;
    @JoinColumn(name = "compra_id", referencedColumnName = "compra_id")
    @ManyToOne(optional = false)
    private Compra compraId;
    @JoinColumn(name = "formadepagamento_id", referencedColumnName = "formadepagamento_id")
    @ManyToOne(optional = false)
    private Formadepagamento formadepagamentoId;
    @JoinColumn(name = "tipodedespesa_id", referencedColumnName = "tipodedespesa_id")
    @ManyToOne(optional = false)
    private Tipodedespesa tipodedespesaId;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "contaapagar")
    private Collection<Parcelaapagar> parcelaapagarCollection = new ArrayList();

    public Contaapagar() {
    }

    public Contaapagar(Integer contaapagarId) {
        this.contaapagarId = contaapagarId;
    }

    public Contaapagar(BigDecimal contaapagarValortotal, Boolean contaapagarSituacao, Integer contaapagarAcrescimo,
            Integer contaapagarDesconto, Integer contaapagarJuros, Date contaapagarData, String contaapagarDescricao,
            int contaapagarParcelas) {
        this.contaapagarValortotal = contaapagarValortotal;
        this.contaapagarSituacao = contaapagarSituacao;
        this.contaapagarAcrescimo = contaapagarAcrescimo;
        this.contaapagarDesconto = contaapagarDesconto;
        this.contaapagarJuros = contaapagarJuros;
        this.contaapagarData = contaapagarData;
        this.contaapagarDescricao = contaapagarDescricao;
        this.contaapagarParcelas = contaapagarParcelas;
    }

    public Contaapagar(Integer contaapagarId, BigDecimal contaapagarValortotal, Boolean contaapagarSituacao,
            Integer contaapagarAcrescimo, Integer contaapagarDesconto, Integer contaapagarJuros, Date contaapagarData,
            String contaapagarDescricao, int contaapagarParcelas) {
        this.contaapagarId = contaapagarId;
        this.contaapagarValortotal = contaapagarValortotal;
        this.contaapagarSituacao = contaapagarSituacao;
        this.contaapagarAcrescimo = contaapagarAcrescimo;
        this.contaapagarDesconto = contaapagarDesconto;
        this.contaapagarJuros = contaapagarJuros;
        this.contaapagarData = contaapagarData;
        this.contaapagarDescricao = contaapagarDescricao;
        this.contaapagarParcelas = contaapagarParcelas;
    }

    public Contaapagar(Integer contaapagarId, BigDecimal contaapagarValortotal, int contaapagarParcelas) {
        this.contaapagarId = contaapagarId;
        this.contaapagarValortotal = contaapagarValortotal;
        this.contaapagarParcelas = contaapagarParcelas;
    }

    public Integer getContaapagarId() {
        return contaapagarId;
    }

    public void setContaapagarId(Integer contaapagarId) {
        this.contaapagarId = contaapagarId;
    }

    public BigDecimal getContaapagarValortotal() {
        return contaapagarValortotal;
    }

    public String Valortotal() {
        return FormatString.MonetaryPersistent(contaapagarValortotal);
    }

    public void setContaapagarValortotal(BigDecimal contaapagarValortotal) {
        this.contaapagarValortotal = contaapagarValortotal;
    }

    public Boolean getContaapagarSituacao() {
        return contaapagarSituacao;
    }

    public void setContaapagarSituacao(Boolean contaapagarSituacao) {
        this.contaapagarSituacao = contaapagarSituacao;
    }

    public Integer getContaapagarAcrescimo() {
        return contaapagarAcrescimo;
    }

    public void setContaapagarAcrescimo(Integer contaapagarAcrescimo) {
        this.contaapagarAcrescimo = contaapagarAcrescimo;
    }

    public Integer getContaapagarDesconto() {
        return contaapagarDesconto;
    }

    public void setContaapagarDesconto(Integer contaapagarDesconto) {
        this.contaapagarDesconto = contaapagarDesconto;
    }

    public Integer getContaapagarJuros() {
        return contaapagarJuros;
    }

    public void setContaapagarJuros(Integer contaapagarJuros) {
        this.contaapagarJuros = contaapagarJuros;
    }

    public int getContaapagarParcelas() {
        return contaapagarParcelas;
    }

    public void setContaapagarParcelas(int contaapagarParcelas) {
        this.contaapagarParcelas = contaapagarParcelas;
    }

    @XmlTransient
    public Collection<Parcelaapagar> getParcelaapagarCollection() {
        return parcelaapagarCollection;
    }

    public void setParcelaapagarCollection(Collection<Parcelaapagar> parcelaapagarCollection) {
        this.parcelaapagarCollection = parcelaapagarCollection;
    }

    public Compra getCompraId() {
        return compraId;
    }

    public void setCompraId(Compra compraId) {
        this.compraId = compraId;
    }

    public Formadepagamento getFormadepagamentoId() {
        return formadepagamentoId;
    }

    public void setFormadepagamentoId(Formadepagamento formadepagamentoId) {
        this.formadepagamentoId = formadepagamentoId;
    }

    public Tipodedespesa getTipodedespesaId() {
        return tipodedespesaId;
    }

    public void setTipodedespesaId(Tipodedespesa tipodedespesaId) {
        this.tipodedespesaId = tipodedespesaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contaapagarId != null ? contaapagarId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contaapagar)) {
            return false;
        }
        Contaapagar other = (Contaapagar) object;
        if ((this.contaapagarId == null && other.contaapagarId != null) || (this.contaapagarId != null && !this.contaapagarId.equals(other.contaapagarId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return contaapagarId + "";
    }

    public Date getContaapagarData() {
        return contaapagarData;
    }

    public String getData() {
        return Variables.getData(contaapagarData);
    }

    public void setContaapagarData(Date contaapagarData) {
        this.contaapagarData = contaapagarData;
    }

    public String getContaapagarDescricao() {
        return contaapagarDescricao;
    }

    public void setContaapagarDescricao(String contaapagarDescricao) {
        this.contaapagarDescricao = contaapagarDescricao;
    }

    @Override
    protected void AddNodes() {
        addNode("Button", "rem", "Remover");
    }
    ////57,47
}
