/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP;

import Empresa.Interface.TelaEmpresaController;
import Utils.Mensagem;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConfiguracaoInicialController implements Initializable {

    @FXML
    private RadioButton rbPg1;
    @FXML
    private RadioButton rbPg2;
    @FXML
    private Button btnProximo;
    @FXML
    private VBox pndados;
    private int Count;
    @FXML
    private Group gpInicial;
    @FXML
    private RadioButton rbPg0;
    @FXML
    private Group gpFinal;
    @FXML
    private RadioButton rbPgFinal;
    private HBox pndadosGeral;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        rbPg0Select(null);
    }

    public static boolean verifica() {
        boolean flag;
        flag = TelaConfiguracaoGeralController.Conecta();
        return flag;
    }

    @FXML
    private void rbPg0Select(Event event) {
        pndados.getChildren().clear();
        pndados.getChildren().add(gpInicial);
        rbPg0.selectedProperty().setValue(Boolean.TRUE);
        Count = -1;
        Default();
    }

    @FXML
    private void rbPgFinalSelect(Event event) {
        Count = 3;
        Default();
        pndados.getChildren().clear();
        pndados.getChildren().add(gpFinal);
        rbPgFinal.selectedProperty().setValue(Boolean.TRUE);
    }

    @FXML
    private void rbPg1Select(Event event) {
        Count = 0;
        Default();
        rbPg1.selectedProperty().setValue(Boolean.TRUE);
        TelaConfiguracaoGeralController.setRoot(pndados);
        carrega("/SGCP/TelaConfiguracaoGeral.fxml");
    }

    @FXML
    private void rbPg2Select(Event event) {
        Count = 1;
        Default();
        rbPg2.selectedProperty().setValue(Boolean.TRUE);
        TelaEmpresaController.setRoot(pndados);
        carrega("/Empresa/Interface/TelaEmpresa.fxml");
    }

    private void Default() {
        rbPg0.selectedProperty().setValue(Boolean.FALSE);
        rbPg1.selectedProperty().setValue(Boolean.FALSE);
        rbPg2.selectedProperty().setValue(Boolean.FALSE);
        rbPgFinal.selectedProperty().setValue(Boolean.FALSE);
        if (Count < 1) {
            rbPg2.setDisable(Boolean.TRUE);
            rbPgFinal.setDisable(Boolean.TRUE);
        } else if (Count <= 2) {
            rbPg2.setDisable(Boolean.FALSE);
            rbPgFinal.setDisable(Boolean.TRUE);
        } else if (Count == 3) {
            rbPgFinal.setDisable(Boolean.FALSE);
        }
        btnProximo.setText("Proximo");
    }

    @FXML
    private void btnProximo(Event event) {
        Default();
        if (Count == -1) {
            rbPg1Select(event);
            rbPg1.selectedProperty().setValue(Boolean.TRUE);
        } else if (Count == 0) {
            if (!TelaConfiguracaoGeralController.isNext()) {
                Mensagem.Exibir("Conclua a Configuração", 2);
                Count--;
            }
        } else if (Count == 1) {
            rbPg2Select(event);
            rbPg2.selectedProperty().setValue(Boolean.TRUE);
        } else if (Count == 2) {
            if (!TelaEmpresaController.isStatusEmp()) {
                Mensagem.Exibir("Conclua a Configuração", 2);
                Count--;
            } else {
                rbPgFinalSelect(event);
                btnProximo.setText("Finalizar");
            }
        } else if (Count > 3) {
            Stage stage = ((Stage) ((Node) event.getSource()).getScene().getWindow());
            stage.close();/////fexa janela
        }
        Count++;
    }

    private void carrega(String Tela) {
        try {
            Label l = new Label();
            l.setText(Tela);
            Parent root = FXMLLoader.load(getClass().getResource(Tela));
            pndados.getChildren().clear();
            pndados.getChildren().add(l);
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
    }
    
    public static void Create(Class Classe) {
        Parent root = null;
        try {
            Stage stage = new Stage();
            root = FXMLLoader.load(Classe.getResource("/SGCP/TelaConfiguracaoInicial.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add("/Estilos/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.showAndWait();
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Configuração Inicial");
        }
    }

}
