/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Interface;

import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaVendaController implements Initializable, Tela {

    @FXML
    private VBox gpLancarVenda;
    @FXML
    private VBox gpQuitarParcelaaReceber;
    private Tela TelaLancarVenda;
    private Tela TelaQuitarParcelasaReceber;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TelaLancarVenda = IniciarTela.Create(this.getClass(), gpLancarVenda,
                "/Venda/Interface/TelaLancarVenda.fxml");
        TelaQuitarParcelasaReceber = IniciarTela.Create(this.getClass(), gpQuitarParcelaaReceber,
                "/Venda/Interface/TelaQuitarParcelasAReceber.fxml");
    }    
    
}
