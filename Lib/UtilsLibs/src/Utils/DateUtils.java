/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static long DiferenceDays(Date firstDate, Date secondDate) {
        long Days = -1;
        try {
            Days = Math.abs(ChronoUnit.DAYS.between(firstDate.toInstant(), secondDate.toInstant()));
        } catch (Exception ex) {
            ex.getMessage();
        }
        return Days;
    }

    public static LocalDate SumDays(Date firstDate, int days) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(firstDate);
        cal.add(Calendar.DAY_OF_MONTH, days);
        return DateUtils.asLocalDate(cal.getTime());
    }


    public static Date asDate(Object data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
