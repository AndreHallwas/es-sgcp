/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caixa.Interface;

import Caixa.Controladora.CtrlCaixa;
import Utils.Controladora.CtrlUtils;
import Utils.DateUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Variables.Variables;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaFecharCaixaController implements Initializable {

    @FXML
    private Label lblDataAtualFecharCaixa;
    @FXML
    private HBox pndadosFecharCaixa;
    @FXML
    private JFXTextField txbDatadeFechamento;
    @FXML
    private JFXTextField txbValorFinalTotal;
    @FXML
    private JFXTextField txbAcrescentarFecharCaixa;
    @FXML
    private JFXTextField txbSaldoTotalFecharCaixa;
    @FXML
    private JFXTextField txbEntradanoCaixaTotal;
    @FXML
    private JFXTextField txbSaidadoCaixaTotal;
    @FXML
    private JFXTextField txbRemoverFecharCaixa;
    @FXML
    private TableView<Object> TabelaMovimentacoes;
    @FXML
    private TableColumn<Object, Object> tcTipo;
    @FXML
    private TableColumn<Object, Object> tcValor;
    @FXML
    private TableColumn<Object, Object> tcFormadePagamento;
    @FXML
    private TableColumn<Object, Object> tcDescricao;
    @FXML
    private JFXTextField txbEntradanoCaixaaPrazo;
    @FXML
    private JFXTextField txbValorFinalaPrazo;
    @FXML
    private JFXTextField txbSaidadoCaixaaPrazo;
    @FXML
    private JFXTextField txbEntradanoCaixaFisico;
    @FXML
    private JFXTextField txbValorFinalFisico;
    @FXML
    private JFXTextField txbSaidadoCaixaFisico;
    @FXML
    private TableView<Object> TabelaFormadepagamento;
    @FXML
    private TableColumn<Object, Object> tcNomeFormadepagamento;
    @FXML
    private TableColumn<Object, Object> tcValorFormadepagmento;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tcTipo.setCellValueFactory(new PropertyValueFactory("TipodeMovimentacao"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("DescricaoPagamento"));
        tcFormadePagamento.setCellValueFactory(new PropertyValueFactory("FormadePagamento"));
        tcValor.setCellValueFactory(new PropertyValueFactory("ValorMovimentado"));
        tcNomeFormadepagamento.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcValorFormadepagmento.setCellValueFactory(new PropertyValueFactory("OValor"));
        
        MaskFieldUtil.monetaryField(txbValorFinalFisico);
        MaskFieldUtil.monetaryField(txbValorFinalTotal);
        MaskFieldUtil.monetaryField(txbValorFinalaPrazo);
        MaskFieldUtil.monetaryField(txbSaidadoCaixaFisico);
        MaskFieldUtil.monetaryField(txbSaidadoCaixaTotal);
        MaskFieldUtil.monetaryField(txbSaidadoCaixaaPrazo);
        MaskFieldUtil.monetaryField(txbEntradanoCaixaFisico);
        MaskFieldUtil.monetaryField(txbEntradanoCaixaTotal);
        MaskFieldUtil.monetaryField(txbEntradanoCaixaaPrazo);
        
        EstadoOriginal();
    }

    private void CarregaTabelas(Object caixa) {
        CtrlCaixa cc = CtrlCaixa.create();
        try {
            TabelaMovimentacoes.getItems().clear();
            TabelaFormadepagamento.getItems().clear();
            if (caixa != null) {
                
                ArrayList<Object> Movimentacoes = new ArrayList();
                
                Movimentacoes.addAll(cc.getMovimentacoesaPagar(caixa));
                Movimentacoes.addAll(cc.getMovimentacoesaReceber(caixa));
                
                TabelaMovimentacoes.setItems(FXCollections.observableList(Movimentacoes));
                TabelaFormadepagamento.setItems(FXCollections.observableList(
                        cc.getRecebimentosporFormadePagamento(caixa)));
                
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tabela");
        }
    }

    @FXML
    private void HandleButtonFecharCaixa(MouseEvent event) {
        if (ValidarFecharCaixa()) {
            BigDecimal Numero = MaskFieldUtil.monetaryValueFromField(txbValorFinalTotal);
            int usuario = 1;
            if (CtrlCaixa.create().Fechar(Numero, usuario)) {
                Mensagem.Exibir(CtrlCaixa.create().getMsg(), 1);
                Variables.setStatusCaixa(false);
                CtrlUtils.CloseChildren(Variables._pndados, true);
            } else {
                Mensagem.Exibir(CtrlCaixa.create().getMsg(), 2);
                EstadoOriginal();
            }
        }
    }

    @FXML
    private void HandleButtonAcrescentarFecharCaixa(KeyEvent event) {
        txbSaldoTotalFecharCaixa.setText(txbValorFinalTotal.getText());
        txbSaldoTotalFecharCaixa.setText(
                Acrescentar(txbSaldoTotalFecharCaixa.getText(),
                        txbAcrescentarFecharCaixa.getText()));
        txbRemoverFecharCaixa.clear();
    }

    @FXML
    private void HandleButtonRemoverFecharCaixa(KeyEvent event) {
        txbSaldoTotalFecharCaixa.setText(txbValorFinalTotal.getText());
        txbSaldoTotalFecharCaixa.setText(
                Remover(txbSaldoTotalFecharCaixa.getText(),
                        txbRemoverFecharCaixa.getText()));
        txbAcrescentarFecharCaixa.clear();
    }

    public String Acrescentar(String ValorInicial, String Valor) {
        try {
            BigDecimal oValor = new BigDecimal(Valor);
            BigDecimal oValorInicial = new BigDecimal(ValorInicial);
            oValorInicial = oValorInicial.add(oValor);
            return oValorInicial.toPlainString();
        } catch (Exception ex) {
            System.out.println("Valor Inesperado ao Acrescentar ao Caixa!");
        }
        return "";
    }

    public String Remover(String ValorInicial, String Valor) {
        try {
            BigDecimal oValor = new BigDecimal(Valor);
            BigDecimal oValorInicial = new BigDecimal(ValorInicial);
            oValorInicial = oValorInicial.subtract(oValor);
            return oValorInicial.toPlainString();
        } catch (Exception ex) {
            System.out.println("Valor Inesperado ao remover do Caixa!");
        }
        return "";
    }

    @FXML
    private void HandleTextFieldValorFinal(KeyEvent event) {
        txbSaldoTotalFecharCaixa.setText(txbValorFinalTotal.getText());
    }

    private void EstadoOriginal() {
        LocalDate data = LocalDate.now();
        CtrlUtils.clear(pndadosFecharCaixa.getChildren());

        txbDatadeFechamento.setText(data.toString());
        TabelaMovimentacoes.getItems().clear();
        TabelaFormadepagamento.getItems().clear();

        if (CtrlCaixa.create().getCaixaAberto()) {
            
            /////Atualiza o Objeto do Caixa
            Object oCaixa = CtrlCaixa.create().Pesquisar(
                    CtrlCaixa.getId(CtrlCaixa.create().getCaixaAbertoObject()),"ID").get(0);
            
            CtrlCaixa.setCampoFinal(oCaixa, txbValorFinalTotal, txbSaldoTotalFecharCaixa);
            txbValorFinalTotal.setText(CtrlCaixa.create().getValorFinal(oCaixa) + "");
            txbEntradanoCaixaTotal.setText(CtrlCaixa.create().getValoraReceber(oCaixa) + "");
            txbSaidadoCaixaTotal.setText(CtrlCaixa.create().getValoraPagar(oCaixa) + "");
            txbSaidadoCaixaFisico.setText(CtrlCaixa.create().getValoraPagarFisico(oCaixa) + "");
            txbEntradanoCaixaFisico.setText(CtrlCaixa.create().getValoraReceberFisico(oCaixa) + "");
            txbEntradanoCaixaaPrazo.setText(CtrlCaixa.create().getValoraReceberaPrazo(oCaixa) + "");
            txbSaidadoCaixaaPrazo.setText(CtrlCaixa.create().getValoraPagaraPrazo(oCaixa) + "");
            txbValorFinalaPrazo.setText(CtrlCaixa.create().getValoraFinalaPrazo(oCaixa) + "");
            txbValorFinalFisico.setText(CtrlCaixa.create().getValoraFinalFisico(oCaixa) + "");
            lblDataAtualFecharCaixa.setText(CtrlCaixa.getDate(oCaixa).toString());
            CarregaTabelas(oCaixa);
        } else {
            txbEntradanoCaixaTotal.setText("0");
            txbSaidadoCaixaTotal.setText("0");
            txbValorFinalTotal.setText("0");
            txbValorFinalFisico.setText("0");
            txbSaidadoCaixaFisico.setText("0");
            txbEntradanoCaixaFisico.setText("0");
            txbEntradanoCaixaaPrazo.setText("0");
            txbSaidadoCaixaaPrazo.setText("0");
            txbValorFinalaPrazo.setText("0");
            txbSaldoTotalFecharCaixa.setText("0");
            lblDataAtualFecharCaixa.setText(Variables.getData(new Date()));
        }
    }

    private boolean ValidarFecharCaixa() {
        if (CtrlCaixa.create().getCaixaAberto()) {
            return true;
        }
        Mensagem.Exibir(CtrlCaixa.create().getMsg(), 2);
        return false;
    }

}
