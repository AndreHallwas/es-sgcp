/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacina.Interface;

import Produto.Controladora.CtrlProduto;
import Produto.Interface.TelaConsultaProdutoController;
import Utils.Acao.InterfaceFxmlAcao;
import Utils.Controladora.CtrlUtils;
import Utils.DateUtils;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Vacina.Controladora.CtrlVacina;
import Vacina.Interface.TabelaVacinas.nodeTabela;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaAdicionarVacinaController implements Initializable, Tela, InterfaceFxmlAcao {

    @FXML
    private VBox pndados;
    @FXML
    private JFXTextArea taDescricao;
    @FXML
    private JFXButton btnSalvar;
    @FXML
    private DatePicker dtpData;
    @FXML
    private JFXTextField txbVacina;
    @FXML
    private JFXButton btnAdicionar;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Object> tcVacina;
    @FXML
    private TableColumn<Object, Object> tcData;
    @FXML
    private TableColumn<Object, Object> tcAcoes;

    private static Object Produto;
    protected static TabelaVacinas TabeladeVacinas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TabeladeVacinas = new TabelaVacinas();
        SetTelaAcao();
        tcVacina.setCellValueFactory(new PropertyValueFactory("Vacina"));
        tcData.setCellValueFactory(new PropertyValueFactory("DataFormatada"));
        tcAcoes.setCellValueFactory(new PropertyValueFactory("Crud"));
        EstadoOriginal();
    }

    @FXML
    private void HandleButtonConsultaProduto(MouseEvent event) {
        TelaConsultaProdutoController.setTipo("Vacina");
        IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaProduto.fxml",
                "Consulta de Produto", null);
        TelaConsultaProdutoController.setTipo("Geral");
        if (TelaConsultaProdutoController.getProduto() != null) {
            Produto = TelaConsultaProdutoController.getProduto();
            CtrlProduto.setCampoBusca(Produto, txbVacina);
        }
    }

    @FXML
    private void HandleButtonSalvar(MouseEvent event) {
        ArrayList<nodeTabela> Vacinas = getTabeladeVacinas().get();
        if (Vacinas != null && !Vacinas.isEmpty()) {
            CtrlUtils.CloseStage(event);
        } else {
            Mensagem.Exibir("Sem Vacinas a Agendar!", 2);
        }
    }

    @FXML
    private void HandleButtonCancelar(MouseEvent event) {
        TabeladeVacinas = new TabelaVacinas();
        EstadoOriginal();
        CtrlUtils.CloseStage(event);
    }

    private void EstadoOriginal() {
        CtrlUtils.clear(pndados.getChildren());
        dtpData.setValue(LocalDate.now());
        CarregaTabela();
    }

    @FXML
    private void HandleButtonAdicionar(MouseEvent event) {
        if (validaCampos()) {
            LocalDate DataInicial = dtpData.getValue();
            ArrayList<LocalDate> Intervalo = CtrlVacina.getIntervalo(Produto, DateUtils.asDate(DataInicial));
            for (LocalDate date : Intervalo) {
                getTabeladeVacinas().add(date, Produto, taDescricao.getText());
            }
            EstadoOriginal();
        }
    }

    private void ExcluirVacina(nodeTabela itemCarrinho) {
        if (getTabeladeVacinas() != null) {
            getTabeladeVacinas().remove(itemCarrinho.getId());
            CarregaTabela();
        }
    }

    private void CarregaTabela() {
        tabela.getSelectionModel().clearSelection();
        tabela.getItems().clear();
        tabela.setItems(FXCollections.observableList(getTabeladeVacinas().getObject()));
    }

    private boolean validaCampos() {

        boolean fl = true;
        StringBuilder Result = new StringBuilder();

        if (dtpData.getValue() == null) {
            Result.append("Campo Obrigatório Vazio : Data, ");
            fl = false;
        }
        if (Produto == null) {
            Result.append("Campo Obrigatório Vazio : Produto, ");
            fl = false;
        }

        return fl;
    }

    /**
     * @return the TabeladeVacinas
     */
    public static TabelaVacinas getTabeladeVacinas() {
        return TabeladeVacinas;
    }

    @Override
    public void SelectEventAcao(Object Reference, String Action, Object element) {
        if (Action.equalsIgnoreCase("rem")) {
            ExcluirVacina((nodeTabela) Reference);
        }
    }

    @Override
    public void SetTelaAcao() {
        nodeTabela.setTelaAcao(this);
    }

}
