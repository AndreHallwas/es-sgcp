/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContaaPagar.Entidade;

import Utils.Entidade.TimestampConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Raizen
 */
@Embeddable
public class ParcelaapagarPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "contaapagar_id")
    private int contaapagarId;
    @Basic(optional = false)
    @Column(name = "parp_numerodaparcela")
    private int parpNumerodaparcela;
    @Basic(optional = false)
    @Column(name = "parp_datadegeracao")
    @Temporal(TemporalType.TIMESTAMP)
    @Convert(converter = TimestampConverter.class)
    private Date parpDatadegeracao;

    public ParcelaapagarPK() {
    }

    public ParcelaapagarPK(int contaapagarId, int parpNumerodaparcela, Date parpDatadegeracao) {
        this.contaapagarId = contaapagarId;
        this.parpNumerodaparcela = parpNumerodaparcela;
        this.parpDatadegeracao = parpDatadegeracao;
    }

    public int getContaapagarId() {
        return contaapagarId;
    }

    public void setContaapagarId(int contaapagarId) {
        this.contaapagarId = contaapagarId;
    }

    public int getParpNumerodaparcela() {
        return parpNumerodaparcela;
    }

    public void setParpNumerodaparcela(int parpNumerodaparcela) {
        this.parpNumerodaparcela = parpNumerodaparcela;
    }

    public Date getParpDatadegeracao() {
        return parpDatadegeracao;
    }

    public void setParpDatadegeracao(Date parpDatadegeracao) {
        this.parpDatadegeracao = parpDatadegeracao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) contaapagarId;
        hash += (int) parpNumerodaparcela;
        hash += (parpDatadegeracao != null ? parpDatadegeracao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParcelaapagarPK)) {
            return false;
        }
        ParcelaapagarPK other = (ParcelaapagarPK) object;
        if (this.contaapagarId != other.contaapagarId) {
            return false;
        }
        if (this.parpNumerodaparcela != other.parpNumerodaparcela) {
            return false;
        }
        if ((this.parpDatadegeracao == null && other.parpDatadegeracao != null) || (this.parpDatadegeracao != null && !this.parpDatadegeracao.equals(other.parpDatadegeracao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "teste.ParcelaapagarPK[ contaapagarId=" + contaapagarId + ", parpNumerodaparcela=" + parpNumerodaparcela + ", parpDatadegeracao=" + parpDatadegeracao + " ]";
    }
    
}
