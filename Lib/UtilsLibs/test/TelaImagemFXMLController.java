/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.UI.Imagem;

import Utils.Imagem;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaImagemFXMLController implements Initializable {

    @FXML
    private ImageView imgImagemUsuario;
    @FXML
    private Label lbErroImagem;
    @FXML
    private Button btnImagem;
    private BufferedImage imagem;
    @FXML
    private Button btnVisualizar;
    private boolean isBtnImagem = true;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        estadoOriginal();
    }

    @FXML
    private void evtEscolheImagem(Event event) {
        Image im = Imagem.capturaImagem();
        if (im != null) {
            setImagem(Imagem.ImageToBufferedImage(im));
            imgImagemUsuario.setImage(im);
        } else {
            Mensagem.Exibir("Não Foi Possivel Abrir a Imagem", 2);
        }
    }

    @FXML
    private void evtVisualizarImagem(ActionEvent event) {
        TelaConsultaImagemFXMLController.setImage(getImagem());
        IniciarTela.loadWindow(this.getClass(), "/Utils/UI/Imagem/TelaConsultaImagemFXML.fxml", "Tela de Consulta de Imagem", null);
        /*Parent root = null;
        try {
            /*
            MainFXMLController._pndados_bak = FXCollections.observableList(new ArrayList());
            MainFXMLController._pndados_bak.addAll(MainFXMLController._pndados.getChildren());
            MainFXMLController._pndados.getChildren().clear();*/

 /*Stage stage = new Stage();
            stage.getIcons().add(new Image(new FileInputStream("Img/Logo.png")));
            TelaConsultaImagemFXMLController.setImage(getImagem());
            root = FXMLLoader.load(getClass().getResource("/Utils/UI/Imagem/TelaConsultaImagemFXML.fxml"));
            
            /*MainFXMLController._pndados.getChildren().add(root);*/
 /*Scene scene = new Scene(root);
            scene.getStylesheets().add("Engenharia/UI/Estilos/DarkTheme.css");
            stage.setScene(scene);
            stage.setMaximized(false);
            stage.initStyle(StageStyle.DECORATED);
            stage.showAndWait();
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela de Consulta de Imagem");
        }*/
    }

    public static TelaImagemFXMLController Create(Class classe, Pane pndados) {
        TelaImagemFXMLController CImagem = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Utils/UI/Imagem/TelaImagemFXML.fxml"));
            Parent root = Loader.load();
            CImagem = (TelaImagemFXMLController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CImagem;
    }

    public void estadoOriginal() {
        imagem = null;
        imgImagemUsuario.setImage(null);
        btnImagem.setDisable(!isIsBtnImagem());
        btnVisualizar.setDisable(true);
    }

    /**
     * @return the imagem
     */
    public BufferedImage getImagem() {
        return imagem;
    }

    /**
     * @param aImagem the imagem to set
     */
    public void setImagem(BufferedImage aImagem) {
        if (aImagem != null) {
            WritableImage im = Imagem.BufferedImageToImage(aImagem);
            imgImagemUsuario.setImage(im);
            imagem = aImagem;
            if (imagem != null) {
                btnVisualizar.setDisable(false);
            }
        }
    }

    /**
     * @return the isBtnImagem
     */
    public boolean isIsBtnImagem() {
        return isBtnImagem;
    }

    /**
     * @param isBtnImagem the isBtnImagem to set
     */
    public void setIsBtnImagem(boolean isBtnImagem) {
        this.isBtnImagem = isBtnImagem;
        btnImagem.setDisable(!isIsBtnImagem());
    }

}
