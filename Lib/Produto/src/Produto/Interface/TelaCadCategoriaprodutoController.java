/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Produto.Controladora.CtrlCategoriaproduto;
import Produto.Controladora.CtrlTipoproduto;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import Utils.UI.Imagem.TelaImagemFXMLController;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadCategoriaprodutoController implements Initializable, Tela {

    @FXML
    private HBox pndados;
    @FXML
    private JFXTextField txbNome;
    @FXML
    private Label lbErroNome1;
    @FXML
    private JFXTextField txbTipo;
    @FXML
    private Label lbErroTipo;
    @FXML
    private JFXTextArea taObs;
    @FXML
    private Label lbErrobs;
    @FXML
    private VBox gpImagem;
    @FXML
    private JFXButton btnovo;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btapagar;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    private int flag;
    private Object ID;
    private boolean EOriginal;
    private Object tipoproduto;
    private TelaImagemFXMLController Imagem;
    private Object Instancia;
    @FXML
    private JFXButton btConsulta;
    @FXML
    private JFXCheckBox cbMedicamento;
    @FXML
    private JFXCheckBox cbVacina;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        Imagem = TelaImagemFXMLController.Create(this.getClass(), gpImagem);

        estadoOriginal();
    }

    private void setAllErro(boolean Enome, boolean Etipo, boolean Edescricao) {
        lbErrobs.setVisible(Edescricao);
        lbErroTipo.setVisible(Etipo);
        lbErroNome1.setVisible(Enome);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlCategoriaproduto cp = CtrlCategoriaproduto.create();
        ArrayList<Object> Pesquisa = cp.Pesquisar(txbNome.getText());
        if (txbNome.getText().trim().isEmpty() || !Pesquisa.isEmpty() || Pesquisa.isEmpty()) {
            if (txbNome.getText().trim().isEmpty()) {
                setErro(lbErroNome1, "Campo Obrigatório Vazio");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            } else if (!Pesquisa.isEmpty() && flag == 1) {
                setErro(lbErroNome1, "Nome Ja Existe");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            }
        }
        if (txbTipo.getText().trim().isEmpty()) {
            setErro(lbErroTipo, "Campo Obrigatório Vazio");
            txbTipo.setText("");
            txbTipo.requestFocus();
            fl = false;
        }
        return fl;
    }

    @FXML
    private void evtNovo(ActionEvent event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(ActionEvent event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(ActionEvent event) {
        CtrlCategoriaproduto cp = CtrlCategoriaproduto.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(ID);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        setAllErro(false, false, false);
        CtrlCategoriaproduto cp = CtrlCategoriaproduto.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, ID, txbNome.getText(), taObs.getText(),
                        Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()), cbMedicamento.isSelected(),
                        cbVacina.isSelected(), tipoproduto)) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(txbNome.getText(), taObs.getText(),
                    Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()),
                    cbMedicamento.isSelected(), cbVacina.isSelected(), tipoproduto)) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);
        Imagem.estadoOriginal();
        cbMedicamento.setSelected(false);
        cbVacina.setSelected(false);
        
        clear(pndados.getChildren());
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlCategoriaproduto.setCampos(p, txbNome, taObs, txbTipo, cbMedicamento, cbVacina);
            ID = CtrlCategoriaproduto.getId(p);
            Imagem.setImagem(CtrlCategoriaproduto.getImagem(p));
        }
    }

    @FXML
    private void evtConsultaAvancada(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaCategoriaproduto.fxml",
                "Consulta de Categoria de Produto", null);
        if (TelaConsultaCategoriaprodutoController.getCategoriaproduto() != null) {
            EstadoConsulta(TelaConsultaCategoriaprodutoController.getCategoriaproduto());
        }
    }

    @FXML
    private void evtConsultaTipo(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaTipoproduto.fxml",
                "Consulta de Tipo de Produto", null);
        tipoproduto = TelaConsultaTipoprodutoController.getTipoproduto();
        setConsulta(tipoproduto, txbTipo);
        CtrlTipoproduto.setCampoBusca(TelaConsultaTipoprodutoController.getTipoproduto(), txbTipo);
    }

    private void setConsulta(Object ob, TextField txb) {
        if (ob != null) {
            txb.setText(ob.toString());
        }
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }
}
