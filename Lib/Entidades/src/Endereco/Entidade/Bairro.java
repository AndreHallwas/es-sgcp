/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "bairro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bairro.findAll", query = "SELECT b FROM Bairro b")
    , @NamedQuery(name = "Bairro.findByBaiId", query = "SELECT b FROM Bairro b WHERE b.baiId = :baiId")
    , @NamedQuery(name = "Bairro.findByBaiNome", query = "SELECT b FROM Bairro b WHERE b.baiNome = :baiNome")})
public class Bairro extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bai_id")
    private Integer baiId;
    @Column(name = "bai_nome")
    private String baiNome;
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "baiId")
    private Collection<Endereco> enderecoCollection = new ArrayList();;
    @JoinColumn(name = "cid_id", referencedColumnName = "cid_id")
    @ManyToOne(optional = false)
    private Cidade cidId;

    public Bairro() {
    }

    public Bairro(Integer baiId) {
        this.baiId = baiId;
    }

    public Bairro(String baiNome) {
        this.baiNome = baiNome;
    }

    public Bairro(Integer baiId, String baiNome) {
        this.baiId = baiId;
        this.baiNome = baiNome;
    }

    public Integer getBaiId() {
        return baiId;
    }

    public void setBaiId(Integer baiId) {
        this.baiId = baiId;
    }

    public String getBaiNome() {
        return baiNome;
    }

    public void setBaiNome(String baiNome) {
        this.baiNome = baiNome;
    }

    @XmlTransient
    public Collection<Endereco> getEnderecoCollection() {
        return enderecoCollection;
    }

    public void setEnderecoCollection(Collection<Endereco> enderecoCollection) {
        this.enderecoCollection = enderecoCollection;
    }

    public Cidade getCidId() {
        return cidId;
    }

    public void setCidId(Cidade cidId) {
        this.cidId = cidId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (baiId != null ? baiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bairro)) {
            return false;
        }
        Bairro other = (Bairro) object;
        if ((this.baiId == null && other.baiId != null) || (this.baiId != null && !this.baiId.equals(other.baiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testedb2.Bairro[ baiId=" + baiId + " ]";
    }
    
}
