/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Controladora;

import Controladora.Base.CtrlBase;
import Produto.Entidade.Categoriaproduto;
import Produto.Entidade.Dosagemproduto;
import Produto.Entidade.Produto;
import Transacao.Transaction;
import Utils.Carrinho.Item;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlProduto extends CtrlBase {

    private static CtrlProduto ctrlproduto;

    public static CtrlProduto create() {
        if (ctrlproduto == null) {
            ctrlproduto = new CtrlProduto();
        }
        return ctrlproduto;
    }

    public CtrlProduto() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object produto, TextField txbNome, TextArea taObs,
            JFXTextField txbPreco, JFXTextField txbQuantidade, JFXTextField txbTipo,
            JFXTextField txbDosagem, JFXTextField txbCategoria, JFXTextField txbPrecoVenda) {
        if (produto != null && produto instanceof Produto) {
            Produto f = (Produto) produto;
            txbNome.setText(f.getProdutoNome());
            taObs.setText(f.getProdutoObs());
            txbPreco.setText(f.getProdutoPreco().toString());
            txbQuantidade.setText(Integer.toString(f.getProdutoQuantidade()));
            if (f.getProdutoPrecoVenda() != null) {
                txbPrecoVenda.setText(f.getProdutoPrecoVenda().toString());
            }
            if (f.getCategoriaproduto() != null) {
                txbCategoria.setText(f.getCategoriaproduto().toString());
                if (f.getCategoriaproduto().getTipoproduto() != null) {
                    txbTipo.setText(f.getCategoriaproduto().getTipoproduto().toString());
                }
            }
            if (f.getDosagemprodutoId() != null) {
                txbDosagem.setText(f.getDosagemprodutoId() + "");
            }
        }
    }

    public static boolean isProduto(Object p) {
        return p != null && p instanceof Produto;
    }

    public static int getQuantidade(Object p) {
        if (p != null && p instanceof Produto) {
            return ((Produto) p).getProdutoQuantidade();
        }
        return -1;
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Produto) {
            return ((Produto) p).getProdutoId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Produto) {
            txBusca.setText(((Produto) p).getProdutoNome());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> produto = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            boolean flag = false, flagVacina = false;
            List<Produto> Resultproduto = null;
            if (Tipo != null && !Tipo.trim().isEmpty()) {
                if (Tipo.equalsIgnoreCase("Vacina")) {
                    produto = PesquisarVacina(Filtro);
                    flagVacina = true;
                }
                flag = true;
            }
            if (!flagVacina) {
                if (Filtro == null || Filtro.isEmpty()) {
                    Resultproduto = em.createNamedQuery("Produto.findAll", Produto.class)
                            .getResultList();
                } else if (flag) {
                    try {
                        if (Tipo.equalsIgnoreCase("Id")) {
                            Resultproduto = em.createNamedQuery("Produto.findByProdutoId", Produto.class)
                                    .setParameter("produtoId", Integer.parseInt(Filtro)).getResultList();
                        } else if (Tipo.equalsIgnoreCase("Geral")) {
                            Resultproduto = em.createNamedQuery("Produto.findByAllGeral", Produto.class)
                                    .setParameter("produtoId", Integer.parseInt(Filtro))
                                    .setParameter("produtoQuantidade", Integer.parseInt(Filtro))
                                    .setParameter("produtoPreco", Double.parseDouble(Filtro)).getResultList();
                        }
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());

                        Resultproduto = em.createNamedQuery("Produto.findByAllString", Produto.class)
                                .setParameter("produtoNome", Filtro)
                                .setParameter("produtoObs", Filtro).getResultList();
                    }
                }
                for (Produto produtos : Resultproduto) {
                    produto.add(produtos);
                }
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return produto;
    }

    private ArrayList<Object> PesquisarVacina(String Filtro) {
        ArrayList<Object> produto = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Produto> Resultproduto = null;
            if (Filtro == null || Filtro.isEmpty()) {
                Resultproduto = em.createNamedQuery("Produto.findByAllVacina", Produto.class)
                        .getResultList();
            } else {
                try {
                    Resultproduto = em.createNamedQuery("Produto.findByVacinaId", Produto.class)
                            .setParameter("filtro", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    Resultproduto = em.createNamedQuery("Produto.findByVacinaGeral", Produto.class)
                            .setParameter("filtro", Filtro).getResultList();
                }
            }

            for (Produto produtos : Resultproduto) {
                produto.add(produtos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return produto;
    }

    public Object Salvar(String Nome, BigDecimal Preco, int Quantidade, String Obs, BigDecimal PrecoVenda,
            Object dosagem, Object categoria) {
        Produto produto = new Produto(Preco, Quantidade, Obs, Nome, PrecoVenda);
        if (dosagem != null && dosagem instanceof Dosagemproduto) {
            produto.setDosagemprodutoId((Dosagemproduto) dosagem);
        }
        if (categoria != null && categoria instanceof Categoriaproduto) {
            produto.setCategoriaproduto((Categoriaproduto) categoria);
        }
        return super.Salvar(produto);
    }

    public Object Alterar(Object oProduto, Integer Id, String Nome, BigDecimal Preco, int Quantidade,
            String Obs, BigDecimal PrecoVenda, Object dosagem, Object categoria) {
        Produto produto = null;
        if (oProduto != null && oProduto instanceof Produto) {
            produto = (Produto) oProduto;
            produto.setProdutoId(Id);
            produto.setProdutoPreco(Preco);
            produto.setProdutoPrecoVenda(PrecoVenda);
            produto.setProdutoNome(Nome);
            produto.setProdutoQuantidade(Quantidade);
            produto.setProdutoObs(Obs);
            if (dosagem != null && dosagem instanceof Dosagemproduto) {
                produto.setDosagemprodutoId((Dosagemproduto) dosagem);
            }
            if (categoria != null && categoria instanceof Categoriaproduto) {
                produto.setCategoriaproduto((Categoriaproduto) categoria);
            }
            produto = (Produto) super.Alterar(produto);
        }
        return produto;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    public boolean AlterarEstoque(Object produto, Integer Quantidade, String Funcao) {
        Produto oProduto = null;
        if (produto != null && produto instanceof Produto) {
            try {
                oProduto = (Produto) produto;
                if (Funcao.equalsIgnoreCase("Adicionar")) {
                    oProduto.setProdutoQuantidade(oProduto.getProdutoQuantidade() + Quantidade);
                    return oProduto.edit() != null;

                } else if (Funcao.equalsIgnoreCase("Subtrair")) {
                    oProduto.setProdutoQuantidade(oProduto.getProdutoQuantidade() - Quantidade);
                    return oProduto.edit() != null;
                }
            } catch (Exception ex) {
                return false;
            }
        }
        return false;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Produto.class);
    }
}
