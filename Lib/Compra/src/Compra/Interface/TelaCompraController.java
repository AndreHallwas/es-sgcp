/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Interface;

import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCompraController implements Initializable, Tela {

    @FXML
    private VBox gpLancarCompra;
    @FXML
    private VBox gpQuitarParcelaaPagar;
    private Tela TelaLancarCompra;
    private Tela TelaQuitarParcelasaPagar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TelaLancarCompra = IniciarTela.Create(this.getClass(), gpLancarCompra,
                "/Compra/Interface/TelaLancarCompra.fxml");
        TelaQuitarParcelasaPagar = IniciarTela.Create(this.getClass(), gpQuitarParcelaaPagar,
                "/Compra/Interface/TelaQuitarParcelasAPagar.fxml");
    }    
    
}
