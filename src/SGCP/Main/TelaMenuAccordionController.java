/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP.Main;

import Acesso.Controladora.AcessoComponente;
import SGCP.MainMenuOptions;
import Utils.UI.Tela.Tela;
import Variables.Variables;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaMenuAccordionController implements Initializable, Tela {

    @FXML
    private JFXDrawer DrawerMenu;
    @FXML
    private VBox MenuLateral;
    @FXML
    private JFXHamburger HamburgerUsuario;
    @FXML
    private JFXDrawer DrawerUsuario;
    @FXML
    private VBox MenuUsuario;
    @FXML
    private Circle imgLogoUsuario;
    @FXML
    private Label txUsuarioInterface;
    @FXML
    private Label txNivelInterface;
    @FXML
    private Accordion AccordionLateral;
    @FXML
    private TitledPane TabCadastro;
    @FXML
    private Label opcUsuario;
    @FXML
    private Label opcNivel;
    @FXML
    private Label opcProduto;
    @FXML
    private Label opcCliente;
    @FXML
    private Label opcFornecedor;
    @FXML
    private Label opcServico;
    @FXML
    private Label opcAnimal;
    @FXML
    private Label opcTipodeDespesa;
    @FXML
    private TitledPane TabFuncoes;
    @FXML
    private Label opcAgendamento;
    @FXML
    private Label opcCaixa;
    @FXML
    private Label opcCompra;
    @FXML
    private Label opcPagamento;
    @FXML
    private Label opcAtendimento;
    @FXML
    private Label opcVenda;
    @FXML
    private Label opcRecebimento;
    @FXML
    private TitledPane TabRelatorio;
    @FXML
    private Label opcFichaUsuario;
    @FXML
    private TitledPane TabConsulta;
    @FXML
    private Label opcConsultaUsuario;
    @FXML
    private Label opcConsultaCliente;
    @FXML
    private Label opcConsultaFornecedor;
    @FXML
    private Label opcConsultaFabricante;
    @FXML
    private Label opcConsultaProduto;
    @FXML
    private TitledPane TabGerenciamento;
    @FXML
    private Label opcBackup;
    @FXML
    private Label opcRestore;
    @FXML
    private JFXHamburger hamburger;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        initDrawer();
        initDrawerUsuario();
        CarregaComponentesAccordion();
        
        Variables._txUsuarioInterface = txUsuarioInterface;
        Variables._txNivelInterface = txNivelInterface;
        Variables._imgLogoUsuario = imgLogoUsuario;
    }    

    private void CarregaComponentesAccordion() {
        AcessoComponente ac = (AcessoComponente) Variables._Acc;
        ac.AdicionaObjeto(TabCadastro, "TabCadastro");
        ac.AdicionaObjeto(opcUsuario, "opcUsuario");
        ac.AdicionaObjeto(opcNivel, "opcNivel");
        ac.AdicionaObjeto(opcCliente, "opcCliente");
        ac.AdicionaObjeto(opcFornecedor, "opcFabricante");////Fabricante
        ac.AdicionaObjeto(opcFornecedor, "opcFornecedor");
        ac.AdicionaObjeto(opcProduto, "opcProduto");
        ac.AdicionaObjeto(TabFuncoes, "TabFuncoes");
        ac.AdicionaObjeto(opcCompra, "TabCompra");
        ac.AdicionaObjeto(opcVenda, "TabVenda");
        ac.AdicionaObjeto(opcRecebimento, "TabRecebimento");
        ac.AdicionaObjeto(opcPagamento, "TabPagamento");
        ac.AdicionaObjeto(TabGerenciamento, "TabGerenciamento");
        ac.AdicionaObjeto(opcBackup, "opcBackup");
        ac.AdicionaObjeto(opcRestore, "opcRestore");
        ac.AdicionaObjeto(TabRelatorio, "TabRelatorio");
        ac.AdicionaObjeto(opcFichaUsuario, "opcFichaUsuario");
        ac.AdicionaObjeto(TabConsulta, "TabConsulta");
        ac.AdicionaObjeto(opcConsultaUsuario, "opcConsultaUsuario");
        ac.AdicionaObjeto(opcConsultaCliente, "opcConsultaCliente");
        ac.AdicionaObjeto(opcConsultaFabricante, "opcConsultaFabricante");
        ac.AdicionaObjeto(opcConsultaFornecedor, "opcConsultaFornecedor");
        ac.AdicionaObjeto(opcConsultaProduto, "opcConsultaProduto");
        /*ac.AdicionaObjeto(opcAgendamento, "opcAgendamento");
        ac.AdicionaObjeto(opcCaixa, "opcCaixa");*/

        ac.Default();

    }


    private void initDrawer() {
        DrawerMenu.getChildren().remove(3);
        DrawerMenu.setPrefSize(0, 0);
        DrawerMenu.setSidePane(new VBox());
        HamburgerSlideCloseTransition task = new HamburgerSlideCloseTransition(hamburger);
        task.setRate(-1);
        hamburger.addEventHandler(MouseEvent.MOUSE_CLICKED, (Event event) -> {
            task.setRate(task.getRate() * -1);
            task.play();
            if (DrawerMenu.isHidden()) {
                DrawerMenu.setSidePane(MenuLateral);
                DrawerMenu.open();
            } else {
                DrawerMenu.setSidePane(new VBox());
                DrawerMenu.setPrefSize(0, 0);
                DrawerMenu.close();
            }
        });
    }

    private void initDrawerUsuario() {
        DrawerUsuario.getChildren().remove(3);
        DrawerUsuario.setPrefSize(0, 0);
        DrawerUsuario.setSidePane(new VBox());
        HamburgerSlideCloseTransition task = new HamburgerSlideCloseTransition(HamburgerUsuario);
        task.setRate(-1);
        HamburgerUsuario.addEventHandler(MouseEvent.MOUSE_CLICKED, (Event event) -> {
            task.setRate(task.getRate() * -1);
            task.play();
            if (DrawerUsuario.isHidden()) {
                DrawerUsuario.setSidePane(MenuUsuario);
                DrawerUsuario.open();
            } else {
                DrawerUsuario.setSidePane(new VBox());
                DrawerUsuario.setPrefSize(0, 0);
                DrawerUsuario.close();
            }
        });
    }
    private void evtInfo(Event event) {
        MainMenuOptions.evtGerenciamentoEmpresa(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoCliente(Event event) {
        MainMenuOptions.evtGerenciamentoCliente(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoUsuario(Event event) {
        MainMenuOptions.evtGerenciamentoUsuario(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoNivel(Event event) {
        MainMenuOptions.evtGerenciamentoNivel(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoProduto(Event event) {
        MainMenuOptions.evtConsultaProduto(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoFornecedor(Event event) {
        MainMenuOptions.evtGerenciamentoFornecedor(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoServico(Event event) {
        MainMenuOptions.evtGerenciamentoServico(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoAnimal(Event event) {
        MainMenuOptions.evtGerenciamentoAnimal(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoTipodeDespesa(Event event) {
        MainMenuOptions.evtGerenciamentoTipodeDespesa(event, this.getClass());
    }

    @FXML
    private void evtConsultaUsuario(Event event) {
        MainMenuOptions.evtConsultaUsuario(event, this.getClass());
    }

    @FXML
    private void evtConsultaCliente(Event event) {
        MainMenuOptions.evtConsultaCliente(event, this.getClass());
    }

    @FXML
    private void evtConsultaFornecedor(Event event) {
       MainMenuOptions.evtConsultaFornecedor(event, this.getClass());
    }

    @FXML
    private void evtConsultaFabricante(Event event) {
        MainMenuOptions.evtConsultaFabricante(event, this.getClass());
    }

    @FXML
    private void evtConsultaProduto(Event event) {
        MainMenuOptions.evtConsultaProduto(event, this.getClass());
    }

    @FXML
    private void evtRelFichadoUsuario(Event event) {
       MainMenuOptions.evtRelFichadoUsuario(event, this.getClass());
    }

    @FXML
    private void evtVenda(Event event) {
        MainMenuOptions.evtVenda(event, this.getClass());
    }

    @FXML
    private void evtCompra(Event event) {
        MainMenuOptions.evtCompra(event, this.getClass());
    }

    @FXML
    private void evtPagamento(Event event) {
        MainMenuOptions.evtPagamento(event, this.getClass());
    }

    @FXML
    private void evtRecebimento(Event event) {
        MainMenuOptions.evtRecebimento(event, this.getClass());
    }

    @FXML
    private void evtAgendamento(MouseEvent event) {
        MainMenuOptions.evtAgendamento(event, this.getClass());
    }

    @FXML
    private void evtCaixa(Event event) {
        MainMenuOptions.evtAbrirCaixa(event, this.getClass());
    }

    @FXML
    private void evtAtendimento(MouseEvent event) {
        MainMenuOptions.evtAtendimento(event, this.getClass());
    }

    @FXML
    private void evtBackup() {
        MainMenuOptions.evtBackup();
    }

    @FXML
    private void evtRestore() {
        MainMenuOptions.evtRestore();
    }

    private void evtConfiguracao(Event event) {
        MainMenuOptions.evtConfiguracao(event, this.getClass());
    }

    private void evtConfiguracaoInicial(Event event) {
        MainMenuOptions.evtConfiguracaoInicial(event, this.getClass());
    }
    
}
