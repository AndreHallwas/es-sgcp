/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caixa.Relatorio;

import Animal.Controladora.CtrlAnimal;
import Pessoa.Controladora.CtrlCliente;
import Pessoa.Interface.TelaConsultaClienteController;
import Transacao.Transaction;
import Utils.GerarRelatorios;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaRelatorioMovimentacoesController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private DatePicker dtpDataInicial;
    @FXML
    private DatePicker dtpDataFinal;
    @FXML
    private ScrollPane spdado;
    @FXML
    private HBox dado;
    @FXML
    private JFXButton btnBuscar;

    private static final String Caminho = "Relatorios/Petshop/Movimentações.jasper";
    private static final String Tela = "Relatório de Movimentações";
    private SwingNode sn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dtpDataInicial.setValue(LocalDate.now());
        dtpDataFinal.setValue(null);
        btnBuscar.setVisible(false);/*Busca Simples*/
    }

    @FXML
    private void btnTelaCheia(MouseEvent event) {
        try {
            if (dtpDataInicial.getValue() != null && dtpDataFinal.getValue() != null) {
                GerarRelatorios.gerarRelatorio(SqlFiltro(), Caminho, Tela, Transaction.getEntityManager());
            } else {
                GerarRelatorios.gerarRelatorio(SqlDefault(), Caminho, Tela, Transaction.getEntityManager());
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Não Há Dados Eminentes Nesta Consulta");
            dtpDataInicial.setValue(LocalDate.now());
            dtpDataFinal.setValue(null);
            Default();
        }
    }

    @FXML
    private void evtBuscar(MouseEvent event) {
        if (dtpDataInicial.getValue() != null && dtpDataFinal.getValue() != null) {
            setRelatorio(SqlFiltro(), Caminho);
        } else {
            Default();
        }
    }

    private void Default() {
        setRelatorio(SqlDefault(), Caminho);
    }

    private void setRelatorio(String Sql, String Caminho) {
        try {
            sn = GerarRelatorios.gerarRelatorio(Sql, Caminho, Transaction.getEntityManager());
            dado.getChildren().clear();
            dado.getChildren().add(sn);
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Não Há Dados Resultantes Desta Consulta");
            dtpDataInicial.setValue(LocalDate.now());
            dtpDataFinal.setValue(null);
            Default();
        }
    }

    private String SqlDefault() {
        return "select *,'Pagamento' as Tipo from caixa "
                + "inner join movimentacaoapagar on movimentacaoapagar.caixa_id = caixa.caixa_id "
                + "inner join parcelaapagar on parcelaapagar.contaapagar_id = movimentacaoapagar.contaapagar_id"
                + " and parcelaapagar.parp_datadegeracao = movimentacaoapagar.parp_datadegeracao"
                + " and parcelaapagar.parp_numerodaparcela = movimentacaoapagar.parp_numerodaparcela "
                + "inner join formadepagamento on formadepagamento.formadepagamento_id = parcelaapagar.formadepagamento_id "
                + "inner join usuario on usuario.usr_id = caixa.usr_id "
                + "union "
                + "select *,'Recebimento' as Tipo from caixa "
                + "inner join movimentacaoareceber on movimentacaoareceber.caixa_id = caixa.caixa_id "
                + "inner join parcelaareceber on parcelaareceber.contaareceber_id = movimentacaoareceber.contaareceber_id"
                + " and parcelaareceber.parr_datadegeracao = movimentacaoareceber.parr_datadegeracao"
                + " and parcelaareceber.parr_numerodaparcela = movimentacaoareceber.parr_numerodaparcela "
                + "inner join formadepagamento on formadepagamento.formadepagamento_id = parcelaareceber.formadepagamento_id "
                + "inner join usuario on usuario.usr_id = caixa.usr_id";
    }

    private String SqlFiltro() {
        String DataInicial = dtpDataInicial.getValue().toString();
        String DataFinal = dtpDataFinal.getValue().toString();
        return "select *,'Pagamento' as Tipo from caixa "
                + "inner join movimentacaoapagar on movimentacaoapagar.caixa_id = caixa.caixa_id "
                + "inner join parcelaapagar on parcelaapagar.contaapagar_id = movimentacaoapagar.contaapagar_id"
                + " and parcelaapagar.parp_datadegeracao = movimentacaoapagar.parp_datadegeracao"
                + " and parcelaapagar.parp_numerodaparcela = movimentacaoapagar.parp_numerodaparcela "
                + "inner join formadepagamento on formadepagamento.formadepagamento_id = parcelaapagar.formadepagamento_id "
                + "inner join usuario on usuario.usr_id = caixa.usr_id "
                + "where caixa.caixa_data between '" + DataInicial + "' and '" + DataFinal + "' "
                + "union "
                + "select *,'Recebimento' as Tipo from caixa "
                + "inner join movimentacaoareceber on movimentacaoareceber.caixa_id = caixa.caixa_id "
                + "inner join parcelaareceber on parcelaareceber.contaareceber_id = movimentacaoareceber.contaareceber_id"
                + " and parcelaareceber.parr_datadegeracao = movimentacaoareceber.parr_datadegeracao"
                + " and parcelaareceber.parr_numerodaparcela = movimentacaoareceber.parr_numerodaparcela "
                + "inner join formadepagamento on formadepagamento.formadepagamento_id = parcelaareceber.formadepagamento_id "
                + "inner join usuario on usuario.usr_id = caixa.usr_id "
                + "where caixa.caixa_data between '" + DataInicial + "' and '" + DataFinal + "' ";
    }

}
