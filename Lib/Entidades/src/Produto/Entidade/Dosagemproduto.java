/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "dosagemproduto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Dosagemproduto.findAll", query = "SELECT m FROM Dosagemproduto m")
    , @NamedQuery(name = "Dosagemproduto.findByDosagemprodutoId", query = "SELECT m FROM Dosagemproduto m WHERE m.dosagemprodutoId = :dosagemprodutoId")
    , @NamedQuery(name = "Dosagemproduto.findByDosagemDesc", query = "SELECT m FROM Dosagemproduto m WHERE m.dosagemDesc = :dosagemDesc")})
public class Dosagemproduto extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dosagemproduto_id")
    private Integer dosagemprodutoId;
    @Column(name = "dosagem_desc")
    private String dosagemDesc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "dosagem_quantidade")
    private BigDecimal dosagemQuantidade;
    @Column(name = "dosagem_intervalo")
    private BigDecimal dosagemIntervalo;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "dosagemprodutoId")
    private Collection<Produto> produtoCollection = new ArrayList();

    public Dosagemproduto() {
    }

    public Dosagemproduto(Integer dosagemprodutoId) {
        this.dosagemprodutoId = dosagemprodutoId;
    }

    public Dosagemproduto(String dosagemDesc, BigDecimal dosagemQuantidade, BigDecimal dosagemIntervalo) {
        this.dosagemDesc = dosagemDesc;
        this.dosagemQuantidade = dosagemQuantidade;
        this.dosagemIntervalo = dosagemIntervalo;
    }

    public Dosagemproduto(Integer dosagemprodutoId, String dosagemDesc,
            BigDecimal dosagemQuantidade, BigDecimal dosagemIntervalo) {
        this.dosagemprodutoId = dosagemprodutoId;
        this.dosagemDesc = dosagemDesc;
        this.dosagemQuantidade = dosagemQuantidade;
        this.dosagemIntervalo = dosagemIntervalo;
    }

    public Integer getDosagemprodutoId() {
        return dosagemprodutoId;
    }

    public void setDosagemprodutoId(Integer dosagemprodutoId) {
        this.dosagemprodutoId = dosagemprodutoId;
    }

    public String getDosagemDesc() {
        return dosagemDesc;
    }

    public void setDosagemDesc(String dosagemDesc) {
        this.dosagemDesc = dosagemDesc;
    }

    @XmlTransient
    public Collection<Produto> getProdutoCollection() {
        return produtoCollection;
    }

    public void setProdutoCollection(Collection<Produto> produtoCollection) {
        this.produtoCollection = produtoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dosagemprodutoId != null ? dosagemprodutoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dosagemproduto)) {
            return false;
        }
        Dosagemproduto other = (Dosagemproduto) object;
        if ((this.dosagemprodutoId == null && other.dosagemprodutoId != null) || (this.dosagemprodutoId != null && !this.dosagemprodutoId.equals(other.dosagemprodutoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return dosagemDesc;
    }

    public BigDecimal getDosagemQuantidade() {
        return dosagemQuantidade;
    }

    public void setDosagemQuantidade(BigDecimal dosagemQuantidade) {
        this.dosagemQuantidade = dosagemQuantidade;
    }

    public BigDecimal getDosagemIntervalo() {
        return dosagemIntervalo;
    }

    public void setDosagemIntervalo(BigDecimal dosagemIntervalo) {
        this.dosagemIntervalo = dosagemIntervalo;
    }
    
}
