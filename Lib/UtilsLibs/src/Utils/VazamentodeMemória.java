/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

/**
 *
 * @author Raizen
 */
/////////1
public class VazamentodeMemória {
    public static void main(String[] args) {
        while (true) {
            Element first = new Element();
            first.next = new Element();
            first.next.next = first;
        }
    }
}

class Element {
    public Element next;
}

/////2
class Leakee {

    public void check() {
        if (depth > 2) {
            Leaker.done();
        }
    }
    private int depth;

    public Leakee(int d) {
        depth = d;
    }

    protected void finalize() {
        new Leakee(depth + 1).check();
        new Leakee(depth + 1).check();
    }
}

class Leaker {

    private static boolean makeMore = true;

    public static void done() {
        makeMore = false;
    }

    public static void main(String[] args) throws InterruptedException {
        // make a bunch of them until the garbage collector gets active
        while (makeMore) {
            new Leakee(0).check();
        }
        // sit back and watch the finalizers chew through memory
        while (true) {
            Thread.sleep(1000);
            System.out.println("memory="
                    + Runtime.getRuntime().freeMemory() + " / "
                    + Runtime.getRuntime().totalMemory());
        }
    }
}
