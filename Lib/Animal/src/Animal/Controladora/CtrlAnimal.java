/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Controladora;

import Animal.Entidade.Animal;
import Animal.Entidade.Anotacao;
import Animal.Entidade.Especie;
import Animal.Entidade.Pelagem;
import Animal.Entidade.Raca;
import Controladora.Base.CtrlBase;
import Pessoa.Entidade.Cliente;
import Transacao.Transaction;
import Utils.Imagem;
import Variables.Variables;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlAnimal extends CtrlBase {

    private static CtrlAnimal ctrlanimal;

    public static CtrlAnimal create() {
        if (ctrlanimal == null) {
            ctrlanimal = new CtrlAnimal();
        }
        return ctrlanimal;
    }

    public static Object getSexo(Object animal) {
        if (animal != null && animal instanceof Animal) {
            Animal f = (Animal) animal;
            return f.getAnimalSexo();
        }
        return "Indefinido!";
    }

    public CtrlAnimal() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object animal, JFXTextField txbNome, JFXTextArea taObs,
            JFXTextField txbCliente, JFXTextField txbCor, JFXTextField txbEspecie,
            JFXTextField txbIdade, JFXTextField txbPelagem, JFXTextField txbPeso, JFXTextField txbRaca) {
        if (animal != null && animal instanceof Animal) {
            Animal f = (Animal) animal;
            txbNome.setText(f.getAnimalNome());
            taObs.setText(f.getAnimalObs());
            txbCliente.setText(Variables.NotFoundDefaultMessage(f.getCliId()));
            txbCor.setText(f.getAnimalCor());
            txbEspecie.setText(Variables.NotFoundDefaultMessage(f.getEspecieId()));
            if (f.getAnimalIdade() != null) {
                txbIdade.setText(Integer.toString(f.getAnimalIdade()));
            }
            txbPelagem.setText(Variables.NotFoundDefaultMessage(f.getPelagemId()));
            if (f.getAnimalPeso() != null) {
                txbPeso.setText(Integer.toString(f.getAnimalPeso()));
            }
            txbRaca.setText(Variables.NotFoundDefaultMessage(f.getRacaId()));
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Animal) {
            return ((Animal) p).getAnimalId();
        }
        return -1;
    }

    public static BufferedImage getImagem(Object p) {
        if (p != null && p instanceof Animal) {
            return Imagem.ByteArrayToBufferedImage(((Animal) p).getAnimalImagem());
        }
        return null;
    }

    public static ArrayList<Object> getAnotacoes(Object p) {
        ArrayList<Object> arr = new ArrayList();
        if (p != null && p instanceof Animal) {
            arr.addAll(((Animal) p).getAnotacaoCollection());
        }
        return arr;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Animal) {
            txBusca.setText(((Animal) p).getAnimalNome());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> animal = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Animal> ResultAnimal;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultAnimal = em.createNamedQuery("Animal.findAll", Animal.class)
                        .getResultList();
            } else {
                try {
                    ResultAnimal = em.createNamedQuery("Animal.findByAnimalId", Animal.class)
                            .setParameter("animalId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultAnimal = em.createNamedQuery("Animal.findByAnimalGeral", Animal.class)
                            .setParameter("filtro", Filtro).getResultList();
                }
            }
            for (Animal animals : ResultAnimal) {
                animal.add(animals);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return animal;
    }

    public Object Salvar(String Nome, Integer Peso, String Cor, Integer Idade, String Obs, String Sexo,
            byte[] Imagem, Object pelagem, Object raca, Object especie, Object cliente) {
        Animal animal = new Animal(Nome, Peso, Cor, Idade, Imagem, Obs, Sexo);
        if (pelagem != null && pelagem instanceof Pelagem) {
            animal.setPelagemId((Pelagem) pelagem);
        }
        if (raca != null && raca instanceof Raca) {
            animal.setRacaId((Raca) raca);
        }
        if (especie != null && especie instanceof Especie) {
            animal.setEspecieId((Especie) especie);
        }
        if (cliente != null && cliente instanceof Cliente) {
            animal.setCliId((Cliente) cliente);
        }
        return super.Salvar(animal);
    }

    public Object Alterar(Object oAnimal, Integer Id, String Nome, Integer Peso, String Cor, Integer Idade,
            String Obs, String Sexo, byte[] Imagem, Object pelagem, Object raca, Object especie, Object cliente) {
        Animal animal = null;
        if (oAnimal != null && oAnimal instanceof Animal) {
            animal = (Animal) oAnimal;
            animal.setAnimalCor(Cor);
            animal.setAnimalIdade(Idade);
            animal.setAnimalImagem(Imagem);
            animal.setAnimalNome(Nome);
            animal.setAnimalObs(Obs);
            animal.setAnimalPeso(Peso);
            animal.setAnimalSexo(Sexo);
            if (pelagem != null && pelagem instanceof Pelagem) {
                animal.setPelagemId((Pelagem) pelagem);
            }
            if (raca != null && raca instanceof Raca) {
                animal.setRacaId((Raca) raca);
            }
            if (especie != null && especie instanceof Especie) {
                animal.setEspecieId((Especie) especie);
            }
            if (cliente != null && cliente instanceof Cliente) {
                animal.setCliId((Cliente) cliente);
            }
            animal = (Animal) super.Alterar(animal);
        }
        return animal;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Animal.class);
    }
}
