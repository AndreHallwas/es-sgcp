/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Raizen
 */
public abstract class AgendarTarefaThread {

    private void AgendarTarefaHora(int Hora, int Minuto, int Segundo) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, Hora);
        c.set(Calendar.MINUTE, Minuto);
        c.set(Calendar.SECOND, Segundo);

        Date time = c.getTime();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                t.cancel();
            }

        }, time);
    }

    private void AgendarTarefaData(int Dia, int Mes, int Ano) {
        Calendar data = Calendar.getInstance();
        data.set(Ano, Mes, Dia);

        Date time = data.getTime();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                t.cancel();
            }

        }, time);
    }

    private void AgendarTarefaContinua(int Inicio, int Intervalo) {
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                t.cancel();
            }

        }, Inicio, Intervalo);
    }

}
