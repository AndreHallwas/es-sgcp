/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContaaReceber.Entidade;

import Utils.Entidade.TimestampConverter;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Raizen
 */
@Embeddable
public class ParcelaareceberPK implements Serializable {

    @Basic(optional = false)
    @Column(name = "contaareceber_id")
    private int contaareceberId;
    @Basic(optional = false)
    @Column(name = "parr_numerodaparcela")
    private int parrNumerodaparcela;
    @Basic(optional = false)
    @Column(name = "parr_datadegeracao")
    @Temporal(TemporalType.TIMESTAMP)
    @Convert(converter = TimestampConverter.class)
    private Date parrDatadegeracao;

    public ParcelaareceberPK() {
    }

    public ParcelaareceberPK(int contaareceberId, int parrNumerodaparcela, Date parrDatadegeracao) {
        this.contaareceberId = contaareceberId;
        this.parrNumerodaparcela = parrNumerodaparcela;
        this.parrDatadegeracao = parrDatadegeracao;
    }

    public int getContaareceberId() {
        return contaareceberId;
    }

    public void setContaareceberId(int contaareceberId) {
        this.contaareceberId = contaareceberId;
    }

    public int getParrNumerodaparcela() {
        return parrNumerodaparcela;
    }

    public void setParrNumerodaparcela(int parrNumerodaparcela) {
        this.parrNumerodaparcela = parrNumerodaparcela;
    }

    public Date getParrDatadegeracao() {
        return parrDatadegeracao;
    }

    public void setParrDatadegeracao(Date parrDatadegeracao) {
        this.parrDatadegeracao = parrDatadegeracao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) contaareceberId;
        hash += (int) parrNumerodaparcela;
        hash += (parrDatadegeracao != null ? parrDatadegeracao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParcelaareceberPK)) {
            return false;
        }
        ParcelaareceberPK other = (ParcelaareceberPK) object;
        if (this.contaareceberId != other.contaareceberId) {
            return false;
        }
        if (this.parrNumerodaparcela != other.parrNumerodaparcela) {
            return false;
        }
        if ((this.parrDatadegeracao == null && other.parrDatadegeracao != null) || (this.parrDatadegeracao != null && !this.parrDatadegeracao.equals(other.parrDatadegeracao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "teste.ParcelaareceberPK[ contaareceberId=" + contaareceberId + ", parrNumerodaparcela=" + parrNumerodaparcela + ", parrDatadegeracao=" + parrDatadegeracao + " ]";
    }
    
}
