/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Controladora;

import Controladora.Base.CtrlBase;
import Endereco.Entidade.Bairro;
import Endereco.Entidade.Endereco;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlEndereco extends CtrlBase {

    private static CtrlEndereco ctrlendereco;

    public static CtrlEndereco create() {
        if (ctrlendereco == null) {
            ctrlendereco = new CtrlEndereco();
        }
        return ctrlendereco;
    }
    
    public static String getCep(Object in){
        if(in != null && in instanceof Endereco){
            return ((Endereco)in).getEndCep();
        }
        return "";
    }

    public static void setCampos(Object oEndereco, TextField txbCep, TextField cbRua, TextField cbBairro,
            TextField cbCidade, TextField cbEstado, TextField cbPais) {
        if (oEndereco != null && oEndereco instanceof Endereco) {
            Endereco en = (Endereco) oEndereco;
            txbCep.setText(en.getEndCep());
            cbRua.setText(en.getEndLocal());
            cbBairro.setText(en.getBaiId().getBaiNome());
            cbCidade.setText(en.getBaiId().getCidId().getCidNome());
            cbEstado.setText(en.getBaiId().getCidId().getEstId().getEstNome());
            cbPais.setText(en.getBaiId().getCidId().getEstId().getPaisId().getPaisNome());
        }
    }

    public CtrlEndereco() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> endereco = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            List<Endereco> ResultEndereco = em.createNamedQuery("Endereco.findByEndCep", Endereco.class)
                    .setParameter("endCep", Filtro).getResultList();

            for (Endereco enderecos : ResultEndereco) {
                endereco.add(enderecos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return endereco;
    }

    public Object Salvar(String Cep, String Local, Object bairro) {
        Endereco endereco = new Endereco(Cep, Local);
        if (bairro != null && bairro instanceof Bairro) {
            endereco.setBaiId((Bairro) bairro);
        }
        return super.Salvar(endereco);
    }

    public Object Alterar(Object oEndereco, String Cep, String Local, Object bairro) {
        Endereco endereco = null;
        if (oEndereco != null && oEndereco instanceof Endereco) {
            endereco = (Endereco) oEndereco;
            endereco.setEndCep(Cep);
            endereco.setEndLocal(Local);
            if (bairro != null && bairro instanceof Bairro) {
                endereco.setBaiId((Bairro) bairro);
            }
            endereco = (Endereco) super.Alterar(endereco);
        }
        return endereco;
    }

    public boolean Remover(String Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Endereco.class);
    }
    
}
