/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Controladora;

import Acesso.Entidade.Componente;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlComponente extends CtrlBase {

    private static CtrlComponente ctrlcomponente;

    public static CtrlComponente create() {
        if (ctrlcomponente == null) {
            ctrlcomponente = new CtrlComponente();
        }
        return ctrlcomponente;
    }

    public CtrlComponente() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> componente = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Componente> ResultComponente;
            if (Filtro != null && Filtro.trim().isEmpty()) {
                ResultComponente = em.createNamedQuery("Componente.findAll", Componente.class).getResultList();
            } else {
                try {
                    ResultComponente = em.createNamedQuery("Componente.findByComponenteId", Componente.class)
                            .setParameter("componenteId", Filtro).getResultList();
                } catch (Exception ex) {
                    ResultComponente = em.createNamedQuery("Componente.findAll", Componente.class).getResultList();
                }
            }
            for (Componente componentes : ResultComponente) {
                componente.add(componentes);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return componente;
    }

    public Object Salvar(String componenteId, String componenteDescricao) {
        Componente componente = new Componente(componenteId, componenteDescricao);
        return super.Salvar(componente);
    }

    public Object Alterar(Object oComponente, String componenteId, String componenteDescricao) {
        Componente componente = null;
        if (oComponente != null && oComponente instanceof Componente) {
            componente = (Componente) oComponente;
            componente.setComponenteId(componenteId);
            componente.setComponenteDescricao(componenteDescricao);
            componente = (Componente) super.Alterar(componente);
        }
        return componente;
    }

    public boolean Remover(String Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Componente.class);
    }
}
