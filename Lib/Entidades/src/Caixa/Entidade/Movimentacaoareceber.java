/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caixa.Entidade;

import ContaaReceber.Entidade.Parcelaareceber;
import Utils.FormatString;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "movimentacaoareceber")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Movimentacaoareceber.findAll", query = "SELECT m FROM Movimentacaoareceber m")
    , @NamedQuery(name = "Movimentacaoareceber.findByMovimentacaoareceberId", query = "SELECT m FROM Movimentacaoareceber m WHERE m.movimentacaoareceberId = :movimentacaoareceberId")
    , @NamedQuery(name = "Movimentacaoareceber.findByMovimentacaoareceberData", query = "SELECT m FROM Movimentacaoareceber m WHERE m.movimentacaoareceberData = :movimentacaoareceberData")})
public class Movimentacaoareceber implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "movimentacaoareceber_id")
    private Integer movimentacaoareceberId;
    @Basic(optional = false)
    @Column(name = "movimentacaoareceber_data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date movimentacaoareceberData;
    @JoinColumn(name = "caixa_id", referencedColumnName = "caixa_id")
    @ManyToOne(optional = false)
    private Caixa caixaId;
    @JoinColumns({
        @JoinColumn(name = "contaareceber_id", referencedColumnName = "contaareceber_id")
        , @JoinColumn(name = "parr_numerodaparcela", referencedColumnName = "parr_numerodaparcela")
        , @JoinColumn(name = "parr_datadegeracao", referencedColumnName = "parr_datadegeracao")})
    @ManyToOne(optional = false)
    private Parcelaareceber parcelaareceber;

    public Movimentacaoareceber() {
    }

    public Movimentacaoareceber(Date movimentacaoareceberData) {
        this.movimentacaoareceberData = movimentacaoareceberData;
    }

    public Movimentacaoareceber(Integer movimentacaoareceberId) {
        this.movimentacaoareceberId = movimentacaoareceberId;
    }

    public Movimentacaoareceber(Integer movimentacaoareceberId, Date movimentacaoareceberData) {
        this.movimentacaoareceberId = movimentacaoareceberId;
        this.movimentacaoareceberData = movimentacaoareceberData;
    }

    public Integer getMovimentacaoareceberId() {
        return movimentacaoareceberId;
    }

    public void setMovimentacaoareceberId(Integer movimentacaoareceberId) {
        this.movimentacaoareceberId = movimentacaoareceberId;
    }

    public Date getMovimentacaoareceberData() {
        return movimentacaoareceberData;
    }

    public void setMovimentacaoareceberData(Date movimentacaoareceberData) {
        this.movimentacaoareceberData = movimentacaoareceberData;
    }

    public Caixa getCaixaId() {
        return caixaId;
    }

    public void setCaixaId(Caixa caixaId) {
        this.caixaId = caixaId;
    }

    public Parcelaareceber getParcelaareceber() {
        return parcelaareceber;
    }

    public void setParcelaareceber(Parcelaareceber parcelaareceber) {
        this.parcelaareceber = parcelaareceber;
    }
    
    public String getTipodeMovimentacao(){
        return "Entrada";
    }
    
    public String getValorMovimentado(){
        BigDecimal Valor = BigDecimal.ZERO;
        if(parcelaareceber != null && parcelaareceber.getParrValorPago() != null){
            Valor = parcelaareceber.getParrValorPago();
        }
        return FormatString.MonetaryPersistent(Valor);
    }
    
    public String getDescricaoPagamento(){
        String result = "Sem Descrição";
        if(parcelaareceber != null && parcelaareceber.getContaareceber() != null &&
                parcelaareceber.getContaareceber().getContaareceberDescricao() != null &&
                !parcelaareceber.getContaareceber().getContaareceberDescricao().trim().isEmpty()){
            result = parcelaareceber.getContaareceber().getContaareceberDescricao();
        }
        return result;
    }
    
    public Object getFormadePagamento(){
        Object result = "Não Definido";
        if(parcelaareceber != null && parcelaareceber.getFormadepagamentoId() != null){
            result = parcelaareceber.getFormadepagamentoId();
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (movimentacaoareceberId != null ? movimentacaoareceberId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movimentacaoareceber)) {
            return false;
        }
        Movimentacaoareceber other = (Movimentacaoareceber) object;
        if ((this.movimentacaoareceberId == null && other.movimentacaoareceberId != null) || (this.movimentacaoareceberId != null && !this.movimentacaoareceberId.equals(other.movimentacaoareceberId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Caixa.Entidade.Movimentacaoareceber[ movimentacaoareceberId=" + movimentacaoareceberId + " ]";
    }
    
}
