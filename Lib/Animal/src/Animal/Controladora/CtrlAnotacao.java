/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Controladora;

import Animal.Entidade.Animal;
import Animal.Entidade.Anotacao;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import Utils.DateUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Aluno
 */
public class CtrlAnotacao extends CtrlBase {

    private static CtrlAnotacao ctrlanotacao;

    public static CtrlAnotacao create() {
        if (ctrlanotacao == null) {
            ctrlanotacao = new CtrlAnotacao();
        }
        return ctrlanotacao;
    }

    public CtrlAnotacao() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object anotacao, TextArea taDescricao, TextField txbAnimal, DatePicker dtpData, TextField txbId) {
        if (anotacao != null && anotacao instanceof Anotacao) {
            Anotacao f = (Anotacao) anotacao;
            taDescricao.setText(f.getAnotacaoDescricao());
            txbId.setText(f.getAnotacaoId() + "");
            if (f.getAnimalId() != null) {
                txbAnimal.setText(f.getAnimalId().getAnimalNome());
            }
            dtpData.setValue(DateUtils.asLocalDate(f.getAnotacaoData()));
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Anotacao) {
            return ((Anotacao) p).getAnotacaoId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Anotacao) {
            txBusca.setText(((Anotacao) p).getAnotacaoId() + "");
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> anotacao = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Anotacao> ResultAnotacao = null;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultAnotacao = em.createNamedQuery("Anotacao.findAll", Anotacao.class)
                        .getResultList();
            } else if (Tipo != null && !Tipo.trim().isEmpty()) {
                try {
                    if (Tipo.equalsIgnoreCase("AnimalId")) {
                        ResultAnotacao = em.createNamedQuery("Anotacao.findByAnotacaoAnimalId", Anotacao.class)
                                .setParameter("animalId", Integer.parseInt(Filtro)).getResultList();
                    } else {
                        ResultAnotacao = em.createNamedQuery("Anotacao.findByAnotacaoId", Anotacao.class)
                                .setParameter("anotacaoId", Integer.parseInt(Filtro)).getResultList();
                    }
                } catch (Exception ex) {
                    try {
                        ResultAnotacao = em.createNamedQuery("Anotacao.findByAnotacaoDescricao", Anotacao.class)
                                .setParameter("anotacaoDescricao", Filtro).getResultList();
                    } catch (Exception exc) {
                        ResultAnotacao = em.createNamedQuery("Anotacao.findByAnotacaoAll", Anotacao.class)
                                .getResultList();
                    }
                }
            }
            for (Anotacao anotacaos : ResultAnotacao) {
                anotacao.add(anotacaos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return anotacao;
    }

    public Object Salvar(String Descricao, Date data, Object animal) {
        Anotacao anotacao = new Anotacao(Descricao, data);
        if (animal != null && animal instanceof Animal) {
            anotacao.setAnimalId((Animal) animal);
        }
        return super.Salvar(anotacao);
    }

    public Object Alterar(Object aAnotacao, Integer Id, String Descricao, Date data, Object animal) {
        Anotacao anotacao = null;
        if (aAnotacao != null && aAnotacao instanceof Anotacao) {
            anotacao = (Anotacao) aAnotacao;
            anotacao.setAnotacaoDescricao(Descricao);
            anotacao.setAnotacaoData(data);
            if (animal != null && animal instanceof Animal) {
                anotacao.setAnimalId((Animal) animal);
            }
            anotacao = (Anotacao) super.Alterar(anotacao);
        }
        return anotacao;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Anotacao.class);
    }

}
