/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javafx.embed.swing.SwingNode;
import javax.persistence.EntityManager;
import javax.swing.SwingUtilities;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import org.eclipse.persistence.internal.jpa.EntityManagerImpl;

/**Modelo*/
/*abstract class Banco{
    public static Conexao getCon(){
        return new Conexao();
    }
    public static class Conexao{
        public ResultSet consultar(String Sql){
            return null;
        }
    }
}*/

public class GerarRelatorios 
{
    
    public static void gerarRelatorio(String sql, String relat, String Titulo, EntityManager em) {
        try {  
            Connection connection = ((EntityManagerImpl) (em.getDelegate())).getServerSession().getAccessor().getConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sql);
            ///Map de Parametros
            Map parametros = new HashMap();
            parametros.put("REPORT_CONNECTION",connection);
            //implementação da interface JRDataSource para DataSource ResultSet
            JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
            //preenchendo e chamando o relatório
            String jasperPrint = JasperFillManager.fillReportToFile(relat, parametros, jrRS);
            JasperViewer viewer = new JasperViewer(jasperPrint, false, false);
            
            viewer.setExtendedState(JasperViewer.MAXIMIZED_BOTH);//maximizado
            viewer.setTitle(Titulo);
            viewer.setVisible(true);
        } catch (Exception erro) {
            Mensagem.ExibirException(erro, "Erro ao Gerar Relatório");
        }
    }
    
    public static SwingNode gerarRelatorio(String sql, String relat, EntityManager em)//dentro de um painel
    {  
        try
        {
            Connection connection = ((EntityManagerImpl) (em.getDelegate())).getServerSession().getAccessor().getConnection();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(sql);
            JRResultSetDataSource jrRS = new JRResultSetDataSource(rs);
            Map parametros = new HashMap();
            parametros.put("REPORT_CONNECTION",connection);
            JasperPrint print = JasperFillManager.fillReport(relat, parametros, jrRS);
            JRViewer viewer = new JRViewer(print);
            viewer.setOpaque(true);
            viewer.setVisible(true);
            viewer.setZoomRatio(0.5f);
            viewer.setVisible(true);
            viewer.getHeight();
            SwingNode swingNode = new SwingNode();
            SwingUtilities.invokeLater(()->{swingNode.setContent(viewer);});
            return swingNode;
        } 
         catch (JRException | SQLException erro)  
         {  
             Mensagem.ExibirException(erro, "Erro ao Gerar Relatório");
             return null;
         }
    }    
}
