/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Despesa.Interface;

import ContaaPagar.Controladora.CtrlContaapagar;
import ContaaPagar.Controladora.CtrlParcelaapagar;
import ContaaPagar.Entidade.Contaapagar;
import Transacao.Utils.ItemCarrinho;
import Utils.Acao.InterfaceFxmlAcao;
import Utils.DateUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaDespesaController implements Initializable, InterfaceFxmlAcao {

    @FXML
    private DatePicker dtpData;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Object> tcDescricao;
    @FXML
    private TableColumn<Object, Object> tcTipo;
    @FXML
    private TableColumn<Object, Object> tcValor;
    @FXML
    private TableColumn<Object, Object> tcData;
    @FXML
    private TableColumn<Object, Object> tcAcoes;
    @FXML
    private JFXTextField tcValorTotal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcData.setCellValueFactory(new PropertyValueFactory("Data"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("ContaapagarDescricao"));
        tcTipo.setCellValueFactory(new PropertyValueFactory("TipodedespesaId"));
        tcValor.setCellValueFactory(new PropertyValueFactory("ContaapagarValortotal"));
        tcAcoes.setCellValueFactory(new PropertyValueFactory("Crud"));

        SetTelaAcao();

        dtpData.setValue(LocalDate.now());
        HandleDateTimePickerChanged(null);
    }

    @FXML
    private void HandleDateTimePickerChanged(Event event) {
        if (dtpData.getValue() != null) {
            ArrayList<Object> asDespesas = CtrlContaapagar.create().Pesquisar(
                    DateUtils.asDate(dtpData.getValue()).getTime() + "", "Data");
            tabela.setItems(FXCollections.observableArrayList(asDespesas));
            tcValorTotal.setText(CtrlContaapagar.getValorTotal(asDespesas));
        }
    }

    public void evtRemover(Object Despesa) {
        Mensagem.Exibir("Só é possivel remover caso o caixa do dia em que foi lançado esteja aberto.", 1);
        if (CtrlContaapagar.create().EstornareRemover(Despesa)) {
            HandleDateTimePickerChanged(null);
        }
    }

    @Override
    public void SelectEventAcao(Object Reference, String Action, Object element) {
        if (Action.equalsIgnoreCase("rem")) {
            evtRemover(Reference);
        }
    }

    @Override
    public void SetTelaAcao() {
        Contaapagar.setTelaAcao(this);
    }

}
