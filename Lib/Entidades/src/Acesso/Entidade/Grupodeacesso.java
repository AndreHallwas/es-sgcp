/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Entidade;

import JPA.Dao.JPADao;
import Pessoa.Entidade.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "grupodeacesso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupodeacesso.findAll", query = "SELECT g FROM Grupodeacesso g")
    , @NamedQuery(name = "Grupodeacesso.findByGrupoId", query = "SELECT g FROM Grupodeacesso g WHERE g.grupoId = :grupoId")
    , @NamedQuery(name = "Grupodeacesso.findByGrupoNome", query = "SELECT g FROM Grupodeacesso g WHERE UPPER(g.grupoNome) like CONCAT(UPPER(:grupoNome),'%')")
    , @NamedQuery(name = "Grupodeacesso.findByGrupoDescricao", query = "SELECT g FROM Grupodeacesso g WHERE g.grupoDescricao = :grupoDescricao")})
public class Grupodeacesso extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id/*
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="grupodeacesso_grupo_id_seq")*/
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "grupo_id")
    private Integer grupoId;
    @Column(name = "grupo_nome")
    private String grupoNome;
    @Column(name = "grupo_descricao")
    private String grupoDescricao;
    @JoinColumn(name = "permacesso_id", referencedColumnName = "permacesso_id")
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    private Permissaodeacesso permacessoId;
    
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "grupoId")
    private Collection<Usuario> usuarioCollection = new ArrayList();

    public Grupodeacesso() {
    }

    public Grupodeacesso(Integer grupoId) {
        this.grupoId = grupoId;
    }

    public Grupodeacesso(String grupoNome, String grupoDescricao) {
        this.grupoNome = grupoNome;
        this.grupoDescricao = grupoDescricao;
    }

    public Grupodeacesso(Integer grupoId, String grupoNome, String grupoDescricao) {
        this.grupoId = grupoId;
        this.grupoNome = grupoNome;
        this.grupoDescricao = grupoDescricao;
    }

    public Integer getGrupoId() {
        return grupoId;
    }

    public void setGrupoId(Integer grupoId) {
        this.grupoId = grupoId;
    }

    public String getGrupoNome() {
        return grupoNome;
    }

    public void setGrupoNome(String grupoNome) {
        this.grupoNome = grupoNome;
    }

    public String getGrupoDescricao() {
        return grupoDescricao;
    }

    public void setGrupoDescricao(String grupoDescricao) {
        this.grupoDescricao = grupoDescricao;
    }

    public Permissaodeacesso getPermacessoId() {
        return permacessoId;
    }

    public void setPermacessoId(Permissaodeacesso permacessoId) {
        this.permacessoId = permacessoId;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grupoId != null ? grupoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupodeacesso)) {
            return false;
        }
        Grupodeacesso other = (Grupodeacesso) object;
        if ((this.grupoId == null && other.grupoId != null) || (this.grupoId != null && !this.grupoId.equals(other.grupoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        /*return "testedb2.Grupodeacesso[ grupoId=" + grupoId + " ]";*/
        return grupoNome;
    }
    
}
