/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Produto.Controladora.CtrlCategoriaproduto;
import Produto.Controladora.CtrlDosagemproduto;
import Produto.Controladora.CtrlProduto;
import Utils.Controladora.CtrlUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadProdutoController implements Initializable, Tela {

    @FXML
    private VBox pndados;
    @FXML
    private JFXTextField txbNome;
    @FXML
    private Label lbErroNome1;
    @FXML
    private JFXTextField txbPreco;
    @FXML
    private Label lbErroPreco;
    @FXML
    private JFXTextField txbQuantidade;
    @FXML
    private Label lbErroQuantidade;
    @FXML
    private JFXTextField txbCategoria;
    @FXML
    private JFXTextField txbTipo;
    @FXML
    private Label lbErroCategoria;
    @FXML
    private JFXTextField txbDosagem;
    @FXML
    private Label lbErroDosagem;
    @FXML
    private JFXTextArea taObs;
    @FXML
    private Label lbErrobs;
    @FXML
    private JFXButton btnovo;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btapagar;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    @FXML
    private JFXButton btConsulta;
    @FXML
    private JFXTextField lbID;
    @FXML
    private JFXTextField txbPrecoVenda;
    @FXML
    private Label lbErroPrecoVenda;
    
    private int flag;
    private Integer ID;
    private boolean EOriginal;
    private Object dosagem;
    private Object categoria;
    private Object Instancia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MaskFieldUtil.numericField(txbQuantidade);
        MaskFieldUtil.monetaryField(txbPreco);
        MaskFieldUtil.monetaryField(txbPrecoVenda);
        
        estadoOriginal();
    }

    private void setAllErro(boolean Enome, boolean Epreco, boolean Edescricao,
            boolean Equantidade, boolean Ecategoria, boolean Edosagem, boolean Eprecovenda) {
        lbErroQuantidade.setVisible(Equantidade);
        lbErroPreco.setVisible(Epreco);
        lbErroNome1.setVisible(Enome);
        lbErrobs.setVisible(Edescricao);
        lbErroCategoria.setVisible(Ecategoria);
        lbErroDosagem.setVisible(Edosagem);
        lbErroPrecoVenda.setVisible(Eprecovenda);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlProduto cp = CtrlProduto.create();
        ArrayList<Object> Pesquisa = cp.Pesquisar(txbNome.getText(), "Geral");
        if (txbNome.getText().trim().isEmpty() || !Pesquisa.isEmpty() || Pesquisa.isEmpty()) {
            if (txbNome.getText().trim().isEmpty()) {
                setErro(lbErroNome1, "Campo Obrigatório Vazio");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            } else if (!Pesquisa.isEmpty() && flag == 1) {
                setErro(lbErroNome1, "Nome Ja Existe");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            }
        }
        if (txbCategoria.getText().trim().isEmpty()) {
            setErro(lbErroCategoria, "Campo Obrigatório Vazio");
            txbCategoria.setText("");
            txbCategoria.requestFocus();
            fl = false;
        }
        if (txbPreco.getText().trim().isEmpty()) {
            setErro(lbErroPreco, "Campo Obrigatório Vazio");
            txbPreco.setText("");
            txbPreco.requestFocus();
            fl = false;
        }
        if (txbPrecoVenda.getText().trim().isEmpty()) {
            setErro(lbErroPrecoVenda, "Campo Obrigatório Vazio");
            txbPrecoVenda.setText("");
            txbPrecoVenda.requestFocus();
            fl = false;
        }
        if (txbQuantidade.getText().trim().isEmpty()) {
            setErro(lbErroQuantidade, "Campo Obrigatório Vazio");
            txbQuantidade.setText("");
            txbQuantidade.requestFocus();
            fl = false;
        }
        return fl;
    }

    @FXML
    private void evtNovo(Event event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(Event event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(Event event) {
        CtrlProduto cp = CtrlProduto.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(ID);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(Event event) {
        setAllErro(false, false, false, false, false, false, false);
        CtrlProduto cp = CtrlProduto.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, ID, txbNome.getText(), MaskFieldUtil.monetaryValueFromField(txbPreco),
                        Integer.parseInt(txbQuantidade.getText()), taObs.getText(),
                    MaskFieldUtil.monetaryValueFromField(txbPrecoVenda), dosagem, categoria)) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(txbNome.getText(), MaskFieldUtil.monetaryValueFromField(txbPreco),
                    Integer.parseInt(txbQuantidade.getText()), taObs.getText(),
                    MaskFieldUtil.monetaryValueFromField(txbPrecoVenda), dosagem, categoria)) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(Event event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false, false, false, false, false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false, false, false, false, false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);

        clear(pndados.getChildren());
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlProduto.setCampos(p, txbNome, taObs, txbPreco, txbQuantidade,
                    txbTipo, txbDosagem, txbCategoria, txbPrecoVenda);
            ID = CtrlProduto.getId(p);
            lbID.setText(ID.toString());
        }
    }

    @FXML
    private void evtConsultaAvancada(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaProduto.fxml",
                "Consulta de Produto", null);
        if (TelaConsultaProdutoController.getProduto() != null) {
            EstadoConsulta(TelaConsultaProdutoController.getProduto());
        }
    }

    @FXML
    private void evtConsultaCategoria(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaCategoriaproduto.fxml",
                "Consulta de Categoria de Produto", null);
        categoria = TelaConsultaCategoriaprodutoController.getCategoriaproduto();
        setConsulta(categoria, txbCategoria);
        CtrlCategoriaproduto.setCampos(categoria, txbCategoria, txbTipo);
        CtrlCategoriaproduto.setCampoBusca(TelaConsultaCategoriaprodutoController.getCategoriaproduto(), txbCategoria);
    }

    private void setConsulta(Object ob, TextField txb) {
        if (ob != null) {
            txb.setText(ob.toString());
        }
    }

    @FXML
    private void evtConsultaDosagem(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaDosagemproduto.fxml",
                "Consulta de Dosagem de Produto", null);
        dosagem = TelaConsultaDosagemprodutoController.getDosagemproduto();
        setConsulta(dosagem, txbDosagem);
        CtrlDosagemproduto.setCampoBusca(TelaConsultaDosagemprodutoController.getDosagemproduto(), txbDosagem);
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }

}
