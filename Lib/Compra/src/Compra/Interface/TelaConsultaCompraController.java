/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Interface;

import Compra.Controladora.CtrlCompra;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaCompraController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcId;
    @FXML
    private TableColumn<Object, String> tcValorTotal;
    @FXML
    private TableColumn<Object, String> tcParcelas;
    @FXML
    private TableColumn<Object, String> tcData;
    @FXML
    private TableColumn<Object, String> tcSituacao;
    @FXML
    private TableColumn<Object, String> tcObs;
    @FXML
    private HBox btnGp;

    private static Object compra;
    private static boolean btnPainel = true;
    protected static int Quantidade;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcData.setCellValueFactory(new PropertyValueFactory("DatadaCompra"));
        tcId.setCellValueFactory(new PropertyValueFactory("CompraId"));
        tcObs.setCellValueFactory(new PropertyValueFactory("CompraObs"));
        tcParcelas.setCellValueFactory(new PropertyValueFactory("Parcelas"));
        tcSituacao.setCellValueFactory(new PropertyValueFactory("Situacao"));
        tcValorTotal.setCellValueFactory(new PropertyValueFactory("ValorTotal"));
        CarregaTabela("");
        compra = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlCompra ctm = CtrlCompra.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, "Geral")));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            compra = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        try {
            if (compra != null) {
                evtCancelar(event);
            } else {
                Mensagem.Exibir("Nenhuma Compra Selecionada", 2);
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex);
        }
    }

    public static TelaConsultaCompraController Create(Class classe, Pane pndados) {
        TelaConsultaCompraController CCompra = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Interfaces/TelaConsultaCompra.fxml"));
            Parent root = Loader.load();
            CCompra = (TelaConsultaCompraController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CCompra;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaCompraController.btnPainel = btnPainel;
    }

    public static Object getCompra() {
        return compra;
    }

}
