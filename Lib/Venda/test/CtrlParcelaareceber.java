package Contaareceber.Controladora;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Caixa.Controladora.CtrlCaixa;
import ContaaReceber.Entidade.Contaareceber;
import ContaaReceber.Entidade.Parcelaareceber;
import ContaaReceber.Entidade.ParcelaareceberPK;
import Controladora.Base.CtrlBase;
import FormadePagamento.Entidade.Formadepagamento;
import Transacao.Transacao;
import Transacao.Transaction;
import Utils.Mensagem;
import Venda.Controladora.CtrlVenda;
import Venda.Entidade.Venda;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlParcelaareceber extends CtrlBase {

    private static CtrlParcelaareceber ctrlparcelaareceber;

    public static CtrlParcelaareceber create() {
        if (ctrlparcelaareceber == null) {
            ctrlparcelaareceber = new CtrlParcelaareceber();
        }
        return ctrlparcelaareceber;
    }

    public static Object getformadepagamento(Object ItemSelecionado) {
        Object aformadepagamento = null;
        if(ItemSelecionado != null && ItemSelecionado instanceof Parcelaareceber) {
            Parcelaareceber parcela = (Parcelaareceber) ItemSelecionado;
            aformadepagamento = parcela.getFormadepagamentoId();
        }
        return aformadepagamento;
    }

    public static Object getvenda(Object ItemSelecionado) {
        Object avenda = null;
        if(ItemSelecionado != null && ItemSelecionado instanceof Parcelaareceber) {
            Parcelaareceber parcela = (Parcelaareceber) ItemSelecionado;
            if(parcela.getContaareceber() != null){
                avenda = parcela.getContaareceber().getVendaId();
            }
        }
        return avenda;
    }

    public CtrlParcelaareceber() {
        super(Transaction.getEntityManagerFactory());
    }

    public static boolean setformadepagamento(Object ItemSelecionado, Object aformadepagamento) {
        return new Transacao() {
            @Override
            public Object Transacao(Object... Params) {
                Parcelaareceber parcela = null;
                if (ItemSelecionado != null && ItemSelecionado instanceof Parcelaareceber
                        && aformadepagamento != null && aformadepagamento instanceof Formadepagamento) {
                    parcela = (Parcelaareceber) ItemSelecionado;
                    Formadepagamento formadepagamento = (Formadepagamento) aformadepagamento;
                    parcela.setFormadepagamentoId(formadepagamento);
                    parcela = em.merge(parcela);
                }
                return parcela;
            }
        }.Realiza() != null;
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> parcelaareceber = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Parcelaareceber> ResultParcelaareceber = new ArrayList();
            if (Filtro == null || Filtro.isEmpty()) {
                ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findAll", Parcelaareceber.class)
                        .getResultList();
            } else {
                try {
                    if (Tipo.equalsIgnoreCase("Geral")) {
                        ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findByAllGeral", Parcelaareceber.class)
                                .setParameter("parrNumerodaparcela", Integer.parseInt(Filtro))
                                .setParameter("parrValor", Double.parseDouble(Filtro))
                                .setParameter("parrValorPago", Double.parseDouble(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("Conta")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findByParcelaareceberPk", Parcelaareceber.class)
                                    .setParameter("contaareceberId", Integer.parseInt(auxFiltro[0]))
                                    .setParameter("parrNumerodaparcela", Integer.parseInt(auxFiltro[1])).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("venda")) {
                        ArrayList<Object> Resultado = CtrlVenda.create().Pesquisar(Filtro, "Id");
                        if (Resultado != null && !Resultado.isEmpty()) {
                            ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findByvendaId", Parcelaareceber.class)
                                    .setParameter("vendaId", Resultado.get(0)).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("Situação")) {
                        ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findByparrSituacao", Parcelaareceber.class)
                                .setParameter("parrSituacao", Boolean.parseBoolean(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("ProximaParcela")) {
                        ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findByNextParcela", Parcelaareceber.class)
                                .setParameter("contaareceberId", Integer.parseInt(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("DataVencimento")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ArrayList<Object> vendas = CtrlVenda.create().Pesquisar(auxFiltro[1], "Id");
                            if (vendas != null && vendas.size() > 0) {
                                Venda venda = (Venda) vendas.get(0);
                                ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findByDataVencimentovenda", Parcelaareceber.class)
                                        .setParameter("parrDataVencimento", new Date(Long.parseLong(auxFiltro[0])))
                                        .setParameter("vendaId", venda).getResultList();
                            }
                        }
                    } else if (Tipo.equalsIgnoreCase("Data")) {
                        ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findByAllData", Parcelaareceber.class)
                                .setParameter("parrDataVencimento", new Date(Long.parseLong(Filtro)))
                                .setParameter("parrDatarecebimento", new Date(Long.parseLong(Filtro))).getResultList();
                    } else if (Tipo.equalsIgnoreCase("PeriodoVencimento")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultParcelaareceber = em.createNamedQuery("venda.findByBetweenVencimento", Parcelaareceber.class)
                                    .setParameter("datainicial", new Date(Long.parseLong(auxFiltro[0])))
                                    .setParameter("datafinal", new Date(Long.parseLong(auxFiltro[1]))).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("Periodorecebimento")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultParcelaareceber = em.createNamedQuery("venda.findByBetweenrecebimento", Parcelaareceber.class)
                                    .setParameter("datainicial", new Date(Long.parseLong(auxFiltro[0])))
                                    .setParameter("datafinal", new Date(Long.parseLong(auxFiltro[1]))).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("Periodo")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultParcelaareceber = em.createNamedQuery("venda.findByAllBetween", Parcelaareceber.class)
                                    .setParameter("datainicial", new Date(Long.parseLong(auxFiltro[0])))
                                    .setParameter("datafinal", new Date(Long.parseLong(auxFiltro[1]))).getResultList();
                        }
                    }
                } catch (Exception ex) {
                    ResultParcelaareceber = em.createNamedQuery("Parcelaareceber.findByFormadepagamento", Parcelaareceber.class)
                            .setParameter("formadepagamentoId", Filtro).getResultList();
                }
            }

            for (Parcelaareceber parcelaarecebers : ResultParcelaareceber) {
                parcelaareceber.add(parcelaarecebers);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return parcelaareceber;
    }

    public Object Salvar(Integer ContaareceberID, Integer Numerodaparcela, Date DataVencimento, Date Datarecebimento, BigDecimal Valor,
            Boolean Situacao, BigDecimal ValorPago, BigDecimal Troco, Object formadepagamento, Object contaareceber) {

        Parcelaareceber parcelaareceber = new Parcelaareceber(ContaareceberID, Numerodaparcela, DataVencimento, Datarecebimento,
                Valor, Situacao, ValorPago, Troco, new Date());

        if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
            parcelaareceber.setFormadepagamentoId((Formadepagamento) formadepagamento);
        }
        if (contaareceber != null && contaareceber instanceof Contaareceber) {
            parcelaareceber.setContaareceber((Contaareceber) contaareceber);
        }

        return super.Salvar(parcelaareceber);
    }

    public Object Alterar(Object aparcelaareceber, Integer ContaareceberID/*...*/, Integer Numerodaparcela, Date DataVencimento, Date Datarecebimento,
            BigDecimal Valor, Boolean Situacao, BigDecimal ValorPago, BigDecimal Troco, Object formadepagamento, Object contaareceber) {
        Parcelaareceber parcelaareceber = null;

        if (aparcelaareceber != null && aparcelaareceber instanceof Parcelaareceber) {

            parcelaareceber = (Parcelaareceber) aparcelaareceber;
            parcelaareceber.setParrDataPagamento(Datarecebimento);
            parcelaareceber.setParrDataVencimento(DataVencimento);
            parcelaareceber.setParrSituacao(Situacao);
            parcelaareceber.setParrTroco(Troco);
            parcelaareceber.setParrValor(Valor);
            parcelaareceber.setParrValorPago(ValorPago);

            if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
                parcelaareceber.setFormadepagamentoId((Formadepagamento) formadepagamento);
            }
            if (contaareceber != null && contaareceber instanceof Parcelaareceber) {
                parcelaareceber.setContaareceber((Contaareceber) contaareceber);
            }
            parcelaareceber = (Parcelaareceber) super.Alterar(parcelaareceber);
        }
        return parcelaareceber;
    }

    public boolean Remover(Object Id) {
        return super.Remover(Id) != null;
    }

    public boolean estornar(Object parcela) {
        boolean flag = true;

        if (parcela != null && parcela instanceof Parcelaareceber) {

            /*Busca Por parcelas com a mesma data de Vencimento*/
            Parcelaareceber conta = (Parcelaareceber) parcela;
            ArrayList<Object> contas = Pesquisar(
                    Long.toString(conta.getParrDataVencimento().getTime())
                    + "#:#" + conta.getContaareceber().getVendaId(), "DataVencimento");
            if (contas != null) {
                EntityManager em = null;

                try {
                    /////Inicia a Transacao
                    em = getEntityManager();
                    em.getTransaction().begin();

                    /*Remove a Movimentação do caixa*/
                    flag = flag && CtrlCaixa.create().RemoveMovimentacao(conta, em) != null;

                    /*Caso só exista uma, esta é estornada ou é a Ultima conta da Lista(Supondo que a Lista vem Ordenada)*/
                    if (contas.size() <= 1 || conta.getParcelaareceberPK().getParrDatadegeracao().getTime()
                            == (((Parcelaareceber) contas.get(contas.size() - 1)).getParcelaareceberPK().getParrDatadegeracao().getTime())) {
                        conta.setParrValorPago(BigDecimal.ZERO);
                        conta.setParrDataPagamento(null);
                        conta.setParrSituacao(false);
                        conta.setMovimentacaoareceberCollection(new ArrayList());
                        /////Altera a conta
                        conta = em.merge(conta);

                    } else {
                        /////Verifica se a Ultima Parcela está Paga
                        Parcelaareceber UltimaConta = (Parcelaareceber) contas.get(contas.size() - 1);
                        if (UltimaConta.getParrSituacao()) {
                            /////Se estiver Paga
                            /////Estorna
                            conta.setParrValorPago(BigDecimal.ZERO);
                            conta.setParrDataPagamento(null);
                            conta.setParrSituacao(false);
                            conta.setMovimentacaoareceberCollection(new ArrayList());
                            /////Troca a Chave Primaria desta parcela com a Ultima
                            ParcelaareceberPK PK = conta.getParcelaareceberPK();
                            conta.setParcelaareceberPK(UltimaConta.getParcelaareceberPK());
                            conta = em.merge(conta);
                            UltimaConta.setParcelaareceberPK(PK);
                            UltimaConta = em.merge(UltimaConta);
                        } else {
                            /////Caso não seja a Ultima da Lista e a Ultima não esteja Paga
                            /////Adiciona o Valor a receber na Ultima Parcela
                            conta.setMovimentacaoareceberCollection(new ArrayList());/////Testar Retirar
                            UltimaConta.setParrValor(UltimaConta.getParrValor().add(conta.getParrValor()));
                            UltimaConta = em.merge(UltimaConta);
                            /////Remove a parcela estornada da conta
                            UltimaConta.getContaareceber().getParcelaareceberCollection().remove(conta);
                            /////Atualiza a Conta
                            UltimaConta.setContaareceber(em.merge(UltimaConta.getContaareceber()));
                            /////Remove a Conta
                            if (!em.contains(conta)) {
                                conta = em.merge(conta);
                            }
                            em.remove(conta);
                        }

                    }
                    if (flag) {
                        em.getTransaction().commit();
                    } else if (em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                    Message = flag ? "Estorno Realizado com Sucesso" : "Erro, o Estorno não foi Realizado";

                } catch (Exception ex) {
                    if (em != null) {
                        Mensagem.ExibirException(ex, ":Ctrlvenda:302");
                        if (em.getTransaction().isActive()) {
                            em.getTransaction().rollback();
                        }
                    }
                    Message = "Erro, o Estorno não foi Realizado";
                } finally {
                    if (em != null) {
                        em.close();
                    }
                }
            }
        } else {
            flag = false;
            Message = "Erro, o Estorno não foi Realizado";
        }
        return flag;
    }

    public boolean receber(Object parcela, String valor, EntityManager em) throws Exception {
        if (em != null) {
            return recebimento(parcela, valor, em);
        } else {
            return receber(parcela, valor);
        }
    }

    public boolean receber(Object parcela, String valor) {
        boolean flag = true;
        if (parcela != null && parcela instanceof Parcelaareceber) {

            EntityManager em = null;

            try {
                /////Inicia a Transacao
                em = getEntityManager();
                em.getTransaction().begin();

                flag = flag && recebimento(parcela, valor, em);

                if (flag) {
                    em.getTransaction().commit();
                } else if (em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
                Message = flag ? "recebimento Realizado com Sucesso" : "Erro, o recebimento não foi Realizado";

            } catch (Exception ex) {
                if (em != null) {
                    Mensagem.ExibirException(ex, ":Ctrlvenda:302");
                    if (em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }
                Message = "Erro, o recebimento não foi Realizado";
            } finally {
                if (em != null) {
                    em.close();
                }
            }

        } else {
            flag = false;
            Message = "Erro, o recebimento não foi Realizado";
        }
        return flag;
    }

    public boolean recebimento(Object parcela, String valor, EntityManager em) throws Exception {
        boolean flag = true;

        Parcelaareceber conta = (Parcelaareceber) parcela;
        BigDecimal ValorPago = new BigDecimal(valor);

        if (ValorPago.compareTo(conta.getParrValor()) <= 0) {//Compara o Valor Pago ao da Parcela.
            /////Altera a Conta
            Parcelaareceber parcelaareceber = conta;
            parcelaareceber.setParrDataPagamento(new Date());
            parcelaareceber.setParrDataVencimento(conta.getParrDataVencimento());
            parcelaareceber.setParrSituacao(true);
            parcelaareceber.setParrTroco(BigDecimal.ZERO/*Sem troco*/);
            parcelaareceber.setParrValorPago(ValorPago);

            BigDecimal Valorareceber = parcelaareceber.getParrValor().subtract(parcelaareceber.getParrValorPago()/*.round(MathContext.DECIMAL64)*/);
            Boolean ValorFaltante = Valorareceber.compareTo(BigDecimal.ZERO) > 0;
            if (ValorFaltante) {
                parcelaareceber.setParrValor(ValorPago);
            } else {
                parcelaareceber.setParrValor(conta.getParrValor());
            }
            /*Não altera a Conta nem a forma de recebimento*/
            /////Realiza a alteração
            parcelaareceber = em.merge(parcelaareceber);

            /*Gerar uma nova Caso haja valor faltante*/
            if (ValorFaltante) {

                try {
                    Contaareceber contaareceber = conta.getContaareceber();
                    /*Caso a parcela não esta totalmente paga é gerada outra com o mesmo vencimento

                            /*Gera a Parcela*/
                    Parcelaareceber ParcelaGerada = new Parcelaareceber(parcelaareceber.getParcelaareceberPK().getContaareceberId(),
                            parcelaareceber.getParcelaareceberPK().getParrNumerodaparcela(),
                            parcelaareceber.getParrDataVencimento(), null, Valorareceber, false,
                            BigDecimal.ZERO, BigDecimal.ZERO/*Sem troco*/, new Date());

                    ParcelaGerada.setFormadepagamentoId(parcelaareceber.getFormadepagamentoId());
                    ParcelaGerada.setContaareceber(contaareceber);
                    em.persist(ParcelaGerada);

                    /////Altera a Conta adicionando a Parcela
                    contaareceber.getParcelaareceberCollection().add(ParcelaGerada);
                    contaareceber = em.merge(contaareceber);
                    em.flush();

                    flag = true;

                } catch (Exception ex) {
                    Mensagem.ExibirException(ex, "Erro ao gerar nova Parcela Com valor Restante CtrlParcelaareceber::312");
                    flag = false;
                }

            } else if (Valorareceber.compareTo(BigDecimal.ZERO) < 0) {
                flag = false;
            }

            /*Gerar Movimentação no Caixa*/
            flag = flag && CtrlCaixa.create().RealizarMovimentacao(parcelaareceber, em) != null;
            flag = flag && parcelaareceber != null;
        }
        return flag;
    }

    protected static void quickSortIterative(List<Parcelaareceber> arr, int l, int h) {
        // Create an auxiliary stack
        int[] stack = new int[h - l + 1];

        // initialize top of stack
        int top = -1;

        // push initial values of l and h to stack
        stack[++top] = l;
        stack[++top] = h;

        // Keep popping from stack while is not empty
        while (top >= 0) {
            // Pop h and l
            h = stack[top--];
            l = stack[top--];

            // Set pivot element at its correct position
            // in sorted array
            int p = partition(arr, l, h);

            // If there are elements on left side of pivot,
            // then push left side to stack
            if (p - 1 > l) {
                stack[++top] = l;
                stack[++top] = p - 1;
            }

            // If there are elements on right side of pivot,
            // then push right side to stack
            if (p + 1 < h) {
                stack[++top] = p + 1;
                stack[++top] = h;
            }
        }
    }

    protected static int partition(List<Parcelaareceber> arr, int low, int high) {
        Parcelaareceber pivot = arr.get(high);

        // index of smaller element
        int i = (low - 1);
        for (int j = low; j <= high - 1; j++) {
            // If current element is smaller than or
            // equal to pivot
            if (arr.get(j).getParrDataVencimento().getTime()
                    <= pivot.getParrDataVencimento().getTime()) {
                i++;

                // swap arr[i] and arr[j]
                Parcelaareceber temp = arr.get(i);
                arr.set(i, arr.get(j));
                arr.set(j, temp);
            }
        }
        // swap arr[i+1] and arr[high] (or pivot)
        Parcelaareceber temp = arr.get(i + 1);
        arr.set(i + 1, arr.get(high));
        arr.set(high, temp);

        return i + 1;
    }

    public void setCampos(Object parcela, TextField txbValorareceber, TextField txbValordaParcela, TextField txbStatus) {
        if (parcela != null && parcela instanceof Parcelaareceber) {
            Parcelaareceber aParcela = (Parcelaareceber) parcela;
            txbValorareceber.setText(aParcela.getParrValor().toString());
            txbValordaParcela.setText(aParcela.getParrValor().toString());
            txbStatus.setText(aParcela.getParrSituacao().toString());
        }
    }

    public Boolean getSituacao(Object parcela) {
        if (parcela != null && parcela instanceof Parcelaareceber) {
            Parcelaareceber aParcela = (Parcelaareceber) parcela;
            return aParcela.getParrSituacao();
        }
        return false;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Parcelaareceber.class);
    }

}
