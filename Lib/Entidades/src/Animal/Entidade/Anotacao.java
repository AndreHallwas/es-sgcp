/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Entidade;

import Utils.Acao.ButtonAcao;
import Utils.Acao.ButtonAcaoAnotacao;
import Variables.Variables;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Aluno
 */
@Entity
@Table(name = "anotacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Anotacao.findAll", query = "SELECT a FROM Anotacao a")
    , @NamedQuery(name = "Anotacao.findByAnotacaoId", query = "SELECT a FROM Anotacao a WHERE a.anotacaoId = :anotacaoId")
    , @NamedQuery(name = "Anotacao.findByAnotacaoDescricao", query = "SELECT a FROM Anotacao a WHERE a.anotacaoDescricao = :anotacaoDescricao")
    , @NamedQuery(name = "Anotacao.findByAnotacaoData", query = "SELECT a FROM Anotacao a WHERE a.anotacaoData = :anotacaoData")
    , @NamedQuery(name = "Anotacao.findByAnotacaoAnimalId", query = "SELECT a FROM Anotacao a WHERE a.animalId.animalId = :animalId")
    , @NamedQuery(name = "Anotacao.findByAnotacaoAll", query = "SELECT a FROM Anotacao a")})
public class Anotacao extends ButtonAcaoAnotacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "anotacao_id")
    private Integer anotacaoId;
    @Basic(optional = false)
    @Column(name = "anotacao_descricao")
    private String anotacaoDescricao;
    @Basic(optional = false)
    @Column(name = "anotacao_data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date anotacaoData;
    @JoinColumn(name = "animal_id", referencedColumnName = "animal_id")
    @ManyToOne(optional = false)
    private Animal animalId;

    public Anotacao() {
    }

    public Anotacao(Integer anotacaoId) {
        this.anotacaoId = anotacaoId;
    }

    public Anotacao(String anotacaoDescricao, Date anotacaoData) {
        this.anotacaoDescricao = anotacaoDescricao;
        this.anotacaoData = anotacaoData;
    }

    public Anotacao(Integer anotacaoId, String anotacaoDescricao, Date anotacaoData) {
        this.anotacaoId = anotacaoId;
        this.anotacaoDescricao = anotacaoDescricao;
        this.anotacaoData = anotacaoData;
    }

    public Integer getAnotacaoId() {
        return anotacaoId;
    }

    public void setAnotacaoId(Integer anotacaoId) {
        this.anotacaoId = anotacaoId;
    }

    public String getAnotacaoDescricao() {
        return anotacaoDescricao;
    }

    public void setAnotacaoDescricao(String anotacaoDescricao) {
        this.anotacaoDescricao = anotacaoDescricao;
    }
    public String getDatadaAnotacao() {
        return Variables.getData(anotacaoData);
    }

    public Date getAnotacaoData() {
        return anotacaoData;
    }

    public void setAnotacaoData(Date anotacaoData) {
        this.anotacaoData = anotacaoData;
    }

    public Animal getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Animal animalId) {
        this.animalId = animalId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (anotacaoId != null ? anotacaoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Anotacao)) {
            return false;
        }
        Anotacao other = (Anotacao) object;
        if ((this.anotacaoId == null && other.anotacaoId != null) || (this.anotacaoId != null && !this.anotacaoId.equals(other.anotacaoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Animal.Entidade.Anotacao[ anotacaoId=" + anotacaoId + " ]";
    }

    @Override
    protected void AddNodes() {
        addNode("Button", "mod", "Modificar");
        addNode("Button", "rem", "Remover");
    }

}
