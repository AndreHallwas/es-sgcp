/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Interface;

import Compra.Controladora.CtrlCompra;
import Compra.Interface.TabelaParcelas.nodeTabela;
import Formadepagamento.Controladora.CtrlFormadepagamento;
import Utils.Controladora.CtrlUtils;
import Utils.DateUtils;
import Utils.FormatString;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaQuitarContasAPagarController implements Initializable, Tela {

    private JFXTextField txbCompra;
    @FXML
    private Label lblStatusConta;
    @FXML
    private JFXTextField txbValorTotal;
    @FXML
    private JFXComboBox<Object> cbFormadePagamento;
    @FXML
    private VBox gpParcelado;
    @FXML
    private JFXTextField txbParcelas;
    @FXML
    private JFXTextField txbValorParcial;
    @FXML
    private DatePicker txbVencimento;
    @FXML
    private VBox pndados;
    @FXML
    private VBox gpEdicao;
    @FXML
    private VBox gpGerarParcelas;
    @FXML
    private JFXTextField txbDiasEntreParcelas;
    @FXML
    private HBox gpModificarCarrinho;
    @FXML
    private JFXButton btnModificar;
    @FXML
    private JFXButton btnConfirmar;
    @FXML
    private HBox gpAlterarCarrinho;
    @FXML
    private VBox gpCarrinho;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNumero;
    @FXML
    private TableColumn<Object, String> tcDataVencimento;
    @FXML
    private TableColumn<Object, String> tcValor;
    @FXML
    private DatePicker txbVencimentoParcela;
    @FXML
    private JFXTextField txbValorParcela;
    @FXML
    private JFXButton btnPagaraVista;
    @FXML
    private JFXButton btnLancarConta;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private HBox gpInicial;
    @FXML
    private HBox gpDadosCarrinho;
    @FXML
    private Label lblErroVencimento;
    @FXML
    private Label lblErroValor;
    @FXML
    private JFXTextField txbValorTotalCarrinho;

    private int flagModificarCarrinho;
    private int flagConfirmarTabela;
    private int flagNivelCancelar;
    private static int flagAlteracao;
    private static TabelaParcelas parcelas;
    private nodeTabela ItemSelecionado;
    private int flagAdicionarCarrinho;
    private static Object Compra;
    private static Double ValorTotal;
    private static Object FormadePagamento;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tcDataVencimento.setCellValueFactory(new PropertyValueFactory("DataVencimento"));
        tcNumero.setCellValueFactory(new PropertyValueFactory("Id"));
        tcValor.setCellValueFactory(new PropertyValueFactory("Valor"));
        MaskFieldUtil.numericField(txbParcelas);
        MaskFieldUtil.numericField(txbDiasEntreParcelas);
        MaskFieldUtil.numericField(txbValorTotal);
        MaskFieldUtil.numericField(txbValorParcela);
        txbParcelas.textProperty().addListener((obs, oldText, newText) -> {
            HandleTextFieldParcelasChanged();
        });
        flagNivelCancelar = 0;
        if (flagAlteracao == 2) {
            CarregaTabelaComTabela();
            EstadoEdicao();
        } else if (flagAlteracao == 1) {
            CarregaTabelaComCompra();
            EstadoEdicao();
        } else {
            estadoOriginal();
        }
        txbValorTotal.setEditable(false);
        lblStatusConta.setText(CtrlCompra.create().VerificaPagamento(Compra).equalsIgnoreCase("Compra Inválida!") ? "Pendente"
                : "Pago");
    }

    private void estadoOriginal() {
        gpParcelado.setDisable(true);
        gpEdicao.setDisable(false);
        gpInicial.setDisable(false);
        btnPagaraVista.setDisable(true);
        btnLancarConta.setDisable(true);
        CtrlUtils.clear(pndados.getChildren());
        estadoOriginalCbParcelas();
        if (flagAlteracao != 2 && parcelas != null) {
            parcelas = null;
        }
        String auxValor = "0";
        if (flagAlteracao == 1) {
            Double valor = CtrlCompra.create().getValorTotal(Compra);
            if (valor != null) {
                auxValor = FormatString.DoubleSimple(valor);
            }
        } else if (flagAlteracao == 0 && ValorTotal != null) {
            auxValor = FormatString.DoubleSimple(ValorTotal);
        }
        txbValorTotal.setText(auxValor);
        CarregaCarrinho();
    }

    private void EstadoEdicao() {
        flagNivelCancelar = 1;
        gpInicial.setDisable(true);
        estadoOriginalCbParcelas();
        estadoOriginalTabeladeParcelas();
    }

    private void HandleButtonPesquisarCompra(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Compra/Interface/TelaConsultaCompra.fxml",
                "Consulta de Compra", null);
        if (TelaConsultaCompraController.getCompra() != null) {
            Compra = TelaConsultaCompraController.getCompra();
            txbCompra.setText(Compra.toString());
            gpEdicao.setDisable(false);
            CtrlCompra.setCampos(Compra, txbValorTotal, null);
        } else {
            estadoOriginal();
        }
    }

    @FXML
    private void HandleButtonLancarConta(MouseEvent event) {
        if (parcelas != null) {
            int Resultado = parcelas.VerificaValor(parcelas.getValorTotal());
            if (Resultado == 0) {
                Mensagem.Exibir("Realizado", 1);
                CtrlUtils.CloseStage(event);
            } else {
                if (Resultado == -1) {
                    Mensagem.Exibir("O Valor das parcelas não deve"
                            + " ser maior que o Valor total", 2);
                } else if (Resultado == 1) {
                    Mensagem.Exibir("O Valor das parcelas não deve"
                            + " ser menor que o Valor total", 2);
                }
            }
        }
    }

    @FXML
    private void HandleButtonRealizarPagamento(MouseEvent event) {
        HandleButtonLancarConta(event);
    }

    @FXML
    private void HandleComboBoxFormadePagamento(Event event) {
        if (cbFormadePagamento.getSelectionModel().getSelectedItem() != null) {
            tabela.getItems().clear();
            Object selecionado = cbFormadePagamento.getSelectionModel().getSelectedItem();
            CtrlFormadepagamento cp = CtrlFormadepagamento.create();
            gpInicial.setDisable(true);
            if (CtrlFormadepagamento.getParcelado(selecionado) != null) {
                FormadePagamento = selecionado;
                if (!CtrlFormadepagamento.getParcelado(selecionado) /*selecionado.equalsIgnoreCase("A Vista")*/) {
                    btnPagaraVista.setDisable(false);
                    btnLancarConta.setDisable(true);

                    if (!txbValorTotal.getText().trim().isEmpty()) {
                        try {
                            Integer Parcelas = 1;
                            Integer DiasEntreParcelas = 1;
                            Date DatadeVencimento = new Date();
                            parcelas = new TabelaParcelas(Double.parseDouble(txbValorTotal.getText()),
                                    FormadePagamento, Parcelas, DatadeVencimento, DiasEntreParcelas);
                            parcelas.GeraParcelasPadrao();
                            CtrlUtils.clear(gpModificarCarrinho.getChildren());
                            flagNivelCancelar = 1;
                            estadoOriginalTabeladeParcelas();
                        } catch (Exception ex) {
                            System.out.println("Erro ao Gerar Parcelas");
                        }
                    } else {
                        Mensagem.Exibir("Campos Inválidos!", 2);
                    }
                } else {
                    gpParcelado.setDisable(false);
                    flagNivelCancelar = 0;
                    gpCarrinho.setDisable(true);
                    gpGerarParcelas.setDisable(false);
                    gpModificarCarrinho.setDisable(true);
                    btnLancarConta.setDisable(false);
                    btnPagaraVista.setDisable(true);
                    txbParcelas.setText("1");
                    txbDiasEntreParcelas.setText("30");
                    txbVencimento.setValue(LocalDate.now());
                }
            }
        }
    }

    private void HandleTextFieldParcelasChanged() {
        try {
            if (!txbParcelas.getText().trim().isEmpty()) {
                Double ValorTotal = Double.parseDouble(txbValorTotal.getText());
                Integer Parcelas = Integer.parseInt(txbParcelas.getText());
                txbValorParcial.setText(FormatString.DoubleSimple(ValorTotal / Parcelas) + "");
            } else {
                txbValorParcial.setText("");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void HandleButtonModificarTabela(MouseEvent event) {
        flagNivelCancelar = 1;
        Object Item = cbFormadePagamento.getSelectionModel().getSelectedItem();
        if (CtrlFormadepagamento.getParcelado(Item) != null) {
            if (flagModificarCarrinho == 0) {
                btnModificar.setText("Salvar");
                gpDadosCarrinho.setDisable(true);
                gpGerarParcelas.setDisable(true);
                if (CtrlFormadepagamento.getParcelado(Item)) {
                    btnLancarConta.setDisable(true);
                } else {
                    btnPagaraVista.setDisable(true);
                }
                flagModificarCarrinho = 1;
                estadoEdicaoTabeladeParcelas();
            } else {
                btnModificar.setText("Modificar");
                if (CtrlFormadepagamento.getParcelado(Item)) {
                    btnLancarConta.setDisable(false);
                } else {
                    btnPagaraVista.setDisable(false);
                }
                flagModificarCarrinho = 0;
                estadoOriginalTabeladeParcelas();
            }
        }
    }

    @FXML
    private void HandleButtonConfirmarTabela(MouseEvent event) {
        if (flagConfirmarTabela == 0) {
            btnConfirmar.setText("Confirmar");
            flagConfirmarTabela = 1;
            flagNivelCancelar = 2;
            flagAdicionarCarrinho = 1;
            gpDadosCarrinho.setDisable(false);
            tabela.setDisable(true);
        } else {
            btnConfirmar.setText("Adicionar");
            flagConfirmarTabela = 0;
            flagNivelCancelar = 1;
            if (flagAdicionarCarrinho == 1) {
                if (txbVencimentoParcela.getValue() != null
                        && !txbValorParcela.getText().trim().isEmpty()) {
                    parcelas.add(DateUtils.asDate(txbVencimentoParcela.getValue()),
                            Double.parseDouble(txbValorParcela.getText()));
                } else {
                    System.out.println("Não Foi Possivel Adicionar a Parcela!");
                }
            } else {
                if (ItemSelecionado != null && txbVencimentoParcela.getValue() != null
                        && !txbValorParcela.getText().trim().isEmpty()) {
                    parcelas.change(ItemSelecionado.getId(),
                            DateUtils.asDate(txbVencimentoParcela.getValue()),
                            Double.parseDouble(txbValorParcela.getText()));
                } else {
                    System.out.println("Não Foi Possivel Alterar a Parcela!");
                }
            }
            if (parcelas != null) {
                txbParcelas.setText(parcelas.getQtdParcelas() + "");
            }
            estadoEdicaoTabeladeParcelas();
        }
    }

    @FXML
    private void HandleButtonAlterarParcelas(MouseEvent event) {
        gpAlterarCarrinho.setDisable(true);
        btnConfirmar.setDisable(false);
        HandleButtonConfirmarTabela(event);
        flagAdicionarCarrinho = 0;
    }

    @FXML
    private void HandleButtonExcluirTabela(MouseEvent event) {
        flagNivelCancelar = 1;
        gpAlterarCarrinho.setDisable(true);
        if (ItemSelecionado != null) {
            parcelas.remove(ItemSelecionado.getId());
        } else {
            System.out.println("Não Foi Possivel Alterar a Parcela!");
        }
        if (parcelas != null && parcelas.getQtdParcelas() > 0) {
            txbParcelas.setText(parcelas.getQtdParcelas() + "");
        }
        estadoEdicaoTabeladeParcelas();
    }

    @FXML
    private void HandleButtonCancelar(MouseEvent event) {
        if (flagNivelCancelar == 0) {
            flagNivelCancelar = -1;
            if (flagAlteracao == 2) {
                estadoOriginal();
                if (parcelas != null) {
                    txbValorTotal.setText(parcelas.getValorTotal() + "");
                    ValorTotal = parcelas.getValorTotal();
                    parcelas = null;
                    if (Compra != null) {
                        flagAlteracao = 1;
                    } else {
                        flagAlteracao = 0;
                    }
                    System.out.println("Ao Continuar voce irá Resetar o Pagamento");
                }
            } else {
                estadoOriginal();
            }
        } else if (flagNivelCancelar == 1) {
            estadoOriginalTabeladeParcelas();
            flagNivelCancelar = 0;
        } else if (flagNivelCancelar == 2) {
            estadoEdicaoTabeladeParcelas();
            flagNivelCancelar = 1;
        } else if (flagNivelCancelar == -1) {
            CtrlUtils.CloseStage(event);
        } else {
            estadoOriginal();
        }
    }

    @FXML
    private void HandleButtonGerar(MouseEvent event) {
        if (!txbValorTotal.getText().trim().isEmpty()
                && !txbParcelas.getText().trim().isEmpty()
                && txbVencimento.getValue() != null
                && !txbDiasEntreParcelas.getText().trim().isEmpty()) {
            try {
                parcelas = new TabelaParcelas(Double.parseDouble(txbValorTotal.getText()), FormadePagamento,
                        Integer.parseInt(txbParcelas.getText()), DateUtils.asDate(txbVencimento.getValue()),
                        Integer.parseInt(txbDiasEntreParcelas.getText()));
                parcelas.GeraParcelasPadrao();
                CtrlUtils.clear(gpModificarCarrinho.getChildren());
                flagNivelCancelar = 1;
                estadoOriginalTabeladeParcelas();
            } catch (Exception ex) {
                System.out.println("Erro ao Gerar Parcelas");
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void clicknatabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            nodeTabela f = (nodeTabela) tabela.getItems().get(lin);
            txbValorParcela.setText(f.getValor() + "");
            txbVencimentoParcela.setValue(DateUtils.asLocalDate(f.getData()));
            flagNivelCancelar = 2;
            gpAlterarCarrinho.setDisable(false);
            btnModificar.setDisable(true);
            btnConfirmar.setDisable(true);
            ItemSelecionado = f;
        }
    }

    private void estadoOriginalTabeladeParcelas() {
        gpModificarCarrinho.setDisable(false);
        btnModificar.setDisable(false);
        gpAlterarCarrinho.setDisable(true);
        gpDadosCarrinho.setDisable(true);
        gpCarrinho.setDisable(true);
        btnConfirmar.setDisable(true);
        btnModificar.setText("Modificar");
        flagModificarCarrinho = 0;
        gpGerarParcelas.setDisable(false);
        Object Item = cbFormadePagamento.getSelectionModel().getSelectedItem();
        if (CtrlFormadepagamento.getParcelado(Item) != null) {
            if (CtrlFormadepagamento.getParcelado(Item)) {
                btnLancarConta.setDisable(false);
            } else {
                btnPagaraVista.setDisable(false);
            }
        }
        CarregaCarrinho();
    }

    private void estadoEdicaoTabeladeParcelas() {
        gpAlterarCarrinho.setDisable(true);
        btnModificar.setDisable(false);
        gpCarrinho.setDisable(false);
        gpDadosCarrinho.setDisable(true);
        btnConfirmar.setDisable(false);
        btnConfirmar.setText("Adicionar");
        flagConfirmarTabela = 0;
        tabela.setDisable(false);
        CarregaCarrinho();
    }

    public void CarregaCarrinho() {
        try {
            if (parcelas != null) {
                tabela.getItems().clear();
                tabela.setItems(FXCollections.observableList(parcelas.getObject()));
                txbValorTotalCarrinho.setText(parcelas.geraValorTotal().toString());
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tabela");
        }
    }

    public static TabelaParcelas getParcelas() {
        return parcelas;
    }

    public static void setCompra(Object Compra) {
        flagAlteracao = 1;
        TelaQuitarContasAPagarController.Compra = Compra;
    }

    public static void setValorTotal(Double ValorTotal) {
        flagAlteracao = 0;
        TelaQuitarContasAPagarController.ValorTotal = ValorTotal;
    }

    public static void setParcelas(TabelaParcelas Parcelas) {
        flagAlteracao = 2;
        TelaQuitarContasAPagarController.parcelas = Parcelas;
    }

    private void CarregaTabelaComTabela() {
        if (parcelas != null) {
            txbValorTotal.setText(parcelas.getValorTotal() + "");
            cbFormadePagamento.getSelectionModel().select(parcelas.getFormadePagamento());
            HandleComboBoxFormadePagamento(null);
            txbParcelas.setText(parcelas.getQtdParcelas() + "");
            txbVencimento.setValue(DateUtils.asLocalDate(parcelas.getVencimentoInicial()));
            txbDiasEntreParcelas.setText(parcelas.getDiasEntreParcelas() + "");
        }
    }

    private void CarregaTabelaComCompra() {
        if (Compra != null) {
            CtrlCompra.create().setCampos(Compra, txbValorTotal,
                    cbFormadePagamento, txbParcelas, txbVencimento,
                    txbDiasEntreParcelas);
            if (txbVencimento.getValue() != null && !txbDiasEntreParcelas.getText().trim().isEmpty()
                    && !txbParcelas.getText().trim().isEmpty()) {
                HandleComboBoxFormadePagamento(null);
                parcelas = new TabelaParcelas(Double.parseDouble(txbValorTotal.getText()), FormadePagamento,
                        Integer.parseInt(txbParcelas.getText()), DateUtils.asDate(txbVencimento.getValue()),
                        Integer.parseInt(txbDiasEntreParcelas.getText()));
                ArrayList<Object> Valor = CtrlCompra.create().getValorParcelas(Compra);
                ArrayList<Object> Data = CtrlCompra.create().getDataParcelas(Compra);
                if (Valor != null && Data != null) {
                    for (int i = 0; i < Data.size(); i++) {
                        parcelas.add((Date) Data.get(i), (Double) Valor.get(i));
                    }
                }
            }
        }
    }

    private void estadoOriginalCbParcelas() {
        cbFormadePagamento.getItems().clear();
        List<Object> FormasdePagamento = CtrlFormadepagamento.create().Pesquisar("");
        if (FormasdePagamento != null && !FormasdePagamento.isEmpty()) {
            cbFormadePagamento.getItems().addAll(FormasdePagamento);
            /*cbFormadePagamento.getSelectionModel().selectFirst();*/
        }
    }

}
