/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fornecedor.Interface;

import Endereco.Interface.TelaEnderecoController;
import Fornecedor.Controladora.CtrlFornecedor;
import Fornecedor.Entidade.Fornecedor;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Imagem.TelaImagemFXMLController;
import Utils.Controladora.CtrlUtils;
import Variables.Variables;
import Utils.UI.Tela.IniciarTela;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadFornecedorController implements Initializable {

    @FXML
    private HBox pndados;
    @FXML
    private TextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private TextField txbCNPJ;
    @FXML
    private Label lbErroCNPJ;
    @FXML
    private TextField txbTelefone;
    @FXML
    private Label lbErroTelefone;
    @FXML
    private TextField txbEmail;
    @FXML
    private Label lbErroEMail;
    @FXML
    private TextArea taObs;
    @FXML
    private Label lbErroObs;
    @FXML
    private Button btnovo;
    @FXML
    private Button btalterar;
    @FXML
    private Button btapagar;
    @FXML
    private Button btconfirmar;
    @FXML
    private Button btcancelar;
    @FXML
    private VBox gpImagem;
    @FXML
    private VBox gpEndereco;
    @FXML
    private JFXButton btConsulta;
    
    private char flag;
    private boolean EOriginal;
    private Integer ID;
    private Object Instancia;
    private static TelaImagemFXMLController Imagem;
    private static TelaEnderecoController Endereco;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Imagem = TelaImagemFXMLController.Create(this.getClass(), gpImagem);
        Endereco = TelaEnderecoController.Create(this.getClass(), gpEndereco);

        estadoOriginal();

        MaskFieldUtil.cnpjField(txbCNPJ);
        MaskFieldUtil.foneField(txbTelefone);

        /////CtrlAcesso.add(gpTabela, "Tabela de Fornecedor", 13, 0);
    }

    private void setAllErro(boolean Ecnpj, boolean Eemail, boolean Etelefone, boolean Enome, boolean Eobs) {
        lbErroCNPJ.setVisible(Ecnpj);
        lbErroEMail.setVisible(Eemail);
        lbErroTelefone.setVisible(Etelefone);
        lbErroNome.setVisible(Enome);
        lbErroObs.setVisible(Eobs);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    @FXML
    private void evtNovo(Event event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        txbCNPJ.setDisable(false);
        flag = 1;
    }

    @FXML
    private void evtAlterar(Event event) {
        EstadoEdicao();
        txbCNPJ.setDisable(true);
        flag = 0;
    }

    @FXML
    private void evtApagar(Event event) {
        CtrlFornecedor cf = CtrlFornecedor.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cf.Remover(ID);
        }
        estadoOriginal();
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlFornecedor cf = CtrlFornecedor.create();
        ArrayList<Object> Resultado = cf.Pesquisar(MaskFieldUtil.onlyAlfaNumericValue(txbCNPJ));
        if (txbCNPJ.getText().trim().isEmpty() || !Resultado.isEmpty()) {
            if (txbCNPJ.getText().trim().isEmpty()) {
                setErro(lbErroCNPJ, "Campo Obrigatório Vazio");
                fl = false;
                txbCNPJ.setText("");
                txbCNPJ.requestFocus();
            } else if (Resultado.isEmpty()) {
                if (flag == 0) {
                    setErro(lbErroCNPJ, "CNPJ Não Existente");
                    fl = false;
                    txbCNPJ.setText("");
                    txbCNPJ.requestFocus();
                }
            } else if (flag == 1) {
                setErro(lbErroCNPJ, "CNPJ Ja Existe");
                fl = false;
                txbCNPJ.setText("");
                txbCNPJ.requestFocus();
            }
        }
        if (txbNome.getText().trim().isEmpty()) {
            setErro(lbErroNome, "Campo Obrigatório Vazio");
            txbNome.setText("");
            txbNome.requestFocus();
            fl = false;
        }
        if (!Endereco.valida()) {
            fl = false;
        }
        return fl;
    }

    @FXML
    private void evtConfirmar(Event event) {
        setAllErro(false, false, false, false, false);
        CtrlFornecedor cf = CtrlFornecedor.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cf.Alterar(Instancia, ID, MaskFieldUtil.onlyAlfaNumericValue(txbCNPJ),
                        txbNome.getText(), txbTelefone.getText(),
                        txbEmail.getText(), taObs.getText(),
                        Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()), Endereco.getComplementoEndereco())) == null) {
                    Mensagem.Exibir(cf.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cf.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cf.Salvar(MaskFieldUtil.onlyAlfaNumericValue(txbCNPJ),
                    txbNome.getText(), txbTelefone.getText(),
                    txbEmail.getText(), taObs.getText(),
                    Utils.Imagem.BufferedImageToByteArray(Imagem.getImagem()), Endereco.getComplementoEndereco())) == null) {
                Mensagem.Exibir(cf.getMsg(), 2);
            } else {
                Mensagem.Exibir(cf.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(Event event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false, false, false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false, false, false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);
        Imagem.estadoOriginal();
        Endereco.estadoOriginal();

        clear(pndados.getChildren());
    }

    private void setCampos(Object f) {
        if (f != null) {
            CtrlFornecedor.setCampos(f, txbNome, txbCNPJ, txbEmail, txbTelefone, taObs);
            ID = CtrlFornecedor.getId(f);
            Imagem.setImagem(CtrlFornecedor.getImagem(f));
            Endereco.setComplementoEndereco(CtrlFornecedor.getEndereco(f));
        }
    }

    @FXML
    private void evtConsultaAvancada(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Fornecedor/Interface/TelaConsultaFornecedor.fxml",
                "Consulta de Fornecedor", null);
        if (TelaConsultaFornecedorController.getFornecedor() != null) {
            EstadoConsulta(TelaConsultaFornecedorController.getFornecedor());
        }
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }

}
