/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Sessao;

import Acesso.Controladora.AcessoComponente;
import Acesso.Controladora.CtrlGrupodeacesso;
import Acesso.Entidade.Individuo;
import Pessoa.Controladora.CtrlUsuario;
import Pessoa.Entidade.Usuario;
import Transacao.Transaction;
import Utils.Imagem;
import Utils.Mensagem;
import Variables.Variables;
import java.awt.image.BufferedImage;
import java.util.List;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javax.persistence.Query;

/**
 *
 * @author Raizen
 */
public class Sessao {

    private static Individuo individuo;

    public static void GeraPainelLogin() {
        if (getIndividuo() != null) {
            GeraPainelLogin(Variables._txNivelInterface, Variables._txUsuarioInterface, Variables._imgLogoUsuario, getIndividuo());
        }
    }

    private static void GeraPainelLogin(Label Nivel, Label Nome, Object Image, Object Usuario) {
        if (Nome != null) {
            Nome.setText(CtrlUsuario.getLogin(Usuario));
        }
        if (Nivel != null) {
            Nivel.setText(CtrlGrupodeacesso.getNome(CtrlUsuario.getGrupo(Usuario)));
        }
        if(Image != null){
            ((ImageView) Image).setImage(Imagem.BufferedImageToImage(CtrlUsuario.getImagem(Usuario)));
        }
        /*setClip(Image, Usuario);*/
    }
    
    private static void setClip(Object Image, Object Usuario) {
        /*Circle Image*/
        if (Image != null) {
            Circle circle = null;
            if (Image instanceof Circle) {
                circle = (Circle) Image;
            } else {
                circle = new Circle(20, 20, 20);
            }
            circle.setStroke(Color.SEAGREEN);
            circle.setEffect(new DropShadow(+15d, 0d, +2d, Color.DARKSEAGREEN));
            Image image = null;
            BufferedImage Foto = CtrlUsuario.getImagem(Usuario);
            if (Foto != null) {
                image = Imagem.BufferedImageToImage(Foto);
                circle.setFill(new ImagePattern(image));
            }
            if (Image instanceof ImageView && image != null) {
                ((ImageView) Image).setImage(image);
                ((ImageView) Image).setClip(circle);
            }
        }
    }

    private Sessao() {
    }

    public static boolean CriarSessao(String Login, String Senha) {
        boolean Resultado = false;

        try {
            StringBuilder sql = new StringBuilder("SELECT valida_senha(?, ?)");
            Query query = Transaction.getEntityManager().createNativeQuery(sql.toString());
            query.setParameter(1, Login);
            query.setParameter(2, Senha);
            List<?> result = query.getResultList();

            if (result != null && !result.isEmpty() && result.get(0) instanceof Boolean) {
                Resultado = (Boolean) result.get(0);
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro no Login");
        }

        if (Resultado) {
            CtrlUsuario cp = CtrlUsuario.create();
            Usuario User = (Usuario) cp.Pesquisar(Login).get(0);
            individuo = User;
            /**
             * GeraPermissao();
             */
            GeraPainelLogin();
            return true;
        }
        return false;
    }

    public static void GeraPermissao() {
        ((AcessoComponente) Variables._Acc).GeraPermissao(individuo);
        GeraPainelLogin();
    }

    protected static boolean AutenticarSessao(String... Params) {
        return true;
    }

    public static boolean FinalizarSessao() {
        individuo = null;
        return true;
    }

    /**
     * @return the individuo
     */
    public static Individuo getIndividuo() {
        return individuo;
    }

    /**
     * @param aIndividuo the individuo to set
     */
    public static void setIndividuo(Individuo aIndividuo) {
        individuo = aIndividuo;
    }

}
