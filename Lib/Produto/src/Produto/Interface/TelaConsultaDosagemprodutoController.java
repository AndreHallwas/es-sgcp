/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Produto.Controladora.CtrlDosagemproduto;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaDosagemprodutoController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private HBox btnGp;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcId;
    @FXML
    private TableColumn<Object, String> tcDescricao;
    @FXML
    private TableColumn<Object, String> tcIntervalo;
    @FXML
    private TableColumn<Object, String> tcQuantidade;

    private static Object dosagemproduto;
    private static boolean btnPainel = true;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcId.setCellValueFactory(new PropertyValueFactory("DosagemprodutoId"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("DosagemDesc"));
        tcIntervalo.setCellValueFactory(new PropertyValueFactory("DosagemIntervalo"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory("DosagemQuantidade"));
        
        CarregaTabela("");
        
        dosagemproduto = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlDosagemproduto ctm = CtrlDosagemproduto.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            dosagemproduto = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (dosagemproduto != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhuma Dosagem de Produto Selecionada", 2);
        }
    }

    public static TelaConsultaDosagemprodutoController Create(Class classe, Pane pndados) {
        TelaConsultaDosagemprodutoController CDosagemproduto = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Produto/Interface/TelaConsultaDosagemproduto.fxml"));
            Parent root = Loader.load();
            CDosagemproduto = (TelaConsultaDosagemprodutoController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CDosagemproduto;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaDosagemprodutoController.btnPainel = btnPainel;
    }

    public static Object getDosagemproduto() {
        return dosagemproduto;
    }
}
