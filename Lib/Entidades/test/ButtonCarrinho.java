/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.Carrinho;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 *
 * @author Raizen
 */
public abstract class ButtonCarrinho {

    /**
     * Para usar é necessário herdar na classe do carrinho
     * E Utilizar a InterfacefxmlCarrinho na tela fxml
     */
    protected static InterfaceFxmlCarrinho TelaCarrinho;
    ///protected static EventHandler addi;
    protected static EventHandler mod;
    protected static EventHandler rem;

    protected HBox Crud;
    ///protected Button Add;
    protected Button Modify;
    protected Button Remove;

    protected ButtonCarrinho() {
        Inicializa();
    }

    protected void Inicializa() {
        ///setAdd(new Button("+"));
        setModify(new Button("Editar"));
        setRemove(new Button("Remover"));

        InicializaHandler();

        ///InicializaEventoMouse(getAdd(), addi);
        InicializaEventoMouse(getModify(), mod);
        InicializaEventoMouse(getRemove(), rem);

        setCrud(new HBox(8));
        ///getCrud().getChildren().add(Add);
        getCrud().getChildren().add(Modify);
        getCrud().getChildren().add(Remove);
    }

    protected void InicializaEventoMouse(Button Botao, EventHandler eventHandler) {
        Botao.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);
    }

    protected void InicializaHandler() {
        ///addi = MouseEvent -> TelaCarrinho.SelectEventCarrinho(getReference(), "add");
        mod = MouseEvent -> TelaCarrinho.SelectEventCarrinho(getReference(), "mod");
        rem = MouseEvent -> TelaCarrinho.SelectEventCarrinho(getReference(), "rem");
    }

    protected abstract Object getReference();

    /**
     * @return the Add
     */
    /*public Button getAdd() {
        return Add;
    }*/

    /**
     * @param Add the Add to set
     */
    /*public void setAdd(Button Add) {
        this.Add = Add;
    }*/

    /**
     * @return the Modify
     */
    public Button getModify() {
        return Modify;
    }

    /**
     * @param Modify the Modify to set
     */
    public void setModify(Button Modify) {
        this.Modify = Modify;
    }

    /**
     * @return the Remove
     */
    public Button getRemove() {
        return Remove;
    }

    /**
     * @param Remove the Remove to set
     */
    public void setRemove(Button Remove) {
        this.Remove = Remove;
    }

    /**
     * @return the Crud
     */
    public HBox getCrud() {
        return Crud;
    }

    /**
     * @param Crud the Crud to set
     */
    public void setCrud(HBox Crud) {
        this.Crud = Crud;
    }

    /**
     * @param aTelaCarrinho the TelaCarrinho to set
     */
    public static void setTelaCarrinho(InterfaceFxmlCarrinho aTelaCarrinho) {
        TelaCarrinho = aTelaCarrinho;
    }
    
    /**
     * Add.addEventHandler(MouseEvent.MOUSE_CLICKED, new
     * EventHandler<MouseEvent>() {
     *
     * @Override public void handle(MouseEvent event) {
     * FXMLDocumentController.mouse(firstName); } });
     */
}
