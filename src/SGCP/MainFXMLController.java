/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP;

import Acesso.Controladora.AcessoComponente;
import Acesso.Sessao.Sessao;
import Transacao.Transaction;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Variables.Variables;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class MainFXMLController implements Initializable {

    @FXML
    private HBox pndados;
    @FXML
    private MenuBar Menu;
    @FXML
    private Menu menuLogin;
    @FXML
    private BorderPane Tela;
    @FXML
    private MenuItem menuItemLogin;
    @FXML
    private VBox tabMenuAccordion;
    @FXML
    private Menu menuGerenciamento;
    @FXML
    private MenuItem miAnimal;
    @FXML
    private MenuItem miCliente;
    @FXML
    private MenuItem miUsuario;
    @FXML
    private MenuItem miFornecedor;
    @FXML
    private MenuItem miProduto;
    @FXML
    private MenuItem miServico;
    @FXML
    private MenuItem miTipodeDespesa;
    @FXML
    private Menu menuRelatorios;
    @FXML
    private Menu menuCaixa;
    @FXML
    private MenuItem miAbrirCaixa;
    @FXML
    private MenuItem miFecharCaixa;
    @FXML
    private Menu menuVenda;
    @FXML
    private MenuItem miRealizarVenda;
    @FXML
    private Menu menuCompra;
    @FXML
    private MenuItem miCompras;
    private Menu menuRecebimento;
    private MenuItem miRecebimento;
    @FXML
    private Menu menuFuncoes;
    @FXML
    private MenuItem miDespesa;
    @FXML
    private MenuItem miAtendimento;
    @FXML
    private MenuItem miAgendamento;
    @FXML
    private Menu menuFerramentas;
    @FXML
    private MenuItem miRestore;
    @FXML
    private Menu menuAjuda;
    @FXML
    private MenuItem miNivel;
    @FXML
    private Label lblData;
    @FXML
    private Label lblRelogio;
    @FXML
    private MenuItem miCadernetadeVacinacao;
    @FXML
    private Label lblStatusCaixa;
    @FXML
    private Label lblCaixa;
    @FXML
    private MenuItem miFormadepagamento;
    @FXML
    private ImageView imgLoginUsuario;
    @FXML
    private Label txLoginUsuario;
    @FXML
    private Menu menuLogin1;
    @FXML
    private MenuItem menuItemLogin1;
    @FXML
    private MenuItem miBackupRestore;
    @FXML
    private Menu menuRelatorios1;
    

    private SimpleDateFormat formatador;
    public static Stage _login;
    private int login;
    @FXML
    private MenuItem menuItemErrorReport;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
        try {
            imgLogo.setImage(Imagem.BufferedImageToImage(CtrlEmpresa.getImagem(CtrlEmpresa.create().Pesquisar())));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro na Imagem da Empresa no inicialize do Main");
        }*/

        Variables._pndados = pndados;
        Variables._lblStatusCaixa = lblStatusCaixa;
        Variables._lblCaixa = lblCaixa;
        Variables._imgLogoUsuario = imgLoginUsuario;
        Variables._txUsuarioInterface = txLoginUsuario;
        
        login = 1;

        formatador = new SimpleDateFormat("HH:mm:ss a");
        IniciaRelogio();

        ///Criar Accordion
        /*IniciarTela.Create(this.getClass(), tabMenuAccordion, "/SGCP/Main/TelaMenuAccordion.fxml");*/
        CarregaComponentesMenuBar();

        Sessao.GeraPermissao();

        /////Inicializa Atalhos de Teclado.
        Platform.runLater(() -> {
            Variables._Scene.addEventHandler(KeyEvent.KEY_PRESSED, (event) -> AtalhosdeTeclado(event));
        });
        
        Transaction.getBackup().start();
    }

    private void CarregaComponentesMenuBar() {
        AcessoComponente ac = (AcessoComponente) Variables._Acc;
        ac.AdicionaObjeto(menuGerenciamento, "Menu de Gerenciamento");
        ac.AdicionaObjeto(miUsuario, "Opção de Usuario");
        ac.AdicionaObjeto(miNivel, "Opção de Nivel");
        ac.AdicionaObjeto(miCliente, "Opção de Cliente");
        ac.AdicionaObjeto(miFornecedor, "Opção de Fornecedor");
        ac.AdicionaObjeto(miProduto, "Opção de Produto");
        ac.AdicionaObjeto(miCliente, "Opção de Serviço");
        ac.AdicionaObjeto(miFornecedor, "Opção de Tipo de Despesa");
        ac.AdicionaObjeto(miProduto, "Opção de Forma de Pagamento");
        ac.AdicionaObjeto(miFornecedor, "Opção de Empresa");
        ac.AdicionaObjeto(miProduto, "Opção de Animal");
        ac.AdicionaObjeto(menuCaixa, "Menu de Abrir/ Fechar Caixa");
        ac.AdicionaObjeto(miAbrirCaixa, "Opção de Abrir Caixa");
        ac.AdicionaObjeto(miFecharCaixa, "Opção de Fechar");
        ac.AdicionaObjeto(menuVenda, "Menu de Realizar Venda");
        ac.AdicionaObjeto(miRealizarVenda, "Opção de Realizar Venda");
        ac.AdicionaObjeto(menuCompra, "Menu de Compra");
        ac.AdicionaObjeto(miCompras, "Opção de Compra");
        ac.AdicionaObjeto(menuRecebimento, "Menu de Recebimento");
        ac.AdicionaObjeto(miRecebimento, "Opção de Recebimento");
        ac.AdicionaObjeto(menuFuncoes, "Menu de Funções");
        ac.AdicionaObjeto(miDespesa, "Opção de Registrar Despesa");
        ac.AdicionaObjeto(miAtendimento, "Opção de Atendimento");
        ac.AdicionaObjeto(miAgendamento, "Opção de Agendamento");
        ac.AdicionaObjeto(miCadernetadeVacinacao, "Opção de Caderneta de Vacinação");
        ac.AdicionaObjeto(menuFerramentas, "Menu de Ferramentas");
        ac.AdicionaObjeto(menuRelatorios, "Menu de Relatórios");
        
        ac.Default();
    }

    private void evtDeslogar(Event event) {
        txLoginUsuario.setText("Nível");
        imgLoginUsuario.setImage(null);
        
        Variables._pndados.getChildren().clear();
        ((AcessoComponente) Variables._Acc).Default();
    }

    private void evtBackup() {
        MainMenuOptions.evtBackup();
    }

    private void evtRestore() {
        MainMenuOptions.evtRestore();
    }

    @FXML
    private void evtConfiguracao(Event event) {
        MainMenuOptions.evtConfiguracao(event, this.getClass());
    }

    @FXML
    private void evtConfiguracaoInicial(Event event) {
        MainMenuOptions.evtConfiguracaoInicial(event, this.getClass());
    }

    @FXML
    private void evtClickLogin(Event event) {
        AcessoComponente ac = (AcessoComponente) Variables._Acc;
        Variables._pndados.getChildren().clear();
        ac.Default();
        Variables._Stage.hide();

        boolean Login = TelaLoginFXMLController.Create(this.getClass());
        if (Login) {
            Sessao.GeraPermissao();
            Variables._Stage.show();
            boolean Iniciado = true;
            if (Iniciado) {
                Mensagem.ExibirLog("Sessão Iniciada!");
            }
        } else {
            evtSair(event);
        }

    }

    @FXML
    private void evtGerenciamentoEmpresa(Event event) {
        MainMenuOptions.evtGerenciamentoEmpresa(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoCliente(Event event) {
        MainMenuOptions.evtGerenciamentoCliente(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoUsuario(Event event) {
        MainMenuOptions.evtGerenciamentoUsuario(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoNivel(Event event) {
        MainMenuOptions.evtGerenciamentoNivel(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoProduto(Event event) {
        MainMenuOptions.evtGerenciamentoProduto(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoFornecedor(Event event) {
        MainMenuOptions.evtGerenciamentoFornecedor(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoServico(Event event) {
        MainMenuOptions.evtGerenciamentoServico(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoAnimal(Event event) {
        MainMenuOptions.evtGerenciamentoAnimal(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoTipodeDespesa(Event event) {
        MainMenuOptions.evtGerenciamentoTipodeDespesa(event, this.getClass());
    }

    @FXML
    private void evtGerenciamentoFormadepagamento(Event event) {
        MainMenuOptions.evtGerenciamentoFormadepagamento(event, this.getClass());
    }

    private void evtConsultaUsuario(Event event) {
        MainMenuOptions.evtConsultaUsuario(event, this.getClass());
    }

    private void evtConsultaCliente(Event event) {
        MainMenuOptions.evtConsultaCliente(event, this.getClass());
    }

    private void evtConsultaFornecedor(Event event) {
        MainMenuOptions.evtConsultaFornecedor(event, this.getClass());
    }

    private void evtConsultaFabricante(Event event) {
        MainMenuOptions.evtConsultaFabricante(event, this.getClass());
    }

    private void evtConsultaProduto(Event event) {
        MainMenuOptions.evtConsultaProduto(event, this.getClass());
    }

    private void evtRelFichadoUsuario(Event event) {
        MainMenuOptions.evtRelFichadoUsuario(event, this.getClass());
    }

    @FXML
    private void evtVenda(Event event) {
        MainMenuOptions.evtVenda(event, this.getClass());
    }

    @FXML
    private void evtCompra(Event event) {
        MainMenuOptions.evtCompra(event, this.getClass());
    }

    private void evtPagamento(Event event) {
        MainMenuOptions.evtPagamento(event, this.getClass());
    }

    private void evtRecebimento(Event event) {
        MainMenuOptions.evtRecebimento(event, this.getClass());
    }

    @FXML
    private void evtAgendamento(Event event) {
        MainMenuOptions.evtAgendamento(event, this.getClass());
    }

    @FXML
    private void evtAbrirCaixa(Event event) {
        MainMenuOptions.evtAbrirCaixa(event, this.getClass());
    }

    @FXML
    private void evtFecharCaixa(Event event) {
        MainMenuOptions.evtFecharCaixa(event, this.getClass());
    }

    @FXML
    private void evtAtendimento(Event event) {
        MainMenuOptions.evtAtendimento(event, this.getClass());
    }

    @FXML
    private void evtCadernetadeVacinacao(Event event) {
        MainMenuOptions.evtCadernetadeVacinacao(event, this.getClass());
    }

    @FXML
    private void evtAjuda(Event event) {
        MainMenuOptions.evtAjuda(event, this.getClass());
    }

    @FXML
    private void evtAtestadoSaúde(Event event) {
        MainMenuOptions.evtAtestadoSaude(event, this.getClass());
    }

    @FXML
    private void evtRelInternação(Event event) {
        MainMenuOptions.evtRelInternação(event, this.getClass());
    }

    @FXML
    private void evtRelCirurgico(Event event) {
        MainMenuOptions.evtRelCirurgico(event, this.getClass());
    }

    @FXML
    private void evtRelSacrificacao(Event event) {
        MainMenuOptions.evtRelSacrificacao(event, this.getClass());
    }

    @FXML
    private void evtRelMovimentacao(Event event) {
        MainMenuOptions.evtRelMovimentacao(event, this.getClass());
    }

    @FXML
    private void evtRelFluxoDoCaixa(Event event) {
        MainMenuOptions.evtRelFluxoDoCaixa(event, this.getClass());
    }

    @FXML
    private void evtRelVacinacao(Event event) {
        MainMenuOptions.evtRelVacinacao(event, this.getClass());
    }

    @FXML
    private void evtRelVendas(Event event) {
        MainMenuOptions.evtRelVendas(event, this.getClass());
    }
    
    @FXML
    private void evtRelAtendimentos(Event event) {
        MainMenuOptions.evtRelAtendimentos(event, this.getClass());
    }

    @FXML
    private void evtRelAgendamentos(Event event) {
        MainMenuOptions.evtRelAgendamentos(event, this.getClass());
    }

    @FXML
    private void evtBackupERestore(Event event) {
        MainMenuOptions.evtBackupERestore(event, this.getClass());
    }
    
    @FXML
    private void evtSair(Event event) {
        if (login == 1) {
            evtDeslogar(event);
        }
        Transaction.getBackup().shutdown();
        System.exit(1);
        Mensagem.ExibirLog("Programa Finalizado!");
    }

    private void AtalhosdeTeclado(KeyEvent keyEvent) {
        KeyCode Key = keyEvent.getCode();
        if (Sessao.getIndividuo() != null) {
            /////Se estiver Logado.
            if (pndados.getChildren().isEmpty() && null != Key) {
                switch (Key) {
                    /////Tela Main.
                    case ESCAPE:
                        evtSair(keyEvent);
                        break;
                    case F1:
                        evtAjuda(keyEvent);
                        break;
                    case F2:
                        evtClickLogin(keyEvent);
                        break;
                    case F3:
                        evtBackupERestore(keyEvent);
                        break;
                    default:
                        break;
                }
            } else {
                switch (Key) {
                    /////Tela Main.
                    case ESCAPE:
                        pndados.getChildren().clear();
                        break;
                }
            }
        }
    }

    public static boolean Create(Class Classe) {
        Parent root = null;
        try {
            root = FXMLLoader.load(Classe.getResource("/SGCP/MainFXML.fxml"));
            Variables._Stage.setWidth(1024);
            Variables._Stage.setHeight(702);
            Variables._Stage.setTitle("Sistema de Gerenciamento de Clinica Veterinária e Pétshop - SGAE");
            Scene scene = IniciarTela.newScene(root);
            Variables._Scene = scene;
            /*Variables._Stage.setResizable(false);*/
            Variables._Stage.setScene(scene);
            Variables._Stage.show();
            return true;
        } catch (IOException ex) {
            Mensagem.Exibir(ex.getMessage(), 2);
        }
        return false;
    }

    @FXML
    private void evtRegistrarDespesa(Event event) {
        MainMenuOptions.evtRegistrarDespesa(event, this.getClass());
    }

    private void IniciaRelogio() {
        KeyFrame frame = new KeyFrame(Duration.millis(1000), e -> AtualizaRelogio());
        Timeline timeline = new Timeline(frame);
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();

        lblData.setText(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
    }

    private void AtualizaRelogio() {
        Date agora = new Date();
        lblRelogio.setText(formatador.format(agora));
    }

    @FXML
    private void evtFichadoAnimal(Event event) {
        MainMenuOptions.evtFichadoAnimal(event, this.getClass());
    }

    @FXML
    private void evtReportError(ActionEvent event) { 
        MainMenuOptions.evtReportErro(event, this.getClass());
    }

}
