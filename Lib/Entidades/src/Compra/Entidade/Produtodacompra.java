/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Entidade;

import JPA.Dao.JPADao;
import Produto.Entidade.Produto;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "produtodacompra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produtodacompra.findAll", query = "SELECT p FROM Produtodacompra p")
    , @NamedQuery(name = "Produtodacompra.findByProdutoId", query = "SELECT p FROM Produtodacompra p WHERE p.produtodacompraPK.produtoId = :produtoId")
    , @NamedQuery(name = "Produtodacompra.findByCompraId", query = "SELECT p FROM Produtodacompra p WHERE p.produtodacompraPK.compraId = :compraId")
    , @NamedQuery(name = "Produtodacompra.findByProdcQuantidade", query = "SELECT p FROM Produtodacompra p WHERE p.prodcQuantidade = :prodcQuantidade")
    , @NamedQuery(name = "Produtodacompra.findByProdcValor", query = "SELECT p FROM Produtodacompra p WHERE p.prodcValor = :prodcValor")
    , @NamedQuery(name = "Produtodacompra.findByPk", query = "SELECT p FROM Produtodacompra p WHERE p.produtodacompraPK.compraId = :compraId and "
        + " p.produtodacompraPK.produtoId = :produtoId")})
public class Produtodacompra extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProdutodacompraPK produtodacompraPK;
    @Basic(optional = false)
    @Column(name = "prodc_quantidade")
    private int prodcQuantidade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "prodc_valor")
    private BigDecimal prodcValor;
    @JoinColumn(name = "compra_id", referencedColumnName = "compra_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Compra compra;
    @JoinColumn(name = "produto_id", referencedColumnName = "produto_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Produto produto;

    public Produtodacompra() {
    }

    public Produtodacompra(ProdutodacompraPK produtodacompraPK) {
        this.produtodacompraPK = produtodacompraPK;
    }

    public Produtodacompra(ProdutodacompraPK produtodacompraPK, int prodcQuantidade, BigDecimal prodcValor) {
        this.produtodacompraPK = produtodacompraPK;
        this.prodcQuantidade = prodcQuantidade;
        this.prodcValor = prodcValor;
    }

    public Produtodacompra(Integer produtoId, Integer compraId, int prodcQuantidade, BigDecimal prodcValor) {
        this.produtodacompraPK = new ProdutodacompraPK(produtoId, compraId);
        this.prodcQuantidade = prodcQuantidade;
        this.prodcValor = prodcValor;
    }

    public Produtodacompra(ProdutodacompraPK produtodacompraPK, int prodcQuantidade) {
        this.produtodacompraPK = produtodacompraPK;
        this.prodcQuantidade = prodcQuantidade;
    }

    public Produtodacompra(int produtoId, int compraId) {
        this.produtodacompraPK = new ProdutodacompraPK(produtoId, compraId);
    }

    public ProdutodacompraPK getProdutodacompraPK() {
        return produtodacompraPK;
    }

    public void setProdutodacompraPK(ProdutodacompraPK produtodacompraPK) {
        this.produtodacompraPK = produtodacompraPK;
    }

    public int getProdcQuantidade() {
        return prodcQuantidade;
    }

    public void setProdcQuantidade(int prodcQuantidade) {
        this.prodcQuantidade = prodcQuantidade;
    }

    public BigDecimal getProdcValor() {
        return prodcValor;
    }

    public void setProdcValor(BigDecimal prodcValor) {
        this.prodcValor = prodcValor;
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (produtodacompraPK != null ? produtodacompraPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produtodacompra)) {
            return false;
        }
        Produtodacompra other = (Produtodacompra) object;
        if ((this.produtodacompraPK == null && other.produtodacompraPK != null) || (this.produtodacompraPK != null && !this.produtodacompraPK.equals(other.produtodacompraPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.Produtodacompra[ produtodacompraPK=" + produtodacompraPK + " ]";
    }
    
}
