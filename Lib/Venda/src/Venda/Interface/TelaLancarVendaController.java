package Venda.Interface;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Acesso.Sessao.Sessao;
import Venda.Controladora.CtrlVenda;
import Contaareceber.Controladora.CtrlContaareceber;
import Pessoa.Controladora.CtrlCliente;
import Pessoa.Interface.TelaConsultaClienteController;
import Produto.Controladora.CtrlProduto;
import Produto.Interface.TelaConsultaProdutoController;
import Servico.Interface.TelaConsultaServicoController;
import Transacao.Transaction;
import Utils.Acao.InterfaceFxmlAcao;
import Utils.Carrinho.Carrinho;
import Utils.Carrinho.Item;
import Utils.Carrinho.ItemCarrinho;
import Utils.Controladora.CtrlUtils;
import Utils.FormatString;
import Utils.GerarRelatorios;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Vacina.Controladora.CtrlVacina;
import Vacina.Interface.TelaConsultaVacinaController;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaLancarVendaController implements Initializable, Tela, InterfaceFxmlAcao {

    @FXML
    private Label lblVenda;
    @FXML
    private Label lblData;
    @FXML
    private Label lblValor;
    @FXML
    private JFXButton btnNovaVenda;
    @FXML
    private JFXButton btnAlterarVenda;
    @FXML
    private JFXButton btnExcluirVenda;
    @FXML
    private JFXTextField txbItem;
    @FXML
    private TableView<ItemCarrinho> tabela;
    @FXML
    private TableColumn<Object, String> tcCodigo;
    @FXML
    private TableColumn<Object, String> tcDescricao;
    @FXML
    private TableColumn<Object, String> tcQuantidade;
    @FXML
    private TableColumn<Object, String> tcValorUnitarito;
    @FXML
    private TableColumn<Object, String> tcSubtotal;
    @FXML
    private TableColumn<Object, Object> tcTipo;
    @FXML
    private JFXButton btnContinuar;
    @FXML
    private JFXButton btnCancelar;
    @FXML
    private VBox gpGeral;
    @FXML
    private VBox gpVenda;
    @FXML
    private HBox gpBuscarItem;
    @FXML
    private HBox gpBuscarCliente;
    @FXML
    private JFXTextField txbCliente;
    @FXML
    private JFXTextField txbStatus;
    @FXML
    private TableColumn<Object, Object> tcAcoes;
    @FXML
    private JFXTextField txbQuantidade;
    @FXML
    private JFXButton btnSalvar;
    @FXML
    private HBox gpPagamento;
    @FXML
    private JFXButton btnAdicionarVacina;

    private int flagNivelCancelar = 0;
    private int flag;
    private ItemCarrinho ItemSelecionado;
    private Carrinho oCarrinho;
    private Item Instancia;
    private int Quantidade;
    private Integer Id;
    private Object venda;
    private Object cliente;
    private TabelaParcelas Parcelas;
    private int flagConta;
    private boolean SalvarItemCarrinho;
    private double ValorTotaldaAlteracao;
    private double Total;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SetTelaAcao();

        tcCodigo.setCellValueFactory(new PropertyValueFactory("Chave"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory("Quantidade"));
        tcValorUnitarito.setCellValueFactory(new PropertyValueFactory("PrecoUnitario"));
        tcSubtotal.setCellValueFactory(new PropertyValueFactory("SubTotal"));
        tcTipo.setCellValueFactory(new PropertyValueFactory("Tipo"));
        tcAcoes.setCellValueFactory(new PropertyValueFactory("Crud"));

        oCarrinho = new Carrinho();

        estadoOriginalVenda();

        MaskFieldUtil.numericField(txbQuantidade);
    }

    private void estadoOriginalVenda() {
        gpVenda.setDisable(true);
        venda = null;
        lblValor.setText("R$ 0.00");
        lblVenda.setText("Venda");
        lblData.setText(Variables.getData(new Date()));
        CtrlUtils.clear(gpGeral.getChildren());
        setButtonVenda(false, false, false);
        estadoOriginalCarrinho();
        tabela.getItems().clear();
        ValorTotaldaAlteracao = Double.MIN_VALUE;
        flagNivelCancelar = 0;
        if (oCarrinho != null) {
            oCarrinho.Limpar();
        }
        Parcelas = null;
        btnNovaVenda.setDisable(true);
        txbStatus.clear();
    }

    @FXML
    private void HandleButtonNovaVenda(MouseEvent event) {
        flag = 0;
        flagConta = 4;
        setButtonVenda(true, true, true);
        txbQuantidade.setText("1");
        estadoEdicaoVenda();
    }

    private void estadoEdicaoVenda() {
        gpVenda.setDisable(false);
        Total = -1;
        estadoOriginalCarrinho();
    }

    @FXML
    private void HandleButtonAlterarVenda(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Venda/Interface/TelaConsultaVenda.fxml",
                "Consulta de Venda", null);
        if (TelaConsultaVendaController.getVenda() != null) {
            venda = TelaConsultaVendaController.getVenda();
            /*txbVenda.setText(venda.toString());*/
            setButtonVenda(true, false, false);
            oCarrinho = CtrlVenda.create().setCarrinho(venda);
            CtrlVenda.create().setCampos(venda, lblValor, lblData);
            CtrlVenda.create().setCampos(venda, txbCliente);
            cliente = CtrlVenda.create().getCliente(venda);
            Id = CtrlVenda.create().getId(venda);
            estadoOriginalCarrinho();
            CarregaCarrinho();
            CarregaEstadoPagamento();

            flag = 1;
            setButtonVenda(true, true, true);
            ValorTotaldaAlteracao = oCarrinho.GeraValorTotal();
            estadoEdicaoVenda();
            if (Mensagem.ExibirConfimacao("Atenção: Ao Alterar qualquer item da venda "
                    + "será necessário gerar novamente as Contas a Receber! Deseja prosseguir?")) {
                flag = 1;
                setButtonVenda(true, true, true);
                estadoEdicaoVenda();
            }
        }
    }

    @FXML
    private void HandleButtonExcluirVenda(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Venda/Interface/TelaConsultaVenda.fxml",
                "Consulta de Venda", null);
        if (TelaConsultaVendaController.getVenda() != null) {
            venda = TelaConsultaVendaController.getVenda();
            if (MensagensConta(false, "Excluir a Venda")) {
                if (Mensagem.ExibirConfimacao("Excluir uma Venda implica em excluir a conta areceber tambem e todas as suas parcelas geradas,"
                        + " e reverter o estoque dos produtos ao estado anterior a venda. Deseja Realmente Excluir?")) {
                    if (CtrlVenda.create().RemoverVenda(Integer.parseInt(venda.toString()))) {
                        Mensagem.Exibir(CtrlVenda.create().getMsg(), 1);
                    } else {
                        Mensagem.Exibir(CtrlVenda.create().getMsg() + ": a Venda não foi removida com Sucesso", 1);
                    }
                }
            }
            setButtonVenda(true, true, true);
            estadoOriginalVenda();
        }
    }

    @FXML
    private void HandleButtonPesquisarItem(MouseEvent event) {
        if (MensagensConta(false, "Adicionar novos Itens")) {
            IniciarTela.loadWindow(this.getClass(), "/Produto/Interface/TelaConsultaProduto.fxml",
                    "Consulta do Produto", null);
            if (TelaConsultaProdutoController.getProduto() != null) {
                Instancia = (Item) TelaConsultaProdutoController.getProduto();
                txbItem.setText(Instancia.getNome());
                txbQuantidade.setDisable(false);
                btnSalvar.setDisable(false);
                btnAdicionarVacina.setDisable(true);
            }
        }
    }

    @FXML
    private void HandleButtonPesquisarServico(MouseEvent event) {
        if (MensagensConta(false, "Adicionar novos Serviços")) {
            IniciarTela.loadWindow(this.getClass(), "/Servico/Interface/TelaConsultaServico.fxml",
                    "Consulta do Serviço", null);
            if (TelaConsultaServicoController.getServico() != null) {
                Instancia = (Item) TelaConsultaServicoController.getServico();
                txbItem.setText(Instancia.getNome());
                txbQuantidade.setDisable(false);
                btnSalvar.setDisable(false);
            }
        }
    }

    private void estadoOriginalCarrinho() {
        SalvarItemCarrinho = true;
        gpBuscarItem.setDisable(false);
        gpBuscarCliente.setDisable(false);
        txbQuantidade.setDisable(true);
        btnSalvar.setDisable(true);
        txbItem.setText("");
        tabela.setDisable(false);
        setButtonGeral(false, false);
        flagNivelCancelar = 1;
        btnAdicionarVacina.setDisable(false);
    }

    private void HandleButtonAlterarCarrinho(MouseEvent event) {
        AlterarCarrinho(ItemSelecionado);
    }

    private void AlterarCarrinho(ItemCarrinho oItem) {
        SalvarItemCarrinho = false;
        ItemSelecionado = oItem;
        Instancia = oItem.getoItem();
        Quantidade = oItem.getQuantidade();
        txbItem.setText(Instancia.getNome());
        txbQuantidade.setText(Quantidade + "");
        txbQuantidade.setDisable(false);
        txbQuantidade.requestFocus();
        btnSalvar.setDisable(false);
    }

    private void HandleButtonExcluirCarrinho(MouseEvent event) {
        oCarrinho.remove(ItemSelecionado);
        CarregaCarrinho();
        estadoOriginalCarrinho();
        gpBuscarItem.setDisable(false);
    }

    private void ExcluirCarrinho(ItemCarrinho oItem) {
        oCarrinho.remove(oItem);
        CarregaCarrinho();
        estadoOriginalCarrinho();
        gpBuscarItem.setDisable(false);
    }

    private void clicknatabela(MouseEvent event) {
        estadoOriginalCarrinho();
    }

    @FXML
    private void HandleButtonContinuar(MouseEvent event) {
        CtrlVenda cp = CtrlVenda.create();
        CtrlContaareceber cn = CtrlContaareceber.create();

        if (validaCampos()) {
            if (flag == 1)//alterar
            {
                if (Mensagem.ExibirConfimacao("Modificar uma Venda implica em areceber todas as suas parcelas geradas,"
                        + " e modificar o estoque dos produtos referenciados. Deseja Realmente Modificar?")) {
                    if ((venda = cp.ModificarVendaNovo(venda, new Date(), false, new BigDecimal(oCarrinho.GeraValorTotal()),
                            Parcelas.getQtdParcelas(), "", cliente, Sessao.getIndividuo(), Parcelas.getFormadepagamento(),
                            0, 0, 0, "", oCarrinho.getAllItems(), oCarrinho.getAllQuantidade(), Parcelas.getValores(),
                            Parcelas.getDatas())) == null) {
                        Mensagem.Exibir(cp.getMsg(), 2);
                    } else {
                        Mensagem.Exibir(cp.getMsg(), 1);
                        estadoOriginalVenda();
                    }
                }
            } else//cadastrar
            if ((venda = cp.RegistrarVendaNovo(new BigDecimal(oCarrinho.GeraValorTotal()), Parcelas.getQtdParcelas(),
                    "", cliente, Sessao.getIndividuo(), Parcelas.getFormadepagamento(), 0, 0, 0, "", oCarrinho.getAllItems(),
                    oCarrinho.getAllQuantidade(), Parcelas.getValores(), Parcelas.getDatas())) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginalVenda();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void HandleButtonCancelar(MouseEvent event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, flagNivelCancelar == 0)) {
            estadoOriginalVenda();
        } else if (flagNivelCancelar == 1) {
            estadoOriginalCarrinho();
            flagNivelCancelar = 0;
        } else {
            estadoOriginalVenda();
        }
    }

    private void setButtonVenda(boolean Bnova, boolean Balterar, boolean Bexcluir) {
        btnAlterarVenda.setDisable(Balterar);
        btnExcluirVenda.setDisable(Bexcluir);
        btnNovaVenda.setDisable(Bnova);
    }

    private void setButtonGeral(boolean Bcontinuar, boolean Bcancelar) {
        btnContinuar.setDisable(Bcontinuar);
        btnCancelar.setDisable(Bcancelar);
    }

    @FXML
    private void clicknatabelaCarrinho(MouseEvent event) {
        if (MensagensConta(false, "Alterar o Carrinho")) {
            int lin = tabela.getSelectionModel().getSelectedIndex();
            if (lin > -1) {
                ItemCarrinho f = (ItemCarrinho) tabela.getItems().get(lin);
                estadoOriginalCarrinho();
                ItemSelecionado = f;
            }
        }
    }

    public void CarregaCarrinho() {
        try {
            tabela.getItems().clear();
            tabela.setItems(FXCollections.observableList(oCarrinho.getItems()));
            Double valor = oCarrinho.GeraValorTotal();
            lblValor.setText(FormatString.MonetaryPersistent(valor));
            Total = valor.doubleValue();
            if (flag == 1) {
                if (valor.compareTo(ValorTotaldaAlteracao) != 0) {
                    txbStatus.setText("Gerar Conta");
                } else if (txbStatus.getText().equalsIgnoreCase("Gerar Conta")) {
                    CarregaEstadoPagamento();
                }
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tabela");
        }
    }

    private boolean validaCampos() {
        return oCarrinho != null && !oCarrinho.isVazio()
                && !txbCliente.getText().trim().isEmpty()
                && txbStatus.getText().equalsIgnoreCase("Conta Gerada")
                && Parcelas != null;
    }

    @FXML
    private void HandleButtonPesquisarCliente(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Pessoa/Interface/TelaConsultaCliente.fxml",
                "Consulta de Cliente", null);
        if (TelaConsultaClienteController.getCliente() != null) {
            cliente = TelaConsultaClienteController.getCliente();
            CtrlCliente.setCampoBusca(cliente, txbCliente);
            btnNovaVenda.setDisable(false);
        }
    }

    @FXML
    private void HandleButtonGerarContasaReceber(MouseEvent event) {
        if (oCarrinho == null || oCarrinho.isVazio()) {
            Mensagem.Exibir("O Carrinho está Vazio, portanto não é possivel gerar uma Conta a Receber!", 2);
        } else if (MensagensConta(true, "Gerar Contas a Receber")) {
            if (Parcelas != null) {
                if (Parcelas.getValorTotal() == Total) {
                    TelaQuitarContasAReceberController.setParcelas(Parcelas);
                } else {
                    TelaQuitarContasAReceberController.setParcelas(null);
                    TelaQuitarContasAReceberController.setValorTotal(Total);
                }
            } else {
                TelaQuitarContasAReceberController.setParcelas(null);
                if (venda != null && !txbStatus.getText().equalsIgnoreCase("Gerar Conta")) {
                    TelaQuitarContasAReceberController.setVenda(venda);
                } else {
                    TelaQuitarContasAReceberController.setValorTotal(Total);
                }
            }
            IniciarTela.loadWindow(this.getClass(), "/Venda/Interface/TelaQuitarContasAReceber.fxml",
                    "Lançar Conta a Receber", null);
            Parcelas = TelaQuitarContasAReceberController.getParcelas();
            if (Parcelas != null) {
                txbStatus.setText("Conta Gerada");
            } else {
                txbStatus.setText("Conta Não Gerada");
            }
        }
    }

    public boolean MensagensConta(boolean Value, String Adc) {
        boolean flagC = true;
        if (flagConta == 1) {
            Mensagem.Exibir("Não é possivel " + Adc
                    + " pois esta conta ja está Paga!", 2);
            flagC = false;
        } else if (flagConta == 2) {
            Mensagem.Exibir("Não é possivel " + Adc
                    + " pois esta conta ja possui parcelas Pagas!", 2);
            flagC = false;
        } else if (flagConta == 3 && Value) {
            Mensagem.Exibir("Esta Conta ja possui Parcelas Geradas,"
                    + " ao prosseguir as Poderá editar elas.", 2);
        } else if (flagConta == 5) {
            Mensagem.Exibir("A Venda é Inválida", 2);
            flagC = false;
        }
        return flagC;
    }

    private void CarregaEstadoPagamento() {
        if (venda != null) {
            String Resultado = CtrlVenda.create().VerificaPagamento(venda);
            txbStatus.setText(Resultado);
            if (Resultado.equalsIgnoreCase("Venda Paga")) {
                flagConta = 1;
            } else if (Resultado.equalsIgnoreCase("Parcela Paga")) {
                flagConta = 2;
            } else if (Resultado.equalsIgnoreCase("Não Pago")) {
                flagConta = 3;
            } else if (Resultado.equalsIgnoreCase("Não Gerado")) {
                flagConta = 4;
            } else {
                flagConta = 5;
            }
        }
    }

    @Override
    public void SelectEventAcao(Object Reference, String Action, Object element) {
        if (Action.equalsIgnoreCase("mod")) {
            AlterarCarrinho((ItemCarrinho) Reference);
        } else if (Action.equalsIgnoreCase("rem")) {
            ExcluirCarrinho((ItemCarrinho) Reference);
        }
        System.out.println(Action + ": " + Reference);
    }

    @Override
    public void SetTelaAcao() {
        ItemCarrinho.setTelaAcao(this);
    }

    @FXML
    private void HandleButtonSalvar(MouseEvent event) {
        if (Instancia != null && getQuantidade()) {
            if (SalvarItemCarrinho) {
                if (oCarrinho.find(Instancia) != -1) {
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Selecione");
                    alert.setContentText("O Item a ser modificado já existe no carrinho,"
                            + " deseja acrescentar a Quantidade selecionada ao item?");

                    ButtonType Acrescentar = new ButtonType("Acrescentar");
                    ButtonType Cancelar = new ButtonType("Cancelar");

                    alert.getButtonTypes().clear();

                    alert.getButtonTypes().addAll(Acrescentar, Cancelar);
                    Optional<ButtonType> option = alert.showAndWait();

                    if (option.get() != null && option.get() == Acrescentar) {
                        oCarrinho.add(Instancia, Quantidade);
                    }
                } else {
                    oCarrinho.add(Instancia, Quantidade);
                }
            } else {
                if (ItemSelecionado != null && oCarrinho.find(Instancia) != -1) {

                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Selecione");
                    alert.setContentText("O Item a ser modificado já existe no carrinho,"
                            + " deseja acrescentar a Quantidade selecionada ao item,"
                            + " Substituir a Quantidade atual pela nova, ou cancelar a modificação?");

                    ButtonType Acrescentar = new ButtonType("Acrescentar");
                    ButtonType Substituir = new ButtonType("Substituir");
                    ButtonType Cancelar = new ButtonType("Cancelar");

                    alert.getButtonTypes().clear();

                    alert.getButtonTypes().addAll(Acrescentar, Substituir, Cancelar);
                    Optional<ButtonType> option = alert.showAndWait();

                    if (option.get() != null && option.get() != Cancelar) {

                        if (option.get() == Acrescentar) {
                            oCarrinho.changeAdd(ItemSelecionado, Instancia, Quantidade);
                        } else if (option.get() == Substituir) {
                            oCarrinho.change(ItemSelecionado, Instancia, Quantidade);
                        }
                    }
                } else {
                    oCarrinho.change(ItemSelecionado, Instancia, Quantidade);
                }

            }
            Instancia = null;
            Quantidade = 0;
            CarregaCarrinho();
            estadoOriginalCarrinho();
            gpBuscarItem.setDisable(false);
        }
    }

    private boolean getQuantidade() {
        if (txbQuantidade.getText().trim().isEmpty()) {
            Mensagem.Exibir("Informe a Quantidade", 2);
            txbQuantidade.requestFocus();
        } else if (Integer.parseInt(txbQuantidade.getText()) < 1) {
            Mensagem.Exibir("informe uma Quantidade Acima de 1", 2);
            txbQuantidade.requestFocus();
        } else if (CtrlProduto.isProduto(Instancia)) {
            int Pos = oCarrinho.find(Instancia);
            if (Pos != -1) {
                int Quant = CtrlProduto.getQuantidade(Instancia) - oCarrinho.getItems().get(Pos).getQuantidade();
                if (Quant < Integer.parseInt(txbQuantidade.getText())) {
                    Mensagem.Exibir("Estoque do Produto Insuficiente: Só existem " + CtrlProduto.getQuantidade(Instancia) + " em estoque!", 2);
                    txbQuantidade.requestFocus();
                } else {
                    Quantidade = Integer.parseInt(txbQuantidade.getText());
                    return true;
                }
            } else if (CtrlProduto.getQuantidade(Instancia) < Integer.parseInt(txbQuantidade.getText())) {
                Mensagem.Exibir("Estoque do Produto Insuficiente: Só existem " + CtrlProduto.getQuantidade(Instancia) + " em estoque!", 2);
                txbQuantidade.requestFocus();
            } else {
                Quantidade = Integer.parseInt(txbQuantidade.getText());
                return true;
            }
        } else if (CtrlVacina.isVacina(Instancia)) {
            Item aInstancia = (Item) CtrlVacina.getProduto(Instancia);
            int Pos = oCarrinho.find(aInstancia);
            if (Pos != -1) {
                int Quant = CtrlProduto.getQuantidade(aInstancia) - oCarrinho.getItems().get(Pos).getQuantidade();
                if (Quant < Integer.parseInt(txbQuantidade.getText())) {
                    Mensagem.Exibir("Estoque da Vacina Insuficiente: Só existem " + CtrlProduto.getQuantidade(aInstancia) + " em estoque!", 2);
                    txbQuantidade.requestFocus();
                } else {
                    Quantidade = Integer.parseInt(txbQuantidade.getText());
                    return true;
                }
            } else if (CtrlProduto.getQuantidade(aInstancia) < Integer.parseInt(txbQuantidade.getText())) {
                Mensagem.Exibir("Estoque da Vacina Insuficiente: Só existem " + CtrlProduto.getQuantidade(aInstancia) + " em estoque!", 2);
                txbQuantidade.requestFocus();
            } else {
                Quantidade = Integer.parseInt(txbQuantidade.getText());
                return true;
            }
        } else {
            Quantidade = Integer.parseInt(txbQuantidade.getText());
            return true;
        }
        return false;
    }

    @FXML
    private void HandleButtonAdicionarVacina(MouseEvent event) {
        if (MensagensConta(false, "Adicionar nova Vacinacao")) {
            if (cliente != null) {
                TelaConsultaVacinaController.setCliente(cliente);
                IniciarTela.loadWindow(this.getClass(), "/Vacina/Interface/TelaConsultaVacina.fxml",
                        "Consulta do Vacinacao", null);
                if (TelaConsultaVacinaController.getVacina() != null) {
                    Instancia = (Item) TelaConsultaVacinaController.getVacina();
                    if (getQuantidade()) {
                        if (oCarrinho.find(Instancia) == -1) {
                            oCarrinho.add(Instancia, 1);
                        } else {
                            Mensagem.Exibir("O Item a ser modificado já existe no carrinho!", 2);
                        }
                    }
                }
            }
        }
        Instancia = null;
        Quantidade = 0;
        CarregaCarrinho();
        estadoOriginalCarrinho();
        gpBuscarItem.setDisable(false);
    }

    @FXML
    public void evtEmitirNota(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Venda/Interface/TelaConsultaVenda.fxml",
                "Consulta de Venda", null);
        if (TelaConsultaVendaController.getVenda() != null) {
            venda = TelaConsultaVendaController.getVenda();
            String Sql = "select * from venda"
                    + " inner join cliente on cliente.cli_id  = venda.cli_id "
                    + " where venda.venda_id = "+CtrlVenda.getId(venda);
            GerarRelatorios.gerarRelatorio(Sql, "Relatorios/Petshop/Vendas.jasper",
                    "Relatório de Vendas", Transaction.getEntityManager());
        }

    }

}
