/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fornecedor.Interface;

import Fornecedor.Controladora.CtrlFornecedor;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Drake
 */
public class TelaConsultaFornecedorController implements Initializable {

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcCNPJ;
    @FXML
    private TableColumn<Object, String> tcTelefone;
    @FXML
    private TableColumn<Object, String> tcEndereco;
    private static boolean btnPainel = true;
    private static Object fornecedor;
    @FXML
    private HBox btnGp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("ForNome"));
        tcEndereco.setCellValueFactory(new PropertyValueFactory("EndId"));
        tcCNPJ.setCellValueFactory(new PropertyValueFactory("ForCnpj"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("ForTelefone"));
        CarregaTabela("");
        fornecedor = null;
        btnGp.setVisible(btnPainel);
    }

    @FXML
    private void evtConfirmar(ActionEvent event) {
        if (fornecedor != null) {
            evtCancelar(event);
        }
    }

    private void CarregaTabela(String filtro) {
        CtrlFornecedor ctm = CtrlFornecedor.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtCancelar(ActionEvent event) {
        CtrlUtils.CloseStage(event);
    }

    @FXML
    private void evtBuscar() {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            fornecedor = tabela.getItems().get(lin);
        }
    }

    public static Object getFornecedor() {
        return fornecedor;
    }

    /**
     * @param aBtnPainel the btnPainel to set
     */
    public static void setBtnPainel(boolean aBtnPainel) {
        btnPainel = aBtnPainel;
    }

}
