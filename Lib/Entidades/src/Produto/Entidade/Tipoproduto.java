/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "tipoproduto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipoproduto.findAll", query = "SELECT t FROM Tipoproduto t")
    , @NamedQuery(name = "Tipoproduto.findByTipoprodutoId", query = "SELECT t FROM Tipoproduto t WHERE t.tipoprodutoId = :tipoprodutoId")
    , @NamedQuery(name = "Tipoproduto.findByTipoNome", query = "SELECT t FROM Tipoproduto t WHERE t.tipoNome = :tipoNome")
    , @NamedQuery(name = "Tipoproduto.findByTipoDesc", query = "SELECT t FROM Tipoproduto t WHERE t.tipoDesc = :tipoDesc")})
public class Tipoproduto extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tipoproduto_id")
    private Integer tipoprodutoId;
    @Basic(optional = false)
    @Column(name = "tipo_nome")
    private String tipoNome;
    @Column(name = "tipo_desc")
    private String tipoDesc;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "tipoproduto")
    private Collection<Categoriaproduto> categoriaprodutoCollection = new ArrayList();

    public Tipoproduto() {
    }

    public Tipoproduto(Integer tipoprodutoId) {
        this.tipoprodutoId = tipoprodutoId;
    }

    public Tipoproduto(String tipoNome, String tipoDesc) {
        this.tipoNome = tipoNome;
        this.tipoDesc = tipoDesc;
    }

    public Tipoproduto(Integer tipoprodutoId, String tipoNome, String tipoDesc) {
        this.tipoprodutoId = tipoprodutoId;
        this.tipoNome = tipoNome;
        this.tipoDesc = tipoDesc;
    }

    public Tipoproduto(Integer tipoprodutoId, String tipoNome) {
        this.tipoprodutoId = tipoprodutoId;
        this.tipoNome = tipoNome;
    }

    public Integer getTipoprodutoId() {
        return tipoprodutoId;
    }

    public void setTipoprodutoId(Integer tipoprodutoId) {
        this.tipoprodutoId = tipoprodutoId;
    }

    public String getTipoNome() {
        return tipoNome;
    }

    public void setTipoNome(String tipoNome) {
        this.tipoNome = tipoNome;
    }

    public String getTipoDesc() {
        return tipoDesc;
    }

    public void setTipoDesc(String tipoDesc) {
        this.tipoDesc = tipoDesc;
    }

    @XmlTransient
    public Collection<Categoriaproduto> getCategoriaprodutoCollection() {
        return categoriaprodutoCollection;
    }

    public void setCategoriaprodutoCollection(Collection<Categoriaproduto> categoriaprodutoCollection) {
        this.categoriaprodutoCollection = categoriaprodutoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipoprodutoId != null ? tipoprodutoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipoproduto)) {
            return false;
        }
        Tipoproduto other = (Tipoproduto) object;
        if ((this.tipoprodutoId == null && other.tipoprodutoId != null) || (this.tipoprodutoId != null && !this.tipoprodutoId.equals(other.tipoprodutoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tipoNome;
    }
    
}
