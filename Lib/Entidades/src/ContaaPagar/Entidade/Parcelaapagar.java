/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContaaPagar.Entidade;

import Caixa.Entidade.Movimentacaoapagar;
import FormadePagamento.Entidade.Formadepagamento;
import JPA.Dao.JPADao;
import Utils.DateUtils;
import Utils.FormatString;
import Variables.Variables;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "parcelaapagar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parcelaapagar.findAll", query = "SELECT p FROM Parcelaapagar p")
    , @NamedQuery(name = "Parcelaapagar.findByContaapagarId", query = "SELECT p FROM Parcelaapagar p WHERE p.parcelaapagarPK.contaapagarId = :contaapagarId")
    , @NamedQuery(name = "Parcelaapagar.findByParpNumerodaparcela", query = "SELECT p FROM Parcelaapagar p WHERE p.parcelaapagarPK.parpNumerodaparcela = :parpNumerodaparcela")
    , @NamedQuery(name = "Parcelaapagar.findByParcelaapagarPk", query = "SELECT p FROM Parcelaapagar p WHERE p.parcelaapagarPK.contaapagarId = :contaapagarId"
            + " and p.parcelaapagarPK.parpNumerodaparcela = :parpNumerodaparcela")
    , @NamedQuery(name = "Parcelaapagar.findByCompraId", query = "SELECT p FROM Parcelaapagar p WHERE p.contaapagar.compraId = :compraId"
            + " group by p.parcelaapagarPK.parpNumerodaparcela, p.parcelaapagarPK.parpDatadegeracao, p")
    , @NamedQuery(name = "Parcelaapagar.findByParpDataVencimento", query = "SELECT p FROM Parcelaapagar p WHERE p.parpDataVencimento = :parpDataVencimento")
    , @NamedQuery(name = "Parcelaapagar.findByParpDataPagamento", query = "SELECT p FROM Parcelaapagar p WHERE p.parpDataPagamento = :parpDataPagamento")
    , @NamedQuery(name = "Parcelaapagar.findByParpValor", query = "SELECT p FROM Parcelaapagar p WHERE p.parpValor = :parpValor")
    , @NamedQuery(name = "Parcelaapagar.findByParpSituacao", query = "SELECT p FROM Parcelaapagar p WHERE p.parpSituacao = :parpSituacao")
    , @NamedQuery(name = "Parcelaapagar.findByParpValorPago", query = "SELECT p FROM Parcelaapagar p WHERE p.parpValorPago = :parpValorPago")
    , @NamedQuery(name = "Parcelaapagar.findByParpTroco", query = "SELECT p FROM Parcelaapagar p WHERE p.parpTroco = :parpTroco")
    , @NamedQuery(name = "Parcelaapagar.findByFormadePagamento", query = "SELECT p FROM Parcelaapagar p WHERE p.formadepagamentoId.formadepagamentoId = :formadepagamentoId")
    , @NamedQuery(name = "Parcelaapagar.findByNextParcela", query = "SELECT p FROM Parcelaapagar p GROUP BY p HAVING MIN(p.parpDataPagamento) = p.parpDataPagamento"
            + " and p.parcelaapagarPK.contaapagarId = :contaapagarId")
    , @NamedQuery(name = "Parcelaapagar.findByAllGeral", query = "SELECT p FROM Parcelaapagar p WHERE p.parcelaapagarPK.parpNumerodaparcela = :parpNumerodaparcela"
            + " or p.parpValor = :parpValor"
            + " or p.parpValorPago = :parpValorPago")
    , @NamedQuery(name = "Parcelaapagar.findByAllData", query = "SELECT p FROM Parcelaapagar p WHERE p.parpDataVencimento = :parpDataVencimento"
            + " or p.parpDataPagamento = :parpDataPagamento")
    , @NamedQuery(name = "Parcelaapagar.findByBetweenVencimento", query = "SELECT p FROM Parcelaapagar p WHERE p.parpDataVencimento"
            + " between :datainicial and :datafinal")
    , @NamedQuery(name = "Parcelaapagar.findByBetweenPagamento", query = "SELECT p FROM Parcelaapagar p WHERE p.parpDataPagamento"
            + " between :datainicial and :datafinal")
    , @NamedQuery(name = "Parcelaapagar.findByAllBetween", query = "SELECT p FROM Parcelaapagar p WHERE p.parpDataPagamento"
            + " between :datainicial and :datafinal"
            + " or p.parpDataVencimento"
            + " between :datainicial and :datafinal")
    , @NamedQuery(name = "Parcelaapagar.findByDataVencimentoCompra", query = "SELECT p FROM Parcelaapagar p "
            + "WHERE p.parpDataVencimento = :parpDataVencimento"
            + " and p.contaapagar.compraId = :compraId order by p.parcelaapagarPK.parpDatadegeracao asc")
    , @NamedQuery(name = "Parcelaapagar.findByParpDatadegeracao", query = "SELECT p FROM Parcelaapagar p WHERE p.parcelaapagarPK.parpDatadegeracao = :parpDatadegeracao")})
public class Parcelaapagar extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ParcelaapagarPK parcelaapagarPK;
    @Basic(optional = false)
    @Column(name = "parp_data_vencimento")
    @Temporal(TemporalType.DATE)
    private Date parpDataVencimento;
    @Column(name = "parp_data_pagamento")
    @Temporal(TemporalType.DATE)
    private Date parpDataPagamento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "parp_valor")
    private BigDecimal parpValor;
    @Column(name = "parp_situacao")
    private Boolean parpSituacao;
    @Column(name = "parp_valor_pago")
    private BigDecimal parpValorPago;
    @Column(name = "parp_troco")
    private BigDecimal parpTroco;
    @JoinColumn(name = "contaapagar_id", referencedColumnName = "contaapagar_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Contaapagar contaapagar;
    @JoinColumn(name = "formadepagamento_id", referencedColumnName = "formadepagamento_id")
    @ManyToOne(optional = false)
    private Formadepagamento formadepagamentoId;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parcelaapagar")
    private Collection<Movimentacaoapagar> movimentacaoapagarCollection;

    public Parcelaapagar() {
    }

    public Parcelaapagar(ParcelaapagarPK parcelaapagarPK) {
        this.parcelaapagarPK = parcelaapagarPK;
    }

    public Parcelaapagar(Integer ContaapagarID, Integer papNumerodaparcela, Date parpDataVencimento, Date parpDataPagamento,
            BigDecimal parpValor, Boolean parpSituacao, BigDecimal parpValorPago, BigDecimal parpTroco, Date parpDatadegeracao) {
        this.parcelaapagarPK = new ParcelaapagarPK(ContaapagarID, papNumerodaparcela, parpDatadegeracao);
        this.parpDataVencimento = parpDataVencimento;
        this.parpDataPagamento = parpDataPagamento;
        this.parpValor = parpValor;
        this.parpSituacao = parpSituacao;
        this.parpValorPago = parpValorPago;
        this.parpTroco = parpTroco;
    }

    public Parcelaapagar(ParcelaapagarPK parcelaapagarPK, Date parpDataVencimento, BigDecimal parpValor) {
        this.parcelaapagarPK = parcelaapagarPK;
        this.parpDataVencimento = parpDataVencimento;
        this.parpValor = parpValor;
    }

    public Parcelaapagar(int contaapagarId, int parpNumerodaparcela, Date parpDatadegeracao) {
        this.parcelaapagarPK = new ParcelaapagarPK(contaapagarId, parpNumerodaparcela, parpDatadegeracao);
    }

    public ParcelaapagarPK getParcelaapagarPK() {
        return parcelaapagarPK;
    }

    public void setParcelaapagarPK(ParcelaapagarPK parcelaapagarPK) {
        this.parcelaapagarPK = parcelaapagarPK;
    }

    public Date getParpDataVencimento() {
        return parpDataVencimento;
    }

    public String getDataVencimento() {
        return parpDataVencimento != null ? Variables.getData(parpDataVencimento) : "Aguardando Pagamento";
    }

    public void setParpDataVencimento(Date parpDataVencimento) {
        this.parpDataVencimento = parpDataVencimento;
    }

    public Date getParpDataPagamento() {
        return parpDataPagamento;
    }

    public String getDataPagamento() {
        return parpDataPagamento != null ? Variables.getData(parpDataPagamento) : "Aguardando Pagamento";
    }

    public void setParpDataPagamento(Date parpDataPagamento) {
        this.parpDataPagamento = parpDataPagamento;
    }

    public BigDecimal getParpValor() {
        return parpValor;
    }

    public String getValor() {
        return FormatString.MonetaryPersistent(parpValor);
    }

    public void setParpValor(BigDecimal parpValor) {
        this.parpValor = parpValor;
    }

    public Boolean getParpSituacao() {
        return parpSituacao;
    }
    
    public String getSituacao(){
        return parpSituacao ? "Pago" : "Pendente";
    }

    public void setParpSituacao(Boolean parpSituacao) {
        this.parpSituacao = parpSituacao;
    }

    public BigDecimal getParpValorPago() {
        return parpValorPago;
    }

    public String getValorPago() {
        return FormatString.MonetaryPersistent(parpValorPago);
    }

    public void setParpValorPago(BigDecimal parpValorPago) {
        this.parpValorPago = parpValorPago;
    }

    public BigDecimal getParpTroco() {
        return parpTroco;
    }

    public void setParpTroco(BigDecimal parpTroco) {
        this.parpTroco = parpTroco;
    }

    public Contaapagar getContaapagar() {
        return contaapagar;
    }

    public void setContaapagar(Contaapagar contaapagar) {
        this.contaapagar = contaapagar;
    }

    public Formadepagamento getFormadepagamentoId() {
        return formadepagamentoId;
    }

    public void setFormadepagamentoId(Formadepagamento formadepagamentoId) {
        this.formadepagamentoId = formadepagamentoId;
    }
    
    public Integer getParpNumerodaparcela(){
        return parcelaapagarPK != null ? parcelaapagarPK.getParpNumerodaparcela() : null;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parcelaapagarPK != null ? parcelaapagarPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parcelaapagar)) {
            return false;
        }
        Parcelaapagar other = (Parcelaapagar) object;
        if ((this.parcelaapagarPK == null && other.parcelaapagarPK != null) || (this.parcelaapagarPK != null && !this.parcelaapagarPK.equals(other.parcelaapagarPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.Parcelaapagar[ parcelaapagarPK=" + parcelaapagarPK + " ]";
    }
    
    /////Trasient
    public String getDatadeVencimento() {
        if (parpDataVencimento != null) {
            return DateUtils.asLocalDate(parpDataVencimento).toString();
        } else {
            return "Sem Data de Vencimento";
        }
    }
    
    /////Trasient
    public String getDatadePagamento() {
        if (parpDataPagamento != null) {
            return DateUtils.asLocalDate(parpDataPagamento).toString();
        } else {
            return "Não foi Pago";
        }
    }

    @XmlTransient
    public Collection<Movimentacaoapagar> getMovimentacaoapagarCollection() {
        return movimentacaoapagarCollection;
    }

    public void setMovimentacaoapagarCollection(Collection<Movimentacaoapagar> movimentacaoapagarCollection) {
        this.movimentacaoapagarCollection = movimentacaoapagarCollection;
    }
    
}
