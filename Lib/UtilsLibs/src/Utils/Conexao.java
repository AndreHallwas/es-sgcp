package Geral.Servidor;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Aluno
 */
class Conexao implements Runnable 
{
    private Socket sock;

    public Conexao(Socket sock) 
    {
        this.sock = sock;
    }    
    @Override
    public void run() 
    {
        try 
        {    
            String mens;
            System.out.println("Conexão realizada por " + sock.getInetAddress().getHostAddress());
            ObjectOutputStream output = new ObjectOutputStream(sock.getOutputStream());
            ObjectInputStream input = new ObjectInputStream(sock.getInputStream());
            do 
            {
                mens = (String) input.readObject();
                System.out.println("mensagem recebida de " + sock.getInetAddress().getHostAddress() + " : " + mens);
                output.writeObject("OK");
            } while (!mens.equals("FIM"));
            output.close();
            input.close();
            sock.close();
            
        } 
        catch (Exception e) 
        {
            System.out.println(e);
        }
    }
}
