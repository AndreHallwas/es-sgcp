/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Controladora;

import java.math.BigDecimal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Raizen
 */
public class CtrlServicoTest {
    
    public CtrlServicoTest() {
    }
    
    @Test
    public void testGeral() {
        
        CtrlServico cg = CtrlServico.create();
        cg.Salvar(new BigDecimal(12.2), "Teste", "Testando o Banco", CtrlGrupodeservico.create().Pesquisar("1").get(0));
        cg.Salvar(new BigDecimal(12.2), "Teste2", "Testando o Banco2", CtrlGrupodeservico.create().Pesquisar("1").get(0));
        /*cg.Alterar(1, new BigDecimal(12.2), "Teste1", "Testando o Banco", CtrlGrupodeservico.create().Pesquisar("1").get(0));*/
        cg.Remover(2);
        System.out.println(cg.Pesquisar("1").get(0).toString());
    }
    
}
