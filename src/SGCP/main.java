/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP;

import Acesso.Controladora.Acesso;
import Acesso.Controladora.AcessoComponente;
import Acesso.Controladora.HabilitaComponente;
import Acesso.Entidade.Componente;
import Acesso.Entidade.ComponenteObjeto;
import Transacao.Transaction;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Variables.Variables;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 *
 * @author Raizen
 */
public class main extends Application {

    private boolean PrimeiroAcesso() {
        boolean flag = true;
        flag = flag && Transaction.criarBD();
        flag = flag && Transaction.realizaBackupRestauracao("RestaurarOriginal.bat");
        return flag;
    }

    public static void RegistrarComponentes(boolean Inicio) {
        Variables._Acc = AcessoComponente.create(new HabilitaComponente() {
            @Override
            public void Habilita(Componente componente) {
                if (componente != null) {
                    ArrayList<Object> objs = componente.getObjetos();
                    if (objs != null && !objs.isEmpty()) {
                        for (Object obj : objs) {
                            if (obj != null) {
                                if (obj instanceof MenuItem) {
                                    ((MenuItem) obj).setVisible(true);
                                } else if (obj instanceof Node) {
                                    ((Node) obj).setVisible(true);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void Default(ArrayList<Componente> Componentes) {
                if (Componentes != null && Componentes.size() > 0) {
                    for (Componente oComponente : Componentes) {
                        ArrayList<Object> objs = oComponente.getObjetos();
                        if (objs != null && !objs.isEmpty()) {
                            for (Object obj : objs) {
                                if (obj != null) {
                                    if (obj instanceof MenuItem) {
                                        ((MenuItem) obj).setVisible(false);
                                    } else if (obj instanceof Node) {
                                        ((Node) obj).setVisible(false);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
        AcessoComponente ac = (AcessoComponente) Variables._Acc;

        ArrayList<Componente> Componentes = new ArrayList();
        
        Componentes.add(new ComponenteObjeto(null, "Menu de Gerenciamento"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Usuario"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Nivel"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Cliente"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Fornecedor"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Produto"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Serviço"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Tipo de Despesa"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Forma de Pagamento"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Empresa"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Animal"));
        Componentes.add(new ComponenteObjeto(null, "Menu de Abrir/ Fechar Caixa"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Abrir Caixa"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Fechar"));
        Componentes.add(new ComponenteObjeto(null, "Menu de Realizar Venda"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Realizar Venda"));
        Componentes.add(new ComponenteObjeto(null, "Menu de Compra"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Compra"));
        Componentes.add(new ComponenteObjeto(null, "Menu de Recebimento"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Recebimento"));
        Componentes.add(new ComponenteObjeto(null, "Menu de Funções"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Registrar Despesa"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Atendimento"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Agendamento"));
        Componentes.add(new ComponenteObjeto(null, "Opção de Caderneta de Vacinação"));
        Componentes.add(new ComponenteObjeto(null, "Menu de Ferramentas"));
        Componentes.add(new ComponenteObjeto(null, "Menu de Relatórios"));
        
        Componentes.add(new ComponenteObjeto(null, "Tabela de Clientes"));
        Componentes.add(new ComponenteObjeto(null, "Nivel Usuario"));
        Componentes.add(new ComponenteObjeto(null, "Tabela de Usuarios"));
        
        
        Componentes.forEach((oComponente) -> {
            ac.Registra(oComponente);
        });

        if (Inicio) {
            Acesso acesso = Acesso.create();
            Componentes.forEach((oComponente) -> {
                acesso.addComponente(oComponente);
            });
            acesso.RegistraComponentes();
        }

    }

    @Override
    public void start(Stage stage) throws Exception {
        boolean Flag = false;
        if (Transaction.Connect()) {
            Inicia(stage);
            Flag = true;
        } else {
            PrimeiroAcesso();
            if (Transaction.Connect()) {
                Inicia(stage);
            } else {
                stage.close();
                System.exit(2);
            }
        }
    }

    public void Inicia(Stage stage) {

        IniciarTela.setIMAGE_LOC("/Imagens/Image.png");
        IniciarTela.setStyle("Estilos/DarkTheme.css");
        IniciarTela.setRef_Class(this.getClass());
        Variables._Stage = IniciarTela.change(stage);
        RegistrarComponentes(true);

        boolean Login = TelaLoginFXMLController.Create(this.getClass());
        if (Login) {
            boolean Iniciado = MainFXMLController.Create(this.getClass());
            if (Iniciado) {
                Mensagem.ExibirLog("Programa Iniciado!");
            }
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
