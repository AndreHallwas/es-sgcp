/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Transacao;

import Utils.Acao.ButtonAcaoAbstract;
import Utils.Arquivo;
import Utils.Carrinho.ItemCarrinho;
import Utils.Mensagem;
import Variables.Variables;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

/**
 *
 * @author Raizen
 */
public class Backup {

    ArrayList<nodeTabela> lista = new ArrayList();
    protected ArrayList<String> Options = getBackupOptions();
    protected int UltimosBackups;
    protected boolean RealizarBackupIniciarSistema;
    protected boolean RealizarBackupFinalizarSistema;

    protected void writeBackupOptions(ArrayList<String> PU) {
        Arquivo.gravaArquivoDeStringUTF("BackupOptions.db", PU);
    }

    protected ArrayList<String> readBackupOptions() {
        ArrayList<String> PUs = null;
        PUs = Arquivo.leArquivoDeStringUTF("BackupOptions.db", 3);
        try {
            if (PUs != null && !PUs.isEmpty()) {
                RealizarBackupIniciarSistema = Boolean.parseBoolean(PUs.get(0));
                RealizarBackupFinalizarSistema = Boolean.parseBoolean(PUs.get(1));
                UltimosBackups = Integer.parseInt(PUs.get(2));
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Backup:52");
        }
        return PUs;
    }

    protected ArrayList<String> readBackupOptionsDefault() {
        ArrayList<String> PUs = new ArrayList();
        PUs.add("true");
        PUs.add("true");
        PUs.add("50");
        return PUs;
    }

    protected void writeBackupOptions() {
        ArrayList<String> PUs = new ArrayList();
        PUs.add(RealizarBackupIniciarSistema + "");
        PUs.add(RealizarBackupFinalizarSistema + "");
        PUs.add(UltimosBackups + "");
        writeBackupOptions(PUs);
    }

    public ArrayList<String> getBackupOptions() {
        Options = readBackupOptions();
        if (getOptions() == null || getOptions().isEmpty()) {
            if (readBackupOptionsDefault() != null && !readBackupOptionsDefault().isEmpty()) {
                Options = readBackupOptionsDefault();
                writeBackupOptions(Options);
            }
        }
        return getOptions();
    }

    public Backup() {
        init();
        removerBackupsExcedentes();
    }

    private void removerBackupsExcedentes() {
        try {
            if (lista != null && !lista.isEmpty() && lista.size() > UltimosBackups) {
                int Prox = 0;
                int Count = 0;/*Chave de Segurança, Só se deve remover até 100 Backups excedentes por vez*/
                while (lista.size() > UltimosBackups && Count < 100) {
                    nodeTabela menor = lista.get(Prox);
                    if (menor.getAttr() != null) {
                        for (int i = 0; i < lista.size(); i++) {
                            nodeTabela object = lista.get(i);
                            if (object.getAttr() != null
                                    && object.getAttr().creationTime().compareTo(menor.getAttr().creationTime()) < 0) {
                                menor = object;
                            }
                        }
                        if (menor.getArquivo().delete()) {
                            lista.remove(menor);
                            Mensagem.ExibirLog("Backup: " + menor.getNome() + " Foi Removido!");
                        }
                    } else {
                        Prox++;
                    }
                    Count++;
                }
            }
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Não foi Possivel remover os Backups Excedentes - Backup:52");
        }
    }

    public void add(File Arquivo) {
        lista.add(new nodeTabela(Arquivo.getName(), Arquivo));
    }

    public ArrayList<nodeTabela> get() {
        return lista;
    }

    public void init() {
        try {
            lista = new ArrayList();
            File dir = new File("Backup");
            File[] filelist = dir.listFiles();
            List<String> theNamesOfFiles = new ArrayList();
            for (int i = 0; i < filelist.length; i++) {
                if (filelist[i].getName().contains(".custom")) {
                    add(filelist[i]);
                }
            }
        }catch(Exception ex){
            Mensagem.ExibirException(ex, "Backup:138");
        }
    }

    public boolean realizaBackupRestauracao(String arqlote, TextArea ta) {
        Runtime r = Runtime.getRuntime();
        try {
            Process p = r.exec("backup\\" + arqlote);
            if (p != null) {
                InputStreamReader str = new InputStreamReader(p.getErrorStream());
                BufferedReader reader = new BufferedReader(str);
                String linha;
                while ((linha = reader.readLine()) != null) {
                    System.out.println(linha);
                    final String lin = linha;
                    Platform.runLater(()
                            -> {
                        ta.appendText(lin + "\n");
                    });
                }
                Platform.runLater(()
                        -> {
                    ta.appendText("Completo....." + "\n");
                });
            }
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Realizar Backup");
            return false;
        }
        return true;
    }

    public boolean realizaBackupRestauracao(String arquivo, String Tipo, TextArea ta) {
        try {
            Process p = null;
            ProcessBuilder pb;
            if (Tipo.equalsIgnoreCase("restore")) {
                pb = new ProcessBuilder("backup\\pg_restore.exe", "-c", "--host", "localhost", "--port",
                        "5432", "--username", "postgres", "--dbname", "petshop", "--verbose", "--no-password", "backup\\" + arquivo);
            } else {
                pb = new ProcessBuilder("backup\\pg_dump.exe", "--host", "localhost", "--port",
                        "5432", "--format", "custom", "--blobs", "--verbose", "--file", "backup\\" + arquivo, "petshop");
                pb.environment().put("PGUSER", "postgres");
            }
            pb.environment().put("PGPASSWORD", "postgres123");
            pb.redirectErrorStream(true);
            p = pb.start();
            if (p != null) {
                InputStreamReader str = new InputStreamReader(p.getInputStream());
                BufferedReader reader = new BufferedReader(str);
                String linha;
                while ((linha = reader.readLine()) != null) {
                    System.out.println(linha);
                    final String lin = linha;
                    Platform.runLater(()
                            -> {
                        ta.appendText(lin + "\n");
                    });
                }
                Platform.runLater(()
                        -> {
                    ta.appendText("Completo....." + "\n");
                });
            }
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Realizar Backup");
            return false;
        }
        return true;
    }

    public boolean realizaBackupRestauracao(String arqlote) {
        Runtime r = Runtime.getRuntime();
        try {
            Process p = r.exec("backup\\" + arqlote);
            if (p != null) {
                InputStreamReader str = new InputStreamReader(p.getErrorStream());
                BufferedReader reader = new BufferedReader(str);
                String linha;
                while ((linha = reader.readLine()) != null) {
                    System.out.println(linha);
                }
                System.out.println("Completo....." + "\n");
            }
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Realizar Restauração");
            return false;
        }
        return true;
    }

    public boolean realizaBackup(String arquivo) {
        try {
            Process p = null;
            ProcessBuilder pb;
            pb = new ProcessBuilder("backup\\pg_dump.exe", "--host", "localhost", "--port",
                    "5432", "--format", "custom", "--blobs", "--verbose", "--file", "backup\\" + arquivo, "petshop");
            pb.environment().put("PGUSER", "postgres");
            pb.environment().put("PGPASSWORD", "postgres123");
            pb.redirectErrorStream(true);
            p = pb.start();
            if (p != null) {
                InputStreamReader str = new InputStreamReader(p.getInputStream());
                BufferedReader reader = new BufferedReader(str);
                String linha;
                while ((linha = reader.readLine()) != null) {
                    System.out.println(linha);
                }
                System.out.println("Completo....." + "\n");
            }
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Realizar Backup");
            return false;
        }
        return true;
    }

    public void shutdown() {
        if (RealizarBackupFinalizarSistema) {
            realizaBackup("Auto_Backup_" + Variables.getDataSimple(new Date()) + ".custom");
        }
    }

    public void start() {
        if (RealizarBackupIniciarSistema) {
            realizaBackup("Auto_Backup_" + Variables.getDataSimple(new Date()) + ".custom");
        }
    }

    public class nodeTabela extends ButtonAcaoAbstract<ItemCarrinho> {

        protected String Nome;
        protected String Data;
        protected File Arquivo;
        protected BasicFileAttributes attr;

        public nodeTabela(String Nome, File Arquivo) {
            this.Nome = Nome;
            this.Arquivo = Arquivo;
            try {
                attr = Files.readAttributes(Arquivo.toPath(), BasicFileAttributes.class);
                this.Data = Variables.getData(new Date(getAttr().creationTime().toMillis()));
            } catch (IOException ex) {
                Mensagem.ExibirException(ex, "nodeTabela:213");
            }
        }

        /**
         * @return the Nome
         */
        public String getNome() {
            return Nome;
        }

        /**
         * @param Nome the Nome to set
         */
        public void setNome(String Nome) {
            this.Nome = Nome;
        }

        /**
         * @return the Arquivo
         */
        public File getArquivo() {
            return Arquivo;
        }

        /**
         * @param Arquivo the Arquivo to set
         */
        public void setArquivo(File Arquivo) {
            this.Arquivo = Arquivo;
        }

        /**
         * @return the Data
         */
        public String getData() {
            return Data;
        }

        /**
         * @param Data the Data to set
         */
        public void setData(String Data) {
            this.Data = Data;
        }

        /**
         * @return the attr
         */
        public BasicFileAttributes getAttr() {
            return attr;
        }

        @Override
        protected void AddNodes() {
            addNode("Button", "res", "Restaurar");
            addNode("Button", "ex", "Excluir");
        }
    }

    /**
     * @return the Options
     */
    public ArrayList<String> getOptions() {
        return Options;
    }

    /**
     * @return the UltimosBackups
     */
    public int getUltimosBackups() {
        return UltimosBackups;
    }

    public String getultimosBackups() {
        return UltimosBackups + " Backups";
    }

    /**
     * @param UltimosBackups the UltimosBackups to set
     */
    public void setUltimosBackups(int UltimosBackups) {
        this.UltimosBackups = UltimosBackups;
        writeBackupOptions();
    }

    /**
     * @return the RealizarBackupIniciarSistema
     */
    public boolean isRealizarBackupIniciarSistema() {
        return RealizarBackupIniciarSistema;
    }

    /**
     * @param RealizarBackupIniciarSistema the RealizarBackupIniciarSistema to
     * set
     */
    public void setRealizarBackupIniciarSistema(boolean RealizarBackupIniciarSistema) {
        this.RealizarBackupIniciarSistema = RealizarBackupIniciarSistema;
        writeBackupOptions();
    }

    /**
     * @return the RealizarBackupFinalizarSistema
     */
    public boolean isRealizarBackupFinalizarSistema() {
        return RealizarBackupFinalizarSistema;
    }

    /**
     * @param RealizarBackupFinalizarSistema the RealizarBackupFinalizarSistema
     * to set
     */
    public void setRealizarBackupFinalizarSistema(boolean RealizarBackupFinalizarSistema) {
        this.RealizarBackupFinalizarSistema = RealizarBackupFinalizarSistema;
        writeBackupOptions();
    }

}
