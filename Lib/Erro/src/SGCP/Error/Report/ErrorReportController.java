/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP.Error.Report;

import Utils.Arquivo;
import Utils.Controladora.CtrlUtils;
import Utils.UI.Tela.IniciarTela;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Remote
 */
public class ErrorReportController implements Initializable {

    @FXML
    private TextArea txbErro;
    @FXML
    private CheckBox cbImagem;
    @FXML
    private Tooltip tipException;

    private ArrayList<String> Reports;
    private static Exception exception = null;
    private static String Message;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Reports = readLastUserReports();
        StringBuilder report = new StringBuilder();
        if(Message != null && !Message.trim().isEmpty()){
            report.append("Mensagem: ").append(Message);
        }
        if(exception != null){
            report.append("Exception: ").append(exception.toString());
        }
        tipException.setText(Message);
    }

    @FXML
    private void evtConfirmar(MouseEvent event) {
        if (txbErro.getText() != null && !txbErro.getText().trim().isEmpty()) {
            StringBuilder report = new StringBuilder()
                    .append(new Date()).append("#+#")
                    .append(": ").append("#+#")
                    .append(txbErro.getText()).append("#+#");
            if (exception != null) {
                report.append("Message: ")
                        .append("#+#")
                        .append(Message)
                        .append("#+#");
                report.append("Exception: ")
                        .append("#+#")
                        .append(exception.toString())
                        .append("#+#");
            }
            Reports.add(report.toString());
            writeReports(Reports);
        }
        evtCancelar(event);
    }

    public static void Create(Object object, Exception ex, String Message) {
        ErrorReportController.setException(exception);
        ErrorReportController.setMessage(Message);
        IniciarTela.loadWindow(object.getClass(), "/SGCP/Error/Report/ErrorReport.fxml",
                "Relatório de Erros", null);
    }

    @FXML
    private void evtCancelar(MouseEvent event) {
        exception = null;
        Message = "";
        CtrlUtils.CloseStage(event);
    }

    private void writeReports(ArrayList<String> Data) {
        Arquivo.gravaArquivoDeStringUTF("ErrorReportLog.db", Data);
    }

    private ArrayList<String> readLastUserReports() {
        ArrayList<String> Data = null;
        Data = Arquivo.leArquivoDeStringUTF("ErrorReportLog.db", 1);
        return Data == null ? new ArrayList() : Data;
    }

    /**
     * @param aException the exception to set
     */
    public static void setException(Exception aException) {
        exception = aException;
    }

    /**
     * @param aMessage the Message to set
     */
    public static void setMessage(String aMessage) {
        Message = aMessage;
    }
}
