/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Controladora;

import Venda.Entidade.Venda;
import Venda.Entidade.Produtodavenda;
import Controladora.Base.CtrlBase;
import Produto.Entidade.Produto;
import Transacao.Transaction;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlProdutodavenda extends CtrlBase {

    private static CtrlProdutodavenda ctrlprodutodavenda;

    public static CtrlProdutodavenda create() {
        if (ctrlprodutodavenda == null) {
            ctrlprodutodavenda = new CtrlProdutodavenda();
        }
        return ctrlprodutodavenda;
    }

    public CtrlProdutodavenda() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> produtodavenda = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Produtodavenda> Resultprodutodavenda = null;
            if (Filtro == null || Filtro.isEmpty()) {
                Resultprodutodavenda = em.createNamedQuery("Produtodavenda.findAll", Produtodavenda.class)
                        .getResultList();
            } else {
                try {
                    Resultprodutodavenda = em.createNamedQuery("Produtodavenda.findByPk", Produtodavenda.class)
                            .setParameter("vendaId", Integer.parseInt(Filtro))
                            .setParameter("produtoId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    Resultprodutodavenda = em.createNamedQuery("Produtodavenda.findAll", Produtodavenda.class)
                            .getResultList();
                }
            }

            for (Produtodavenda produtodavendas : Resultprodutodavenda) {
                produtodavenda.add(produtodavendas);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return produtodavenda;
    }

    public Object Salvar(int prodvQuantidade, BigDecimal prodvValor, Object aVenda, Object oProduto) {
        Produtodavenda produtodavenda = null;
        Produto produto = null;
        Venda venda = null;
        if(aVenda != null && aVenda instanceof Venda){
            venda = (Venda) aVenda;
        }
        if(oProduto != null && oProduto instanceof Produto){
            produto = (Produto) oProduto;
        }
        if(venda != null && produto != null){
           produtodavenda = new Produtodavenda(produto.getProdutoId(), venda.getVendaId(),
                   prodvQuantidade, prodvValor); 
           produtodavenda.setVenda(venda);
           produtodavenda.setProduto(produto);
           return super.Salvar(produtodavenda);
        }
        return produtodavenda;
    }

    public Object Alterar(Object oProdutodavenda, int prodvQuantidade, BigDecimal prodvValor,
            Object aVenda, Object oProduto) {
        Produtodavenda produtodavenda = null;
        Produto produto = null;
        Venda venda = null;
        if(aVenda != null && aVenda instanceof Venda){
            venda = (Venda) aVenda;
        }
        if(oProduto != null && oProduto instanceof Produto){
            produto = (Produto) oProduto;
        }
        if (oProdutodavenda != null && oProdutodavenda instanceof Produtodavenda &&
                venda != null && produto != null) {
            produtodavenda = (Produtodavenda) oProdutodavenda;
            produtodavenda.setProdvQuantidade(prodvQuantidade);
            produtodavenda.setProdvValor(prodvValor);
            produtodavenda.setVenda(venda);
            produtodavenda.setProduto(produto);
            produtodavenda = (Produtodavenda) super.Alterar(produtodavenda);
        }
        return produtodavenda;
    }

    public boolean Remover(Object Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Produtodavenda.class);
    }

}
