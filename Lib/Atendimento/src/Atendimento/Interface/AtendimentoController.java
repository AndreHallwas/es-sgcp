/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Atendimento.Interface;

import Animal.Controladora.CtrlAnimal;
import Animal.Interface.TelaConsultaAnimalController;
import Atendimento.Controladora.CtrlAtendimento;
import Utils.Controladora.CtrlUtils;
import Variables.Variables;
import Utils.DateUtils;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class AtendimentoController implements Initializable, Tela {

    @FXML
    private JFXTextArea taDescricaoGeral;
    @FXML
    private JFXTextArea taDescricaoVeterinario;
    @FXML
    private JFXTextArea taSintomas;
    @FXML
    private JFXTextArea taObservacoes;
    @FXML
    private JFXTextField txbAnimal;
    @FXML
    private DatePicker dtpData;
    @FXML
    private VBox pndados;
    
    private Object Animal;
    private Object Atendimento;
    private Object Agendamento;
    private int flag;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        EstadoOriginal();
    }

    private void EstadoOriginal() {
        CtrlUtils.clear(pndados.getChildren());
        if (dtpData.getValue() == null) {
            dtpData.setValue(LocalDate.now());
        }
        Atendimento = null;
        Agendamento = null;
        flag = 1;
    }

    @FXML
    private void HandleButtonPesquisarAnimal(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Animal/Interface/TelaConsultaAnimal.fxml",
                "Consulta de Animal", null);
        Animal = TelaConsultaAnimalController.getAnimal();
        CtrlAnimal.setCampoBusca(TelaConsultaAnimalController.getAnimal(), txbAnimal);
        EstadoConsulta();
    }

    @FXML
    private void HandleButtonCancelarClicked(MouseEvent event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, true)) {
            EstadoOriginal();
        }
    }

    @FXML
    private void HandleButtonSalvarClicked(MouseEvent event) {
        CtrlAtendimento cp = CtrlAtendimento.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if (null == cp.Alterar(Atendimento, CtrlAtendimento.getId(Atendimento), taDescricaoGeral.getText(),
                        taDescricaoVeterinario.getText(), taSintomas.getText(), taObservacoes.getText(),
                        DateUtils.asDate(dtpData.getValue()), Animal, Agendamento)) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    EstadoOriginal();
                    EstadoConsulta();
                }
            } else//cadastrar
            if (null == cp.Salvar(taDescricaoGeral.getText(), taDescricaoVeterinario.getText(), taSintomas.getText(),
                    taObservacoes.getText(), DateUtils.asDate(dtpData.getValue()), Animal, Agendamento)) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                EstadoOriginal();
                EstadoConsulta();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void HandleButtonDateTimePickerData(Event event) {
        EstadoConsulta();
    }

    private void EstadoConsulta() {
        if (Animal != null && dtpData.getValue() != null) {
            ArrayList<Object> Atendimentos = CtrlAtendimento.create().Pesquisar(
                    CtrlAnimal.getId(Animal) + "#:#" + DateUtils.asDate(dtpData.getValue()).getTime(), "Animal_Data");
            setCampos(Atendimentos);
        }
    }

    private void setCampos(ArrayList<Object> Atendimentos) {
        if (Atendimentos != null && !Atendimentos.isEmpty()) {
            Atendimento = Atendimentos.get(0);
            CtrlAtendimento.setCampos(Atendimento, taDescricaoGeral, taDescricaoVeterinario,
                    taObservacoes, taSintomas);
            flag = 0;
        } else {
            EstadoOriginal();
        }
    }

    public boolean validaCampos() {
        boolean fl = true;
        StringBuilder Result = new StringBuilder();

        if (taDescricaoGeral.getText().trim().isEmpty()) {
            Result.append("Campo Obrigatório Vazio : Descrição Geral, ");
            taDescricaoGeral.setText("");
            taDescricaoGeral.requestFocus();
            fl = false;
        }
        if (txbAnimal.getText().trim().isEmpty()) {
            Result.append("Campo Obrigatório Vazio : Pesquisa de Animal, ");
            txbAnimal.setText("");
            txbAnimal.requestFocus();
            fl = false;
        }
        if (dtpData.getValue() == null) {
            Result.append("Campo Obrigatório Vazio : Data; ");
            dtpData.requestFocus();
            fl = false;
        }
        if (!fl) {
            Mensagem.ExibirLog(Result.toString());
        }

        return fl;
    }

}
