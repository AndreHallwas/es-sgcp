/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP;

import Caixa.Controladora.CtrlCaixa;
import Empresa.Interface.TelaEmpresaController;
import Fornecedor.Interface.TelaConsultaFornecedorController;
import Pessoa.Interface.TelaConsultaClienteController;
import Pessoa.Interface.TelaConsultaUsuarioController;
import SGCP.Error.Report.ErrorReportController;
import Transacao.Transaction;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Variables.Variables;
import com.jfoenix.controls.JFXTextArea;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import javafx.event.Event;
import javafx.event.Event;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

/**
 *
 * @author Raizen
 */
public class MainMenuOptions {

    private MainMenuOptions() {
    }

    public static boolean evtLogin(Event event, Class Classe) {
        return false;
    }

    public static void evtBackup() {
        VBox painel = new VBox();
        TextArea ta = new JFXTextArea();
        painel.getChildren().add(ta);
        Variables._pndados.getChildren().clear();
        Variables._pndados.getChildren().add(painel);
        new Thread(()
                -> {
            if (Transaction.realizaBackupRestauracao("copiar.bat", ta)) {
                Mensagem.Exibir("Backup efetuado!", 1);
                ta.appendText("Concluido!");
            } else {
                Mensagem.Exibir("Erro ao efetuar o backup!", 2);
                ta.appendText("Erro!");
            }
        }).start();
    }

    public static void evtRestore() {
        VBox painel = new VBox();
        TextArea ta = new JFXTextArea();
        painel.getChildren().add(ta);
        Variables._pndados.getChildren().clear();
        Variables._pndados.getChildren().add(painel);
        new Thread(()
                -> {
            if (Transaction.realizaBackupRestauracao("restaurar.bat", ta)) {
                Mensagem.Exibir("Restauração efetuada!", 1);
                ta.appendText("Concluido!");
            } else {
                Mensagem.Exibir("Erro ao efetuar a restauração!", 2);
                ta.appendText("Erro!");
            }
        }).start();
    }

    public static void evtConfiguracao(Event event, Class Classe) {
        /* TelaConfiguracaoGeralController.setRoot(Variables._pndados);
        IniciarTela.Load(Classe, Variables._pndados, "TelaConfiguracaoGeral.fxml");
        if (Banco.conectar(TelaConfiguracaoGeralController.getStringConexao(),
                TelaConfiguracaoGeralController.getEndereco(),
                TelaConfiguracaoGeralController.getPorta(),
                TelaConfiguracaoGeralController.getUsuarioBanco(),
                TelaConfiguracaoGeralController.getSenhaBanco(),
                TelaConfiguracaoGeralController.getoBanco(), true)) {
            Mensagem.ExibirLog("Conexão Efetuada COm Sucesso");
        } else {
            Mensagem.ExibirLog("Conexão Não EFEtuada");
        }*/
    }

    public static void evtConfiguracaoInicial(Event event, Class Classe) {
        TelaConfiguracaoInicialController.Create(Classe);
    }

    public static void evtGerenciamentoEmpresa(Event event, Class Classe) {
        TelaEmpresaController.setRoot(Variables._pndados);
        IniciarTela.Load(Classe, Variables._pndados, "/Empresa/Interface/TelaEmpresa.fxml");
    }

    public static void evtGerenciamentoCliente(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Pessoa/Interface/TelaCadCliente.fxml");
    }

    public static void evtGerenciamentoUsuario(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Pessoa/Interface/TelaCadUsuario.fxml");
    }

    public static void evtGerenciamentoNivel(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Acesso/Interface/TelaCadNivel.fxml");
    }

    public static void evtGerenciamentoProduto(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Produto/Interface/TelaProduto.fxml");
    }

    public static void evtGerenciamentoFornecedor(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Fornecedor/Interface/TelaCadFornecedor.fxml");
    }

    public static void evtGerenciamentoServico(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Servico/Interface/TelaServico.fxml");
    }

    public static void evtGerenciamentoAnimal(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Animal/Interface/TelaAnimal.fxml");
    }

    public static void evtGerenciamentoTipodeDespesa(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Despesa/Interface/TelaCadTipodeDespesa.fxml");
    }

    public static void evtGerenciamentoFormadepagamento(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Formadepagamento/Interface/TelaCadFormadepagamento.fxml");
    }

    public static void evtConsultaUsuario(Event event, Class Classe) {
        TelaConsultaUsuarioController.setBtnPainel(false);
        IniciarTela.Load(Classe, Variables._pndados, "/Pessoa/Interface/TelaConsultaUsuario.fxml");
    }

    public static void evtConsultaCliente(Event event, Class Classe) {
        TelaConsultaClienteController.setBtnPainel(false);
        IniciarTela.Load(Classe, Variables._pndados, "/Pessoa/Interface/TelaConsultaCliente.fxml");
    }

    public static void evtConsultaFornecedor(Event event, Class Classe) {
        TelaConsultaFornecedorController.setBtnPainel(false);
        IniciarTela.Load(Classe, Variables._pndados, "/Fornecedor/Interface/TelaConsultaFornecedor.fxml");
    }

    public static void evtConsultaFabricante(Event event, Class Classe) {
        /*TelaConsultaFabricanteController.setBtnPainel(false);
        IniciarTela.Load(Classe, Variables._pndados, "/Produto/UI/Consulta/TelaConsultaFabricante.fxml");*/
    }

    public static void evtConsultaProduto(Event event, Class Classe) {
        /* TelaConsultaProdutoController.setBtnPainel(false);
        IniciarTela.Load(Classe, Variables._pndados, "/Produto/UI/Consulta/TelaConsultaProduto.fxml");*/
    }

    public static void evtRelFichadoUsuario(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Pessoa/UI/Relatorio/TelaRelatFichaUsuario.fxml");
    }

    public static void evtVenda(Event event, Class Classe) {
        /*TelaVendaController.Create(Classe, null, null,
                "/Transacao/UI/TelaVenda.fxml", "/Engenharia/UI/Estilos/dark-theme.css");*/
        IniciarTela.Load(Classe, Variables._pndados, "/Venda/Interface/TelaVenda.fxml");
    }

    public static void evtCompra(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Compra/Interface/TelaCompra.fxml");
    }

    public static void evtPagamento(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Compra/Interface/TelaQuitarParcelasAPagar.fxml");
    }

    public static void evtRecebimento(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Movimentacao/UI/TelaRecebimento.fxml");
    }

    public static void evtAgendamento(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Agendamento/Interface/TelaCadAgendamento.fxml");
    }

    public static void evtAbrirCaixa(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Caixa/Interface/TelaAbrirCaixa.fxml");
    }

    public static void evtFecharCaixa(Event event, Class Classe) {
        CtrlCaixa cc = CtrlCaixa.create();
        if (cc.getCaixaAberto()) {
            IniciarTela.Load(Classe, Variables._pndados, "/Caixa/Interface/TelaFecharCaixa.fxml");
        }else{
            Mensagem.Exibir(cc.getMsg(), 2);
        }
    }

    public static void evtAtendimento(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Atendimento/Interface/Atendimento.fxml");
    }

    public static void evtFichadoAnimal(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Animal/FichadoAnimal/Interface/TelaFichadoAnimal.fxml");
    }

    public static void evtRegistrarDespesa(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Despesa/Interface/TelaDespesa.fxml");
    }

    public static void evtCadernetadeVacinacao(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Vacina/Interface/TelaVacina.fxml");
    }

    public static void evtAtestadoSaude(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Animal/Relatorio/TelaRelatorioAtestadodeSaude.fxml");
    }

    public static void evtRelInternação(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Animal/Relatorio/TelaRelatorioInternacao.fxml");
    }

    public static void evtRelCirurgico(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Animal/Relatorio/TelaRelatorioAtoCirurgico.fxml");
    }

    public static void evtRelSacrificacao(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Animal/Relatorio/TelaRelatorioSacrificacao.fxml");
    }

    public static void evtRelMovimentacao(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Caixa/Relatorio/TelaRelatorioMovimentacoes.fxml");
    }

    public static void evtRelFluxoDoCaixa(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Caixa/Relatorio/TelaRelatorioFluxoDoCaixa.fxml");
    }

    public static void evtRelVacinacao(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Vacina/Relatorio/TelaRelatorioVacinacao.fxml");
    }

    public static void evtBackupERestore(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/SGCP/TelaBackupERestore.fxml");
    }

    public static void evtRelVendas(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Venda/Relatorio/TelaRelatorioVenda.fxml");
    }

    public static void evtRelAtendimentos(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Animal/Relatorio/TelaRelatorioAtendimento.fxml");
    }

    public static void evtRelAgendamentos(Event event, Class Classe) {
        IniciarTela.Load(Classe, Variables._pndados, "/Animal/Relatorio/TelaRelatorioAgendamento.fxml");
    }

    public static void evtReportErro(Event event, Class Classe) {
        IniciarTela.loadWindow(Classe, "/SGCP/Error/Report/ErrorReport.fxml",
                "Relatório de Erros", null);
    }

    public static void evtAjuda(Event event, Class Classe) {
        String s;
        Process p;
        try {
            p = Runtime.getRuntime().exec("Ajuda\\Manual.bat");
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            p.waitFor();
            System.out.println("exit: " + p.exitValue());
            p.destroy();
        } catch (Exception e) {
            Mensagem.ExibirException(e, "evtAjuda:232");
        }
    }

}
