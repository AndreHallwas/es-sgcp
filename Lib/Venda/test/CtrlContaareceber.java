package Contaareceber.Controladora;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import ContaaReceber.Entidade.Contaareceber;
import ContaaReceber.Entidade.Parcelaareceber;
import Controladora.Base.CtrlBase;
import FormadePagamento.Entidade.Formadepagamento;
import Transacao.Transaction;
import Utils.Mensagem;
import Venda.Entidade.Venda;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlContaareceber extends CtrlBase {

    private static CtrlContaareceber ctrlcontaareceber;

    public static CtrlContaareceber create() {
        if (ctrlcontaareceber == null) {
            ctrlcontaareceber = new CtrlContaareceber();
        }
        return ctrlcontaareceber;
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Contaareceber) {
            return ((Contaareceber) p).getContaareceberId();
        }
        return -1;
    }

    public static String getValorTotal(ArrayList<Object> asDespesas) {
        BigDecimal ValorTotal = new BigDecimal(0);
        if (asDespesas != null && !asDespesas.isEmpty()) {
            Contaareceber aConta = null;
            for (Object aDespesa : asDespesas) {
                aConta = (Contaareceber) aDespesa;
                ValorTotal = ValorTotal.add(aConta.getContaareceberValortotal());
            }
        }
        return ValorTotal.toPlainString();
    }

    public CtrlContaareceber() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> contaareceber = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Contaareceber> ResultContaareceber = new ArrayList();
            if (Filtro == null || Filtro.isEmpty()) {
                ResultContaareceber = em.createNamedQuery("Contaareceber.findAll", Contaareceber.class)
                        .getResultList();
            } else {
                try {
                    if (Tipo.equalsIgnoreCase("Data")) {
                        ResultContaareceber = em.createNamedQuery("Contaareceber.findByContaareceberData", Contaareceber.class)
                                .setParameter("contaareceberData", new Date(Long.parseLong(Filtro))).getResultList();
                    } else {
                        ResultContaareceber = em.createNamedQuery("Contaareceber.findByContaareceberId", Contaareceber.class)
                                .setParameter("contaareceberId", Integer.parseInt(Filtro)).getResultList();
                    }
                } catch (Exception ex) {
                    ResultContaareceber = em.createNamedQuery("Contaareceber.findByContaareceberId", Contaareceber.class)
                            .setParameter("contaareceberId", Integer.parseInt(Filtro)).getResultList();
                }
            }

            for (Contaareceber contaarecebers : ResultContaareceber) {
                contaareceber.add(contaarecebers);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return contaareceber;
    }

    public Object Salvar(BigDecimal Valortotal, Boolean Situacao, Integer Acrescimo, Integer Desconto,
            Integer Juros, String Descricao, int Parcelas, Object formadepagamento, Object venda) {
        Contaareceber contaareceber = new Contaareceber(Valortotal, Situacao, Acrescimo,
                Desconto, Juros, new Date(), Descricao, Parcelas);
        if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
            contaareceber.setFormadepagamentoId((Formadepagamento) formadepagamento);
        }
        if (venda != null && venda instanceof Venda) {
            contaareceber.setVendaId((Venda) venda);
        }
        return super.Salvar(contaareceber);
    }

    public Object Alterar(Object Conta, BigDecimal Valortotal, Boolean Situacao, Integer Acrescimo, Integer Desconto,
            Integer Juros, Date Data, String Descricao, Integer Parcelas, Object formadepagamento, Object venda) {
        Contaareceber contaareceber = null;
        if (Conta != null && Conta instanceof Contaareceber) {

            contaareceber = (Contaareceber) Conta;
            contaareceber.setContaareceberAcrescimo(Acrescimo);
            contaareceber.setContaareceberDesconto(Desconto);
            contaareceber.setContaareceberJuros(Juros);
            contaareceber.setContaareceberParcelas(Parcelas);
            contaareceber.setContaareceberSituacao(Situacao);
            contaareceber.setContaareceberValortotal(Valortotal);
            contaareceber.setContaareceberDescricao(Descricao);
            contaareceber.setContaareceberData(Data);

            if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
                contaareceber.setFormadepagamentoId((Formadepagamento) formadepagamento);
            }
            if (venda != null && venda instanceof Venda) {
                contaareceber.setVendaId((Venda) venda);
            }
            contaareceber = (Contaareceber) super.Alterar(contaareceber);
        }
        return contaareceber;
    }

    public Boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    public Object RegistrarParcelas(Object venda, String Descricao, Object tipodedespesa,
            Object formadepagamento, ArrayList<Object> valores, ArrayList<Object> datas, String Param) {
        Boolean flag = true;
        EntityManager em = null;

        try {
            /////Inicia a Transacao
            em = getEntityManager();
            em.getTransaction().begin();
            Contaareceber conta = null;
            if (venda != null && venda instanceof Venda) {
                ArrayList<Object> auxvendas = Pesquisar(((Venda) venda).getVendaId() + "", "Id");
                if (auxvendas != null && auxvendas.size() > 0) {
                    Venda auxvenda = (Venda) auxvendas.get(0);
                    if (auxvenda.getContaareceberCollection() == null
                            || auxvenda.getContaareceberCollection().isEmpty()) {
                        conta = Salvar(venda, Descricao, formadepagamento, valores, datas, em);
                        flag = flag && conta != null;
                    } else {
                        flag = false;
                    }
                }
            }else {
                flag = false;
            }
            /*if (Param != null && !Param.trim().isEmpty() && Param.equalsIgnoreCase("avista")) {
                flag = flag && AVista(conta, em);
            }*/
            /////Finaliza a Transação
            em.getTransaction().commit();

            Message = flag ? "Registro Realizado com Sucesso" : "Erro, o Registro não foi Realizado";

        } catch (Exception ex) {
            if (em != null) {
                Mensagem.ExibirException(ex, ":CtrlContaareceber:182");
                em.getTransaction().rollback();
            }
            Message = "Erro, o Registro não foi Realizado";
        } finally {
            if (em != null) {
                em.close();
            }
        }

        return flag ? flag : null;
    }

    protected Boolean AVista(Contaareceber conta, EntityManager em) throws Exception {
        /*receber a Conta caso seja a Vista*/
        Boolean flag = true;
        if (conta != null) {
            for (Parcelaareceber parcelaareceber : conta.getParcelaareceberCollection()) {
                flag = flag && CtrlParcelaareceber.create().receber(parcelaareceber,
                        parcelaareceber.getParrValor().toString(), em);
            }
        } else {
            flag = false;
        }
        return flag;
    }

    protected Contaareceber Salvar(Object venda, String Descricao, Object formadepagamento,
            ArrayList<Object> valores, ArrayList<Object> datas, EntityManager em) throws Exception {
        Contaareceber conta = null;
        if (valores != null && datas != null && valores.size() == datas.size()) {
            ///Adiciona as Novas
            BigDecimal oValor = new BigDecimal(0);
            for (Object valor : valores) {
                oValor = oValor.add(new BigDecimal((Double) valor));
            }
            /////Gera a Conta a receber
            conta = new Contaareceber(oValor, false, 0, 0, 0, new Date(), Descricao, valores.size());
            if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
                conta.setFormadepagamentoId((Formadepagamento) formadepagamento);
            }
            if (venda != null && venda instanceof Venda) {
                conta.setVendaId((Venda) venda);
            }
            /////Persiste a Conta a receber
            em.persist(conta);
            em.flush();

            for (int i = 0; i < valores.size(); i++) {
                double valor = (Double) valores.get(i);
                /////Gera a Parcela a receber
                Parcelaareceber parcelaareceber = new Parcelaareceber(conta.getContaareceberId(), i + 1, (Date) datas.get(i),
                        null, new BigDecimal(valor), false, BigDecimal.ZERO, BigDecimal.ZERO, new Date());

                if (formadepagamento != null && formadepagamento instanceof Formadepagamento) {
                    parcelaareceber.setFormadepagamentoId((Formadepagamento) formadepagamento);
                }
                parcelaareceber.setContaareceber((Contaareceber) conta);

                /////Persiste a Parcela a receber
                em.persist(parcelaareceber);
            }
        }
        return conta;
    }

    public Object ModificarParcelas(Object venda, String Descricao, Object tipodedespesa, Object formadepagamento, ArrayList<Object> valores,
            ArrayList<Object> datas, String Param) {
        Boolean flag = true;
        if (venda != null && venda instanceof Venda) {
            ArrayList<Object> auxvendas = Pesquisar(((Venda) venda).getVendaId() + "", "Id");
            if (auxvendas != null && auxvendas.size() > 0) {
                Venda auxvenda = (Venda) auxvendas.get(0);/////Altera as Parcelas
                EntityManager em = null;

                try {
                    /////Inicia a Transacao
                    em = getEntityManager();
                    em.getTransaction().begin();
                    Contaareceber conta = null;
                    ///Remove as Parcelas
                    if (auxvenda.getContaareceberCollection() != null) {
                        for (Contaareceber contaareceber : auxvenda.getContaareceberCollection()) {
                            /////Remover a Conta a receber
                            em.remove(contaareceber);
                        }
                    }

                    conta = Salvar(venda, Descricao, formadepagamento, valores, datas, em);
                    flag = flag && conta != null;

                    /*if (Param != null && !Param.trim().isEmpty() && Param.equalsIgnoreCase("avista")) {
                        flag = flag && AVista(conta, em);
                    }*/
                    
                    /////Finaliza a Transação
                    em.getTransaction().commit();

                    Message = flag ? "Modificação Realizada com Sucesso" : "Erro, a Modificação não foi Realizada";

                } catch (Exception ex) {
                    if (em != null) {
                        Mensagem.ExibirException(ex, ":CtrlContaareceber:267");
                        em.getTransaction().rollback();
                    }
                    Message = "Erro, a Modificação não foi Realizada";
                } finally {
                    if (em != null) {
                        em.close();
                    }
                }
            }
        }
        return flag ? flag : null;
    }

    public Boolean VerificaProximoVencimento(Object Parcela) {
        /*
        *Verifica se a parcela é o Proximo Vencimento.
         */
        if (Parcela != null && Parcela instanceof Parcelaareceber) {
            Parcelaareceber aParcela = (Parcelaareceber) Parcela;
            Contaareceber aConta = (Contaareceber) aParcela.getContaareceber();
            ArrayList<Parcelaareceber> Parcelas = new ArrayList(aConta.getParcelaareceberCollection());
            if (Parcelas.size() > 0) {
                Long Mils = null;
                Boolean flag = true;
                for (int i = 0; i < Parcelas.size(); i++) {
                    if (!Parcelas.get(i).getParrSituacao()
                            && (flag || Parcelas.get(i).getParrDataVencimento().getTime() < Mils)) {
                        Mils = Parcelas.get(i).getParrDataVencimento().getTime();
                        flag = false;
                    }
                }
                if (aParcela.getParrDataVencimento().getTime() <= Mils) {
                    return true;
                }
            }
        }
        return false;
    }

    public Boolean VerificaUltimorecebimento(Object Parcela) {
        /*
        *Verifica se a parcela é a ultima paga.
         */
        if (Parcela != null && Parcela instanceof Parcelaareceber) {
            Parcelaareceber aParcela = (Parcelaareceber) Parcela;
            Contaareceber aConta = (Contaareceber) aParcela.getContaareceber();
            ArrayList<Parcelaareceber> Parcelas = new ArrayList(aConta.getParcelaareceberCollection());
            if (Parcelas.size() > 0) {
                Long Mils = null;
                Long Mils2 = null;
                Boolean flag = true;

                for (int i = 0; i < Parcelas.size(); i++) {

                    if (Parcelas.get(i).getParrSituacao()
                            && (flag || Parcelas.get(i).getParrDataVencimento().getTime() >= Mils)) {

                        if (Mils != null && Parcelas.get(i).getParrDataVencimento().getTime() == Mils) {

                            if (Parcelas.get(i).getParcelaareceberPK().getParrDatadegeracao().getTime() > Mils2) {

                                Mils = Parcelas.get(i).getParrDataVencimento().getTime();
                                Mils2 = Parcelas.get(i).getParcelaareceberPK().getParrDatadegeracao().getTime();
                            }
                        } else {

                            Mils = Parcelas.get(i).getParrDataVencimento().getTime();
                            Mils2 = Parcelas.get(i).getParcelaareceberPK().getParrDatadegeracao().getTime();
                        }

                        flag = false;
                    }
                }
                if (aParcela.getParrDataVencimento().getTime() >= Mils) {
                    return true;
                }
            }
        }
        return false;
    }

    public ArrayList<Object> getValorParcelaaVista(Double Valor) {
        ArrayList<Object> aParcela = new ArrayList();
        aParcela.add(Valor);
        return aParcela;
    }

    public ArrayList<Object> getDataParcelaaVista(Date aData) {
        ArrayList<Object> data = new ArrayList();
        data.add(aData);
        return data;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Contaareceber.class);
    }

}
