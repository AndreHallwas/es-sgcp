/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pessoa.Interface;

import Pessoa.Controladora.CtrlUsuario;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaUsuarioController implements Initializable {

    @FXML
    private TextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcRG;
    @FXML
    private TableColumn<Object, String> tcCPF;
    @FXML
    private TableColumn<Object, String> tcTelefone;
    @FXML
    private TableColumn<Object, String> tcEndereco;
    @FXML
    private TableColumn<Object, String> tcEmail;
    @FXML
    private TableColumn<Object, Double> tcSalario;
    @FXML
    private TableColumn<Object, String> tcNivel;
    @FXML
    private TableColumn<Object, String> tcObs;
    @FXML
    private TableColumn<Object, String> tcLogin;
    private static Object usuario;
    private static boolean btnPainel = true;
    @FXML
    private HBox btnGp;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         tcNome.setCellValueFactory(new PropertyValueFactory("UsrNome"));
        tcEmail.setCellValueFactory(new PropertyValueFactory("UsrEmail"));
        tcEndereco.setCellValueFactory(new PropertyValueFactory("EndId"));
        tcNivel.setCellValueFactory(new PropertyValueFactory("GrupoId"));
        tcCPF.setCellValueFactory(new PropertyValueFactory("UsrCpf"));
        tcObs.setCellValueFactory(new PropertyValueFactory("UsrObs"));
        tcRG.setCellValueFactory(new PropertyValueFactory("UsrRg"));
        tcSalario.setCellValueFactory(new PropertyValueFactory("UsrSalario"));
        tcTelefone.setCellValueFactory(new PropertyValueFactory("UsrTelefone"));
        tcLogin.setCellValueFactory(new PropertyValueFactory("UsrLogin"));
        CarregaTabela("");
        usuario = null;
        btnGp.setVisible(btnPainel);
    }    

    private void CarregaTabela(String filtro) {
        CtrlUsuario ctm = CtrlUsuario.create();
        try
        {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        }
        catch(Exception ex)
        {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }
    
    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if(lin > -1){
            usuario = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if(usuario!=null){
            evtCancelar(event);
        }else{
            Mensagem.Exibir("Nenhum Usuario Selecionado", 2);
        }
    }

    public static TelaConsultaUsuarioController Create(Class classe, Pane pndados) {
        TelaConsultaUsuarioController CUsuario = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Pessoa/Interface/TelaConsultaUsuarioFXML.fxml"));
            Parent root = Loader.load();
            CUsuario = (TelaConsultaUsuarioController)Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CUsuario;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaUsuarioController.btnPainel = btnPainel;
    }

    public static Object getUsuario() {
        return usuario;
    }
    
}
