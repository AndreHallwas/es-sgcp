/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "especie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Especie.findAll", query = "SELECT e FROM Especie e")
    , @NamedQuery(name = "Especie.findByEspecieId", query = "SELECT e FROM Especie e WHERE e.especieId = :especieId")
    , @NamedQuery(name = "Especie.findByEspecieNome", query = "SELECT e FROM Especie e WHERE e.especieNome = :especieNome")
    , @NamedQuery(name = "Especie.findByEspecieDescricao", query = "SELECT e FROM Especie e WHERE e.especieDescricao = :especieDescricao")})
public class Especie extends JPADao implements Serializable {

    @Lob
    @Column(name = "especie_imagem")
    private byte[] especieImagem;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "especie_id")
    private Integer especieId;
    @Basic(optional = false)
    @Column(name = "especie_nome")
    private String especieNome;
    @Column(name = "especie_descricao")
    private String especieDescricao;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "especieId")
    private Collection<Animal> animalCollection = new ArrayList();

    public Especie() {
    }

    public Especie(Integer especieId) {
        this.especieId = especieId;
    }

    public Especie(String especieNome, String especieDescricao) {
        this.especieNome = especieNome;
        this.especieDescricao = especieDescricao;
    }

    public Especie(String especieNome, String especieDescricao, byte[] especieImagem) {
        this.especieImagem = especieImagem;
        this.especieNome = especieNome;
        this.especieDescricao = especieDescricao;
    }

    public Especie(Integer especieId, String especieNome, String especieDescricao) {
        this.especieId = especieId;
        this.especieNome = especieNome;
        this.especieDescricao = especieDescricao;
    }

    public Especie(Integer especieId, String especieNome) {
        this.especieId = especieId;
        this.especieNome = especieNome;
    }

    public Integer getEspecieId() {
        return especieId;
    }

    public void setEspecieId(Integer especieId) {
        this.especieId = especieId;
    }

    public String getEspecieNome() {
        return especieNome;
    }

    public void setEspecieNome(String especieNome) {
        this.especieNome = especieNome;
    }

    public String getEspecieDescricao() {
        return especieDescricao;
    }

    public void setEspecieDescricao(String especieDescricao) {
        this.especieDescricao = especieDescricao;
    }

    @XmlTransient
    public Collection<Animal> getAnimalCollection() {
        return animalCollection;
    }

    public void setAnimalCollection(Collection<Animal> animalCollection) {
        this.animalCollection = animalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (especieId != null ? especieId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Especie)) {
            return false;
        }
        Especie other = (Especie) object;
        if ((this.especieId == null && other.especieId != null) || (this.especieId != null && !this.especieId.equals(other.especieId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return especieNome;
    }

    public byte[] getEspecieImagem() {
        return especieImagem;
    }

    public void setEspecieImagem(byte[] especieImagem) {
        this.especieImagem = especieImagem;
    }
}
