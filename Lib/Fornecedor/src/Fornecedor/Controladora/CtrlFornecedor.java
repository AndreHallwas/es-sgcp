/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fornecedor.Controladora;

import Controladora.Base.CtrlBase;
import Endereco.Entidade.Complementoendereco;
import Fornecedor.Entidade.Fornecedor;
import Transacao.Transaction;
import Utils.Imagem;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlFornecedor extends CtrlBase {

    private static CtrlFornecedor ctrlfornecedor;

    public static CtrlFornecedor create() {
        if (ctrlfornecedor == null) {
            ctrlfornecedor = new CtrlFornecedor();
        }
        return ctrlfornecedor;
    }

    public CtrlFornecedor() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object fornecedor, TextField txbNome, TextField txbCNPJ, TextField txbEmail, TextField txbTelefone, TextArea taObs) {
        if (fornecedor != null && fornecedor instanceof Fornecedor) {
            Fornecedor f = (Fornecedor) fornecedor;
            txbNome.setText(f.getForNome());
            txbCNPJ.setText(f.getForCnpj());
            txbEmail.setText(f.getForEmail());
            txbTelefone.setText(f.getForTelefone());
            taObs.setText(f.getForObs());
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Fornecedor) {
            return ((Fornecedor) p).getForId();
        }
        return -1;
    }

    public static Object getEndereco(Object p) {
        if (p != null && p instanceof Fornecedor) {
            return ((Fornecedor) p).getEndId();
        }
        return null;
    }

    public static BufferedImage getImagem(Object p) {
        if (p != null && p instanceof Fornecedor) {
            return Imagem.ByteArrayToBufferedImage(((Fornecedor) p).getForImagem());
        }
        return null;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Fornecedor) {
            txBusca.setText(((Fornecedor) p).getForNome());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> fornecedor = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Fornecedor> ResultFornecedor;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultFornecedor = em.createNamedQuery("Fornecedor.findAll", Fornecedor.class)
                        .getResultList();
            } else {
                ResultFornecedor = em.createNamedQuery("Fornecedor.findByForGeral", Fornecedor.class)
                        .setParameter("filtro", Filtro).getResultList();
            }
            for (Fornecedor fornecedors : ResultFornecedor) {
                fornecedor.add(fornecedors);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return fornecedor;
    }

    public Object Salvar(String Cnpj, String Nome, String Telefone, String Email, String Obs, byte[] Imagem, Object oEndereco) {
        Fornecedor fornecedor = new Fornecedor(Cnpj, Nome, Telefone, Email, Obs, Imagem);
        if (oEndereco != null && oEndereco instanceof Complementoendereco) {
            fornecedor.setEndId((Complementoendereco) oEndereco);
        }
        return super.Salvar(fornecedor);
    }

    public Object Alterar(Object oFornecedor, Integer Id, String Cnpj, String Nome, String Telefone,
            String Email, String Obs, byte[] Imagem, Object oEndereco) {
        Fornecedor fornecedor = null;
        if (oFornecedor != null && oFornecedor instanceof Fornecedor) {
            fornecedor = (Fornecedor) oFornecedor;
            fornecedor.setForCnpj(Cnpj);
            fornecedor.setForEmail(Email);
            fornecedor.setForImagem(Imagem);
            fornecedor.setForNome(Nome);
            fornecedor.setForObs(Obs);
            fornecedor.setForTelefone(Telefone);
            if (oEndereco != null && oEndereco instanceof Complementoendereco) {
                fornecedor.setEndId((Complementoendereco) oEndereco);
            }
            fornecedor = (Fornecedor) super.Alterar(fornecedor);
        }
        return fornecedor;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Fornecedor.class);
    }
}
