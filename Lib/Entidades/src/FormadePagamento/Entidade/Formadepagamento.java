/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FormadePagamento.Entidade;

import ContaaReceber.Entidade.Contaareceber;
import ContaaReceber.Entidade.Parcelaareceber;
import ContaaPagar.Entidade.Parcelaapagar;
import ContaaPagar.Entidade.Contaapagar;
import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "formadepagamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Formadepagamento.findAll", query = "SELECT f FROM Formadepagamento f")
    , @NamedQuery(name = "Formadepagamento.findByFormadepagamentoId", query = "SELECT f FROM Formadepagamento f WHERE f.formadepagamentoId = :formadepagamentoId")
    , @NamedQuery(name = "Formadepagamento.findByFormadepagamentoDesc", query = "SELECT f FROM Formadepagamento f WHERE f.formadepagamentoDesc = :formadepagamentoDesc")
    , @NamedQuery(name = "Formadepagamento.findByFormadepagamentoNome", query = "SELECT f FROM Formadepagamento f WHERE f.formadepagamentoNome = :formadepagamentoNome")})
public class Formadepagamento extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "formadepagamento_id")
    private Integer formadepagamentoId;
    @Column(name = "formadepagamento_desc")
    private String formadepagamentoDesc;
    @Basic(optional = false)
    @Column(name = "formadepagamento_nome")
    private String formadepagamentoNome;
    @Basic(optional = false)
    @Column(name = "formadepagamento_recebimentoaprazo")
    private boolean formadepagamentoRecebimentoaprazo;
    @Basic(optional = false)
    @Column(name = "formadepagamento_parcelado")
    private boolean formadepagamentoParcelado;
    
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "formadepagamentoId")
    private Collection<Parcelaapagar> parcelaapagarCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "formadepagamentoId")
    private Collection<Contaareceber> contaareceberCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "formadepagamentoId")
    private Collection<Parcelaareceber> parcelaareceberCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "formadepagamentoId")
    private Collection<Contaapagar> contaapagarCollection = new ArrayList();

    public Formadepagamento() {
    }

    public Formadepagamento(Integer formadepagamentoId) {
        this.formadepagamentoId = formadepagamentoId;
    }

    public Formadepagamento(String formadepagamentoDesc, String formadepagamentoNome, boolean formadepagamentoRecebimentoaprazo, boolean formadepagamentoParcelado) {
        this.formadepagamentoDesc = formadepagamentoDesc;
        this.formadepagamentoNome = formadepagamentoNome;
        this.formadepagamentoRecebimentoaprazo = formadepagamentoRecebimentoaprazo;
        this.formadepagamentoParcelado = formadepagamentoParcelado;
    }

    public Formadepagamento(Integer formadepagamentoId, String formadepagamentoDesc, String formadepagamentoNome, boolean formadepagamentoRecebimentoaprazo, boolean formadepagamentoParcelado) {
        this.formadepagamentoId = formadepagamentoId;
        this.formadepagamentoDesc = formadepagamentoDesc;
        this.formadepagamentoNome = formadepagamentoNome;
        this.formadepagamentoRecebimentoaprazo = formadepagamentoRecebimentoaprazo;
        this.formadepagamentoParcelado = formadepagamentoParcelado;
    }

    public Integer getFormadepagamentoId() {
        return formadepagamentoId;
    }

    public void setFormadepagamentoId(Integer formadepagamentoId) {
        this.formadepagamentoId = formadepagamentoId;
    }

    public String getFormadepagamentoDesc() {
        return formadepagamentoDesc;
    }

    public void setFormadepagamentoDesc(String formadepagamentoDesc) {
        this.formadepagamentoDesc = formadepagamentoDesc;
    }

    public String getFormadepagamentoNome() {
        return formadepagamentoNome;
    }

    public void setFormadepagamentoNome(String formadepagamentoNome) {
        this.formadepagamentoNome = formadepagamentoNome;
    }

    @XmlTransient
    public Collection<Parcelaapagar> getParcelaapagarCollection() {
        return parcelaapagarCollection;
    }

    public void setParcelaapagarCollection(Collection<Parcelaapagar> parcelaapagarCollection) {
        this.parcelaapagarCollection = parcelaapagarCollection;
    }

    @XmlTransient
    public Collection<Contaareceber> getContaareceberCollection() {
        return contaareceberCollection;
    }

    public void setContaareceberCollection(Collection<Contaareceber> contaareceberCollection) {
        this.contaareceberCollection = contaareceberCollection;
    }

    @XmlTransient
    public Collection<Parcelaareceber> getParcelaareceberCollection() {
        return parcelaareceberCollection;
    }

    public void setParcelaareceberCollection(Collection<Parcelaareceber> parcelaareceberCollection) {
        this.parcelaareceberCollection = parcelaareceberCollection;
    }

    @XmlTransient
    public Collection<Contaapagar> getContaapagarCollection() {
        return contaapagarCollection;
    }

    public void setContaapagarCollection(Collection<Contaapagar> contaapagarCollection) {
        this.contaapagarCollection = contaapagarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (formadepagamentoId != null ? formadepagamentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formadepagamento)) {
            return false;
        }
        Formadepagamento other = (Formadepagamento) object;
        if ((this.formadepagamentoId == null && other.formadepagamentoId != null) || (this.formadepagamentoId != null && !this.formadepagamentoId.equals(other.formadepagamentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return formadepagamentoNome;
    }

    public boolean getFormadepagamentoRecebimentoaprazo() {
        return formadepagamentoRecebimentoaprazo;
    }

    public void setFormadepagamentoRecebimentoaprazo(boolean formadepagamentoRecebimentoaprazo) {
        this.formadepagamentoRecebimentoaprazo = formadepagamentoRecebimentoaprazo;
    }

    public boolean getFormadepagamentoParcelado() {
        return formadepagamentoParcelado;
    }

    public void setFormadepagamentoParcelado(boolean formadepagamentoParcelado) {
        this.formadepagamentoParcelado = formadepagamentoParcelado;
    }
    
}
