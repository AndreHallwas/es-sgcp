/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Controladora;

import Acesso.Entidade.Componente;
import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public interface HabilitaComponente {
    public void Habilita(Componente componente);
    public void Default(ArrayList<Componente> Componentes);
}
