/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Raizen
 */
@Embeddable
public class CategoriaprodutoPK implements Serializable {
    
    @Basic(optional = false)
    @Column(name = "categoriaprod_id")
    private int categoriaprodId;
    @Basic(optional = false)
    @Column(name = "tipoproduto_id")
    private int tipoprodutoId;

    public CategoriaprodutoPK() {
    }

    public CategoriaprodutoPK(int categoriaprodId, int tipoprodutoId) {
        this.categoriaprodId = categoriaprodId;
        this.tipoprodutoId = tipoprodutoId;
    }

    public int getCategoriaprodId() {
        return categoriaprodId;
    }

    public void setCategoriaprodId(int categoriaprodId) {
        this.categoriaprodId = categoriaprodId;
    }

    public int getTipoprodutoId() {
        return tipoprodutoId;
    }

    public void setTipoprodutoId(int tipoprodutoId) {
        this.tipoprodutoId = tipoprodutoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) categoriaprodId;
        hash += (int) tipoprodutoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaprodutoPK)) {
            return false;
        }
        CategoriaprodutoPK other = (CategoriaprodutoPK) object;
        if (this.categoriaprodId != other.categoriaprodId) {
            return false;
        }
        if (this.tipoprodutoId != other.tipoprodutoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testedb2.CategoriaprodutoPK[ categoriaprodId=" + categoriaprodId + ", tipoprodutoId=" + tipoprodutoId + " ]";
    }
    
}
