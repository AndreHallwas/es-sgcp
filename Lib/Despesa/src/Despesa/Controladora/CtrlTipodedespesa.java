/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Despesa.Controladora;

import Controladora.Base.CtrlBase;
import Despesa.Entidade.Tipodedespesa;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlTipodedespesa extends CtrlBase {

    private static CtrlTipodedespesa ctrltipodedespesa;

    public static CtrlTipodedespesa create() {
        if (ctrltipodedespesa == null) {
            ctrltipodedespesa = new CtrlTipodedespesa();
        }
        return ctrltipodedespesa;
    }

    public CtrlTipodedespesa() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object tipodedespesa, TextField txbNome, TextArea taObs) {
        if (tipodedespesa != null && tipodedespesa instanceof Tipodedespesa) {
            Tipodedespesa f = (Tipodedespesa) tipodedespesa;
            txbNome.setText(f.getTipodedespesaNome());
            taObs.setText(f.getTipodedespesaDescricao());
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Tipodedespesa) {
            return ((Tipodedespesa) p).getTipodedespesaId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Tipodedespesa) {
            txBusca.setText(((Tipodedespesa) p).getTipodedespesaNome());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> tipodedespesa = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Tipodedespesa> ResultTipodedespesa;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultTipodedespesa = em.createNamedQuery("Tipodedespesa.findAll", Tipodedespesa.class)
                        .getResultList();
            } else {
                try {
                    ResultTipodedespesa = em.createNamedQuery("Tipodedespesa.findByTipodedespesaId", Tipodedespesa.class)
                            .setParameter("tipodedespesaId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultTipodedespesa = em.createNamedQuery("Tipodedespesa.findByTipodedespesaNome", Tipodedespesa.class)
                            .setParameter("tipodedespesaNome", Filtro).getResultList();
                }
            }
            for (Tipodedespesa tipodedespesas : ResultTipodedespesa) {
                tipodedespesa.add(tipodedespesas);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return tipodedespesa;
    }

    public Object Salvar(String Nome, String Descricao) {
        Tipodedespesa tipodedespesa = new Tipodedespesa(Nome, Descricao);
        return super.Salvar(tipodedespesa);
    }

    public Object Alterar(Object aDespesa, Integer Id, String Nome, String Descricao) {
        Tipodedespesa tipodedespesa = null;
        if(aDespesa != null && aDespesa instanceof Tipodedespesa){
            tipodedespesa = (Tipodedespesa) aDespesa;
            tipodedespesa.setTipodedespesaDescricao(Descricao);
            tipodedespesa.setTipodedespesaNome(Nome);
            tipodedespesa = (Tipodedespesa) super.Alterar(tipodedespesa);
        }
        return tipodedespesa;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Tipodedespesa.class);
    }

}
