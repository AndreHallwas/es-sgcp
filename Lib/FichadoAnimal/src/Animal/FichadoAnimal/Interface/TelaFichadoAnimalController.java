/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.FichadoAnimal.Interface;

import Utils.Controladora.CtrlUtils;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Remote
 */
public class TelaFichadoAnimalController implements Initializable {

    @FXML
    private VBox tabAnimal;
    @FXML
    private VBox tabCliente;
    @FXML
    private VBox tabVacinacao;
    @FXML
    private VBox tabAtendimento;
    @FXML
    private VBox tabAgendamento;

    private Tela TelaAnimal;
    private Tela TelaCliente;
    private Tela TelaVacinacao;
    private Tela TelaAtendimento;
    private Tela TelaAgendamento;
    private Tela TelaAnotacao;
    @FXML
    private VBox tabAnotacao;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TelaAnimal = IniciarTela.Create(this.getClass(), tabAnimal,
                "/Animal/Interface/TelaCadAnimal.fxml");
        TelaCliente = IniciarTela.Create(this.getClass(), tabCliente,
                "/Pessoa/Interface/TelaCadCliente.fxml");
        TelaVacinacao = IniciarTela.Create(this.getClass(), tabVacinacao,
                "/Vacina/Interface/TelaVacina.fxml");
        TelaAtendimento = IniciarTela.Create(this.getClass(), tabAtendimento,
                "/Atendimento/Interface/Atendimento.fxml");
        TelaAgendamento = IniciarTela.Create(this.getClass(), tabAgendamento,
                "/Agendamento/Interface/TelaCadAgendamento.fxml");
        TelaAnotacao = IniciarTela.Create(this.getClass(), tabAnotacao,
                "/Animal/Interface/TelaCadAnotacao.fxml");
        
        change(tabCliente);
        change(tabAtendimento);
        change(tabAgendamento);
        change(tabVacinacao);
        change(tabAnotacao);

    }

    private void change(Pane Box) {
        ObservableList<Node> nodes = CtrlUtils.getChildren(Box.getChildren());
        Box.getChildren().setAll(nodes);
    }

}
