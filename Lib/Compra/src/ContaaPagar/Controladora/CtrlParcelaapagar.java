/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContaaPagar.Controladora;

import Caixa.Controladora.CtrlCaixa;
import Compra.Controladora.CtrlCompra;
import Compra.Entidade.Compra;
import ContaaPagar.Entidade.Contaapagar;
import ContaaPagar.Entidade.Parcelaapagar;
import ContaaPagar.Entidade.ParcelaapagarPK;
import Controladora.Base.CtrlBase;
import FormadePagamento.Entidade.Formadepagamento;
import Transacao.Transacao;
import Transacao.Transaction;
import Utils.Mensagem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlParcelaapagar extends CtrlBase {

    private static CtrlParcelaapagar ctrlparcelaaPagar;

    public static CtrlParcelaapagar create() {
        if (ctrlparcelaaPagar == null) {
            ctrlparcelaaPagar = new CtrlParcelaapagar();
        }
        return ctrlparcelaaPagar;
    }

    public static Object getFormadepagamento(Object ItemSelecionado) {
        Object aFormadepagamento = null;
        if(ItemSelecionado != null && ItemSelecionado instanceof Parcelaapagar) {
            Parcelaapagar parcela = (Parcelaapagar) ItemSelecionado;
            aFormadepagamento = parcela.getFormadepagamentoId();
        }
        return aFormadepagamento;
    }

    public static Object getCompra(Object ItemSelecionado) {
        Object aCompra = null;
        if(ItemSelecionado != null && ItemSelecionado instanceof Parcelaapagar) {
            Parcelaapagar parcela = (Parcelaapagar) ItemSelecionado;
            if(parcela.getContaapagar() != null){
                aCompra = parcela.getContaapagar().getCompraId();
            }
        }
        return aCompra;
    }

    public CtrlParcelaapagar() {
        super(Transaction.getEntityManagerFactory());
    }

    public static boolean setFormadepagamento(Object ItemSelecionado, Object aFormadepagamento) {
        return new Transacao() {
            @Override
            public Object Transacao(Object... Params) {
                Parcelaapagar parcela = null;
                if (ItemSelecionado != null && ItemSelecionado instanceof Parcelaapagar
                        && aFormadepagamento != null && aFormadepagamento instanceof Formadepagamento) {
                    parcela = (Parcelaapagar) ItemSelecionado;
                    Formadepagamento formadepagamento = (Formadepagamento) aFormadepagamento;
                    parcela.setFormadepagamentoId(formadepagamento);
                    parcela = em.merge(parcela);
                }
                return parcela;
            }
        }.Realiza() != null;
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> parcelaaPagar = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Parcelaapagar> ResultParcelaapagar = new ArrayList();
            if (Filtro == null || Filtro.isEmpty()) {
                ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findAll", Parcelaapagar.class)
                        .getResultList();
            } else {
                try {
                    if (Tipo.equalsIgnoreCase("Geral")) {
                        ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findByAllGeral", Parcelaapagar.class)
                                .setParameter("parpNumerodaparcela", Integer.parseInt(Filtro))
                                .setParameter("parpValor", Double.parseDouble(Filtro))
                                .setParameter("parpValorPago", Double.parseDouble(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("Conta")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findByParcelaapagarPk", Parcelaapagar.class)
                                    .setParameter("contaapagarId", Integer.parseInt(auxFiltro[0]))
                                    .setParameter("parpNumerodaparcela", Integer.parseInt(auxFiltro[1])).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("Compra")) {
                        ArrayList<Object> Resultado = CtrlCompra.create().Pesquisar(Filtro, "Id");
                        if (Resultado != null && !Resultado.isEmpty()) {
                            ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findByCompraId", Parcelaapagar.class)
                                    .setParameter("compraId", Resultado.get(0)).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("Situação")) {
                        ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findByParpSituacao", Parcelaapagar.class)
                                .setParameter("parpSituacao", Boolean.parseBoolean(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("ProximaParcela")) {
                        ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findByNextParcela", Parcelaapagar.class)
                                .setParameter("contaapagarId", Integer.parseInt(Filtro)).getResultList();
                    } else if (Tipo.equalsIgnoreCase("DataVencimento")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ArrayList<Object> compras = CtrlCompra.create().Pesquisar(auxFiltro[1], "Id");
                            if (compras != null && compras.size() > 0) {
                                Compra compra = (Compra) compras.get(0);
                                ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findByDataVencimentoCompra", Parcelaapagar.class)
                                        .setParameter("parpDataVencimento", new Date(Long.parseLong(auxFiltro[0])))
                                        .setParameter("compraId", compra).getResultList();
                            }
                        }
                    } else if (Tipo.equalsIgnoreCase("Data")) {
                        ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findByAllData", Parcelaapagar.class)
                                .setParameter("parpDataVencimento", new Date(Long.parseLong(Filtro)))
                                .setParameter("parpDataPagamento", new Date(Long.parseLong(Filtro))).getResultList();
                    } else if (Tipo.equalsIgnoreCase("PeriodoVencimento")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultParcelaapagar = em.createNamedQuery("Compra.findByBetweenVencimento", Parcelaapagar.class)
                                    .setParameter("datainicial", new Date(Long.parseLong(auxFiltro[0])))
                                    .setParameter("datafinal", new Date(Long.parseLong(auxFiltro[1]))).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("PeriodoPagamento")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultParcelaapagar = em.createNamedQuery("Compra.findByBetweenPagamento", Parcelaapagar.class)
                                    .setParameter("datainicial", new Date(Long.parseLong(auxFiltro[0])))
                                    .setParameter("datafinal", new Date(Long.parseLong(auxFiltro[1]))).getResultList();
                        }
                    } else if (Tipo.equalsIgnoreCase("Periodo")) {
                        String[] auxFiltro = Filtro.split("#:#");
                        if (auxFiltro != null && auxFiltro.length == 2) {
                            ResultParcelaapagar = em.createNamedQuery("Compra.findByAllBetween", Parcelaapagar.class)
                                    .setParameter("datainicial", new Date(Long.parseLong(auxFiltro[0])))
                                    .setParameter("datafinal", new Date(Long.parseLong(auxFiltro[1]))).getResultList();
                        }
                    }
                } catch (Exception ex) {
                    ResultParcelaapagar = em.createNamedQuery("Parcelaapagar.findByFormadePagamento", Parcelaapagar.class)
                            .setParameter("formadepagamentoId", Filtro).getResultList();
                }
            }

            for (Parcelaapagar parcelaaPagars : ResultParcelaapagar) {
                parcelaaPagar.add(parcelaaPagars);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return parcelaaPagar;
    }

    public Object Salvar(Integer ContaapagarID, Integer Numerodaparcela, Date DataVencimento, Date DataPagamento, BigDecimal Valor,
            Boolean Situacao, BigDecimal ValorPago, BigDecimal Troco, Object formaDePagamento, Object contaapagar) {

        Parcelaapagar parcelaaPagar = new Parcelaapagar(ContaapagarID, Numerodaparcela, DataVencimento, DataPagamento,
                Valor, Situacao, ValorPago, Troco, new Date());

        if (formaDePagamento != null && formaDePagamento instanceof Formadepagamento) {
            parcelaaPagar.setFormadepagamentoId((Formadepagamento) formaDePagamento);
        }
        if (contaapagar != null && contaapagar instanceof Contaapagar) {
            parcelaaPagar.setContaapagar((Contaapagar) contaapagar);
        }

        return super.Salvar(parcelaaPagar);
    }

    public Object Alterar(Object parcelaapagar, Integer ContaapagarID/*...*/, Integer Numerodaparcela, Date DataVencimento, Date DataPagamento,
            BigDecimal Valor, Boolean Situacao, BigDecimal ValorPago, BigDecimal Troco, Object formaDePagamento, Object contaapagar) {
        Parcelaapagar parcelaaPagar = null;

        if (parcelaapagar != null && parcelaapagar instanceof Parcelaapagar) {

            parcelaaPagar = (Parcelaapagar) parcelaapagar;
            parcelaaPagar.setParpDataPagamento(DataPagamento);
            parcelaaPagar.setParpDataVencimento(DataVencimento);
            parcelaaPagar.setParpSituacao(Situacao);
            parcelaaPagar.setParpTroco(Troco);
            parcelaaPagar.setParpValor(Valor);
            parcelaaPagar.setParpValorPago(ValorPago);

            if (formaDePagamento != null && formaDePagamento instanceof Formadepagamento) {
                parcelaaPagar.setFormadepagamentoId((Formadepagamento) formaDePagamento);
            }
            if (contaapagar != null && contaapagar instanceof Parcelaapagar) {
                parcelaaPagar.setContaapagar((Contaapagar) contaapagar);
            }
            parcelaaPagar = (Parcelaapagar) super.Alterar(parcelaaPagar);
        }
        return parcelaaPagar;
    }

    public boolean Remover(Object Id) {
        return super.Remover(Id) != null;
    }

    public boolean estornar(Object parcela) {
        boolean flag = true;

        if (parcela != null && parcela instanceof Parcelaapagar) {

            /*Busca Por parcelas com a mesma data de Vencimento*/
            Parcelaapagar conta = (Parcelaapagar) parcela;
            ArrayList<Object> contas = Pesquisar(
                    Long.toString(conta.getParpDataVencimento().getTime())
                    + "#:#" + conta.getContaapagar().getCompraId(), "DataVencimento");
            if (contas != null) {
                EntityManager em = null;

                try {
                    /////Inicia a Transacao
                    em = getEntityManager();
                    em.getTransaction().begin();

                    /*Remove a Movimentação do caixa*/
                    flag = flag && CtrlCaixa.create().RemoveMovimentacao(conta, em) != null;

                    /*Caso só exista uma, esta é estornada ou é a Ultima conta da Lista(Supondo que a Lista vem Ordenada)*/
                    if (contas.size() <= 1 || conta.getParcelaapagarPK().getParpDatadegeracao().getTime()
                            == (((Parcelaapagar) contas.get(contas.size() - 1)).getParcelaapagarPK().getParpDatadegeracao().getTime())) {
                        conta.setParpValorPago(BigDecimal.ZERO);
                        conta.setParpDataPagamento(null);
                        conta.setParpSituacao(false);
                        conta.setMovimentacaoapagarCollection(new ArrayList());
                        /////Altera a conta
                        conta = em.merge(conta);

                    } else {
                        /////Verifica se a Ultima Parcela está Paga
                        Parcelaapagar UltimaConta = (Parcelaapagar) contas.get(contas.size() - 1);
                        if (UltimaConta.getParpSituacao()) {
                            /////Se estiver Paga
                            /////Estorna
                            conta.setParpValorPago(BigDecimal.ZERO);
                            conta.setParpDataPagamento(null);
                            conta.setParpSituacao(false);
                            conta.setMovimentacaoapagarCollection(new ArrayList());
                            /////Troca a Chave Primaria desta parcela com a Ultima
                            ParcelaapagarPK PK = conta.getParcelaapagarPK();
                            conta.setParcelaapagarPK(UltimaConta.getParcelaapagarPK());
                            conta = em.merge(conta);
                            UltimaConta.setParcelaapagarPK(PK);
                            UltimaConta = em.merge(UltimaConta);
                        } else {
                            /////Caso não seja a Ultima da Lista e a Ultima não esteja Paga
                            /////Adiciona o Valor a Pagar na Ultima Parcela
                            conta.setMovimentacaoapagarCollection(new ArrayList());/////Testar Retirar
                            UltimaConta.setParpValor(UltimaConta.getParpValor().add(conta.getParpValor()));
                            UltimaConta = em.merge(UltimaConta);
                            /////Remove a parcela estornada da conta
                            UltimaConta.getContaapagar().getParcelaapagarCollection().remove(conta);
                            /////Atualiza a Conta
                            UltimaConta.setContaapagar(em.merge(UltimaConta.getContaapagar()));
                            /////Remove a Conta
                            if (!em.contains(conta)) {
                                conta = em.merge(conta);
                            }
                            em.remove(conta);
                        }

                    }
                    if (flag) {
                        em.getTransaction().commit();
                    } else if (em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                    Message = flag ? "Estorno Realizado com Sucesso" : "Erro, o Estorno não foi Realizado";

                } catch (Exception ex) {
                    if (em != null) {
                        Mensagem.ExibirException(ex, ":CtrlCompra:302");
                        if (em.getTransaction().isActive()) {
                            em.getTransaction().rollback();
                        }
                    }
                    Message = "Erro, o Estorno não foi Realizado";
                } finally {
                    if (em != null) {
                        em.close();
                    }
                }
            }
        } else {
            flag = false;
            Message = "Erro, o Estorno não foi Realizado";
        }
        return flag;
    }

    public boolean Pagar(Object parcela, String valor, EntityManager em) throws Exception {
        if (em != null) {
            return Pagamento(parcela, valor, em);
        } else {
            return Pagar(parcela, valor);
        }
    }

    public boolean Pagar(Object parcela, String valor) {
        boolean flag = true;
        if (parcela != null && parcela instanceof Parcelaapagar) {

            EntityManager em = null;

            try {
                /////Inicia a Transacao
                em = getEntityManager();
                em.getTransaction().begin();

                flag = flag && Pagamento(parcela, valor, em);

                if (flag) {
                    em.getTransaction().commit();
                } else if (em.getTransaction().isActive()) {
                    em.getTransaction().rollback();
                }
                Message = flag ? "Pagamento Realizado com Sucesso" : "Erro, o Pagamento não foi Realizado";

            } catch (Exception ex) {
                if (em != null) {
                    Mensagem.ExibirException(ex, ":CtrlCompra:302");
                    if (em.getTransaction().isActive()) {
                        em.getTransaction().rollback();
                    }
                }
                Message = "Erro, o Pagamento não foi Realizado";
            } finally {
                if (em != null) {
                    em.close();
                }
            }

        } else {
            flag = false;
            Message = "Erro, o Pagamento não foi Realizado";
        }
        return flag;
    }

    public boolean Pagamento(Object parcela, String valor, EntityManager em) throws Exception {
        boolean flag = true;

        Parcelaapagar conta = (Parcelaapagar) parcela;
        BigDecimal ValorPago = new BigDecimal(valor);

        if (ValorPago.compareTo(conta.getParpValor()) <= 0) {//Compara o Valor Pago ao da Parcela.
            /////Altera a Conta
            Parcelaapagar parcelaaPagar = conta;
            parcelaaPagar.setParpDataPagamento(new Date());
            parcelaaPagar.setParpDataVencimento(conta.getParpDataVencimento());
            parcelaaPagar.setParpSituacao(true);
            parcelaaPagar.setParpTroco(BigDecimal.ZERO/*Sem troco*/);
            parcelaaPagar.setParpValorPago(ValorPago);

            BigDecimal ValoraPagar = parcelaaPagar.getParpValor().subtract(parcelaaPagar.getParpValorPago()/*.round(MathContext.DECIMAL64)*/);
            Boolean ValorFaltante = ValoraPagar.compareTo(BigDecimal.ZERO) > 0;
            if (ValorFaltante) {
                parcelaaPagar.setParpValor(ValorPago);
            } else {
                parcelaaPagar.setParpValor(conta.getParpValor());
            }
            /*Não altera a Conta nem a forma de pagamento*/
            /////Realiza a alteração
            parcelaaPagar = em.merge(parcelaaPagar);

            /*Gerar uma nova Caso haja valor faltante*/
            if (ValorFaltante) {

                try {
                    Contaapagar contaapagar = conta.getContaapagar();
                    /*Caso a parcela não esta totalmente paga é gerada outra com o mesmo vencimento

                            /*Gera a Parcela*/
                    Parcelaapagar ParcelaGerada = new Parcelaapagar(parcelaaPagar.getParcelaapagarPK().getContaapagarId(),
                            parcelaaPagar.getParcelaapagarPK().getParpNumerodaparcela(),
                            parcelaaPagar.getParpDataVencimento(), null, ValoraPagar, false,
                            BigDecimal.ZERO, BigDecimal.ZERO/*Sem troco*/, new Date());

                    ParcelaGerada.setFormadepagamentoId(parcelaaPagar.getFormadepagamentoId());
                    ParcelaGerada.setContaapagar(contaapagar);
                    em.persist(ParcelaGerada);

                    /////Altera a Conta adicionando a Parcela
                    contaapagar.getParcelaapagarCollection().add(ParcelaGerada);
                    contaapagar = em.merge(contaapagar);
                    em.flush();

                    flag = true;

                } catch (Exception ex) {
                    Mensagem.ExibirException(ex, "Erro ao gerar nova Parcela Com valor Restante CtrlParcelaaPagar::312");
                    flag = false;
                }

            } else if (ValoraPagar.compareTo(BigDecimal.ZERO) < 0) {
                flag = false;
            }

            /*Gerar Movimentação no Caixa*/
            flag = flag && CtrlCaixa.create().RealizarMovimentacao(parcelaaPagar, em) != null;
            flag = flag && parcelaaPagar != null;
        }
        return flag;
    }

    protected static void quickSortIterative(List<Parcelaapagar> arr, int l, int h) {
        // Create an auxiliary stack
        int[] stack = new int[h - l + 1];

        // initialize top of stack
        int top = -1;

        // push initial values of l and h to stack
        stack[++top] = l;
        stack[++top] = h;

        // Keep popping from stack while is not empty
        while (top >= 0) {
            // Pop h and l
            h = stack[top--];
            l = stack[top--];

            // Set pivot element at its correct position
            // in sorted array
            int p = partition(arr, l, h);

            // If there are elements on left side of pivot,
            // then push left side to stack
            if (p - 1 > l) {
                stack[++top] = l;
                stack[++top] = p - 1;
            }

            // If there are elements on right side of pivot,
            // then push right side to stack
            if (p + 1 < h) {
                stack[++top] = p + 1;
                stack[++top] = h;
            }
        }
    }

    protected static int partition(List<Parcelaapagar> arr, int low, int high) {
        Parcelaapagar pivot = arr.get(high);

        // index of smaller element
        int i = (low - 1);
        for (int j = low; j <= high - 1; j++) {
            // If current element is smaller than or
            // equal to pivot
            if (arr.get(j).getParpDataVencimento().getTime()
                    <= pivot.getParpDataVencimento().getTime()) {
                i++;

                // swap arr[i] and arr[j]
                Parcelaapagar temp = arr.get(i);
                arr.set(i, arr.get(j));
                arr.set(j, temp);
            }
        }
        // swap arr[i+1] and arr[high] (or pivot)
        Parcelaapagar temp = arr.get(i + 1);
        arr.set(i + 1, arr.get(high));
        arr.set(high, temp);

        return i + 1;
    }

    public void setCampos(Object parcela, TextField txbValoraPagar, TextField txbValordaParcela) {
        if (parcela != null && parcela instanceof Parcelaapagar) {
            Parcelaapagar aParcela = (Parcelaapagar) parcela;
            txbValoraPagar.setText(aParcela.getParpValor().toString());
            txbValordaParcela.setText(aParcela.getParpValor().toString());
        }
    }

    public Boolean getSituacao(Object parcela) {
        if (parcela != null && parcela instanceof Parcelaapagar) {
            Parcelaapagar aParcela = (Parcelaapagar) parcela;
            return aParcela.getParpSituacao();
        }
        return false;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Parcelaapagar.class);
    }

}
