/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.ArrayList;

/**
 *
 * @author Raizen
 */
public class Nodo {

    public static ArrayList<Object> getComponentes(ArrayList<Nodo> Componentes) {
        ArrayList<Object> osComponentes = new ArrayList();
        Componentes.forEach((Componente) -> {
            if (Componente.isAtivo()) {
                osComponentes.add(Componente.getObjeto());
            }
        });
        return osComponentes;
    }

    private Object Objeto;
    private boolean Ativo = false;

    public Nodo(Object Objeto, boolean Ativo) {
        this.Objeto = Objeto;
        this.Ativo = Ativo;
    }

    /**
     * @return the Objeto
     */
    public Object getObjeto() {
        return Objeto;
    }

    /**
     * @param Objeto the Objeto to set
     */
    public void setObjeto(Object Objeto) {
        this.Objeto = Objeto;
    }

    /**
     * @return the Ativo
     */
    public boolean isAtivo() {
        return Ativo;
    }

    /**
     * @param Ativo the Ativo to set
     */
    public void setAtivo(boolean Ativo) {
        this.Ativo = Ativo;
    }

}
