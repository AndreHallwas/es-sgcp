/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Interface;

import Utils.DateUtils;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Raizen
 */
public final class TabelaParcelas {

    protected IDSeq seq = new IDSeq();
    protected ArrayList<nodeTabela> nodes;
    protected Double ValorTotal;
    protected Object Formadepagamento;
    protected int QtdParcelas;
    protected Date VencimentoInicial;
    protected int DiasEntreParcelas;

    public TabelaParcelas(Double ValorTotal,
            Object Formadepagamento, int Parcelas,
            Date VencimentoInicial, int DiasEntreParcelas) {
        this.ValorTotal = ValorTotal;
        this.Formadepagamento = Formadepagamento;
        this.QtdParcelas = Parcelas;
        this.VencimentoInicial = VencimentoInicial;
        this.DiasEntreParcelas = DiasEntreParcelas;
        Inicializa();
    }

    public void Inicializa() {
        if (nodes == null) {
            nodes = new ArrayList();
        }
    }

    public void add(Date Data, Double Valor) {
        Inicializa();
        nodes.add(new nodeTabela(seq.getNextId(), Data, Valor));
        QtdParcelas = nodes.size();
    }

    public void change(int ID, Date Data, Double Valor) {
        Inicializa();
        int Pos = 0;
        while (Pos < nodes.size() && nodes.get(Pos).getId() != ID) {
            Pos++;
        }
        if (Pos < nodes.size()) {
            nodes.get(Pos).setData(Data);
            nodes.get(Pos).setValor(Valor);
        }
    }

    public void remove(Integer ID) {
        Inicializa();
        int Pos = 0;
        while (Pos < nodes.size() && nodes.get(Pos).getId() != ID) {
            Pos++;
        }
        if (Pos < nodes.size()) {
            nodes.remove(Pos);
            QtdParcelas = nodes.size();
            ReordenaIndices();
        }
    }

    public ArrayList<nodeTabela> get() {
        return nodes;
    }

    public ArrayList<Object> getObject() {
        ArrayList<Object> osNodes = new ArrayList();
        for (nodeTabela node : nodes) {
            osNodes.add(node);
        }
        return osNodes;
    }

    public ArrayList<Object> getValores() {
        ArrayList<Object> Valores = new ArrayList();
        for (nodeTabela node : nodes) {
            Valores.add(node.getValor());
        }
        return Valores;
    }

    public ArrayList<Object> getDatas() {
        ArrayList<Object> Datas = new ArrayList();
        for (nodeTabela node : nodes) {
            Datas.add(node.getData());
        }
        return Datas;
    }

    public int VerificaValor(Double ValorTotal) {
        Double ValorTotalCarrinho = 0.0;
        for (nodeTabela node : nodes) {
            ValorTotalCarrinho += node.getValor();
        }
        long VT, VTC;
        VT = Math.round(ValorTotal);
        VTC = Math.round(ValorTotalCarrinho);
        if (VT < VTC) {
            return -1;
        } else if (VT > VTC) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public void GeraParcelasPadrao(){
        Double ValorParcial = getValorTotal()/getQtdParcelas();
        BigDecimal bd = new BigDecimal(ValorParcial);
        bd = bd.setScale(2, RoundingMode.DOWN);
        ValorParcial = bd.doubleValue();
        Double ValorFinal = getValorTotal() - (ValorParcial*(getQtdParcelas()-1));
        bd = new BigDecimal(ValorFinal);
        bd = bd.setScale(2, RoundingMode.HALF_DOWN);
        ValorFinal = bd.doubleValue();
        int parcelas = getQtdParcelas();
        int i = 0;
        while(i < parcelas-1) {
            Date Data = new Date(getVencimentoInicial().getTime());
            Data.setDate(getVencimentoInicial().getDate()+(getDiasEntreParcelas()*i++));
            add(Data, ValorParcial);
        }
        Date Data = new Date(getVencimentoInicial().getTime());
        Data.setDate(getVencimentoInicial().getDate()+(getDiasEntreParcelas()*i));
        add(Data, ValorFinal);
    }

    public Double geraValorTotal() {
        BigDecimal ValorT = new BigDecimal(0);
        for (nodeTabela node : nodes) {
            ValorT = ValorT.add(new BigDecimal(node.getValor()));
        }
        return ValorT.doubleValue();
    }
    
    public void Zerar(){
        nodes = new ArrayList();
    }

    private void ReordenaIndices() {
        seq.Restart();
        for (nodeTabela node : nodes) {
            node.setId(seq.getNextId());
        }
    }

    public class nodeTabela {

        protected int Id;
        protected Date Data;
        protected Double Valor;

        public nodeTabela(int Id, Date Data, Double Valor) {
            this.Id = Id;
            this.Data = Data;
            this.Valor = Valor;
        }

        /**
         * @return the Id
         */
        public int getId() {
            return Id;
        }

        /**
         * @return the Data
         */
        public Date getData() {
            return Data;
        }

        /**
         * @return the Vencimento
         */
        public String getDataVencimento() {
            return DateUtils.asLocalDate(Data).toString();
        }

        /**
         * @return the Valor
         */
        public Double getValor() {
            return Valor;
        }

        /**
         * @param Id the Id to set
         */
        public void setId(int Id) {
            this.Id = Id;
        }

        /**
         * @param Data the Data to set
         */
        public void setData(Date Data) {
            this.Data = Data;
        }

        /**
         * @param Valor the Valor to set
         */
        public void setValor(Double Valor) {
            this.Valor = Valor;
        }
    }

    public class IDSeq {

        private int IdCurrent = 0;

        /**
         * @return the IdCurrent
         */
        public int getCurrentId() {
            return IdCurrent;
        }

        public int getNextId() {
            return ++IdCurrent;
        }
        
        public void Restart(){
            IdCurrent = 0;
        }

    }

    /**
     * @return the ValorTotal
     */
    public Double getValorTotal() {
        return ValorTotal;
    }

    /**
     * @return the Formadepagamento
     */
    public Object getFormadepagamento() {
        return Formadepagamento;
    }

    /**
     * @return the QtdParcelas
     */
    public int getQtdParcelas() {
        return QtdParcelas;
    }

    /**
     * @return the VencimentoInicial
     */
    public Date getVencimentoInicial() {
        return VencimentoInicial;
    }

    /**
     * @return the DiasEntreParcelas
     */
    public int getDiasEntreParcelas() {
        return DiasEntreParcelas;
    }

    /**
     * @param Formadepagamento the Formadepagamento to set
     */
    public void setFormadepagamento(Object Formadepagamento) {
        this.Formadepagamento = Formadepagamento;
    }

    /**
     * @param VencimentoInicial the VencimentoInicial to set
     */
    public void setVencimentoInicial(Date VencimentoInicial) {
        this.VencimentoInicial = VencimentoInicial;
    }

    /**
     * @param DiasEntreParcelas the DiasEntreParcelas to set
     */
    public void setDiasEntreParcelas(int DiasEntreParcelas) {
        this.DiasEntreParcelas = DiasEntreParcelas;
    }
}
