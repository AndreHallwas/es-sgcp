/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Entidade;

import JPA.Dao.JPADao;
import Pessoa.Entidade.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "permissaodeacesso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Permissaodeacesso.findAll", query = "SELECT p FROM Permissaodeacesso p")
    , @NamedQuery(name = "Permissaodeacesso.findByPermacessoId", query = "SELECT p FROM Permissaodeacesso p WHERE p.permacessoId = :permacessoId")
    , @NamedQuery(name = "Permissaodeacesso.findByPermacessoDetalhes", query = "SELECT p FROM Permissaodeacesso p WHERE p.permacessoDetalhes = :permacessoDetalhes")})
public class Permissaodeacesso extends JPADao implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id/*
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="permissaodeacesso_permacesso_id_seq")*/
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "permacesso_id")
    private Integer permacessoId;
    @Column(name = "permacesso_detalhes")
    private String permacessoDetalhes;
    
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "permacessoId")
    private Collection<Grupodeacesso> grupodeacessoCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "permacessoId")
    private Collection<Usuario> usuarioCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "permacessoId")
    private Collection<Permissaocomponente> permissaocomponenteCollection = new ArrayList();

    public Permissaodeacesso() {
    }

    public Permissaodeacesso(Integer permacessoId) {
        this.permacessoId = permacessoId;
    }

    public Permissaodeacesso(String permacessoDetalhes) {
        this.permacessoDetalhes = permacessoDetalhes;
    }

    public Permissaodeacesso(Integer permacessoId, String permacessoDetalhes) {
        this.permacessoId = permacessoId;
        this.permacessoDetalhes = permacessoDetalhes;
    }

    public Integer getPermacessoId() {
        return permacessoId;
    }

    public void setPermacessoId(Integer permacessoId) {
        this.permacessoId = permacessoId;
    }

    public String getPermacessoDetalhes() {
        return permacessoDetalhes;
    }

    public void setPermacessoDetalhes(String permacessoDetalhes) {
        this.permacessoDetalhes = permacessoDetalhes;
    }

    @XmlTransient
    public Collection<Grupodeacesso> getGrupodeacessoCollection() {
        return grupodeacessoCollection;
    }

    public void setGrupodeacessoCollection(Collection<Grupodeacesso> grupodeacessoCollection) {
        this.grupodeacessoCollection = grupodeacessoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permacessoId != null ? permacessoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permissaodeacesso)) {
            return false;
        }
        Permissaodeacesso other = (Permissaodeacesso) object;
        if ((this.permacessoId == null && other.permacessoId != null) || (this.permacessoId != null && !this.permacessoId.equals(other.permacessoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Acesso.Entidade.Permissaodeacesso[ permacessoId=" + permacessoId + " ]";
    }

    @XmlTransient
    public Collection<Permissaocomponente> getPermissaocomponenteCollection() {
        return permissaocomponenteCollection;
    }

    public void setPermissaocomponenteCollection(Collection<Permissaocomponente> permissaocomponenteCollection) {
        this.permissaocomponenteCollection = permissaocomponenteCollection;
    }

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }
    
}
