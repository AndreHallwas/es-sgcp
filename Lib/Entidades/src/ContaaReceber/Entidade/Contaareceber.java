/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContaaReceber.Entidade;

import FormadePagamento.Entidade.Formadepagamento;
import Utils.Acao.ButtonAcao;
import Utils.FormatString;
import Variables.Variables;
import Venda.Entidade.Venda;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "contaareceber")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contaareceber.findAll", query = "SELECT c FROM Contaareceber c")
    , @NamedQuery(name = "Contaareceber.findByContaareceberId", query = "SELECT c FROM Contaareceber c WHERE c.contaareceberId = :contaareceberId")
    , @NamedQuery(name = "Contaareceber.findByContaareceberValortotal", query = "SELECT c FROM Contaareceber c WHERE c.contaareceberValortotal = :contaareceberValortotal")
    , @NamedQuery(name = "Contaareceber.findByContaareceberSituacao", query = "SELECT c FROM Contaareceber c WHERE c.contaareceberSituacao = :contaareceberSituacao")
    , @NamedQuery(name = "Contaareceber.findByContaareceberAcrescimo", query = "SELECT c FROM Contaareceber c WHERE c.contaareceberAcrescimo = :contaareceberAcrescimo")
    , @NamedQuery(name = "Contaareceber.findByContaareceberDesconto", query = "SELECT c FROM Contaareceber c WHERE c.contaareceberDesconto = :contaareceberDesconto")
    , @NamedQuery(name = "Contaareceber.findByContaareceberJuros", query = "SELECT c FROM Contaareceber c WHERE c.contaareceberJuros = :contaareceberJuros")
    , @NamedQuery(name = "Contaareceber.findByContaareceberParcelas", query = "SELECT c FROM Contaareceber c WHERE c.contaareceberParcelas = :contaareceberParcelas")
    })
public class Contaareceber extends ButtonAcao<Contaareceber> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "contaareceber_id")
    private Integer contaareceberId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "contaareceber_valortotal")
    private BigDecimal contaareceberValortotal;
    @Column(name = "contaareceber_situacao")
    private Boolean contaareceberSituacao;
    @Column(name = "contaareceber_acrescimo")
    private Integer contaareceberAcrescimo;
    @Column(name = "contaareceber_desconto")
    private Integer contaareceberDesconto;
    @Column(name = "contaareceber_juros")
    private Integer contaareceberJuros;
    @Basic(optional = false)
    @Column(name = "contaareceber_parcelas")
    private int contaareceberParcelas;
    @JoinColumn(name = "formadepagamento_id", referencedColumnName = "formadepagamento_id")
    @ManyToOne(optional = false)
    private Formadepagamento formadepagamentoId;
    @JoinColumn(name = "venda_id", referencedColumnName = "venda_id")
    @ManyToOne(optional = false)
    private Venda vendaId;
    @Transient
    private Date contaareceberData;
    @Transient
    private String contaareceberDescricao;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contaareceber")
    private Collection<Parcelaareceber> parcelaareceberCollection = new ArrayList();

    public Contaareceber() {
    }

    public Contaareceber(Integer contaareceberId) {
        this.contaareceberId = contaareceberId;
    }

    public Contaareceber(BigDecimal contaareceberValortotal, Boolean contaareceberSituacao, Integer contaareceberAcrescimo,
            Integer contaareceberDesconto, Integer contaareceberJuros, Date contaareceberData, String contaareceberDescricao,
            int contaareceberParcelas) {
        this.contaareceberValortotal = contaareceberValortotal;
        this.contaareceberSituacao = contaareceberSituacao;
        this.contaareceberAcrescimo = contaareceberAcrescimo;
        this.contaareceberDesconto = contaareceberDesconto;
        this.contaareceberJuros = contaareceberJuros;
        this.contaareceberData = contaareceberData;
        this.contaareceberDescricao = contaareceberDescricao;
        this.contaareceberParcelas = contaareceberParcelas;
    }

    public Contaareceber(Integer contaareceberId, BigDecimal contaareceberValortotal, Boolean contaareceberSituacao,
            Integer contaareceberAcrescimo, Integer contaareceberDesconto, Integer contaareceberJuros, Date contaareceberData,
            String contaareceberDescricao, int contaareceberParcelas) {
        this.contaareceberId = contaareceberId;
        this.contaareceberValortotal = contaareceberValortotal;
        this.contaareceberSituacao = contaareceberSituacao;
        this.contaareceberAcrescimo = contaareceberAcrescimo;
        this.contaareceberDesconto = contaareceberDesconto;
        this.contaareceberJuros = contaareceberJuros;
        this.contaareceberData = contaareceberData;
        this.contaareceberDescricao = contaareceberDescricao;
        this.contaareceberParcelas = contaareceberParcelas;
    }

    public Contaareceber(Integer contaareceberId, BigDecimal contaareceberValortotal, int contaareceberParcelas) {
        this.contaareceberId = contaareceberId;
        this.contaareceberValortotal = contaareceberValortotal;
        this.contaareceberParcelas = contaareceberParcelas;
    }

    public Integer getContaareceberId() {
        return contaareceberId;
    }

    public void setContaareceberId(Integer contaareceberId) {
        this.contaareceberId = contaareceberId;
    }

    public BigDecimal getContaareceberValortotal() {
        return contaareceberValortotal;
    }

    public String Valortotal() {
        return FormatString.MonetaryPersistent(contaareceberValortotal);
    }

    public void setContaareceberValortotal(BigDecimal contaareceberValortotal) {
        this.contaareceberValortotal = contaareceberValortotal;
    }

    public Boolean getContaareceberSituacao() {
        return contaareceberSituacao;
    }

    public void setContaareceberSituacao(Boolean contaareceberSituacao) {
        this.contaareceberSituacao = contaareceberSituacao;
    }

    public Integer getContaareceberAcrescimo() {
        return contaareceberAcrescimo;
    }

    public void setContaareceberAcrescimo(Integer contaareceberAcrescimo) {
        this.contaareceberAcrescimo = contaareceberAcrescimo;
    }

    public Integer getContaareceberDesconto() {
        return contaareceberDesconto;
    }

    public void setContaareceberDesconto(Integer contaareceberDesconto) {
        this.contaareceberDesconto = contaareceberDesconto;
    }

    public Integer getContaareceberJuros() {
        return contaareceberJuros;
    }

    public void setContaareceberJuros(Integer contaareceberJuros) {
        this.contaareceberJuros = contaareceberJuros;
    }

    public int getContaareceberParcelas() {
        return contaareceberParcelas;
    }

    public void setContaareceberParcelas(int contaareceberParcelas) {
        this.contaareceberParcelas = contaareceberParcelas;
    }

    public Formadepagamento getFormadepagamentoId() {
        return formadepagamentoId;
    }

    public void setFormadepagamentoId(Formadepagamento formadepagamentoId) {
        this.formadepagamentoId = formadepagamentoId;
    }

    public Venda getVendaId() {
        return vendaId;
    }

    public void setVendaId(Venda vendaId) {
        this.vendaId = vendaId;
    }

    @XmlTransient
    public Collection<Parcelaareceber> getParcelaareceberCollection() {
        return parcelaareceberCollection;
    }

    public void setParcelaareceberCollection(Collection<Parcelaareceber> parcelaareceberCollection) {
        this.parcelaareceberCollection = parcelaareceberCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contaareceberId != null ? contaareceberId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contaareceber)) {
            return false;
        }
        Contaareceber other = (Contaareceber) object;
        if ((this.contaareceberId == null && other.contaareceberId != null) || (this.contaareceberId != null && !this.contaareceberId.equals(other.contaareceberId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return contaareceberId + "";
    }

    public Date getContaareceberData() {
        return contaareceberData;
    }

    public String getData() {
        return Variables.getData(contaareceberData);
    }

    public void setContaareceberData(Date contaareceberData) {
        this.contaareceberData = contaareceberData;
    }

    public String getContaareceberDescricao() {
        return contaareceberDescricao;
    }

    public void setContaareceberDescricao(String contaareceberDescricao) {
        this.contaareceberDescricao = contaareceberDescricao;
    }

    @Override
    protected void AddNodes() {
        addNode("Button", "rem", "Remover");
    }

}
