/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendamento.Entidade;

import Animal.Entidade.Animal;
import Atendimento.Entidade.Atendimento;
import JPA.Dao.JPADao;
import Pessoa.Entidade.Usuario;
import Produto.Entidade.Produto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "agendamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agendamento.findAll", query = "SELECT a FROM Agendamento a")
    , @NamedQuery(name = "Agendamento.findByAgendamentoId", query = "SELECT a FROM Agendamento a WHERE a.agendamentoId = :agendamentoId")
    , @NamedQuery(name = "Agendamento.findByAgendamentoData", query = "SELECT a FROM Agendamento a WHERE a.agendamentoData = :agendamentoData")
    , @NamedQuery(name = "Agendamento.findByAgendamentoDetalhes", query = "SELECT a FROM Agendamento a WHERE a.agendamentoDetalhes = :agendamentoDetalhes")})
public class Agendamento extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "agendamento_id")
    private Integer agendamentoId;
    @Basic(optional = false)
    @Column(name = "agendamento_data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date agendamentoData;
    @Column(name = "agendamento_detalhes")
    private String agendamentoDetalhes;
    @JoinColumn(name = "animal_id", referencedColumnName = "animal_id")
    @ManyToOne(optional = false)
    private Animal animalId;
    @JoinColumn(name = "produto_id", referencedColumnName = "produto_id")
    @ManyToOne
    private Produto produtoId;
    @JoinColumn(name = "usr_id", referencedColumnName = "usr_id")
    @ManyToOne(optional = false)
    private Usuario usrId;

    @OneToMany(fetch=FetchType.LAZY, mappedBy = "agendamentoId")
    private Collection<Atendimento> atendimentoCollection = new ArrayList();

    public Agendamento() {
    }

    public Agendamento(Integer agendamentoId) {
        this.agendamentoId = agendamentoId;
    }

    public Agendamento(Date agendamentoData, String agendamentoDetalhes) {
        this.agendamentoData = agendamentoData;
        this.agendamentoDetalhes = agendamentoDetalhes;
    }

    public Agendamento(Date agendamentoData) {
        this.agendamentoData = agendamentoData;
    }

    public Agendamento(Integer agendamentoId, Date agendamentoData) {
        this.agendamentoId = agendamentoId;
        this.agendamentoData = agendamentoData;
    }

    public Integer getAgendamentoId() {
        return agendamentoId;
    }

    public void setAgendamentoId(Integer agendamentoId) {
        this.agendamentoId = agendamentoId;
    }

    public Date getAgendamentoData() {
        return agendamentoData;
    }

    public void setAgendamentoData(Date agendamentoData) {
        this.agendamentoData = agendamentoData;
    }

    public String getAgendamentoDetalhes() {
        return agendamentoDetalhes;
    }

    public void setAgendamentoDetalhes(String agendamentoDetalhes) {
        this.agendamentoDetalhes = agendamentoDetalhes;
    }

    public Animal getAnimalId() {
        return animalId;
    }

    public void setAnimalId(Animal animalId) {
        this.animalId = animalId;
    }

    public Produto getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(Produto produtoId) {
        this.produtoId = produtoId;
    }

    public Usuario getUsrId() {
        return usrId;
    }

    public void setUsrId(Usuario usrId) {
        this.usrId = usrId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agendamentoId != null ? agendamentoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agendamento)) {
            return false;
        }
        Agendamento other = (Agendamento) object;
        if ((this.agendamentoId == null && other.agendamentoId != null) || (this.agendamentoId != null && !this.agendamentoId.equals(other.agendamentoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Agendamento.Entidade.Agendamento[ agendamentoId=" + agendamentoId + " ]";
    }

    public String getAgendamentoHorario() {
        return agendamentoData != null ? agendamentoData.getHours() + ":00" : "";
    }
    
    public Object getClienteId(){
        return animalId!= null ? animalId.getCliId() : null;
    }
    
    public String getTelefone(){
        return animalId!= null ? animalId.getCliId() != null ? animalId.getCliId().getCliTelefone() : null : null;
    }

    @XmlTransient
    public Collection<Atendimento> getAtendimentoCollection() {
        return atendimentoCollection;
    }

    public void setAtendimentoCollection(Collection<Atendimento> atendimentoCollection) {
        this.atendimentoCollection = atendimentoCollection;
    }
    
}
