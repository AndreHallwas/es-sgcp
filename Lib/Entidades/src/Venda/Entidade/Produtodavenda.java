/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Venda.Entidade;

import JPA.Dao.JPADao;
import Produto.Entidade.Produto;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "produtodavenda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produtodavenda.findAll", query = "SELECT p FROM Produtodavenda p")
    , @NamedQuery(name = "Produtodavenda.findByVendaId", query = "SELECT p FROM Produtodavenda p WHERE p.produtodavendaPK.vendaId = :vendaId")
    , @NamedQuery(name = "Produtodavenda.findByProdutoId", query = "SELECT p FROM Produtodavenda p WHERE p.produtodavendaPK.produtoId = :produtoId")
    , @NamedQuery(name = "Produtodavenda.findByProdvQuantidade", query = "SELECT p FROM Produtodavenda p WHERE p.prodvQuantidade = :prodvQuantidade")
    , @NamedQuery(name = "Produtodavenda.findByProdvValor", query = "SELECT p FROM Produtodavenda p WHERE p.prodvValor = :prodvValor")
    , @NamedQuery(name = "Produtodavenda.findByPk", query = "SELECT p FROM Produtodavenda p WHERE p.produtodavendaPK.vendaId = :vendaId and "
        + " p.produtodavendaPK.produtoId = :produtoId")})
public class Produtodavenda extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProdutodavendaPK produtodavendaPK;
    @Basic(optional = false)
    @Column(name = "prodv_quantidade")
    private int prodvQuantidade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "prodv_valor")
    private BigDecimal prodvValor;
    @JoinColumn(name = "produto_id", referencedColumnName = "produto_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Produto produto;
    @JoinColumn(name = "venda_id", referencedColumnName = "venda_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Venda venda;

    public Produtodavenda() {
    }

    public Produtodavenda(ProdutodavendaPK produtodavendaPK) {
        this.produtodavendaPK = produtodavendaPK;
    }

    public Produtodavenda(ProdutodavendaPK produtodavendaPK, int prodvQuantidade, BigDecimal prodvValor) {
        this.produtodavendaPK = produtodavendaPK;
        this.prodvQuantidade = prodvQuantidade;
        this.prodvValor = prodvValor;
    }

    public Produtodavenda(Integer produtoId, Integer vendaId, int prodvQuantidade, BigDecimal prodvValor) {
        this.produtodavendaPK = new ProdutodavendaPK(vendaId, produtoId);
        this.prodvQuantidade = prodvQuantidade;
        this.prodvValor = prodvValor;
    }

    public Produtodavenda(int vendaId, int produtoId) {
        this.produtodavendaPK = new ProdutodavendaPK(vendaId, produtoId);
    }

    public ProdutodavendaPK getProdutodavendaPK() {
        return produtodavendaPK;
    }

    public void setProdutodavendaPK(ProdutodavendaPK produtodavendaPK) {
        this.produtodavendaPK = produtodavendaPK;
    }

    public int getProdvQuantidade() {
        return prodvQuantidade;
    }

    public void setProdvQuantidade(int prodvQuantidade) {
        this.prodvQuantidade = prodvQuantidade;
    }

    public BigDecimal getProdvValor() {
        return prodvValor;
    }

    public void setProdvValor(BigDecimal prodvValor) {
        this.prodvValor = prodvValor;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (produtodavendaPK != null ? produtodavendaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produtodavenda)) {
            return false;
        }
        Produtodavenda other = (Produtodavenda) object;
        if ((this.produtodavendaPK == null && other.produtodavendaPK != null) || (this.produtodavendaPK != null && !this.produtodavendaPK.equals(other.produtodavendaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidade.Produtodavenda[ produtodavendaPK=" + produtodavendaPK + " ]";
    }
    
}
