/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.UI.Pagina;

/**
 *
 * @author Raizen
 */
public class Lista {

    private Node Inicio;
    private Node Fim;
    private Node Atual;

    public void add(Acao info) {
        if (Inicio == null) {
            Inicio = Fim = new Node(null, null, info);
        } else {
            Fim.setProximo(new Node(null, Fim, info));
            Fim = Fim.getProximo();
        }
    }

    public boolean SelecionaProximo(Object... Params) {
        boolean flag = true;
        if (Atual == null && Inicio != null) {
            Atual = Inicio;
            if (Atual != null) {
                flag = flag && Atual.Seleciona(Params);
            }
        }else{
            Atual = Atual.Proximo(Params);
            flag = flag && true;
        }
        return flag;
    }

    public Node get(int Pos) {
        Node aux = Inicio;
        int pos = 0;
        while (aux.getProximo() != null && pos < Pos) {
            pos++;
        }
        if (aux.getProximo() != null) {
            return aux;
        }
        return null;
    }

    /**
     * @return the Inicio
     */
    public Node getInicio() {
        return Inicio;
    }

    /**
     * @param Inicio the Inicio to set
     */
    public void setInicio(Node Inicio) {
        this.Inicio = Inicio;
    }

    /**
     * @return the Fim
     */
    public Node getFim() {
        return Fim;
    }

    /**
     * @param Fim the Fim to set
     */
    public void setFim(Node Fim) {
        this.Fim = Fim;
    }

}
