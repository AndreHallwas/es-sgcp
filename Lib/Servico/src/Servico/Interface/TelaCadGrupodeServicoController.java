/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Interface;

import Servico.Controladora.CtrlGrupodeservico;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadGrupodeServicoController implements Initializable, Tela {

    @FXML
    private HBox pndados;
    @FXML
    private JFXTextArea taDescricao;
    @FXML
    private Label lbErroDescricao;
    @FXML
    private JFXButton btnovo;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btapagar;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    @FXML
    private JFXButton btConsulta;

    private int flag;
    private Integer ID;
    private boolean EOriginal;
    private Object Instancia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        estadoOriginal();
    }

    private void setAllErro(boolean Edescricao) {
        lbErroDescricao.setVisible(Edescricao);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlGrupodeservico cp = CtrlGrupodeservico.create();
        ArrayList<Object> Pesquisa = cp.Pesquisar(taDescricao.getText());
        if (taDescricao.getText().trim().isEmpty() || !Pesquisa.isEmpty() || Pesquisa.isEmpty()) {
            if (taDescricao.getText().trim().isEmpty()) {
                setErro(lbErroDescricao, "Campo Obrigatório Vazio");
                fl = false;
                taDescricao.setText("");
                taDescricao.requestFocus();
            } else if (!Pesquisa.isEmpty() && flag == 1) {
                setErro(lbErroDescricao, "Nome Ja Existe");
                fl = false;
                taDescricao.setText("");
                taDescricao.requestFocus();
            }
        }
        return fl;
    }

    @FXML
    private void evtNovo(Event event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(Event event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(Event event) {
        CtrlGrupodeservico cp = CtrlGrupodeservico.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(ID);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(Event event) {
        setAllErro(false);
        CtrlGrupodeservico cp = CtrlGrupodeservico.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, ID, taDescricao.getText())) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(taDescricao.getText())) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(Event event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);

        clear(pndados.getChildren());
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlGrupodeservico.setCampos(p, taDescricao);
            ID = CtrlGrupodeservico.getId(p);
        }
    }

    @FXML
    private void evtConsultaAvancada(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Servico/Interface/TelaConsultaGrupodeServico.fxml",
                "Consulta de Grupo de Serviço", null);
        if (TelaConsultaGrupodeServicoController.getGrupodeservico() != null) {
            EstadoConsulta(TelaConsultaGrupodeServicoController.getGrupodeservico());
        }
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }

}
