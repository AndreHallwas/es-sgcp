/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Raizen
 */
public class Requisicao {

    public static Requisicao Create(String Endereco, String TipoDaRequisicao, String... ParametrosDaRequisicao) {
        return new Requisicao(Endereco, TipoDaRequisicao, ParametrosDaRequisicao);
    }

    private URL url;
    private HttpURLConnection con;
    private StringBuffer Resposta = null;
    private BufferedReader in;
    private String Endereco;
    private String TipoDeRequisicao;
    private String[] Parametros;

    private Requisicao(String Endereco, String TipoDaRequisicao, String... ParametrosDaRequisicao) {
        Inicializa(Endereco, TipoDaRequisicao, ParametrosDaRequisicao);
    }

    public void Inicializa(String Endereco, String TipoDaRequisicao, String... ParametrosDaRequisicao) {
        this.setEndereco(Endereco);
        this.setTipoDeRequisicao(TipoDaRequisicao);
        addParametros(ParametrosDaRequisicao);
    }

    public void addParametros(String... ParametrosDaRequisicao) {
        setParametros(ParametrosDaRequisicao);
    }

    public String Consulta(String Requisicao) {
        Requisita(Requisicao);
        try {
            if (getCon().getResponseCode() == 200) {
                return LeResposta();
            }
            return getCon().getResponseCode() + "";
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro na Consulta da Requisição");
        }
        return "404";
    }

    public HttpURLConnection Requisita(String Requisicao) {
        try {
            url = new URL(getEndereco() + Requisicao.toLowerCase());
            setCon((HttpURLConnection) url.openConnection());

            getCon().setRequestMethod(getTipoDeRequisicao());
            for (int i = 0; i <= getParametros().length / 2; i += 2) {
                getCon().setRequestProperty(getParametros()[i], getParametros()[i + 1]);
            }

            getCon().connect();
        } catch (MalformedURLException ex) {
            Mensagem.ExibirException(ex, "Erro na Requisição");
        } catch (ProtocolException ex) {
            Mensagem.ExibirException(ex, "Erro na Requisição");
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro na Requisição");
        }

        return getCon();
    }

    public String LeResposta() {
        try {
            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            setResposta(new StringBuffer());
            while ((inputLine = in.readLine()) != null) {
                Resposta.append(inputLine);
            }
            in.close();
            con.disconnect();
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Ler Resposta da Requisição");
        }

        return Resposta.toString();
    }

    public int getResponse() {
        try {
            return getCon().getResponseCode();
        } catch (IOException ex) {
            Logger.getLogger(Requisicao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 404;
    }

    public String getResponseMessage() {
        try {
            return getCon().getResponseMessage();
        } catch (IOException ex) {
            Logger.getLogger(Requisicao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "404";
    }

    /**
     * @return the con
     */
    public HttpURLConnection getCon() {
        return con;
    }

    /**
     * @param con the con to set
     */
    public void setCon(HttpURLConnection con) {
        this.con = con;
    }

    /**
     * @return the Resposta
     */
    public StringBuffer getResposta() {
        return Resposta;
    }

    /**
     * @param Resposta the Resposta to set
     */
    public void setResposta(StringBuffer Resposta) {
        this.Resposta = Resposta;
    }

    /**
     * @return the Endereco
     */
    public String getEndereco() {
        return Endereco;
    }

    /**
     * @param Endereco the Endereco to set
     */
    public void setEndereco(String Endereco) {
        this.Endereco = Endereco;
    }

    /**
     * @return the TipoDeRequisicao
     */
    public String getTipoDeRequisicao() {
        return TipoDeRequisicao;
    }

    /**
     * @param TipoDeRequisicao the TipoDeRequisicao to set
     */
    public void setTipoDeRequisicao(String TipoDeRequisicao) {
        this.TipoDeRequisicao = TipoDeRequisicao;
    }

    /**
     * @return the Parametros
     */
    public String[] getParametros() {
        return Parametros;
    }

    /**
     * @param Parametros the Parametros to set
     */
    public void setParametros(String[] Parametros) {
        this.Parametros = Parametros;
    }

}
