/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Empresa.Entidade;

import Endereco.Entidade.Complementoendereco;
import JPA.Dao.JPADao;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e")
    , @NamedQuery(name = "Empresa.findByEmpId", query = "SELECT e FROM Empresa e WHERE e.empId = :empId")
    , @NamedQuery(name = "Empresa.findByEmpRazaosocial", query = "SELECT e FROM Empresa e WHERE e.empRazaosocial = :empRazaosocial")
    , @NamedQuery(name = "Empresa.findByEmpTelefone", query = "SELECT e FROM Empresa e WHERE e.empTelefone = :empTelefone")
    , @NamedQuery(name = "Empresa.findByEmpNome", query = "SELECT e FROM Empresa e WHERE e.empNome = :empNome")
    , @NamedQuery(name = "Empresa.findByEmpCnpj", query = "SELECT e FROM Empresa e WHERE e.empCnpj = :empCnpj")})
public class Empresa extends JPADao implements Serializable {

    @Lob
    @Column(name = "emp_logo")
    private byte[] empLogo;
    @JoinColumn(name = "end_id", referencedColumnName = "end_id")
    @ManyToOne(optional = false)
    private Complementoendereco endId;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "emp_id")
    private Integer empId = 1;
    @Column(name = "emp_razaosocial")
    private String empRazaosocial;
    @Basic(optional = false)
    @Column(name = "emp_telefone")
    private String empTelefone;
    @Basic(optional = false)
    @Column(name = "emp_nome")
    private String empNome;
    @Basic(optional = false)
    @Column(name = "emp_cnpj")
    private String empCnpj;

    public Empresa() {
    }

    public Empresa(Integer empId) {
        this.empId = empId;
    }

    public Empresa(String empRazaosocial, String empTelefone, String empNome, String empCnpj, byte[] empLogo) {
        this.empRazaosocial = empRazaosocial;
        this.empTelefone = empTelefone;
        this.empNome = empNome;
        this.empCnpj = empCnpj;
        this.empLogo = empLogo;
    }

    public Empresa(Integer empId, String empTelefone, String empNome, String empCnpj) {
        this.empId = empId;
        this.empTelefone = empTelefone;
        this.empNome = empNome;
        this.empCnpj = empCnpj;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getEmpRazaosocial() {
        return empRazaosocial;
    }

    public void setEmpRazaosocial(String empRazaosocial) {
        this.empRazaosocial = empRazaosocial;
    }

    public String getEmpTelefone() {
        return empTelefone;
    }

    public void setEmpTelefone(String empTelefone) {
        this.empTelefone = empTelefone;
    }

    public String getEmpNome() {
        return empNome;
    }

    public void setEmpNome(String empNome) {
        this.empNome = empNome;
    }

    public String getEmpCnpj() {
        return empCnpj;
    }

    public void setEmpCnpj(String empCnpj) {
        this.empCnpj = empCnpj;
    }

    public byte[] getEmpLogo() {
        return empLogo;
    }

    public void setEmpLogo(byte[] empLogo) {
        this.empLogo = empLogo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empId != null ? empId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.empId == null && other.empId != null) || (this.empId != null && !this.empId.equals(other.empId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testedb2.Empresa[ empId=" + empId + " ]";
    }

    public Complementoendereco getEndId() {
        return endId;
    }

    public void setEndId(Complementoendereco endId) {
        this.endId = endId;
    }
}
