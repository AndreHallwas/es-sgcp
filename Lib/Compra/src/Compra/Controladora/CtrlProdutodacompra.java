/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Controladora;

import Compra.Entidade.Compra;
import Compra.Entidade.Produtodacompra;
import Controladora.Base.CtrlBase;
import Produto.Entidade.Produto;
import Transacao.Transaction;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlProdutodacompra extends CtrlBase {

    private static CtrlProdutodacompra ctrlprodutodacompra;

    public static CtrlProdutodacompra create() {
        if (ctrlprodutodacompra == null) {
            ctrlprodutodacompra = new CtrlProdutodacompra();
        }
        return ctrlprodutodacompra;
    }

    public CtrlProdutodacompra() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> produtodacompra = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Produtodacompra> Resultprodutodacompra = null;
            if (Filtro == null || Filtro.isEmpty()) {
                Resultprodutodacompra = em.createNamedQuery("Produtodacompra.findAll", Produtodacompra.class)
                        .getResultList();
            } else {
                try {
                    Resultprodutodacompra = em.createNamedQuery("Produtodacompra.findByPk", Produtodacompra.class)
                            .setParameter("compraId", Integer.parseInt(Filtro))
                            .setParameter("produtoId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    Resultprodutodacompra = em.createNamedQuery("Produtodacompra.findAll", Produtodacompra.class)
                            .getResultList();
                }
            }

            for (Produtodacompra produtodacompras : Resultprodutodacompra) {
                produtodacompra.add(produtodacompras);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return produtodacompra;
    }

    public Object Salvar(int prodcQuantidade, BigDecimal prodcValor, Object aCompra, Object oProduto) {
        Produtodacompra produtodacompra = null;
        Produto produto = null;
        Compra compra = null;
        if(aCompra != null && aCompra instanceof Compra){
            compra = (Compra) aCompra;
        }
        if(oProduto != null && oProduto instanceof Produto){
            produto = (Produto) oProduto;
        }
        if(compra != null && produto != null){
           produtodacompra = new Produtodacompra(produto.getProdutoId(), compra.getCompraId(),
                   prodcQuantidade, prodcValor); 
           produtodacompra.setCompra(compra);
           produtodacompra.setProduto(produto);
           return super.Salvar(produtodacompra);
        }
        return produtodacompra;
    }

    public Object Alterar(Object oProdutodacompra, int prodcQuantidade, BigDecimal prodcValor,
            Object aCompra, Object oProduto) {
        Produtodacompra produtodacompra = null;
        Produto produto = null;
        Compra compra = null;
        if(aCompra != null && aCompra instanceof Compra){
            compra = (Compra) aCompra;
        }
        if(oProduto != null && oProduto instanceof Produto){
            produto = (Produto) oProduto;
        }
        if (oProdutodacompra != null && oProdutodacompra instanceof Produtodacompra &&
                compra != null && produto != null) {
            produtodacompra = (Produtodacompra) oProdutodacompra;
            produtodacompra.setProdcQuantidade(prodcQuantidade);
            produtodacompra.setProdcValor(prodcValor);
            produtodacompra.setCompra(compra);
            produtodacompra.setProduto(produto);
            produtodacompra = (Produtodacompra) super.Alterar(produtodacompra);
        }
        return produtodacompra;
    }

    public boolean Remover(Object Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Produtodacompra.class);
    }

}
