/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Compra.Entidade;

import ContaaPagar.Entidade.Contaapagar;
import ContaaPagar.Entidade.Parcelaapagar;
import Fornecedor.Entidade.Fornecedor;
import JPA.Dao.JPADao;
import Pessoa.Entidade.Usuario;
import Utils.FormatString;
import Variables.Variables;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "compra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compra.findAll", query = "SELECT c FROM Compra c")
    , @NamedQuery(name = "Compra.findByCompraId", query = "SELECT c FROM Compra c WHERE c.compraId = :compraId")
    , @NamedQuery(name = "Compra.findByCompraValorTotal", query = "SELECT c FROM Compra c WHERE c.compraValorTotal = :compraValorTotal")
    , @NamedQuery(name = "Compra.findByCompraData", query = "SELECT c FROM Compra c WHERE c.compraData = :compraData")
    , @NamedQuery(name = "Compra.findByCompraObs", query = "SELECT c FROM Compra c WHERE c.compraObs = :compraObs")
    , @NamedQuery(name = "Compra.findByCompraSituacao", query = "SELECT c FROM Compra c WHERE c.compraSituacao = :compraSituacao")
    , @NamedQuery(name = "Compra.findByBetween", query = "SELECT c FROM Compra c WHERE c.compraData between :datainicial and :datafinal")
    , @NamedQuery(name = "Compra.findByAllGeral", query = "SELECT c FROM Compra c WHERE c.compraId = :compraId"
            + " or c.compraValorTotal = :compraValorTotal")})
public class Compra extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "compra_id")
    private Integer compraId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "compra_valor_total")
    private BigDecimal compraValorTotal;
    @Basic(optional = false)
    @Column(name = "compra_data")
    @Temporal(TemporalType.DATE)
    private Date compraData;
    @Column(name = "compra_obs")
    private String compraObs;
    @Column(name = "compra_situacao")
    private Boolean compraSituacao;
    @JoinColumn(name = "for_id", referencedColumnName = "for_id")
    @ManyToOne(optional = false)
    private Fornecedor forId;
    @JoinColumn(name = "usr_id", referencedColumnName = "usr_id")
    @ManyToOne(optional = false)
    private Usuario usrId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "compra")
    private Collection<Produtodacompra> produtodacompraCollection = new ArrayList();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "compraId")
    private Collection<Contaapagar> contaapagarCollection = new ArrayList();

    public Compra() {
    }

    public Compra(Integer compraId) {
        this.compraId = compraId;
    }

    public Compra(BigDecimal compraValorTotal, Date compraData, String compraObs, Boolean compraSituacao) {
        this.compraValorTotal = compraValorTotal;
        this.compraData = compraData;
        this.compraObs = compraObs;
        this.compraSituacao = compraSituacao;
    }

    public Compra(Integer compraId, BigDecimal compraValorTotal, Date compraData, String compraObs, Boolean compraSituacao) {
        this.compraId = compraId;
        this.compraValorTotal = compraValorTotal;
        this.compraData = compraData;
        this.compraObs = compraObs;
        this.compraSituacao = compraSituacao;
    }

    public Compra(Integer compraId, BigDecimal compraValorTotal, Date compraData) {
        this.compraId = compraId;
        this.compraValorTotal = compraValorTotal;
        this.compraData = compraData;
    }

    public Integer getCompraId() {
        return compraId;
    }

    public void setCompraId(Integer compraId) {
        this.compraId = compraId;
    }

    public BigDecimal getCompraValorTotal() {
        return compraValorTotal;
    }
    
    public String getValorTotal(){
        return FormatString.MonetaryPersistent(compraValorTotal);
    }

    public void setCompraValorTotal(BigDecimal compraValorTotal) {
        this.compraValorTotal = compraValorTotal;
    }

    public Date getCompraData() {
        return compraData;
    }

    public void setCompraData(Date compraData) {
        this.compraData = compraData;
    }

    ///trasient
    public String getDatadaCompra() {
        return Variables.getData(compraData);
    }

    public String getCompraObs() {
        return compraObs;
    }

    public void setCompraObs(String compraObs) {
        this.compraObs = compraObs;
    }

    public Boolean getCompraSituacao() {
        return compraSituacao;
    }

    public void setCompraSituacao(Boolean compraSituacao) {
        this.compraSituacao = compraSituacao;
    }

    public Fornecedor getForId() {
        return forId;
    }

    public void setForId(Fornecedor forId) {
        this.forId = forId;
    }

    public Usuario getUsrId() {
        return usrId;
    }

    public void setUsrId(Usuario usrId) {
        this.usrId = usrId;
    }

    public Object getParcelas() {
        Object Parcelas = null;
        if (contaapagarCollection != null && !contaapagarCollection.isEmpty()) {
            for (Contaapagar contaapagar : contaapagarCollection) {
                if (contaapagar.getParcelaapagarCollection() != null
                        && !contaapagar.getParcelaapagarCollection().isEmpty()) {
                    Parcelas = contaapagar.getContaapagarParcelas();
                } else {
                    Parcelas = "Sem Parcelas";
                }
            }
        } else {
            Parcelas = "Conta Não Gerada";
        }
        return Parcelas;
    }

    public Object getSituacao() {
        Object Parcelas = null;
        if (contaapagarCollection != null && !contaapagarCollection.isEmpty()) {
            for (Contaapagar contaapagar : contaapagarCollection) {
                if (contaapagar.getParcelaapagarCollection() != null
                        && !contaapagar.getParcelaapagarCollection().isEmpty()) {
                    boolean result = true;
                    for (Parcelaapagar parcelaapagar : contaapagar.getParcelaapagarCollection()) {
                        result = parcelaapagar.getParpSituacao() && result;
                    }
                    if(result){
                        Parcelas = "Pago";
                    }else{
                        Parcelas = "Pendente";
                    }
                } else {
                    Parcelas = "Gerar Parcelas";
                }
            }
        } else {
            Parcelas = "Gerar Conta";
        }
        return Parcelas;
    }

    @XmlTransient
    public Collection<Produtodacompra> getProdutodacompraCollection() {
        return produtodacompraCollection;
    }

    public void setProdutodacompraCollection(Collection<Produtodacompra> produtodacompraCollection) {
        this.produtodacompraCollection = produtodacompraCollection;
    }

    @XmlTransient
    public Collection<Contaapagar> getContaapagarCollection() {
        return contaapagarCollection;
    }

    public void setContaapagarCollection(Collection<Contaapagar> contaapagarCollection) {
        this.contaapagarCollection = contaapagarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compraId != null ? compraId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compra)) {
            return false;
        }
        Compra other = (Compra) object;
        if ((this.compraId == null && other.compraId != null) || (this.compraId != null && !this.compraId.equals(other.compraId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return compraId + "";
    }

}
