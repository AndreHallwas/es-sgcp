/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Controladora;

import Animal.Entidade.Pelagem;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlPelagem extends CtrlBase {

    private static CtrlPelagem ctrlpelagem;

    public static CtrlPelagem create() {
        if (ctrlpelagem == null) {
            ctrlpelagem = new CtrlPelagem();
        }
        return ctrlpelagem;
    }

    public CtrlPelagem() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object pelagem, TextArea taObs) {
        if (pelagem != null && pelagem instanceof Pelagem) {
            Pelagem f = (Pelagem) pelagem;
            taObs.setText(f.getPelagemDescricao());
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Pelagem) {
            return ((Pelagem) p).getPelagemId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Pelagem) {
            txBusca.setText(((Pelagem) p).getPelagemDescricao());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> pelagem = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Pelagem> ResultPelagem;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultPelagem = em.createNamedQuery("Pelagem.findAll", Pelagem.class)
                        .getResultList();
            } else {
                try {
                    ResultPelagem = em.createNamedQuery("Pelagem.findByPelagemId", Pelagem.class)
                            .setParameter("pelagemId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultPelagem = em.createNamedQuery("Pelagem.findByPelagemDescricao", Pelagem.class)
                            .setParameter("pelagemDescricao", Filtro).getResultList();
                }
            }
            for (Pelagem pelagems : ResultPelagem) {
                pelagem.add(pelagems);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return pelagem;
    }

    public Object Salvar(String Descricao) {
        Pelagem pelagem = new Pelagem(Descricao);
        return super.Salvar(pelagem);
    }

    public Object Alterar(Object aPelagem, Integer Id, String Descricao) {
        Pelagem pelagem = null;
        if (aPelagem != null && aPelagem instanceof Pelagem){
            pelagem = (Pelagem) aPelagem;
            pelagem.setPelagemDescricao(Descricao);
            pelagem = (Pelagem) super.Alterar(pelagem);
        }
        return pelagem;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Pelagem.class);
    }
    
}
