/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "pelagem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pelagem.findAll", query = "SELECT p FROM Pelagem p")
    , @NamedQuery(name = "Pelagem.findByPelagemId", query = "SELECT p FROM Pelagem p WHERE p.pelagemId = :pelagemId")
    , @NamedQuery(name = "Pelagem.findByPelagemDescricao", query = "SELECT p FROM Pelagem p WHERE p.pelagemDescricao = :pelagemDescricao")})
public class Pelagem extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pelagem_id")
    private Integer pelagemId;
    @Basic(optional = false)
    @Column(name = "pelagem_descricao")
    private String pelagemDescricao;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "pelagemId")
    private Collection<Animal> animalCollection = new ArrayList();

    public Pelagem() {
    }

    public Pelagem(Integer pelagemId) {
        this.pelagemId = pelagemId;
    }

    public Pelagem(String pelagemDescricao) {
        this.pelagemDescricao = pelagemDescricao;
    }

    public Pelagem(Integer pelagemId, String pelagemDescricao) {
        this.pelagemId = pelagemId;
        this.pelagemDescricao = pelagemDescricao;
    }

    public Integer getPelagemId() {
        return pelagemId;
    }

    public void setPelagemId(Integer pelagemId) {
        this.pelagemId = pelagemId;
    }

    public String getPelagemDescricao() {
        return pelagemDescricao;
    }

    public void setPelagemDescricao(String pelagemDescricao) {
        this.pelagemDescricao = pelagemDescricao;
    }

    @XmlTransient
    public Collection<Animal> getAnimalCollection() {
        return animalCollection;
    }

    public void setAnimalCollection(Collection<Animal> animalCollection) {
        this.animalCollection = animalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pelagemId != null ? pelagemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pelagem)) {
            return false;
        }
        Pelagem other = (Pelagem) object;
        if ((this.pelagemId == null && other.pelagemId != null) || (this.pelagemId != null && !this.pelagemId.equals(other.pelagemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return pelagemDescricao;
    }
    
}
