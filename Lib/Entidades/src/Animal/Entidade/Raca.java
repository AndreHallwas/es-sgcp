/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Entidade;

import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "raca")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Raca.findAll", query = "SELECT r FROM Raca r")
    , @NamedQuery(name = "Raca.findByRacaId", query = "SELECT r FROM Raca r WHERE r.racaId = :racaId")
    , @NamedQuery(name = "Raca.findByRacaNome", query = "SELECT r FROM Raca r WHERE r.racaNome = :racaNome")
    , @NamedQuery(name = "Raca.findByRacaDescricao", query = "SELECT r FROM Raca r WHERE r.racaDescricao = :racaDescricao")})
public class Raca extends JPADao implements Serializable {

    @Lob
    @Column(name = "raca_imagem")
    private byte[] racaImagem;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "raca_id")
    private Integer racaId;
    @Basic(optional = false)
    @Column(name = "raca_nome")
    private String racaNome;
    @Column(name = "raca_descricao")
    private String racaDescricao;
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "racaId")
    private Collection<Animal> animalCollection = new ArrayList();

    public Raca() {
    }

    public Raca(Integer racaId) {
        this.racaId = racaId;
    }

    public Raca(String racaNome, String racaDescricao) {
        this.racaNome = racaNome;
        this.racaDescricao = racaDescricao;
    }

    public Raca(String racaNome, String racaDescricao, byte[] racaImagem){
        this.racaImagem = racaImagem;
        this.racaNome = racaNome;
        this.racaDescricao = racaDescricao;
    }

    public Raca(Integer racaId, String racaNome, String racaDescricao) {
        this.racaId = racaId;
        this.racaNome = racaNome;
        this.racaDescricao = racaDescricao;
    }

    public Raca(Integer racaId, String racaNome) {
        this.racaId = racaId;
        this.racaNome = racaNome;
    }

    public Integer getRacaId() {
        return racaId;
    }

    public void setRacaId(Integer racaId) {
        this.racaId = racaId;
    }

    public String getRacaNome() {
        return racaNome;
    }

    public void setRacaNome(String racaNome) {
        this.racaNome = racaNome;
    }

    public String getRacaDescricao() {
        return racaDescricao;
    }

    public void setRacaDescricao(String racaDescricao) {
        this.racaDescricao = racaDescricao;
    }

    @XmlTransient
    public Collection<Animal> getAnimalCollection() {
        return animalCollection;
    }

    public void setAnimalCollection(Collection<Animal> animalCollection) {
        this.animalCollection = animalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (racaId != null ? racaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Raca)) {
            return false;
        }
        Raca other = (Raca) object;
        if ((this.racaId == null && other.racaId != null) || (this.racaId != null && !this.racaId.equals(other.racaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return racaNome;
    }

    public byte[] getRacaImagem() {
        return racaImagem;
    }

    public void setRacaImagem(byte[] racaImagem) {
        this.racaImagem = racaImagem;
    }
}
