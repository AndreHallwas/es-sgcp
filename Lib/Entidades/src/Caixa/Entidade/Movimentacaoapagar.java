/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caixa.Entidade;

import ContaaPagar.Entidade.Parcelaapagar;
import Utils.FormatString;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "movimentacaoapagar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Movimentacaoapagar.findAll", query = "SELECT m FROM Movimentacaoapagar m")
    , @NamedQuery(name = "Movimentacaoapagar.findByMovimentacaoapagarId", query = "SELECT m FROM Movimentacaoapagar m WHERE m.movimentacaoapagarId = :movimentacaoapagarId")
    , @NamedQuery(name = "Movimentacaoapagar.findByMovimentacaoapagarData", query = "SELECT m FROM Movimentacaoapagar m WHERE m.movimentacaoapagarData = :movimentacaoapagarData")})
public class Movimentacaoapagar implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "movimentacaoapagar_id")
    private Integer movimentacaoapagarId;
    @Basic(optional = false)
    @Column(name = "movimentacaoapagar_data")
    @Temporal(TemporalType.TIMESTAMP)
    private Date movimentacaoapagarData;
    @JoinColumn(name = "caixa_id", referencedColumnName = "caixa_id")
    @ManyToOne(optional = false)
    private Caixa caixaId;
    @JoinColumns({
        @JoinColumn(name = "contaapagar_id", referencedColumnName = "contaapagar_id")
        , @JoinColumn(name = "parp_numerodaparcela", referencedColumnName = "parp_numerodaparcela")
        , @JoinColumn(name = "parp_datadegeracao", referencedColumnName = "parp_datadegeracao")})
    @ManyToOne(optional = false)
    private Parcelaapagar parcelaapagar;

    public Movimentacaoapagar() {
    }

    public Movimentacaoapagar(Date movimentacaoapagarData) {
        this.movimentacaoapagarData = movimentacaoapagarData;
    }

    public Movimentacaoapagar(Integer movimentacaoapagarId) {
        this.movimentacaoapagarId = movimentacaoapagarId;
    }

    public Movimentacaoapagar(Integer movimentacaoapagarId, Date movimentacaoapagarData) {
        this.movimentacaoapagarId = movimentacaoapagarId;
        this.movimentacaoapagarData = movimentacaoapagarData;
    }

    public Integer getMovimentacaoapagarId() {
        return movimentacaoapagarId;
    }

    public void setMovimentacaoapagarId(Integer movimentacaoapagarId) {
        this.movimentacaoapagarId = movimentacaoapagarId;
    }

    public Date getMovimentacaoapagarData() {
        return movimentacaoapagarData;
    }

    public void setMovimentacaoapagarData(Date movimentacaoapagarData) {
        this.movimentacaoapagarData = movimentacaoapagarData;
    }

    public Caixa getCaixaId() {
        return caixaId;
    }

    public void setCaixaId(Caixa caixaId) {
        this.caixaId = caixaId;
    }

    public Parcelaapagar getParcelaapagar() {
        return parcelaapagar;
    }

    public void setParcelaapagar(Parcelaapagar parcelaapagar) {
        this.parcelaapagar = parcelaapagar;
    }
    
    public String getTipodeMovimentacao(){
        return "Saída";
    }
    
    public String getValorMovimentado(){
        BigDecimal Valor = BigDecimal.ZERO;
        if(parcelaapagar != null && parcelaapagar.getParpValorPago() != null){
            Valor = parcelaapagar.getParpValorPago();
        }
        return FormatString.MonetaryPersistent(Valor);
    }
    
    public String getDescricaoPagamento(){
        String result = "Sem Descrição";
        if(parcelaapagar != null && parcelaapagar.getContaapagar() != null &&
                parcelaapagar.getContaapagar().getContaapagarDescricao() != null &&
                !parcelaapagar.getContaapagar().getContaapagarDescricao().trim().isEmpty()){
            result = parcelaapagar.getContaapagar().getContaapagarDescricao();
        }
        return result;
    }
    
    public Object getFormadePagamento(){
        Object result = "Não Definido";
        if(parcelaapagar != null && parcelaapagar.getFormadepagamentoId() != null){
            result = parcelaapagar.getFormadepagamentoId();
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (movimentacaoapagarId != null ? movimentacaoapagarId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Movimentacaoapagar)) {
            return false;
        }
        Movimentacaoapagar other = (Movimentacaoapagar) object;
        if ((this.movimentacaoapagarId == null && other.movimentacaoapagarId != null) || (this.movimentacaoapagarId != null && !this.movimentacaoapagarId.equals(other.movimentacaoapagarId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Caixa.Entidade.Movimentacaoapagar[ movimentacaoapagarId=" + movimentacaoapagarId + " ]";
    }
    
}
