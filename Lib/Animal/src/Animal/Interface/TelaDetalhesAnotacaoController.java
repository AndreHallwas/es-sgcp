/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Interface;

import Animal.Controladora.CtrlAnimal;
import Animal.Controladora.CtrlAnotacao;
import Utils.Controladora.CtrlUtils;
import Utils.DateUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaDetalhesAnotacaoController implements Initializable {

    @FXML
    private DatePicker dtpData;
    @FXML
    private JFXTextField txbAnimal;
    @FXML
    private JFXTextArea taDescricao;
    @FXML
    private JFXTextField txbId;

    private static int flag;
    private static Object animal;

    private static Object Anotacao;
    private Integer ID;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        if (Anotacao != null) {
            CtrlAnotacao.setCampos(Anotacao, taDescricao, txbAnimal, dtpData, txbId);
        }else{
            CtrlAnimal.setCampoBusca(animal, txbAnimal);
        }
        dtpData.setValue(LocalDate.now());
    }

    @FXML
    private void evtConfirmar(MouseEvent event) {
        setAllErro(false, false);
        CtrlAnotacao cp = CtrlAnotacao.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Anotacao = cp.Alterar(Anotacao, ID, taDescricao.getText(),
                        DateUtils.asDate(dtpData.getValue()), animal)) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                    evtClose(event);
                }
            } else//cadastrar
            if ((Anotacao = cp.Salvar(taDescricao.getText(),
                    DateUtils.asDate(dtpData.getValue()), animal)) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
                evtClose(event);
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }

    }

    private void estadoOriginal() {
        setFlag(1);
        setAnimal(null);
    }

    /**
     * @return the Anotacao
     */
    public static Object getAnotacao() {
        return Anotacao;
    }

    /**
     * @param aFlag the flag to set
     */
    public static void setFlag(int aFlag) {
        flag = aFlag;
    }

    /**
     * @param aAnimal the animal to set
     */
    public static void setAnimal(Object aAnimal) {
        animal = aAnimal;
    }

    /**
     * @param aAnotacao the Anotacao to set
     */
    public static void setAnotacao(Object aAnotacao) {
        Anotacao = aAnotacao;
    }

    private void setAllErro(boolean Obs, boolean Data) {
        //Erros não Implementados
    }

    private boolean validaCampos() {
        //Validações não implementadas
        /**
         * boolean fl = true; if (txbNome.getText().trim().isEmpty()) {
         * setErro(lbErroNome, "Campo Obrigatório Vazio"); fl = false;
         * txbNome.setText(""); txbNome.requestFocus(); }
         */
        return true;
    }

    @FXML
    private void evtClose(Event event) {
        CtrlUtils.CloseStage(event);
    }

}
