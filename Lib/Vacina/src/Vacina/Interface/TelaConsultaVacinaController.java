/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vacina.Interface;

import Pessoa.Controladora.CtrlCliente;
import Produto.Controladora.CtrlProduto;
import Produto.Interface.TelaConsultaProdutoController;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import Vacina.Controladora.CtrlVacina;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaVacinaController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Object> tcID;
    @FXML
    private TableColumn<Object, Object> tcVacina;
    @FXML
    private TableColumn<Object, Object> tcData;
    @FXML
    private TableColumn<Object, Object> tcAplicacao;
    @FXML
    private HBox btnGp;
    
    private static Object vacina;
    protected static Object Cliente;
    private static boolean btnPainel = true;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcAplicacao.setCellValueFactory(new PropertyValueFactory("DataAplicacao"));
        tcData.setCellValueFactory(new PropertyValueFactory("Data"));
        tcVacina.setCellValueFactory(new PropertyValueFactory("Produto"));     
        tcID.setCellValueFactory(new PropertyValueFactory("Id"));
        CarregaTabela("");
        vacina = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlVacina ctv = CtrlVacina.create();
        try {
            tabela.setItems(FXCollections.observableList(ctv.Pesquisar(CtrlCliente.getID(Cliente)+"#:#"+filtro, "Venda")));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            vacina = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (vacina != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhuma Vacinacao Selecionada", 2);
        }
    }

    public static TelaConsultaVacinaController Create(Class classe, Pane pndados) {
        TelaConsultaVacinaController CProduto = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Vacina/Interface/TelaConsultaVacina.fxml"));
            Parent root = Loader.load();
            CProduto = (TelaConsultaVacinaController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CProduto;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaVacinaController.btnPainel = btnPainel;
    }

    public static Object getVacina() {
        return vacina;
    }

    /**
     * @param aCliente the Cliente to set
     */
    public static void setCliente(Object aCliente) {
        Cliente = aCliente;
    }
}
