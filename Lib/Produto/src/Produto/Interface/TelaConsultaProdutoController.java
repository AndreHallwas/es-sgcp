/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Produto.Controladora.CtrlProduto;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaProdutoController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcQuantidade;
    @FXML
    private TableColumn<Object, String> tcCategoria;
    @FXML
    private TableColumn<Object, String> tcObs;
    @FXML
    private TableColumn<Object, String> tcID;
    @FXML
    private TableColumn<Object, String> tcPrecoCompra;
    @FXML
    private TableColumn<Object, String> tcPrecoVenda;
    @FXML
    private HBox btnGp;
    
    private static Object produto;
    private static boolean btnPainel = true;
    protected static String Tipo = "Geral";

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("ProdutoNome"));
        tcPrecoCompra.setCellValueFactory(new PropertyValueFactory("PrecoCompra"));
        tcPrecoVenda.setCellValueFactory(new PropertyValueFactory("PrecoVenda"));
        tcQuantidade.setCellValueFactory(new PropertyValueFactory("ProdutoQuantidade"));
        tcCategoria.setCellValueFactory(new PropertyValueFactory("Categoriaproduto"));
        tcObs.setCellValueFactory(new PropertyValueFactory("ProdutoObs"));        
        tcID.setCellValueFactory(new PropertyValueFactory("ProdutoId"));
        CarregaTabela("");
        produto = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlProduto ctm = CtrlProduto.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, Tipo)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            produto = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (produto != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhum Serviço Selecionado", 2);
        }
    }

    public static TelaConsultaProdutoController Create(Class classe, Pane pndados) {
        TelaConsultaProdutoController CProduto = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Produto/Interface/TelaConsultaProduto.fxml"));
            Parent root = Loader.load();
            CProduto = (TelaConsultaProdutoController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CProduto;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaProdutoController.btnPainel = btnPainel;
    }

    public static Object getProduto() {
        return produto;
    }

    /**
     * @param aTipo the Tipo to set
     */
    public static void setTipo(String aTipo) {
        Tipo = aTipo;
    }
}
