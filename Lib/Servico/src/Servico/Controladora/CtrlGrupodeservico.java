/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Controladora;

import Controladora.Base.CtrlBase;
import Servico.Entidade.Grupodeservico;
import Transacao.Transaction;
import com.jfoenix.controls.JFXTextArea;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlGrupodeservico extends CtrlBase {

    private static CtrlGrupodeservico ctrlservico;

    public static CtrlGrupodeservico create() {
        if (ctrlservico == null) {
            ctrlservico = new CtrlGrupodeservico();
        }
        return ctrlservico;
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Grupodeservico) {
            return ((Grupodeservico) p).getGrupodeservicoId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Grupodeservico) {
            txBusca.setText(Integer.toString(((Grupodeservico) p).getGrupodeservicoId()));
        }
    }

    public static void setCampos(Object p, JFXTextArea taDescricao) {
         if (p != null && p instanceof Grupodeservico) {
             Grupodeservico gp = (Grupodeservico) p;
             taDescricao.setText(gp.getGrupodeservicoDescricao());
         }
    }

    private CtrlGrupodeservico() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> servico = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            List<Grupodeservico> ResultGrupodeservico;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultGrupodeservico = em.createNamedQuery("Grupodeservico.findAll", Grupodeservico.class)
                        .getResultList();
            } else {
                try {
                    ResultGrupodeservico = em.createNamedQuery("Grupodeservico.findByGrupodeservicoId", Grupodeservico.class)
                            .setParameter("grupodeservicoId", Integer.parseInt(Filtro)).getResultList();
                } catch (NumberFormatException ex) {
                    ResultGrupodeservico = em.createNamedQuery("Grupodeservico.findByGrupodeservicoDescricao", Grupodeservico.class)
                            .setParameter("grupodeservicoDescricao", Filtro).getResultList();
                }
            }

            for (Grupodeservico Gruposdeservico : ResultGrupodeservico) {
                servico.add(Gruposdeservico);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return servico;
    }

    public Object Salvar(String Descricao) {
        Grupodeservico grupodeservico = new Grupodeservico(Descricao);
        return super.Salvar(grupodeservico);
    }

    public Object Alterar(Object oGrupodeServico, Integer Id, String Descricao) {
        Grupodeservico grupodeservico = null;
        if(oGrupodeServico != null && oGrupodeServico instanceof Grupodeservico){
            grupodeservico = (Grupodeservico) oGrupodeServico;
            grupodeservico.setGrupodeservicoDescricao(Descricao);
            grupodeservico = (Grupodeservico) super.Alterar(grupodeservico);
        }
        return grupodeservico;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Grupodeservico.class);
    }
}
