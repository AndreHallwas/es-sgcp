/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servico.Interface;

import Servico.Controladora.CtrlGrupodeservico;
import Servico.Controladora.CtrlServico;
import Utils.Controladora.CtrlUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import Utils.UI.Tela.Tela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaCadServicoController implements Initializable, Tela {

    @FXML
    private HBox pndados;
    @FXML
    private JFXTextField txbNome;
    @FXML
    private Label lbErroNome;
    @FXML
    private JFXTextField txbValor;
    @FXML
    private Label lbErroValor;
    @FXML
    private JFXTextArea taDescricao;
    @FXML
    private Label lbErroDescricao;
    @FXML
    private ComboBox<Object> cbGrupodeServiço;
    @FXML
    private JFXButton btnovo;
    @FXML
    private JFXButton btalterar;
    @FXML
    private JFXButton btapagar;
    @FXML
    private JFXButton btconfirmar;
    @FXML
    private JFXButton btcancelar;
    @FXML
    private JFXButton btConsulta;

    private int flag;
    private Integer ID;
    private boolean EOriginal;
    private Object Instancia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MaskFieldUtil.monetaryField(txbValor);
        estadoOriginal();
    }

    private void setAllErro(boolean Enome, boolean Evalor, boolean Edescricao) {
        lbErroDescricao.setVisible(Edescricao);
        lbErroValor.setVisible(Evalor);
        lbErroNome.setVisible(Enome);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public boolean validaCampos() //Validações de banco
    {
        boolean fl = true;
        CtrlServico cp = CtrlServico.create();
        ArrayList<Object> Pesquisa = cp.Pesquisar(txbNome.getText());
        if (txbNome.getText().trim().isEmpty() || !Pesquisa.isEmpty() || Pesquisa.isEmpty()) {
            if (txbNome.getText().trim().isEmpty()) {
                setErro(lbErroNome, "Campo Obrigatório Vazio");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            } else if (!Pesquisa.isEmpty() && flag == 1) {
                setErro(lbErroNome, "Nome Ja Existe");
                fl = false;
                txbNome.setText("");
                txbNome.requestFocus();
            }
        }
        if (txbValor.getText().trim().isEmpty()) {
            setErro(lbErroValor, "Campo Obrigatório Vazio");
            txbValor.setText("");
            txbValor.requestFocus();
            fl = false;
        }
        return fl;
    }

    @FXML
    private void evtNovo(Event event) {
        clear(pndados.getChildren());
        EstadoEdicao();
        flag = 1;
    }

    @FXML
    private void evtAlterar(Event event) {
        EstadoEdicao();
        flag = 0;
    }

    @FXML
    private void evtApagar(Event event) {
        CtrlServico cp = CtrlServico.create();
        if (Mensagem.ExibirConfimacao("Deseja Realmente Excluir")) {
            cp.Remover(ID);
        }
        estadoOriginal();
    }

    @FXML
    private void evtConfirmar(Event event) {
        setAllErro(false, false, false);
        CtrlServico cp = CtrlServico.create();
        if (validaCampos()) {
            if (flag == 0)//alterar
            {
                if ((Instancia = cp.Alterar(Instancia, ID, MaskFieldUtil.monetaryValueFromField(txbValor), txbNome.getText(),
                        taDescricao.getText(), cbGrupodeServiço.getSelectionModel().getSelectedItem())) == null) {
                    Mensagem.Exibir(cp.getMsg(), 2);
                } else {
                    Mensagem.Exibir(cp.getMsg(), 1);
                    estadoOriginal();
                }
            } else//cadastrar
            if ((Instancia = cp.Salvar(MaskFieldUtil.monetaryValueFromField(txbValor), txbNome.getText(),
                    taDescricao.getText(), cbGrupodeServiço.getSelectionModel().getSelectedItem())) == null) {
                Mensagem.Exibir(cp.getMsg(), 2);
            } else {
                Mensagem.Exibir(cp.getMsg(), 1);
                estadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void evtCancelar(Event event) {
        if (CtrlUtils.CloseChildren(Variables._pndados, EOriginal)) {
            estadoOriginal();
        }
    }

    public void EstadoEdicao() {
        EOriginal = false;
        setAllErro(false, false, false);
        setButton(true, false, true, true, false, true);
        pndados.setDisable(false);
    }

    private void setButton(boolean novo, boolean confirmar, boolean alterar, boolean apagar, boolean cancelar, boolean consulta) {
        btnovo.setDisable(novo);
        btconfirmar.setDisable(confirmar);
        btalterar.setDisable(alterar);
        btapagar.setDisable(apagar);
        btcancelar.setDisable(cancelar);
        btConsulta.setDisable(consulta);
    }

    private void estadoOriginal() {
        EOriginal = true;

        setAllErro(false, false, false);
        setButton(false, true, true, true, false, false);
        pndados.setDisable(true);

        clear(pndados.getChildren());
        cbGrupodeServiço.getItems().clear();
        cbGrupodeServiço.getItems().addAll(CtrlGrupodeservico.create().Pesquisar(""));
        cbGrupodeServiço.getSelectionModel().select(0);
    }

    private void setCampos(Object p) {
        if (p != null) {
            CtrlServico.setCampos(p, txbNome, txbValor, taDescricao, cbGrupodeServiço);
            ID = CtrlServico.getId(p);
        }
    }

    @FXML
    private void evtConsultaAvancada(Event event) {
        IniciarTela.loadWindow(this.getClass(), "/Servico/Interface/TelaConsultaServico.fxml",
                "Consulta de Serviço", null);
        if (TelaConsultaServicoController.getServico() != null) {
            EstadoConsulta(TelaConsultaServicoController.getServico());
        }
    }

    private void EstadoConsulta(Object f) {
        setCampos(f);
        setButton(true, true, false, false, false, true);
        Instancia = f;
        flag = 0;
        EOriginal = false;
    }

    public void clear(ObservableList<Node> pn) {
        CtrlUtils.clear(pn);
    }
}
