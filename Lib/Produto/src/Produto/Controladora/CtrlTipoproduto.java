/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Controladora;

import Controladora.Base.CtrlBase;
import Produto.Entidade.Tipoproduto;
import Transacao.Transaction;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlTipoproduto extends CtrlBase{

    private static CtrlTipoproduto ctrltipoproduto;

    public static CtrlTipoproduto create() {
        if (ctrltipoproduto == null) {
            ctrltipoproduto = new CtrlTipoproduto();
        }
        return ctrltipoproduto;
    }

    public CtrlTipoproduto() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object tipoproduto, TextField txbNome, TextArea taObs) {
        if (tipoproduto != null && tipoproduto instanceof Tipoproduto) {
            Tipoproduto f = (Tipoproduto) tipoproduto;
            txbNome.setText(f.getTipoNome());
            taObs.setText(f.getTipoDesc());
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Tipoproduto) {
            return ((Tipoproduto) p).getTipoprodutoId();
        }
        return -1;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Tipoproduto) {
            txBusca.setText(((Tipoproduto) p).getTipoNome());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> tipoproduto = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Tipoproduto> ResultTipoproduto;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultTipoproduto = em.createNamedQuery("Tipoproduto.findAll", Tipoproduto.class)
                        .getResultList();
            } else {
                try {
                    ResultTipoproduto = em.createNamedQuery("Tipoproduto.findByTipoprodutoId", Tipoproduto.class)
                            .setParameter("tipoprodutoId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultTipoproduto = em.createNamedQuery("Tipoproduto.findByTipoNome", Tipoproduto.class)
                            .setParameter("tipoNome", Filtro).getResultList();
                }
            }
            for (Tipoproduto tipoprodutos : ResultTipoproduto) {
                tipoproduto.add(tipoprodutos);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return tipoproduto;
    }

    public Object Salvar(String Nome, String Descricao) {
        Tipoproduto tipoproduto = new Tipoproduto(Nome, Descricao);
        return super.Salvar(tipoproduto);
    }

    public Object Alterar(Object oTipodoproduto, Integer Id, String Nome, String Descricao) {
        Tipoproduto tipoproduto = null;
        if(oTipodoproduto != null && oTipodoproduto instanceof Tipoproduto){
            tipoproduto = (Tipoproduto) oTipodoproduto;
            tipoproduto.setTipoDesc(Descricao);
            tipoproduto.setTipoNome(Nome);
            tipoproduto = (Tipoproduto) super.Alterar(tipoproduto);
        }
        return tipoproduto;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Tipoproduto.class);
    }
}
