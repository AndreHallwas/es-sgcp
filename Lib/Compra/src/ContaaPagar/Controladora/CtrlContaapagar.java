/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ContaaPagar.Controladora;

import Compra.Entidade.Compra;
import ContaaPagar.Entidade.Contaapagar;
import ContaaPagar.Entidade.Parcelaapagar;
import Controladora.Base.CtrlBase;
import Despesa.Entidade.Tipodedespesa;
import FormadePagamento.Entidade.Formadepagamento;
import Transacao.Transaction;
import Utils.Mensagem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlContaapagar extends CtrlBase {

    private static CtrlContaapagar ctrlcontaapagar;

    public static CtrlContaapagar create() {
        if (ctrlcontaapagar == null) {
            ctrlcontaapagar = new CtrlContaapagar();
        }
        return ctrlcontaapagar;
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Contaapagar) {
            return ((Contaapagar) p).getContaapagarId();
        }
        return -1;
    }

    public static String getValorTotal(ArrayList<Object> asDespesas) {
        BigDecimal ValorTotal = new BigDecimal(0);
        if (asDespesas != null && !asDespesas.isEmpty()) {
            Contaapagar aConta = null;
            for (Object aDespesa : asDespesas) {
                aConta = (Contaapagar) aDespesa;
                ValorTotal = ValorTotal.add(aConta.getContaapagarValortotal());
            }
        }
        return ValorTotal.toPlainString();
    }

    public CtrlContaapagar() {
        super(Transaction.getEntityManagerFactory());
    }

    public ArrayList<Object> Pesquisar(String Filtro, String Tipo) {
        ArrayList<Object> contaapagar = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Contaapagar> ResultContaapagar = new ArrayList();
            if (Filtro == null || Filtro.isEmpty()) {
                ResultContaapagar = em.createNamedQuery("Contaapagar.findAll", Contaapagar.class)
                        .getResultList();
            } else {
                try {
                    if (Tipo.equalsIgnoreCase("Data")) {
                        ResultContaapagar = em.createNamedQuery("Contaapagar.findByContaapagarData", Contaapagar.class)
                                .setParameter("contaapagarData", new Date(Long.parseLong(Filtro))).getResultList();
                    } else {
                        ResultContaapagar = em.createNamedQuery("Contaapagar.findByContaapagarId", Contaapagar.class)
                                .setParameter("contaapagarId", Integer.parseInt(Filtro)).getResultList();
                    }
                } catch (Exception ex) {
                    ResultContaapagar = em.createNamedQuery("Contaapagar.findByContaapagarId", Contaapagar.class)
                            .setParameter("contaapagarId", Integer.parseInt(Filtro)).getResultList();
                }
            }

            for (Contaapagar contaapagars : ResultContaapagar) {
                contaapagar.add(contaapagars);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return contaapagar;
    }

    public Object Salvar(BigDecimal Valortotal, Boolean Situacao, Integer Acrescimo, Integer Desconto,
            Integer Juros, String Descricao, int Parcelas, Object formaDePagamento, Object compra,
            Object tipodeDespesa) {
        Contaapagar contaapagar = new Contaapagar(Valortotal, Situacao, Acrescimo,
                Desconto, Juros, new Date(), Descricao, Parcelas);
        if (formaDePagamento != null && formaDePagamento instanceof Formadepagamento) {
            contaapagar.setFormadepagamentoId((Formadepagamento) formaDePagamento);
        }
        if (compra != null && compra instanceof Compra) {
            contaapagar.setCompraId((Compra) compra);
        }
        if (tipodeDespesa != null && tipodeDespesa instanceof Tipodedespesa) {
            contaapagar.setTipodedespesaId((Tipodedespesa) tipodeDespesa);
        }
        return super.Salvar(contaapagar);
    }

    public Object Alterar(Object Conta, BigDecimal Valortotal, Boolean Situacao, Integer Acrescimo, Integer Desconto,
            Integer Juros, Date Data, String Descricao, Integer Parcelas, Object formaDePagamento, Object compra,
            Object tipodeDespesa) {
        Contaapagar contaapagar = null;
        if (Conta != null && Conta instanceof Contaapagar) {

            contaapagar = (Contaapagar) Conta;
            contaapagar.setContaapagarAcrescimo(Acrescimo);
            contaapagar.setContaapagarDesconto(Desconto);
            contaapagar.setContaapagarJuros(Juros);
            contaapagar.setContaapagarParcelas(Parcelas);
            contaapagar.setContaapagarSituacao(Situacao);
            contaapagar.setContaapagarValortotal(Valortotal);
            contaapagar.setContaapagarDescricao(Descricao);
            contaapagar.setContaapagarData(Data);

            if (formaDePagamento != null && formaDePagamento instanceof Formadepagamento) {
                contaapagar.setFormadepagamentoId((Formadepagamento) formaDePagamento);
            }
            if (compra != null && compra instanceof Compra) {
                contaapagar.setCompraId((Compra) compra);
            }
            if (tipodeDespesa != null && tipodeDespesa instanceof Tipodedespesa) {
                contaapagar.setTipodedespesaId((Tipodedespesa) tipodeDespesa);
            }
            contaapagar = (Contaapagar) super.Alterar(contaapagar);
        }
        return contaapagar;
    }

    public Boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    public Object RegistrarParcelas(Object compra, String Descricao, Object tipodedespesa,
            Object formadePagamento, ArrayList<Object> valores, ArrayList<Object> datas, String Param) {
        Boolean flag = true;
        EntityManager em = null;

        try {
            /////Inicia a Transacao
            em = getEntityManager();
            em.getTransaction().begin();
            Contaapagar conta = null;
            if (compra != null && compra instanceof Compra) {
                ArrayList<Object> auxCompras = Pesquisar(((Compra) compra).getCompraId() + "", "Id");
                if (auxCompras != null && auxCompras.size() > 0) {
                    Compra auxCompra = (Compra) auxCompras.get(0);
                    if (auxCompra.getContaapagarCollection() == null
                            || auxCompra.getContaapagarCollection().isEmpty()) {
                        conta = Salvar(compra, Descricao, tipodedespesa, formadePagamento, valores, datas, em);
                        flag = flag && conta != null;
                    } else {
                        flag = false;
                    }
                }
            } else if (tipodedespesa != null && tipodedespesa instanceof Tipodedespesa) {
                conta = Salvar(compra, Descricao, tipodedespesa, formadePagamento, valores, datas, em);
                flag = flag && conta != null;
            } else {
                flag = false;
            }

            /////A VISTA
            if (Param != null && !Param.trim().isEmpty() && Param.equalsIgnoreCase("avista")) {
                flag = flag && AVista(conta, em);
            }
            if(flag){
                /////Finaliza a Transação
                em.getTransaction().commit();
            }else{
                em.getTransaction().rollback();
            }
            Message = flag ? "Registro Realizado com Sucesso" : "Erro, o Registro não foi Realizado";

        } catch (Exception ex) {
            if (em != null) {
                Mensagem.ExibirException(ex, ":CtrlContaapagar:182");
                em.getTransaction().rollback();
            }
            Message = "Erro, o Registro não foi Realizado";
        } finally {
            if (em != null) {
                em.close();
            }
        }

        return flag ? flag : null;
    }

    protected Boolean AVista(Contaapagar conta, EntityManager em) throws Exception {
        /*Pagar a Conta caso seja a Vista*/
        Boolean flag = true;
        if (conta != null) {
            for (Parcelaapagar parcelaapagar : conta.getParcelaapagarCollection()) {
                flag = flag && CtrlParcelaapagar.create().Pagar(parcelaapagar,
                        parcelaapagar.getParpValor().toString(), em);
            }
        } else {
            flag = false;
        }
        return flag;
    }

    protected Contaapagar Salvar(Object compra, String Descricao, Object tipodedespesa, Object formadePagamento,
            ArrayList<Object> valores, ArrayList<Object> datas, EntityManager em) throws Exception {
        Contaapagar conta = null;
        if (valores != null && datas != null && valores.size() == datas.size()) {
            ///Adiciona as Novas
            BigDecimal oValor = new BigDecimal(0);
            for (Object valor : valores) {
                oValor = oValor.add(new BigDecimal((Double) valor));
            }
            /////Gera a Conta a Pagar
            conta = new Contaapagar(oValor, false, 0, 0, 0, new Date(), Descricao, valores.size());
            if (formadePagamento != null && formadePagamento instanceof Formadepagamento) {
                conta.setFormadepagamentoId((Formadepagamento) formadePagamento);
            }
            if (compra != null && compra instanceof Compra) {
                conta.setCompraId((Compra) compra);
            }
            if (tipodedespesa != null && tipodedespesa instanceof Tipodedespesa) {
                conta.setTipodedespesaId((Tipodedespesa) tipodedespesa);
            }
            /////Persiste a Conta a Pagar
            em.persist(conta);
            em.flush();
            /////Inicializa as Parcelas na Conta
            conta.setParcelaapagarCollection(new ArrayList());
            for (int i = 0; i < valores.size(); i++) {
                double valor = (Double) valores.get(i);
                /////Gera a Parcela a Pagar
                Parcelaapagar parcelaaPagar = new Parcelaapagar(conta.getContaapagarId(), i + 1, (Date) datas.get(i),
                        null, new BigDecimal(valor), false, BigDecimal.ZERO, BigDecimal.ZERO, new Date());

                if (formadePagamento != null && formadePagamento instanceof Formadepagamento) {
                    parcelaaPagar.setFormadepagamentoId((Formadepagamento) formadePagamento);
                }
                parcelaaPagar.setContaapagar((Contaapagar) conta);
                conta.getParcelaapagarCollection().add(parcelaaPagar);
                /////Persiste a Parcela a Pagar
                em.persist(parcelaaPagar);
            }
            /////Atualiza a Conta
            conta = em.merge(conta);
        }
        return conta;
    }

    public Object ModificarParcelas(Object compra, String Descricao, Object tipodedespesa, Object formadePagamento, ArrayList<Object> valores,
            ArrayList<Object> datas, String Param) {
        Boolean flag = true;
        if (compra != null && compra instanceof Compra) {
            ArrayList<Object> auxCompras = Pesquisar(((Compra) compra).getCompraId() + "", "Id");
            if (auxCompras != null && auxCompras.size() > 0) {
                Compra auxCompra = (Compra) auxCompras.get(0);/////Altera as Parcelas
                EntityManager em = null;

                try {
                    /////Inicia a Transacao
                    em = getEntityManager();
                    em.getTransaction().begin();
                    Contaapagar conta = null;
                    ///Remove as Parcelas
                    if (auxCompra.getContaapagarCollection() != null) {
                        for (Contaapagar contaapagar : auxCompra.getContaapagarCollection()) {
                            /////Remover a Conta a Pagar
                            em.remove(contaapagar);
                        }
                    }

                    conta = Salvar(compra, Descricao, tipodedespesa, formadePagamento, valores, datas, em);
                    flag = flag && conta != null;

                    /*if (Param != null && !Param.trim().isEmpty() && Param.equalsIgnoreCase("avista")) {
                        flag = flag && AVista(conta, em);
                    }*/
                    /////Finaliza a Transação
                    em.getTransaction().commit();

                    Message = flag ? "Modificação Realizada com Sucesso" : "Erro, a Modificação não foi Realizada";

                } catch (Exception ex) {
                    if (em != null) {
                        Mensagem.ExibirException(ex, ":CtrlContaapagar:267");
                        em.getTransaction().rollback();
                    }
                    Message = "Erro, a Modificação não foi Realizada";
                } finally {
                    if (em != null) {
                        em.close();
                    }
                }
            }
        }
        return flag ? flag : null;
    }

    public Boolean VerificaProximoVencimento(Object Parcela) {
        /*
        *Verifica se a parcela é o Proximo Vencimento.
         */
        if (Parcela != null && Parcela instanceof Parcelaapagar) {
            Parcelaapagar aParcela = (Parcelaapagar) Parcela;
            Contaapagar aConta = (Contaapagar) aParcela.getContaapagar();
            ArrayList<Parcelaapagar> Parcelas = new ArrayList(aConta.getParcelaapagarCollection());
            if (Parcelas.size() > 0) {
                Long Mils = null;
                Boolean flag = true;
                for (int i = 0; i < Parcelas.size(); i++) {
                    if (!Parcelas.get(i).getParpSituacao()
                            && (flag || Parcelas.get(i).getParpDataVencimento().getTime() < Mils)) {
                        Mils = Parcelas.get(i).getParpDataVencimento().getTime();
                        flag = false;
                    }
                }
                if (aParcela.getParpDataVencimento().getTime() <= Mils) {
                    return true;
                }
            }
        }
        return false;
    }

    public Boolean VerificaUltimoPagamento(Object Parcela) {
        /*
        *Verifica se a parcela é a ultima paga.
         */
        if (Parcela != null && Parcela instanceof Parcelaapagar) {
            Parcelaapagar aParcela = (Parcelaapagar) Parcela;
            Contaapagar aConta = (Contaapagar) aParcela.getContaapagar();
            ArrayList<Parcelaapagar> Parcelas = new ArrayList(aConta.getParcelaapagarCollection());
            if (Parcelas.size() > 0) {
                Long Mils = null;
                Long Mils2 = null;
                Boolean flag = true;

                for (int i = 0; i < Parcelas.size(); i++) {

                    if (Parcelas.get(i).getParpSituacao()
                            && (flag || Parcelas.get(i).getParpDataVencimento().getTime() >= Mils)) {

                        if (Mils != null && Parcelas.get(i).getParpDataVencimento().getTime() == Mils) {

                            if (Parcelas.get(i).getParcelaapagarPK().getParpDatadegeracao().getTime() > Mils2) {

                                Mils = Parcelas.get(i).getParpDataVencimento().getTime();
                                Mils2 = Parcelas.get(i).getParcelaapagarPK().getParpDatadegeracao().getTime();
                            }
                        } else {

                            Mils = Parcelas.get(i).getParpDataVencimento().getTime();
                            Mils2 = Parcelas.get(i).getParcelaapagarPK().getParpDatadegeracao().getTime();
                        }

                        flag = false;
                    }
                }
                if (aParcela.getParpDataVencimento().getTime() >= Mils) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean EstornareRemover(Object Conta) {
        Contaapagar contaapagar = null;
        boolean result = true;
        if (Conta != null && Conta instanceof Contaapagar) {
            contaapagar = (Contaapagar) Conta;
            if (contaapagar.getParcelaapagarCollection() != null) {
                for (Parcelaapagar parcelaapagar : contaapagar.getParcelaapagarCollection()) {
                    result = result && CtrlParcelaapagar.create().estornar(parcelaapagar);
                }
                if(result){
                    result = result && super.Remover(contaapagar.getContaapagarId()) != null;
                }
            }
        }
        return result;
    }

    public ArrayList<Object> getValorParcelaaVista(Double Valor) {
        ArrayList<Object> aParcela = new ArrayList();
        aParcela.add(Valor);
        return aParcela;
    }

    public ArrayList<Object> getDataParcelaaVista(Date aData) {
        ArrayList<Object> data = new ArrayList();
        data.add(aData);
        return data;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Contaapagar.class);
    }

}
