/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Interface;

import Animal.Controladora.CtrlAnotacao;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelaConsultaAnotacaoController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Object> tcId;
    @FXML
    private TableColumn<Object, Object> tcAnimal;
    @FXML
    private TableColumn<Object, Object> tcData;
    @FXML
    private TableColumn<Object, Object> tcDescricao;
    
    private static Object anotacao;
    private static boolean btnPainel = true;
    @FXML
    private HBox btnGp;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcId.setCellValueFactory(new PropertyValueFactory("AnotacaoId"));
        tcAnimal.setCellValueFactory(new PropertyValueFactory("AnotacaoAnimal"));
        tcData.setCellValueFactory(new PropertyValueFactory("AnotacaoData"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("AnotacaoDescricao"));
        CarregaTabela("");
        anotacao = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlAnotacao ctm = CtrlAnotacao.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro, "AnimalId")));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            anotacao = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (anotacao != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhuma Anotacao Selecionada", 2);
        }
    }

    public static TelaConsultaAnotacaoController Create(Class classe, Pane pndados) {
        TelaConsultaAnotacaoController CAnotacao = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Animal/Interface/TelaConsultaAnotacao.fxml"));
            Parent root = Loader.load();
            CAnotacao = (TelaConsultaAnotacaoController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CAnotacao;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaAnotacaoController.btnPainel = btnPainel;
    }

    public static Object getAnotacao() {
        return anotacao;
    }
}
