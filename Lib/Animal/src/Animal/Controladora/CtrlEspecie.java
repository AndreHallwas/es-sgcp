/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Controladora;

import Animal.Entidade.Especie;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import Utils.Imagem;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlEspecie extends CtrlBase {

    private static CtrlEspecie ctrlespecie;

    public static CtrlEspecie create() {
        if (ctrlespecie == null) {
            ctrlespecie = new CtrlEspecie();
        }
        return ctrlespecie;
    }

    public CtrlEspecie() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object especie, TextField txbNome, TextArea taObs) {
        if (especie != null && especie instanceof Especie) {
            Especie f = (Especie) especie;
            taObs.setText(f.getEspecieDescricao());
            txbNome.setText(f.getEspecieNome());
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Especie) {
            return ((Especie) p).getEspecieId();
        }
        return -1;
    }

    public static BufferedImage getImagem(Object p) {
        if (p != null && p instanceof Especie) {
            return Imagem.ByteArrayToBufferedImage(((Especie) p).getEspecieImagem());
        }
        return null;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Especie) {
            txBusca.setText(((Especie) p).getEspecieNome());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> especie = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Especie> ResultEspecie;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultEspecie = em.createNamedQuery("Especie.findAll", Especie.class)
                        .getResultList();
            } else {
                try {
                    ResultEspecie = em.createNamedQuery("Especie.findByEspecieId", Especie.class)
                            .setParameter("especieId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultEspecie = em.createNamedQuery("Especie.findByEspecieNome", Especie.class)
                            .setParameter("especieNome", Filtro).getResultList();
                }
            }
            for (Especie especies : ResultEspecie) {
                especie.add(especies);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return especie;
    }

    public Object Salvar(String Nome, String Descricao, byte[] Imagem) {
        Especie especie = new Especie(Nome, Descricao, Imagem);
        return super.Salvar(especie);
    }

    public Object Alterar(Object aEspecie, Integer Id, String Nome, String Descricao, byte[] Imagem) {
        Especie especie = null;
        if(aEspecie != null && aEspecie instanceof Especie){
            especie = (Especie) aEspecie;
            especie.setEspecieImagem(Imagem);
            especie.setEspecieNome(Nome);
            especie.setEspecieDescricao(Descricao);
            especie = (Especie) super.Alterar(especie);
        }
        return especie;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Especie.class);
    }

}
