/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.Carrinho;

/**
 *
 * @author Raizen
 */
public interface Item {
    public boolean Compara(Item oItem);
    public double getValor();
    public String getNome();
    public String getChave();
    public String getPrecoUnitario();
    public String getTipo();
}
