/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Despesa.Interface;

import ContaaPagar.Controladora.CtrlContaapagar;
import Despesa.Controladora.CtrlTipodedespesa;
import Formadepagamento.Controladora.CtrlFormadepagamento;
import Utils.Controladora.CtrlUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Utils.UI.Tela.IniciarTela;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaDespesaController implements Initializable {

    @FXML
    private VBox pndados;
    @FXML
    private JFXTextField tcDescricao;
    @FXML
    private JFXTextField tcValor;
    @FXML
    private JFXTextField tcTipodeDespesa;
    @FXML
    private Label lblErroValor;
    @FXML
    private Label lblErroTipodeDespesa;

    private Object TipodeDespesa;
    private Object Instancia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MaskFieldUtil.monetaryField(tcValor);
        EstadoOriginal();
    }

    @FXML
    private void HandleButtonConsultarDespesa(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Despesa/Interface/TelaConsultaTipodeDespesa.fxml",
                "Consulta de Tipodedespesa", null);
        if (TelaConsultaTipodeDespesaController.getTipodedespesa() != null) {
            TipodeDespesa = TelaConsultaTipodeDespesaController.getTipodedespesa();
            CtrlTipodedespesa.setCampoBusca(TipodeDespesa, tcTipodeDespesa);
        }
    }

    @FXML
    private void HandleButtonRegistrar(MouseEvent event) {
        CtrlContaapagar cn = CtrlContaapagar.create();

        if (validaCampos()) {
            if ((Instancia = cn.RegistrarParcelas(null/*Sem Compra*/, tcDescricao.getText(),
                    TipodeDespesa, CtrlFormadepagamento.create().getDebitonoCaixa(),
                    cn.getValorParcelaaVista(MaskFieldUtil.monetaryValueFromField(tcValor).doubleValue()),
                    cn.getDataParcelaaVista(new Date()), "avista")) == null) {
                Mensagem.Exibir(cn.getMsg(), 2);
            } else {
                Mensagem.Exibir(cn.getMsg(), 1);
                EstadoOriginal();
            }
        } else {
            Mensagem.Exibir("Campos Inválidos!", 2);
        }
    }

    @FXML
    private void HandleButtonConsultar(MouseEvent event) {
        IniciarTela.loadWindow(this.getClass(), "/Despesa/Interface/TelaConsultaDespesa.fxml",
                "Consulta de Despesa", null);
        EstadoOriginal();
    }

    private void EstadoOriginal() {
        CtrlUtils.clear(pndados.getChildren());
        setAllErro(false, false);
        TipodeDespesa = null;
        Instancia = null;
    }

    private void setAllErro(boolean valor, boolean tipodedespesa) {
        lblErroValor.setVisible(valor);
        lblErroTipodeDespesa.setVisible(tipodedespesa);
    }

    private void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    private boolean validaCampos() {
        Boolean fl = true;
        if (tcValor.getText().trim().isEmpty()) {
            setErro(lblErroValor, "Valor Inválido!");
            fl = false;
            tcValor.setText("");
            tcValor.requestFocus();
        }
        if (TipodeDespesa == null) {
            setErro(lblErroTipodeDespesa, "Campo Obrigatório Vazio!");
            fl = false;
            tcValor.setText("");
            tcValor.requestFocus();
        }
        return fl;
    }

}
