/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geral.Cliente;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author Aluno
 */
public class Cliente 
{
    /////Criar os objetos da Conexão
    private static Socket Client;
    private static ObjectOutputStream output;
    private static ObjectInputStream input;
    private static String mensagem;

    private static void run(String mensagem, String ip, int porta) 
    {////////127.0.0.1 , 8081 ;
        try 
        {
            ///////Instanciar os objetos
            Client = new Socket(ip, porta);
            output = new ObjectOutputStream(Client.getOutputStream());
            input = new ObjectInputStream(Client.getInputStream());

            ///////Enviar informações ao Servidor (por meio de botões, edits...)
            output.writeObject(mensagem);
            output.flush();

            ///////Tratar informações recebidas do Servidor
            do 
            {
                mensagem = (String) input.readObject();
            } while (!mensagem.equals("TERMINATE"));

            ///////Destruir os objetos e finalizar a conexão
            output.writeObject("FIM");
            output.flush();
            output.close();
            input.close();
            Client.close();
        }
        catch (Exception ex) 
        {
            System.out.println("Erro: " + ex);
        }
    }
}
