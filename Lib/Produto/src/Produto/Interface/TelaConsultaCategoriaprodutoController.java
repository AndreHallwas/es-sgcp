/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Interface;

import Produto.Controladora.CtrlCategoriaproduto;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaCategoriaprodutoController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, String> tcNome;
    @FXML
    private TableColumn<Object, String> tcTipo;
    @FXML
    private TableColumn<Object, String> tcObs;
    
    private static Object categoriaproduto;
    private static boolean btnPainel = true;
    @FXML
    private HBox btnGp;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("CategoriaprodNome"));
        tcTipo.setCellValueFactory(new PropertyValueFactory("Tipoproduto"));
        tcObs.setCellValueFactory(new PropertyValueFactory("CategoriaprodObs"));
        CarregaTabela("");
        categoriaproduto = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        CtrlCategoriaproduto ctm = CtrlCategoriaproduto.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.Pesquisar(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            categoriaproduto = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (categoriaproduto != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhuma Categoria Selecionada", 2);
        }
    }

    public static TelaConsultaCategoriaprodutoController Create(Class classe, Pane pndados) {
        TelaConsultaCategoriaprodutoController CCategoriaproduto = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Produto/Interface/TelaConsultaCategoriaproduto.fxml"));
            Parent root = Loader.load();
            CCategoriaproduto = (TelaConsultaCategoriaprodutoController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CCategoriaproduto;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaCategoriaprodutoController.btnPainel = btnPainel;
    }

    public static Object getCategoriaproduto() {
        return categoriaproduto;
    }
}
