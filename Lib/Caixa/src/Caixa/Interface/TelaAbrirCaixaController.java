/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Caixa.Interface;

import Acesso.Sessao.Sessao;
import Caixa.Controladora.CtrlCaixa;
import Utils.Controladora.CtrlUtils;
import Utils.DateUtils;
import Utils.MaskFieldUtil;
import Utils.Mensagem;
import Variables.Variables;
import com.jfoenix.controls.JFXTextField;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaAbrirCaixaController implements Initializable {

    @FXML
    private AnchorPane pndadosAbrirCaixa;
    @FXML
    private JFXTextField txbDatadeAbertura;
    @FXML
    private JFXTextField txbValorInicial;
    @FXML
    private JFXTextField txbAcescentarAbrirCaixa;
    @FXML
    private JFXTextField txbSaldoTotalAbrirCaixa;
    @FXML
    private JFXTextField txbRemoverAbrirCaixa;
    @FXML
    private DatePicker dtpData;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        MaskFieldUtil.monetaryField(txbValorInicial);
        EstadoOriginal();
    }

    @FXML
    private void HandleButtonAbrirCaixa(MouseEvent event) {
        if (ValidarAbrirCaixa()) {
            BigDecimal Numero = MaskFieldUtil.monetaryValueFromField(txbValorInicial);
            if (CtrlCaixa.create().Abrir(Numero, Sessao.getIndividuo(), dtpData.getValue())) {
                Mensagem.Exibir(CtrlCaixa.create().getMsg(), 1);
                Variables.setStatusCaixa(true);
                CtrlUtils.CloseChildren(Variables._pndados, true);
            } else {
                Mensagem.Exibir(CtrlCaixa.create().getMsg(), 2);
                EstadoOriginal();
            }
        }
    }

    @FXML
    private void HandleButtonAcrescentarAbrirCaixa(KeyEvent event) {
        txbSaldoTotalAbrirCaixa.setText(txbValorInicial.getText());
        txbSaldoTotalAbrirCaixa.setText(
                Acrescentar(txbSaldoTotalAbrirCaixa.getText(),
                        txbAcescentarAbrirCaixa.getText()));
        txbRemoverAbrirCaixa.clear();
    }

    @FXML
    private void HandleButtonRemoverAbrirCaixa(KeyEvent event) {
        txbSaldoTotalAbrirCaixa.setText(txbValorInicial.getText());
        txbSaldoTotalAbrirCaixa.setText(
                Remover(txbSaldoTotalAbrirCaixa.getText(),
                        txbRemoverAbrirCaixa.getText()));
        txbAcescentarAbrirCaixa.clear();
    }

    public String Acrescentar(String ValorInicial, String Valor) {
        try {
            BigDecimal oValor = new BigDecimal(Valor);
            BigDecimal oValorInicial = new BigDecimal(ValorInicial);
            oValorInicial = oValorInicial.add(oValor);
            return oValorInicial.toPlainString();
        } catch (Exception ex) {
            System.out.println("Valor Inesperado ao Acrescentar ao Caixa!");
        }
        return "";
    }

    public String Remover(String ValorInicial, String Valor) {
        try {
            BigDecimal oValor = new BigDecimal(Valor);
            BigDecimal oValorInicial = new BigDecimal(ValorInicial);
            oValorInicial = oValorInicial.subtract(oValor);
            return oValorInicial.toPlainString();
        } catch (Exception ex) {
            System.out.println("Valor Inesperado ao remover do Caixa!");
        }
        return "";
    }

    @FXML
    private void HandleTextFieldValorInicial(KeyEvent event) {
        txbSaldoTotalAbrirCaixa.setText(txbValorInicial.getText());
    }

    private void EstadoOriginal() {
        LocalDate DataCaixa = LocalDate.now();
        CtrlUtils.clear(pndadosAbrirCaixa.getChildren());
        dtpData.setDisable(false);
        ArrayList<Object> oCaixa = CtrlCaixa.create().Pesquisar(DateUtils.asDate(DataCaixa), "data");
        if (!oCaixa.isEmpty()) {
            if (CtrlCaixa.create().getCaixaAberto()) {
                CtrlCaixa.setCampoInicial(oCaixa.get(0), txbValorInicial, txbSaldoTotalAbrirCaixa);
            } else {
                CtrlCaixa.setCampoInicial(oCaixa.get(0), txbValorInicial, txbSaldoTotalAbrirCaixa);
            }
        } else {
            oCaixa = CtrlCaixa.create().Pesquisar(DateUtils.asDate(DataCaixa), "caixa_ultimo");
            if (!oCaixa.isEmpty()) {
                if (CtrlCaixa.create().getCaixaFechado(oCaixa.get(0))) {
                    txbValorInicial.setText(CtrlCaixa.getValorFinalCaixa(oCaixa.get(0)) + "");
                    txbSaldoTotalAbrirCaixa.setText("0");
                } else if(!CtrlCaixa.create().getCaixaAberto()){
                    /////Abrir o Caixa Anterior;
                    DataCaixa = CtrlCaixa.getDate(oCaixa.get(0));
                    txbValorInicial.setText(CtrlCaixa.getValorInicialCaixa(oCaixa.get(0)) + "");
                    txbSaldoTotalAbrirCaixa.setText("0");
                    Mensagem.Exibir("O Caixa do Dia " + DataCaixa.toString()
                            + " Não Foi Fechado, Portanto ele Deverá ser Fechado", 2);
                    dtpData.setDisable(true);
                }
            }else{
                txbValorInicial.setText("0");
                txbSaldoTotalAbrirCaixa.setText("0");
            }
        }

        dtpData.setValue(DataCaixa);
        txbDatadeAbertura.setText(DataCaixa.toString());

    }

    private boolean ValidarAbrirCaixa() {
        if (CtrlCaixa.create().getCaixaAberto()) {
            Mensagem.Exibir(CtrlCaixa.create().getMsg(), 2);
            return false;
        }
        return true;
    }

    @FXML
    private void HandleDtpCaixaChanged(Event event) {
        if (dtpData.getValue() != null) {
            if (dtpData.getValue().equals(LocalDate.now())) {
                EstadoOriginal();
            } else {
                ArrayList<Object> oCaixa = CtrlCaixa.create().
                        Pesquisar(DateUtils.asDate(dtpData.getValue()), "data");
                if (!oCaixa.isEmpty()) {
                    CtrlCaixa.setCampoInicial(oCaixa.get(0), txbValorInicial, txbSaldoTotalAbrirCaixa);
                }
            }
        }
    }

}
