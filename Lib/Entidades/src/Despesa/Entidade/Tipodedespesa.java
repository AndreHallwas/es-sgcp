/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Despesa.Entidade;

import ContaaPagar.Entidade.Contaapagar;
import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "tipodedespesa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipodedespesa.findAll", query = "SELECT t FROM Tipodedespesa t")
    , @NamedQuery(name = "Tipodedespesa.findByTipodedespesaId", query = "SELECT t FROM Tipodedespesa t WHERE t.tipodedespesaId = :tipodedespesaId")
    , @NamedQuery(name = "Tipodedespesa.findByTipodedespesaNome", query = "SELECT t FROM Tipodedespesa t WHERE t.tipodedespesaNome = :tipodedespesaNome")
    , @NamedQuery(name = "Tipodedespesa.findByTipodedespesaDescricao", query = "SELECT t FROM Tipodedespesa t WHERE t.tipodedespesaDescricao = :tipodedespesaDescricao")})
public class Tipodedespesa extends JPADao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tipodedespesa_id")
    private Integer tipodedespesaId;
    @Basic(optional = false)
    @Column(name = "tipodedespesa_nome")
    private String tipodedespesaNome;
    @Column(name = "tipodedespesa_descricao")
    private String tipodedespesaDescricao;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "tipodedespesaId")
    private Collection<Contaapagar> contaapagarCollection = new ArrayList();

    public Tipodedespesa() {
    }

    public Tipodedespesa(Integer tipodedespesaId) {
        this.tipodedespesaId = tipodedespesaId;
    }

    public Tipodedespesa(String tipodedespesaNome, String tipodedespesaDescricao) {
        this.tipodedespesaNome = tipodedespesaNome;
        this.tipodedespesaDescricao = tipodedespesaDescricao;
    }

    public Tipodedespesa(Integer tipodedespesaId, String tipodedespesaNome, String tipodedespesaDescricao) {
        this.tipodedespesaId = tipodedespesaId;
        this.tipodedespesaNome = tipodedespesaNome;
        this.tipodedespesaDescricao = tipodedespesaDescricao;
    }

    public Tipodedespesa(Integer tipodedespesaId, String tipodedespesaNome) {
        this.tipodedespesaId = tipodedespesaId;
        this.tipodedespesaNome = tipodedespesaNome;
    }

    public Integer getTipodedespesaId() {
        return tipodedespesaId;
    }

    public void setTipodedespesaId(Integer tipodedespesaId) {
        this.tipodedespesaId = tipodedespesaId;
    }

    public String getTipodedespesaNome() {
        return tipodedespesaNome;
    }

    public void setTipodedespesaNome(String tipodedespesaNome) {
        this.tipodedespesaNome = tipodedespesaNome;
    }

    public String getTipodedespesaDescricao() {
        return tipodedespesaDescricao;
    }

    public void setTipodedespesaDescricao(String tipodedespesaDescricao) {
        this.tipodedespesaDescricao = tipodedespesaDescricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tipodedespesaId != null ? tipodedespesaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipodedespesa)) {
            return false;
        }
        Tipodedespesa other = (Tipodedespesa) object;
        if ((this.tipodedespesaId == null && other.tipodedespesaId != null) || (this.tipodedespesaId != null && !this.tipodedespesaId.equals(other.tipodedespesaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return tipodedespesaNome;
    }

    @XmlTransient
    public Collection<Contaapagar> getContaapagarCollection() {
        return contaapagarCollection;
    }

    public void setContaapagarCollection(Collection<Contaapagar> contaapagarCollection) {
        this.contaapagarCollection = contaapagarCollection;
    }
    
}
