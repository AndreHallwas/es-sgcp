/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Endereco.Interface;

import Endereco.Controladora.CtrlComplementoendereco;
import Endereco.Controladora.CtrlEnderecosimples;
import Endereco.Entidade.Complementoendereco;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import Utils.UI.Tela.Tela;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Remote
 */
public class TelaEnderecoSimplesController implements Initializable, Tela {

    @FXML
    private JFXTextField cbBairro;
    @FXML
    private JFXTextField cbRua;
    @FXML
    private JFXTextField cbComplemento;
    @FXML
    private Label lblErroComplemento;
    @FXML
    private JFXTextField cbNumero;
    @FXML
    private Label lblErroNumero;
    @FXML
    private VBox pnEndereco;
    @FXML
    private Label lblErroRua;

    private static Object Endereco;
    private Object Complemento;
    private boolean EOriginal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        estadoOriginal();
    }

    public boolean valida() //Validações de banco
    {
        return TelaEnderecoSimplesController.valida(cbRua, cbNumero, lblErroNumero, lblErroRua);
    }

    public void estadoOriginal() {
        lblErroNumero.setVisible(false);
        lblErroRua.setVisible(false);
        lblErroComplemento.setVisible(false);
        Complemento = null;
        CtrlUtils.clear(pnEndereco.getChildren());
    }

    public static boolean valida(TextField txbLocal, TextField cbNumero, Label lblErroNumero, Label lblErroRua) //Validações de banco
    {
        boolean fl = true;

        if (txbLocal.getText().trim().isEmpty()) {
            setErro(lblErroRua, "Rua Inválida");
            txbLocal.setText("");
            txbLocal.requestFocus();
            fl = false;
        }
        if (cbNumero.getText().trim().isEmpty()) {
            setErro(lblErroNumero, "Numero Inválido");
            cbNumero.setText("");
            cbNumero.requestFocus();
            fl = false;
        }

        return fl;
    }

    private static void setErro(Label lb, String Msg) {
        lb.setVisible(true);
        lb.setText(Msg);
        Mensagem.ExibirLog(Msg);
    }

    public Object getComplementoEndereco() {
        if (EOriginal && Endereco != null && Complemento != null) {
            EOriginal = CtrlComplementoendereco.ComparaEnderecoSimples(Complemento, Endereco,
                    cbComplemento.getText(), cbNumero.getText());

        }
        if (!EOriginal) {
            Complemento = null;
        }
        if (Endereco != null && Complemento == null) {
            CtrlComplementoendereco ce = CtrlComplementoendereco.create();
            ArrayList<Object> Complementos = ce.Pesquisar(CtrlEnderecosimples.getLocal(Endereco)
                    + "#:#" + cbNumero.getText() + "#:#"
                    + cbComplemento.getText(), "LocalNumeroComplemento");
            if (Complementos.isEmpty()) {
                Complemento = ce.Salvar(cbComplemento.getText(), cbNumero.getText(), Endereco);
            } else {
                Complemento = Complementos.get(0);
            }
        } else {

            CtrlEnderecosimples cs = CtrlEnderecosimples.create();
            CtrlComplementoendereco ce = CtrlComplementoendereco.create();
            ArrayList<Object> Complementos = ce.Pesquisar(cbRua.getText()
                    + "#:#" + cbNumero.getText()
                    + "#:#" + cbBairro.getText()+"#b#"
                    + "#<:>#" + cbComplemento.getText()+"#c#", "LocalNumeroBairroComplemento");
            if (Complementos.isEmpty()) {
                Endereco = cs.Salvar(cbRua.getText(), cbBairro.getText());
                if (Endereco != null) {
                    Complemento = ce.Salvar(cbComplemento.getText(), cbNumero.getText(), Endereco);
                } else {
                    Mensagem.ExibirLog("TelaEnderecoSimplesController:168 -> Endereço não salvo");
                }
            } else {
                Complemento = Complementos.get(0);
            }
        }
        return Complemento;
    }

    public void setComplementoEndereco(Object oComplementodoEndereco) {
        if (CtrlComplementoendereco.getEnderecoSimples(oComplementodoEndereco) != null) {
            CtrlComplementoendereco.setCampos(oComplementodoEndereco, cbRua, cbBairro, cbNumero, cbComplemento);
            Complemento = oComplementodoEndereco;
            Endereco = CtrlComplementoendereco.getEnderecoSimples(oComplementodoEndereco);
            EOriginal = true;
        }
    }

    public static TelaEnderecoSimplesController Create(Class classe, Pane pndados) {
        TelaEnderecoSimplesController CEndereco = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Endereco/Interface/TelaEnderecoSimples.fxml"));
            Parent root = Loader.load();
            CEndereco = (TelaEnderecoSimplesController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CEndereco;
    }

}
