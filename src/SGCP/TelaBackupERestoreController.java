/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SGCP;

import Transacao.Backup.nodeTabela;
import Transacao.Transaction;
import Utils.Acao.InterfaceFxmlAcao;
import Utils.Mensagem;
import Utils.UI.Tela.Tela;
import Variables.Variables;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextArea;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaBackupERestoreController implements Initializable, Tela, InterfaceFxmlAcao {

    @FXML
    private JFXButton btnBackup;
    @FXML
    private JFXTextArea taInfo;
    @FXML
    private Label lblStatus;
    @FXML
    private JFXCheckBox cbBackupIniciarSistema;
    @FXML
    private JFXCheckBox cbBackupFinalizarSistema;
    @FXML
    private TableView<nodeTabela> tabela;
    @FXML
    private TableColumn<Object, Object> tcNome;
    @FXML
    private TableColumn<Object, Object> tcAcoes;
    @FXML
    private TableColumn<Object, Object> tcData;
    @FXML
    private JFXComboBox<String> cbManterUltimosBackups;

    private static String state = null;
    private boolean locked;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        SetTelaAcao();

        tcNome.setCellValueFactory(new PropertyValueFactory("Nome"));
        tcData.setCellValueFactory(new PropertyValueFactory("Data"));
        tcAcoes.setCellValueFactory(new PropertyValueFactory("Crud"));

        taInfo.setText("");

        EstadoOriginal();
    }

    @FXML
    private void evtBackup(MouseEvent event) {

        if (!locked) {
            locked = true;
            new Thread(()
                    -> {
                Platform.runLater(() -> {
                    lblStatus.setText(state);
                });
                String nomeBackup = "Backup_" + Variables.getDataSimple(new Date()) + ".custom";
                state = "Inicial";
                state = Transaction.realizaBackupRestauracao(nomeBackup, "backup", taInfo) ? "Realizado com Sucesso" : "Erro";
                Platform.runLater(() -> {
                    lblStatus.setText(state);
                });
                Platform.runLater(() -> {
                    if (state.equalsIgnoreCase("Realizado com Sucesso")) {
                        Mensagem.Exibir("Backup efetuado!", 1);
                        taInfo.appendText("Concluido!");
                    } else {
                        Mensagem.Exibir("Erro ao efetuar o backup!", 2);
                        taInfo.appendText("Erro!");
                    }
                    locked = false;
                    EstadoOriginal();
                });
            }).start();
        }
    }

    private void Restore(nodeTabela Arquivo) {

        if (!locked) {
            locked = true;
            new Thread(()
                    -> {
                Platform.runLater(() -> {
                    lblStatus.setText(state);
                });
                state = "Inicial";
                state = Transaction.realizaBackupRestauracao(Arquivo.getNome(), "restore", taInfo) ? "Realizado com Sucesso" : "Erro";
                Platform.runLater(() -> {
                    lblStatus.setText(state);
                });
                Platform.runLater(() -> {
                    if (state.equalsIgnoreCase("Realizado com Sucesso")) {
                        Mensagem.Exibir("Realizado com Sucesso", 1);
                        taInfo.appendText("Concluido!");
                    } else {
                        Mensagem.Exibir("Erro ao efetuar a restauração!", 2);
                        taInfo.appendText("Erro!");
                    }
                    locked = false;
                });
            }).start();
        }
    }

    private void Excluir(nodeTabela nodeTabela) {
        if (!nodeTabela.getArquivo().delete()) {
            Mensagem.Exibir("Não foi Possivel Remover", 2);
        }
        EstadoOriginal();
    }

    private void EstadoOriginal() {
        Transaction.getBackup().init();
        locked = false;
        tabela.setItems(FXCollections.observableList(Transaction.getBackup().get()));
        cbManterUltimosBackups.getSelectionModel().clearSelection();
        cbManterUltimosBackups.getItems().clear();
        cbManterUltimosBackups.getItems().addAll("10 Backups",
                "25 Backups", "50 Backups", "100 Backups", "500 Backups", "1000 Backups");
        cbManterUltimosBackups.getSelectionModel().select(Transaction.getBackup().getultimosBackups());
        cbBackupIniciarSistema.setSelected(Transaction.getBackup().isRealizarBackupIniciarSistema());
        cbBackupFinalizarSistema.setSelected(Transaction.getBackup().isRealizarBackupFinalizarSistema());
        
    }

    @FXML
    private void evtBackupIniciarSistema(MouseEvent event) {
        Transaction.getBackup().setRealizarBackupIniciarSistema(cbBackupIniciarSistema.isSelected());
    }

    @FXML
    private void evtBackupFinalizarSistema(MouseEvent event) {
        Transaction.getBackup().setRealizarBackupFinalizarSistema(cbBackupFinalizarSistema.isSelected());
    }

    @Override
    public void SelectEventAcao(Object Reference, String Action, Object element) {
        if (Action.equalsIgnoreCase("res")) {
            Restore((nodeTabela) Reference);
        } else if (Action.equalsIgnoreCase("ex")) {
            Excluir((nodeTabela) Reference);
        }
        System.out.println(Action + ": " + Reference);
    }

    @Override
    public void SetTelaAcao() {
        nodeTabela.setTelaAcao(this);
    }

    @FXML
    private void evtCBUltimosChanged(Event event) {
        String Selecionado = cbManterUltimosBackups.getSelectionModel().getSelectedItem();
        if (Selecionado != null) {
            int Valor;
            if(Selecionado.equalsIgnoreCase("10 Backups")){
                Valor = 10;
            }else if(Selecionado.equalsIgnoreCase("25 Backups")){
                Valor = 25;
            }else if(Selecionado.equalsIgnoreCase("50 Backups")){
                Valor = 50;
            }else if(Selecionado.equalsIgnoreCase("100 Backups")){
                Valor = 100;
            }else if(Selecionado.equalsIgnoreCase("500 Backups")){
                Valor = 500;
            }else{
                Valor = 1000;
            }
            Transaction.getBackup().setUltimosBackups(Valor);
        }
    }

}
