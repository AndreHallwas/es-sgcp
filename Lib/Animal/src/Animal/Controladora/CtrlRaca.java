/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal.Controladora;

import Animal.Entidade.Raca;
import Controladora.Base.CtrlBase;
import Transacao.Transaction;
import Utils.Imagem;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.persistence.EntityManager;

/**
 *
 * @author Raizen
 */
public class CtrlRaca extends CtrlBase {
    

    private static CtrlRaca ctrlraca;

    public static CtrlRaca create() {
        if (ctrlraca == null) {
            ctrlraca = new CtrlRaca();
        }
        return ctrlraca;
    }

    public CtrlRaca() {
        super(Transaction.getEntityManagerFactory());
    }

    public static void setCampos(Object raca, TextField txbNome, TextArea taObs) {
        if (raca != null && raca instanceof Raca) {
            Raca f = (Raca) raca;
            txbNome.setText(f.getRacaNome());
            taObs.setText(f.getRacaDescricao());
        }
    }

    public static Integer getId(Object p) {
        if (p != null && p instanceof Raca) {
            return ((Raca) p).getRacaId();
        }
        return -1;
    }

    public static BufferedImage getImagem(Object p) {
        if (p != null && p instanceof Raca) {
            return Imagem.ByteArrayToBufferedImage(((Raca) p).getRacaImagem());
        }
        return null;
    }

    public static void setCampoBusca(Object p, TextField txBusca) {
        if (p != null && p instanceof Raca) {
            txBusca.setText(((Raca) p).getRacaNome());
        }
    }

    public ArrayList<Object> Pesquisar(String Filtro) {
        ArrayList<Object> raca = new ArrayList();
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Raca> ResultRaca;
            if (Filtro != null && Filtro.isEmpty()) {
                ResultRaca = em.createNamedQuery("Raca.findAll", Raca.class)
                        .getResultList();
            } else {
                try {
                    ResultRaca = em.createNamedQuery("Raca.findByRacaId", Raca.class)
                            .setParameter("racaId", Integer.parseInt(Filtro)).getResultList();
                } catch (Exception ex) {
                    ResultRaca = em.createNamedQuery("Raca.findByRacaNome", Raca.class)
                            .setParameter("racaNome", Filtro).getResultList();
                }
            }
            for (Raca racas : ResultRaca) {
                raca.add(racas);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return raca;
    }

    public Object Salvar(String Nome, String Descricao, byte[] Imagem) {
        Raca raca = new Raca(Nome, Descricao, Imagem);
        return super.Salvar(raca);
    }

    public Object Alterar(Object aRaca, Integer Id, String Nome, String Descricao, byte[] Imagem) {
        Raca raca = null;
        if(aRaca != null && aRaca instanceof Raca){
            raca = (Raca) aRaca;
            raca.setRacaDescricao(Descricao);
            raca.setRacaImagem(Imagem);
            raca.setRacaNome(Nome);
            raca = (Raca) super.Alterar(raca);
        }
        return raca;
    }

    public boolean Remover(Integer Id) {
        return super.Remover(Id) != null;
    }

    @Override
    protected void setEntityReference() {
        setEntityReference(Raca.class);
    }
    
}
