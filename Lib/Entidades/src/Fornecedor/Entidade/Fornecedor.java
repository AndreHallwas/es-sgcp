/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Fornecedor.Entidade;

import Endereco.Entidade.Complementoendereco;
import Compra.Entidade.Compra;
import JPA.Dao.JPADao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "fornecedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fornecedor.findAll", query = "SELECT f FROM Fornecedor f")
    , @NamedQuery(name = "Fornecedor.findByForId", query = "SELECT f FROM Fornecedor f WHERE f.forId = :forId")
    , @NamedQuery(name = "Fornecedor.findByForCnpj", query = "SELECT f FROM Fornecedor f WHERE f.forCnpj = :forCnpj")
    , @NamedQuery(name = "Fornecedor.findByForNome", query = "SELECT f FROM Fornecedor f WHERE f.forNome = :forNome")
    , @NamedQuery(name = "Fornecedor.findByForTelefone", query = "SELECT f FROM Fornecedor f WHERE f.forTelefone = :forTelefone")
    , @NamedQuery(name = "Fornecedor.findByForEmail", query = "SELECT f FROM Fornecedor f WHERE f.forEmail = :forEmail")
    , @NamedQuery(name = "Fornecedor.findByForObs", query = "SELECT f FROM Fornecedor f WHERE f.forObs = :forObs")
    , @NamedQuery(name = "Fornecedor.findByForGeral", query = "SELECT f FROM Fornecedor f WHERE upper(f.forNome) like upper(concat(:filtro,'%'))"
            + " or upper(f.forCnpj) like upper(concat(:filtro,'%'))")})
public class Fornecedor extends JPADao implements Serializable {

    @Lob
    @Column(name = "for_imagem")
    private byte[] forImagem;
    @JoinColumn(name = "end_id", referencedColumnName = "end_id")
    @ManyToOne(optional = false)
    private Complementoendereco endId;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "for_id")
    private Integer forId;
    @Basic(optional = false)
    @Column(name = "for_cnpj")
    private String forCnpj;
    @Basic(optional = false)
    @Column(name = "for_nome")
    private String forNome;
    @Column(name = "for_telefone")
    private String forTelefone;
    @Column(name = "for_email")
    private String forEmail;
    @Column(name = "for_obs")
    private String forObs;
    
    @OneToMany(mappedBy = "forId")
    private Collection<Compra> compraCollection = new ArrayList();

    public Fornecedor() {
    }

    public Fornecedor(Integer forId) {
        this.forId = forId;
    }

    public Fornecedor(String forCnpj, String forNome, String forTelefone, String forEmail, String forObs, byte[] forImagem) {
        this.forCnpj = forCnpj;
        this.forNome = forNome;
        this.forTelefone = forTelefone;
        this.forEmail = forEmail;
        this.forObs = forObs;
        this.forImagem = forImagem;
    }

    public Fornecedor(Integer forId, String forCnpj, String forNome, String forTelefone, String forEmail, String forObs, byte[] forImagem) {
        this.forId = forId;
        this.forCnpj = forCnpj;
        this.forNome = forNome;
        this.forTelefone = forTelefone;
        this.forEmail = forEmail;
        this.forObs = forObs;
        this.forImagem = forImagem;
    }

    public Fornecedor(Integer forId, String forCnpj, String forNome) {
        this.forId = forId;
        this.forCnpj = forCnpj;
        this.forNome = forNome;
    }

    public Integer getForId() {
        return forId;
    }

    public void setForId(Integer forId) {
        this.forId = forId;
    }

    public String getForCnpj() {
        return forCnpj;
    }

    public void setForCnpj(String forCnpj) {
        this.forCnpj = forCnpj;
    }

    public String getForNome() {
        return forNome;
    }

    public void setForNome(String forNome) {
        this.forNome = forNome;
    }

    public String getForTelefone() {
        return forTelefone;
    }

    public void setForTelefone(String forTelefone) {
        this.forTelefone = forTelefone;
    }

    public String getForEmail() {
        return forEmail;
    }

    public void setForEmail(String forEmail) {
        this.forEmail = forEmail;
    }

    public String getForObs() {
        return forObs;
    }

    public void setForObs(String forObs) {
        this.forObs = forObs;
    }

    public byte[] getForImagem() {
        return forImagem;
    }

    public void setForImagem(byte[] forImagem) {
        this.forImagem = forImagem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (forId != null ? forId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fornecedor)) {
            return false;
        }
        Fornecedor other = (Fornecedor) object;
        if ((this.forId == null && other.forId != null) || (this.forId != null && !this.forId.equals(other.forId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Acesso.Entidade.Fornecedor[ forId=" + forId + " ]";
    }

    @XmlTransient
    public Collection<Compra> getCompraCollection() {
        return compraCollection;
    }

    public void setCompraCollection(Collection<Compra> compraCollection) {
        this.compraCollection = compraCollection;
    }

    public Complementoendereco getEndId() {
        return endId;
    }

    public void setEndId(Complementoendereco endId) {
        this.endId = endId;
    }
    
}
