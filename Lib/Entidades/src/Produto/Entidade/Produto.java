/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Produto.Entidade;

import Agendamento.Entidade.Agendamento;
import Compra.Entidade.Produtodacompra;
import JPA.Dao.JPADao;
import Utils.Carrinho.Item;
import Utils.FormatString;
import Venda.Entidade.Produtodavenda;
import Vacinacao.Entidade.Vacinacao;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Raizen
 */
@Entity
@Table(name = "produto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p")
    , @NamedQuery(name = "Produto.findByProdutoId", query = "SELECT p FROM Produto p WHERE p.produtoId = :produtoId")
    , @NamedQuery(name = "Produto.findByProdutoPreco", query = "SELECT p FROM Produto p WHERE p.produtoPreco = :produtoPreco")
    , @NamedQuery(name = "Produto.findByProdutoQuantidade", query = "SELECT p FROM Produto p WHERE p.produtoQuantidade = :produtoQuantidade")
    , @NamedQuery(name = "Produto.findByProdutoObs", query = "SELECT p FROM Produto p WHERE p.produtoObs = :produtoObs")
    , @NamedQuery(name = "Produto.findByProdutoNome", query = "SELECT p FROM Produto p WHERE p.produtoNome = :produtoNome")
    , @NamedQuery(name = "Produto.findByAllString", query = "SELECT p FROM Produto p WHERE upper(p.produtoNome) like upper(concat(:produtoNome,'%')) or "
            + " upper(p.produtoObs) like upper(concat(:produtoObs,'%'))")
    , @NamedQuery(name = "Produto.findByAllGeral", query = "SELECT p FROM Produto p WHERE p.produtoId = :produtoId"
            + " or p.produtoQuantidade = :produtoQuantidade"
            + " or p.produtoPreco = :produtoPreco")
    , @NamedQuery(name = "Produto.findByAllVacina", query = "SELECT p FROM Produto p WHERE p.categoriaproduto.categoriaprodVacina = true"
            + " and p.dosagemprodutoId is not NULL")
    , @NamedQuery(name = "Produto.findByVacinaGeral", query = "SELECT p FROM Produto p WHERE p.categoriaproduto.categoriaprodVacina = true"
            + " and p.dosagemprodutoId is not NULL and upper(p.produtoNome) like upper(concat(:filtro,'%'))"
            + " or p.categoriaproduto.categoriaprodVacina = true"
            + " and p.dosagemprodutoId is not NULL and upper(p.produtoObs) like upper(concat(:filtro,'%'))")
    , @NamedQuery(name = "Produto.findByVacinaId", query = "SELECT p FROM Produto p WHERE p.categoriaproduto.categoriaprodVacina = true"
            + " and p.dosagemprodutoId is not NULL and p.produtoId = :filtro")})
public class Produto extends JPADao implements Serializable, Item {

    @OneToMany(fetch=FetchType.LAZY, mappedBy = "produto")
    private Collection<Produtodacompra> produtodacompraCollection = new ArrayList();
    @OneToMany(fetch=FetchType.LAZY, mappedBy = "produto")
    private Collection<Produtodavenda> produtodavendaCollection = new ArrayList();;

    @OneToMany(fetch=FetchType.LAZY, mappedBy = "produtoId")
    private Collection<Agendamento> agendamentoCollection = new ArrayList();

    @OneToMany(fetch=FetchType.LAZY, mappedBy = "produto")
    private Collection<Vacinacao> vacinacaoCollection = new ArrayList();

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "produto_id")
    private Integer produtoId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "produto_preco")
    private BigDecimal produtoPreco;
    @Column(name = "produto_preco_venda")
    protected BigDecimal produtoPrecoVenda;
    @Basic(optional = false)
    @Column(name = "produto_quantidade")
    private int produtoQuantidade;
    @Column(name = "produto_obs")
    private String produtoObs;
    @Basic(optional = false)
    @Column(name = "produto_nome")
    private String produtoNome;
    @JoinColumns({
        @JoinColumn(name = "categoriaprod_id", referencedColumnName = "categoriaprod_id")
        , @JoinColumn(name = "tipoproduto_id", referencedColumnName = "tipoproduto_id")})
    @ManyToOne(optional = false)
    private Categoriaproduto categoriaproduto;
    @JoinColumn(name = "dosagemproduto_id", referencedColumnName = "dosagemproduto_id")
    @ManyToOne
    private Dosagemproduto dosagemprodutoId;

    public Produto() {
    }

    public Produto(Integer produtoId) {
        this.produtoId = produtoId;
    }

    public Produto(BigDecimal produtoPreco, int produtoQuantidade, String produtoObs, String produtoNome,
            BigDecimal produtoPrecoVenda) {
        this.produtoPreco = produtoPreco;
        this.produtoQuantidade = produtoQuantidade;
        this.produtoObs = produtoObs;
        this.produtoNome = produtoNome;
        this.produtoPrecoVenda = produtoPrecoVenda;
    }

    public Produto(Integer produtoId, BigDecimal produtoPreco, int produtoQuantidade,
            String produtoObs, String produtoNome, BigDecimal produtoPrecoVenda) {
        this.produtoId = produtoId;
        this.produtoPreco = produtoPreco;
        this.produtoQuantidade = produtoQuantidade;
        this.produtoObs = produtoObs;
        this.produtoNome = produtoNome;
        this.produtoPrecoVenda = produtoPrecoVenda;
    }

    public Produto(Integer produtoId, BigDecimal produtoPreco, int produtoQuantidade,
            String produtoNome, BigDecimal produtoPrecoVenda) {
        this.produtoId = produtoId;
        this.produtoPreco = produtoPreco;
        this.produtoQuantidade = produtoQuantidade;
        this.produtoNome = produtoNome;
        this.produtoPrecoVenda = produtoPrecoVenda;
    }

    public Integer getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(Integer produtoId) {
        this.produtoId = produtoId;
    }

    public BigDecimal getProdutoPreco() {
        return produtoPreco;
    }

    public String getPrecoCompra() {
        return FormatString.MonetaryPersistent(produtoPreco);
    }

    public String getPrecoVenda() {
        return FormatString.MonetaryPersistent(produtoPrecoVenda);
    }

    public void setProdutoPreco(BigDecimal produtoPreco) {
        this.produtoPreco = produtoPreco;
    }

    public int getProdutoQuantidade() {
        return produtoQuantidade;
    }

    public void setProdutoQuantidade(int produtoQuantidade) {
        this.produtoQuantidade = produtoQuantidade;
    }

    public String getProdutoObs() {
        return produtoObs;
    }

    public void setProdutoObs(String produtoObs) {
        this.produtoObs = produtoObs;
    }

    public String getProdutoNome() {
        return produtoNome;
    }

    public void setProdutoNome(String produtoNome) {
        this.produtoNome = produtoNome;
    }

    public Categoriaproduto getCategoriaproduto() {
        return categoriaproduto;
    }

    public void setCategoriaproduto(Categoriaproduto categoriaproduto) {
        this.categoriaproduto = categoriaproduto;
    }

    public Dosagemproduto getDosagemprodutoId() {
        return dosagemprodutoId;
    }

    public void setDosagemprodutoId(Dosagemproduto dosagemprodutoId) {
        this.dosagemprodutoId = dosagemprodutoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (produtoId != null ? produtoId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.produtoId == null && other.produtoId != null) || (this.produtoId != null && !this.produtoId.equals(other.produtoId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return produtoNome;
    }

    @XmlTransient
    public Collection<Vacinacao> getVacinacaoCollection() {
        return vacinacaoCollection;
    }

    public void setVacinacaoCollection(Collection<Vacinacao> vacinacaoCollection) {
        this.vacinacaoCollection = vacinacaoCollection;
    }

    @XmlTransient
    public Collection<Agendamento> getAgendamentoCollection() {
        return agendamentoCollection;
    }

    public void setAgendamentoCollection(Collection<Agendamento> agendamentoCollection) {
        this.agendamentoCollection = agendamentoCollection;
    }

    @XmlTransient
    public Collection<Produtodacompra> getProdutodacompraCollection() {
        return produtodacompraCollection;
    }

    public void setProdutodacompraCollection(Collection<Produtodacompra> produtodacompraCollection) {
        this.produtodacompraCollection = produtodacompraCollection;
    }

    @XmlTransient
    public Collection<Produtodavenda> getProdutodavendaCollection() {
        return produtodavendaCollection;
    }

    public void setProdutodavendaCollection(Collection<Produtodavenda> produtodavendaCollection) {
        this.produtodavendaCollection = produtodavendaCollection;
    }

    @Override
    public boolean Compara(Item oItem) {
        return equals(oItem);
    }

    @Override
    public double getValor() {
        return getProdutoPrecoVenda() != null ? getProdutoPrecoVenda().doubleValue() : new Double(0);
    }

    @Override
    public String getNome() {
        return getProdutoNome();
    }

    @Override
    public String getChave() {
        return getProdutoId()+"";
    }

    @Override
    public String getPrecoUnitario() {
        return getValor()+"";
    }

    @Override
    public String getTipo() {
        return "Produto";
    }

    /**
     * @return the produtoPrecoVenda
     */
    public BigDecimal getProdutoPrecoVenda() {
        return produtoPrecoVenda;
    }

    /**
     * @param produtoPrecoVenda the produtoPrecoVenda to set
     */
    public void setProdutoPrecoVenda(BigDecimal produtoPrecoVenda) {
        this.produtoPrecoVenda = produtoPrecoVenda;
    }
    
}
