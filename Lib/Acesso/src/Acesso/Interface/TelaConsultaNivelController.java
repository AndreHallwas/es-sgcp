/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Acesso.Interface;

import Acesso.Controladora.Acesso;
import Utils.Controladora.CtrlUtils;
import Utils.Mensagem;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Raizen
 */
public class TelaConsultaNivelController implements Initializable {

    @FXML
    private JFXTextField txBusca;
    @FXML
    private TableView<Object> tabela;
    @FXML
    private TableColumn<Object, Object> tcNome;
    @FXML
    private TableColumn<Object, Object> tcVisivel;
    @FXML
    private TableColumn<Object, Object> tcDescricao;
    @FXML
    private HBox btnGp;
    
    private static Object nivel;
    private static boolean btnPainel = true;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tcNome.setCellValueFactory(new PropertyValueFactory("GrupoNome"));
        tcDescricao.setCellValueFactory(new PropertyValueFactory("GrupoDescricao"));
        tcVisivel.setCellValueFactory(new PropertyValueFactory("Visivel"));
        CarregaTabela("");
        nivel = null;
        btnGp.setVisible(btnPainel);
    }

    private void CarregaTabela(String filtro) {
        Acesso ctm = Acesso.create();
        try {
            tabela.setItems(FXCollections.observableList(ctm.PesquisarGrupo(filtro)));
        } catch (Exception ex) {
            Mensagem.ExibirException(ex, "Erro AO Carregar Tabela");
        }
    }

    @FXML
    private void evtBuscar(Event event) {
        CarregaTabela(txBusca.getText());
    }

    @FXML
    private void ClicknaTabela(MouseEvent event) {
        int lin = tabela.getSelectionModel().getSelectedIndex();
        if (lin > -1) {
            nivel = tabela.getItems().get(lin);
        }
    }

    @FXML
    private void evtConfirmar(Event event) {
        if (nivel != null) {
            evtCancelar(event);
        } else {
            Mensagem.Exibir("Nenhum Nível de Acesso Selecionado", 2);
        }
    }

    public static TelaConsultaNivelController Create(Class classe, Pane pndados) {
        TelaConsultaNivelController CNivel = null;
        try {
            FXMLLoader Loader = new FXMLLoader(classe.getResource("/Acesso/Interface/TelaConsultaNivel.fxml"));
            Parent root = Loader.load();
            CNivel = (TelaConsultaNivelController) Loader.getController();
            root.getClass();
            pndados.getChildren().clear();
            pndados.getChildren().add(root);
        } catch (IOException ex) {
            Mensagem.ExibirException(ex, "Erro ao Carregar Tela");
        }
        return CNivel;
    }

    @FXML
    private void evtCancelar(Event event) {
        CtrlUtils.CloseStage(event);
    }

    public static void setBtnPainel(boolean btnPainel) {
        TelaConsultaNivelController.btnPainel = btnPainel;
    }

    public static Object getNivel() {
        return nivel;
    }
}
