/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils.Entidade;

import java.util.Date;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author Raizen
 */
@Converter
public class TimestampConverter implements AttributeConverter<Object, Object> {

    @Override
    public Object convertToDatabaseColumn(Object attribute) {
        if (attribute != null) {
            if (attribute instanceof Date) {
                return (Date) attribute;
            } else if (attribute instanceof String) {
                return new Date((String) attribute);
            }
        }
        return null;
    }

    @Override
    public Object convertToEntityAttribute(Object dbData) {
        return dbData;
    }

}
